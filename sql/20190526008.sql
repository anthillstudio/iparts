CREATE TABLE `iparts_storage` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT 'Уникальный идентификатор',
  `user_id` CHAR(255) NOT NULL DEFAULT '' COMMENT 'Id пользователя',
  `type` CHAR(255) NOT NULL DEFAULT '' COMMENT 'Тип хранимых данных',
  `data` TEXT NOT NULL DEFAULT '' COMMENT 'Хранимые данные',
  KEY `type` (`type`),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
