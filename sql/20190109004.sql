ALTER TABLE `iparts_catalog_elements`
  ADD `is_action` INT DEFAULT '0',
  ADD `prev_price` DECIMAL(10,2) DEFAULT 0.00 COMMENT 'Предыдущая цена (можно провести расчёт подешевел или подорожал товар)';