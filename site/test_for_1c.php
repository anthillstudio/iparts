<?php
	$msg = PHP_EOL . PHP_EOL . date("Y-m-d H:i:s") . PHP_EOL;
	$msg .= "GET:" . PHP_EOL . print_r($_GET, true) . PHP_EOL;
	$msg .= "POST:" . PHP_EOL . print_r($_POST, true) . PHP_EOL;
	$msg .= "FILES:" . PHP_EOL . print_r($_FILES, true) . PHP_EOL;
	$msg .= "COOKIE:" . PHP_EOL . print_r($_COOKIE, true) . PHP_EOL;
	$msg .= "SERVER:" . PHP_EOL . print_r($_SERVER, true) . PHP_EOL;
	$msg .= "SESSION:" . PHP_EOL . print_r($_SESSION, true) . PHP_EOL;
	$msg .= "ENV:" . PHP_EOL . print_r($_ENV, true) . PHP_EOL;
	$msg .= "REQUEST:" . PHP_EOL . print_r($_REQUEST, true) . PHP_EOL;

	$file = file_get_contents('php://input');
	
	file_put_contents("test_integration_log.txt", $msg, FILE_APPEND);
	file_put_contents("catalog" . date("YmdHis") .".xml", $file, FILE_APPEND);