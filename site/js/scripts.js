$( document ).ready(function() {
    $('a[rel="external"]').click(function () {
        window.open($(this).attr('href'));
        return false;
    });

    $('.gototop').click(function () {
        $('body,html').animate({scrollTop: 0}, 500);
    });


});


function addDeleveryMessage(){

    var name = $(".el-delevery #name").val();
    var mail = $(".el-delevery #mail").val();
    var mssg = $(".el-delevery #mssg").val();

    var toreturn = true;

    if (name==''){
        toreturn = false;
        $(".el-delevery #name").focus();
        showBoxMssg('Вы не указали имя',0);
    }else if(mail==''){
        toreturn = false;
        $(".el-delevery #mail").focus();
        showBoxMssg('Вы не указали e-mail',0);
    }else if(mssg==''){
        toreturn = false;
        $(".el-delevery #mssg").focus();
        showBoxMssg('Вы написали сообщение',0);
    }

    if (toreturn){
        $.get("/site/sendmail",{
                action : 'addDeleveryMessages',
                name : name,
                mail : mail,
                mssg : mssg
            },
            function(data) {
                showBoxMssg('Сообщение отправлено',1);
                setTimeout(hideBoxMssg, 3000)
                document.getElementById("el-obr").reset();
            }
        );
    }
}

function addFeedbackMessage(){

    var name = $(".el-obr #name").val();
    var mail = $(".el-obr #mail").val();
    var mssg = $(".el-obr #mssg").val();

    var toreturn = true;

    if (name==''){
        toreturn = false;
        $(".el-obr #name").focus();
        showBoxMssg('Вы не указали имя',0,'obratnaya-sviaz-form-wrapper');
    }else if(mail==''){
        toreturn = false;
        $(".el-obr #mail").focus();
        showBoxMssg('Вы не указали e-mail',0,'obratnaya-sviaz-form-wrapper');
    }else if(mssg==''){
        toreturn = false;
        $(".el-obr #mssg").focus();
        showBoxMssg('Вы написали сообщение',0,'obratnaya-sviaz-form-wrapper');
    }

    if (toreturn){
        $.get("/site/sendmail",{
                action : 'addFeedbackMessages',
                name : name,
                mail : mail,
                mssg : mssg
            },
            function(data) {
                showBoxMssg('Сообщение отправлено',1,'obratnaya-sviaz-form-wrapper');
                setTimeout(hideBoxMssg, 3000)
                //setTimeout(fbclose, 3500)
                document.getElementById("el-obr").reset();
            }
        );
    }
}

function addMessage(){

    var name = $(".el-obr #name").val();
    var mail = $(".el-obr #mail").val();
    var company = $(".el-obr #company").val();
    var postbox = $(".el-obr #postbox").val();
    var mssg = $(".el-obr #mssg").val();

    var toreturn = true;

    if (name==''){
        toreturn = false;
        $(".el-obr #name").focus();
        showBoxMssg('Вы не указали имя',0);
    }else if(mail==''){
        toreturn = false;
        $(".el-obr #mail").focus();
        showBoxMssg('Вы не указали e-mail',0);
    }else if(mssg==''){
        toreturn = false;
        $(".el-obr #mssg").focus();
        showBoxMssg('Вы написали сообщение',0);
    }

    if (toreturn){
        $.get("/site/sendmail",{
                action : 'addMessages',
                name : name,
                mail : mail,
                mssg : mssg,
                company : company,
                postbox : postbox
            },
            function(data) {
                showBoxMssg('Сообщение отправлено',1);
                setTimeout(hideBoxMssg, 3000)
                setTimeout(fbclose, 3500)
                document.getElementById("el-obr").reset();
            }
        );
    }
}




function hideBoxMssg() {
    $(".box-mssg").fadeOut();
    $(".box-mssg").removeClass('fly-box-mssg-success');
}

function showBoxMssg(txt,type,box) {
    $("." + box + " .box-mssg").fadeIn();
    $("." + box + " .box-mssg").html(txt);
    if (type==1){
        $("." + box + " .box-mssg").addClass('box-mssg-success');
    }else{
        $("." + box + " .box-mssg").removeClass('box-mssg-success');
    }
}



function showbigpreloader(){
    $("#bigpreloader").fadeIn();
}
function hidebigpreloader(){
    $("#bigpreloader").fadeOut();
}




function showMobMenu(){
    $(".mainMobMenu").fadeIn();
}

function closeMobMenu(){
    $(".mainMobMenu").fadeOut();
}


function showDetals(el) {
    $.get("/catalog/detals",{
            el : el,
        },
        function(data) {
            $(".goods-detals-box-wrapper").fadeIn();
            $(".goods-detals-box-html").html(data);
        }
    );
}

function addToPartners(){
    $.get("/site/addtopartners",{
            //el : el,
        },
        function(data) {
            $(".popup-box-wrapper").fadeIn();
            $(".popup-box-html").html(data);
        }
    );
}

function showPopUpBox(el) {
    $.get("/catalog/detals",{
            el : el,
        },
        function(data) {
            $(".popup-box-wrapper").fadeIn();
            $(".popup-box-html").html(data);
        }
    );
}


function loadgoods(subaction, cat, page) {
    $(".goods-list-box").html("<div class='preloader goods-list-box-preloader'></div>");
    $.get("/catalog/load-goods",{
            action : 'loadgoods',
            subaction : subaction,
            cat : cat,
            curr : $("#f_curr").val(),
            reg : $("#f_reg").val(),
            page : page,
        },
        function(data) {
            $(".goods-list-box").html(data);
        }
    );
}


function searchGoods(){
    var str = $("#search-goods").val();
    $.get(
        "/searchgoods",
        {
            str : str
        },
        function(data) {
            var curStr = $("#search-goods").val();
            if (data.str == curStr) {
                // Возвращённый результат соответствует текущему тексту для поиска
                if (data.res != '') $("#searchgoods-box").css('display', 'block');
                else $("#searchgoods-box").css('display', 'none');
                $("#searchgoods-box").html(data.res);
            }
            else {
                // Пропускаем т.к. уже изменился ввод текста для поиска
            }
        },
        "json"
    );

}


function goToContent(){
    $(document).scrollTop("30");
    $(".mainGoToBttn").css("display","none");
}




function nav(el,h){
    $('body,html').animate({scrollTop: $(el).offset().top - h}, 500);
}

function isValidEmail (email) {
    var toReturn = true;
    var r = /^([a-z0-9_\.-])+@\w+\.\w{2,4}$/i;
    if (!r.test(email)) {
        toReturn = false;
    }
    return toReturn;
}



$(document).ready(function() {

    $(".aaa-menu-item-name-box").hover(function () {
        $(".aaa-sub-menu-wrapper").css("display","block");
    }, function () {
        $(".aaa-sub-menu-wrapper").css("display","none");
    });

});


$( document ).ready(function() {

    $("#search-goods").blur(function () {
        //$("#searchgoods-box").css('display','none');
    });

});



$(document).on("scroll",function(){
    if($(document).scrollTop()>180){
        $(".basket-box").addClass('scroll-basket');
        $(".gototop").fadeIn();
    }else{
        $(".basket-box").removeClass('scroll-basket');
        $(".gototop").fadeOut();
    }
});


function showFMSSG(txt){
    $(".flash-mssg").html(txt);
    $(".flash-mssg-box").fadeIn();
    setTimeout(closeFMSSG, 3000);
}

function closeFMSSG(){
    $(".flash-mssg-box").fadeOut();
}

function rgb_to_hex(color){
    var rgb = color.replace(/\s/g,'').match(/^rgba?\((\d+),(\d+),(\d+)/i);
    return (rgb && rgb.length === 4) ? "#" + ("0" + parseInt(rgb[1],10).toString(16)).slice(-2) + ("0" + parseInt(rgb[2],10).toString(16)).slice(-2) + ("0" + parseInt(rgb[3],10).toString(16)).slice(-2) : color;
}

$( document ).ready(function() {








});


function showloadBox(){
    loadBox();
    setTimeout(hideLoadBox, 1000)
}

function hideLoadBox(){
    $(".load-box").css('display','none');
}

function loadBox(){
    $(".load-box").css('display','block');
}








function buildBasketBox(){
    $.get("/basket/orders/basket", {
            action: "buildBasketBox",
            curr : $("#f_curr").val(),
        },
        function (data){
            $(".basket-box span b").html(data);
            $(".basket-main-summ-price").html(data);
            hideLoadBox();
        }
    );
}



function delFromBasket(el){

    loadBox();
    $.get("/basket/orders/basket", {
            action: "del",
            el: el
        },
        function (data){
            showFMSSG('Товар удален из корзины');
            buildBasketBox();
            $("#good-item"+el+" .good-tbl-cap-delete").html("");
            $("#good-item"+el+" .good-tbl-cap-add").html("<a href=\"javascript:addToBasket('"+el+"','list')\"><img src='/img/icons/add.png'></a>");
            $("#good-item"+el+" .item-count").val('');
            $("#good-item"+el+" .item-summ").html('0.00');
            hideLoadBox();
        }
    );

}


function addToBasket(el,type){

    if (type=='list') {
        var count = $("#good-item" + el + " .item-count").val();
        var price = parseFloat($("#good-item" + el + " .item-price-ue").html());
    }else{
        var count = $("#good-box-" + el + " .item-count").val();
        var price = parseFloat($("#good-box-" + el + " .goods-detals-price-ue").html());
    }

    // alert(count);

    if (count==''){
        showFMSSG('Укажите количество');
    }else if (count==0){
        showFMSSG('Количество должно быть не меньше 1');
    }else{

        loadBox();

        $.get("/basket/orders/basket", {
                action: "add",
                count: count,
                price: price.toFixed(2),
                el: el,
                curr : $("#f_curr").val(),
                reg : $("#f_reg").val()
            },
            function (data){
                //alert(data);
                buildBasketBox();
                showFMSSG('Количество товаров в корзине изменено');
                $("#good-item"+el+" .good-tbl-cap-delete").html("<a href=\"javascript:delFromBasket('"+el+"')\"><img title='Удалить из корзины' alt='Удалить из корзины' src='/img/icons/delete.svg'></a>");
                hideLoadBox();
            }
        );

    }
}



function addToBasket2(el){


    var count = $("#good-item" + el + " .item-count").val();
    var price = parseFloat($("#good-item" + el + " .item-price-ue").html());


    if (count==''){
        showFMSSG('Укажите количество');
    }else if (count==0){
        showFMSSG('Количество должно быть не меньше 1');
    }else{

        loadBox();

        $.get("/basket/orders/basket", {
                action: "add",
                count: count,
                price: price.toFixed(2),
                el: el
            },
            function (data){
                buildBasketBox();
            }
        );

    }
}



function addToBasketFromSearch(el){

    var count = 1;

    var price = parseFloat($("#pricelist-subitem"+el+" .pricelist-subitem-price span").html());


        loadBox();

        $.get("/basket/orders/basket", {
                action: "add",
                count: count,
                price: price.toFixed(2),
                el: el
            },
            function (data){
                buildBasketBox();
                //showFMSSG('Товар добавлен в корзину');
                $("#pricelist-subitem"+el+" .pricelist-subitem-bttn").html("<img title='Удалить из корзины' alt='Удалить из корзины' src='/img/icons/check-white.png'>");
                //hideLoadBox();
                window.location.href='/catalog?subaction=showuse';
            }
        );


}





function order(){

    var mail = $("#el-obr #mail").val();
    var phone = $("#el-obr #phone").val();
    var mssg = $("#el-obr #mssg").val();
    var name = $("#el-obr #name").val();
    var city = $("#el-obr #city").val();

    var isGuest = $("#el-obr #isGuest").val();

    var toreturn = true;

    if (isGuest==1) {
        if (name == '') {
            toreturn = false;
            $(".el-obr #name").focus();
            showBoxMssg('Вы не указали имя', 0, 'el-obr');
        } else if (city == '') {
            toreturn = false;
            $(".el-obr #city").focus();
            showBoxMssg('Вы не указали город', 0, 'el-obr');
        } else if (mail == '') {
            toreturn = false;
            $(".el-obr #mail").focus();
            showBoxMssg('Вы не указали e-mail', 0, 'el-obr');
        } else if (phone == '') {
            toreturn = false;
            $(".el-obr #phone").focus();
            showBoxMssg('Вы не указали телефон', 0, 'el-obr');
        }
    }

    if (toreturn) {

        $.get("/basket/orders/basket", {
                action: "order",
                phone: phone,
                mssg: mssg,
                mail: mail,
                name: name,
                city: city
            },
            function (data) {


                setTimeout(hideBoxMssg, 3000)
                //alert(data);
                //buildBasketBox();
                showFMSSG('Заказ принят. Наш менеджер свяжется с Вами в ближайшее время!');
                //alert('d');
                //$(".basket-items").css("display", "none");
                //$("#basket-res").css("display", "none");
                $(".goods-list").html("");
                document.getElementById("el-obr").reset();
                //hideLoadBox();
            }
        );
    }

}


/*
 * type = 0 - в магазине
 * type = 1 - в корзине
 */
function basketPlus(el,type){
    var count = $(".cat-el"+el+" input").val();

    if (count<=99) {
        $(".cat-el" + el + " .catalog-list-element-basket-preloader").css("display", "block");
        $.post("/basket/orders/basket", {
                action: "add",
                count: count,
                el: el
            },
            function (data){

                flymssgbox('Товар добавлен в корзину');
                $(".cat-el" + el + " .catalog-list-element-basket-preloader").css("display", "none");
                $(".cat-el" + el + " input").val(data);
                $(".basket-bttn span").html($(".basket-bttn span").html() * 1 + 1);

                if (type==1){

                    var mainprice = $(".basket-main-summ-price").html()*1;
                    var mainpricepv = $(".basket-main-summ-pricepv").html()*1;

                    var price = $("#basket-item-cost-"+el+" span.basket-el-price").html()*1;
                    var pricepv = $("#basket-item-cost-"+el+" span.basket-el-price-pv").html()*1;
                    var count = $("#basket-item-nav-"+el+" input").val();
                    $("#basket-item-cost-summ-"+el+" span.basket-sum-price").html(price*count);
                    $("#basket-item-cost-summ-"+el+" span.basket-sum-price-pv").html(pricepv*count);

                    $(".basket-main-summ-price").html(mainprice+price);
                    $(".basket-main-summ-pricepv").html(mainpricepv+pricepv);

                }
            }
        );
    }
}

/*
 * type = 0 - в магазине
 * type = 1 - в корзине
 */
function basketMinus(el,type){
    var count = $(".cat-el"+el+" input").val();

    if (count>1) {
        $(".cat-el" + el + " .catalog-list-element-basket-preloader").css("display", "block");
        $.post("/basket/orders/basket", {
                action: "del",
                count: count,
                el: el
            },
            function (data) {
                flymssgbox('Товар выложен из корзины');
                $(".cat-el" + el + " .catalog-list-element-basket-preloader").css("display", "none");
                $(".cat-el" + el + " input").val(data);
                $(".basket-bttn span").html($(".basket-bttn span").html() * 1 - 1);
                if (type==1){

                    var mainprice = $(".basket-main-summ-price").html()*1;
                    var mainpricepv = $(".basket-main-summ-pricepv").html()*1;

                    var price = $("#basket-item-cost-"+el+" span.basket-el-price").html()*1;
                    var pricepv = $("#basket-item-cost-"+el+" span.basket-el-price-pv").html()*1;
                    var count = $("#basket-item-nav-"+el+" input").val();
                    $("#basket-item-cost-summ-"+el+" span.basket-sum-price").html(price*count);
                    $("#basket-item-cost-summ-"+el+" span.basket-sum-price-pv").html(pricepv*count);

                    $(".basket-main-summ-price").html(mainprice-price);
                    $(".basket-main-summ-pricepv").html(mainpricepv-pricepv);

                }
            }
        );
    }
}


function getBalance(){
    loadBox();
    $.get("/profile/report/balance", {
            //action: "select_order",
            //status: type
        },
        function (data){
            //showFMSSG('Заказ принят. Наш менеджер свяжется с Вами в ближайшее время!');
            hideLoadBox();
        }
    );
}



function selectStatements(){

    toreturn = true;

    if ($(".cal-box #from").val()==''){
        showFMSSG('Выберите начальную дату');
        toreturn = false;
    }else if ($(".cal-box #to").val()==''){
        showFMSSG('Выберите конечную дату');
        toreturn = false;
    }

    if (toreturn) {
        showloadBox();
        $.get("/profile/report/orders", {
                type: 2,
                from: $(".cal-box #from").val(),
                to: $(".cal-box #to").val(),
            },
            function (data) {
                console.log(data);
                $(".profile-content").html(data);
                hideLoadBox();
            });
    }
}

function selectPurchases(){

    toreturn = true;

    if ($(".cal-box #from").val()==''){
        showFMSSG('Выберите начальную дату');
        toreturn = false;
    }else if ($(".cal-box #to").val()==''){
        showFMSSG('Выберите конечную дату');
        toreturn = false;
    }

    if (toreturn) {
        showloadBox();
        $.get("/profile/report/orders", {
                type: 2,
                from: $(".cal-box #from").val(),
                to: $(".cal-box #to").val(),
            },
            function (data) {
                console.log(data);
                $(".profile-content").html(data);
                hideLoadBox();
            });
    }
}

function selectReturns(){

    toreturn = true;

    if ($(".cal-box #from").val()==''){
        showFMSSG('Выберите начальную дату');
        toreturn = false;
    }else if ($(".cal-box #to").val()==''){
        showFMSSG('Выберите конечную дату');
        toreturn = false;
    }

    if (toreturn) {
        showloadBox();
        $.get("/profile/report/orders", {
                type: 3,
                from: $(".cal-box #from").val(),
                to: $(".cal-box #to").val(),
            },
            function (data) {
                console.log(data);
                $(".profile-content").html(data);
                hideLoadBox();
            });
    }
}

function selectOrders(){

    toreturn = true;

    if ($(".cal-box #from").val()==''){
        showFMSSG('Выберите начальную дату');
        toreturn = false;
    }else if ($(".cal-box #to").val()==''){
        showFMSSG('Выберите конечную дату');
        toreturn = false;
    }

    if (toreturn) {
        console.log('sd');
        showloadBox();
        $.get("/profile/report/orders", {
                type: 1,
                from: $(".cal-box #from").val(),
                to: $(".cal-box #to").val(),
            },
            function (data) {
                console.log(data);
                $(".profile-content").html(data);
                hideLoadBox();
            });
    }
}


var ch;

function selectStatements(){

    toreturn = true;

    if ($(".cal-box #from").val()==''){
        showFMSSG('Выберите начальную дату');
        toreturn = false;
    }else if ($(".cal-box #to").val()==''){
        showFMSSG('Выберите конечную дату');
        toreturn = false;
    }

    $(".profile-content").html("Пожалуйста, ожидайте. Ведомость формируется и скоро будет вам предоставлена.");

    if (toreturn) {
        $.ajax({
            url: "/profile/report/orders",
            type: 'GET',
            data: 'action=select_order&type=4&from=' + $(".cal-box #from").val() + '&to=' + $(".cal-box #to").val(),
            dataType: 'json',
            success: function(data) {
                var el = data['id'];
                //console.log(el);
                setTimeout(function(){chechStat(el)},2000);

            }
        });

    }
}

function chechStat(el){
    $.ajax({
        url: "/profile/report/check-stat",
        type: 'GET',
        data: 'el=' + el,
        dataType: 'json',
        success: function(data) {

            if(data['state']==0){
                $(".profile-content").html("Запрос обработан");
                getStatement(el);
            }else if(data['state']==1){
                //$(".profile-content").html("Запрос передан для обработки");
                setTimeout(function(){chechStat(el)},2000);
            }else if(data['state']==2){
                //$(".profile-content").html("Запрос обрабатывается");
                setTimeout(function(){chechStat(el)},2000);
            }

            //$(".profile-content").html(data);
            //clearInterval(ch);
        }
    });
}


function getStatement(el) {
    $.ajax({
        url: "/profile/report/elements",
        type: 'GET',
        data: 'el=' + el,
        //dataType: 'json',
        success: function(data) {
            $(".profile-content").html(data);
        }
    });
}