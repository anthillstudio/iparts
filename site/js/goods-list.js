$( document ).ready(function() {

    $(".good-info-i").hover(function () {
        var id = $(this).attr("id");
        $("#" + id + " .good-info-box").css('display','block');
    }, function () {
        var id = $(this).attr("id");
        $("#" + id + " .good-info-box").css('display','none');
    });


    $('.basket-item-count').bind("change input click", function() {

        if (this.value.match(/[^0-9]/g)) {
            this.value = this.value.replace(/[^0-9]/g, '');
        }
        var idstr = $(this).attr("id");
        id = idstr.replace('basket-item-count','');


        var count = $("#good-item"+id+" .basket-item-count").val();
        var price = parseFloat($("#good-item"+id+" .item-price").html());

        sm = (price*count).toFixed(2);

        $("#good-item"+id+" .item-summ").html(sm);

        loadBox();

        $.get("/basket/orders/basket", {
                action: "add",
                count: count,
                price: price.toFixed(2),
                el: id
            },
            function (data){
                buildBasketBox();
                //$("#good-item"+id+" .good-tbl-cap-add").html("<a href='javascript:delFromBasket("+id+")'><img title='Удалить из корзины' alt='Удалить из корзины' src='/img/icons/check-black.png'></a>");
                //hideLoadBox();
            }
        );
    });

    $('.catalog-page .item-count').bind("change keyup input click", function() {
        if (this.value.match(/[^0-9]/g)) {
            this.value = this.value.replace(/[^0-9]/g, '');
        }
        var idstr = $(this).attr("id");
        id = idstr.replace('item-count','');
        buildSumm(id);
    });

    $('.bascket-page .item-count').bind("change keyup input click", function() {
        if (this.value.match(/[^0-9]/g)) {
            this.value = this.value.replace(/[^0-9]/g, '');
        }
        if (this.value == 0) this.value = 1;
        var idstr = $(this).attr("id");
        id = idstr.replace('item-count','');

        addToBasket2(id);

        buildSumm(id);
    });

    function buildSumm(id){
        var count = $("#good-item"+id+" .item-count").val();
        var price = parseFloat($("#good-item"+id+" .item-price").html());

        sm = (price*count).toFixed(2);
        $("#good-item"+id+" .item-summ").html(sm);
    }

    var oldColor;
    $(".good-tbl-tr").hover(function () {
        var id = $(this).attr("id");
        $("#" + id).addClass("good-tbl-tr-active");
    }, function () {
        var id = $(this).attr("id");
        $("#" + id).removeClass("good-tbl-tr-active");
    });

});