<?php
namespace frontend\models;

use yii\base\Model;
use common\models\User;
use common\models\UserInfo;
use Yii;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $partner_code;
    public $password;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
        ];
    }


    public function attributeLabels()
    {
        return [
            'username' => 'Логин',
            'password' => 'Пароль',
            'email' => 'E-mail',
            'partner_code' => 'Код партнера',
            'rememberMe' => 'Запомнить меня'
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }
        
        $user = new User();
        $user->username = $this->username;
        $user->email = $this->email;
        $user->id = rand(10000,90000);
        $user->setPassword($this->password);
        $user->generateAuthKey();

        if ($user->save()){

            $userInfo = new UserInfo();
            $userInfo->user_id = $user->getPrimaryKey();
            $userInfo->code = '';
            $userInfo->phone = '';
            //$post = Yii::$app->request->post();
            //$userInfo->code = $user->buildCode();


            $userInfo->save();

            $toReturn = $user;
        }else{
            $toReturn = null;
        }

        return $toReturn;
    }
}
