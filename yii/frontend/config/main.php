<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',

    'on beforeRequest' => function($event){
        return \common\modules\basket\models\Basket::loadFromStorage();
    },

    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                [   // правило для карты сайта
                    'pattern' => 'sitemap.xml',
                    'route' => 'sitemap/index'
                ],
                [
                    'pattern' => 'requestxxx',
                    'route' => 'site/request'
                ],
                [
                    'pattern' => 'mezhdunarodnie-denezhnie-perevodi-8734',
                    'route' => 'content/view-element',
                    'defaults' => ['url' => 'mezhdunarodnie-denezhnie-perevodi-8734'],
                ],
                [
                    'pattern' => 'oplata-uslug',
                    'route' => 'content/view-element',
                    'defaults' => ['url' => 'oplata-uslug'],
                ],
                [
                    'pattern' => 'depozitarnie-yacheiki',
                    'route' => 'content/view-element',
                    'defaults' => ['url' => 'depozitarnie-yacheiki'],
                ],
                [
                    'pattern' => 'dostavka-i-oplata',
                    'route' => 'content/view-element',
                    'defaults' => ['url' => 'dostavka-i-oplata'],
                ],


                [
                    'pattern' => 'catalog/detals',
                    'route' => 'catalog/detals',
                ],
                [
                    'pattern' => 'specifics/<el:.+>',
                    'route' => 'catalog/specifics',
                ],

                [
                    'pattern' => 'contacts',
                    'route' => 'content/view-element',
                    'defaults' => ['url' => 'contacts'],
                ],
                [
                    'pattern' => 'contacts',
                    'route' => 'content/view-element',
                    'defaults' => ['url' => 'contacts'],
                ],
                [
                    'pattern' => 'contacts/<url:[a-z0-9_\-]+>',
                    'route' => 'content/view-element',
                    'defaults' => ['base' => 'contacts'],
                ],
                [
                    'pattern' => 'english',
                    'route' => 'content/view-element',
                    'defaults' => ['url' => 'english'],
                ],
                [
                    'pattern' => 'english/<url:[a-z0-9_\/-]+>',
                    'route' => 'content/view-element',
                    'defaults' => ['base' => 'english'],
                ],
                [
                    'pattern' => 'fizicheskim-licam',
                    'route' => 'content/view-element',
                    'defaults' => ['url' => 'fizicheskim-licam'],
                ],

                [
                    'pattern' => 'fizicheskim-licam/<url:[a-z0-9_\/-]+>',
                    'route' => 'content/view-element',
                    'defaults' => ['base' => 'fizicheskim-licam'],
                ],
                [
                    'pattern' => 'korporativnim-klientam',
                    'route' => 'content/view-element',
                    'defaults' => ['url' => 'korporativnim-klientam'],
                ],
                [
                    'pattern' => 'korporativnim-klientam/<url:[a-z0-9_\/-]+>',
                    'route' => 'content/view-element',
                    'defaults' => ['base' => 'korporativnim-klientam'],
                ],
                [
                    'pattern' => 'bank',
                    'route' => 'content/view-element',
                    'defaults' => ['url' => 'bank'],
                ],
                [
                    'pattern' => 'bank/<url:[a-z0-9_\/-]+>',
                    'route' => 'content/view-element',
                    'defaults' => ['base' => 'bank'],
                ],
                [
                    'pattern' => 'presscentr',
                    'route' => 'content/view-element',
                    'defaults' => ['url' => 'presscentr'],
                ],
                [
                    'pattern' => 'presscentr/<url:[a-z0-9_\/-]+>',
                    'route' => 'content/view-element',
                    'defaults' => ['base' => 'presscentr'],
                ],
                /*[
                    'pattern' => '/bank/contacts/cbu/<url:[a-z0-9_\-]+>/<url:[a-z0-9_\-]+>/',
                    'route' => '/bank/contacts/cbu/',
                    'defaults' => ['base' => 'presscentr'],
                ],*/
                [
                    'pattern' => 'cursi',
                    'route' => 'content/view-element',
                    'defaults' => ['url' => 'cursi'],
                ],
                [
                    'pattern' => 'cursi/<url:[a-z0-9_\/-]+>',
                    'route' => 'content/view-element',
                    'defaults' => ['base' => 'cursi'],
                ],
                [
                    'pattern' => 'tarifi',
                    'route' => 'content/view-element',
                    'defaults' => ['url' => 'tarifi'],
                ],
                [
                    'pattern' => 'tarifi/<url:[a-z0-9_\-]+>',
                    'route' => 'content/view-element',
                    'defaults' => ['base' => 'tarifi'],
                ],
                [
                    'pattern' => 'company',
                    'route' => 'content/view-element',
                    'defaults' => ['url' => 'company'],
                ],
                [
                    'pattern' => 'company/<url:[a-z0-9_\-]+>',
                    'route' => 'content/view-element',
                    'defaults' => ['base' => 'company'],
                ],
                [
                    'pattern' => 'events',
                    'route' => 'events/view-events',
                ],
                [
                    'pattern' => 'events/<url:[a-z0-9_\-]+>',
                    'route' => 'events/view-events-element',
                    'defaults' => ['base' => 'events'],
                ],
                [
                    'pattern' => 'delivery',
                    'route' => 'content/view-element',
                    'defaults' => ['url' => 'delivery'],
                ],
                [
                    'pattern' => 'delivery/<url:[a-z0-9_\/-]+>',
                    'route' => 'content/view-element',
                    'defaults' => ['base' => 'delivery'],
                ],
                [
                    'pattern' => 'searchgoods',
                    'route' => 'catalog/search',
                    'defaults' => ['url' => 'searchgoods'],
                ],
                [
                    'pattern' => 'catalog/search',
                    'route' => 'catalog/search',
                    'defaults' => ['url' => 'searchgoods'],
                ],
                [
                    'pattern' => 'catalog/load-goods',
                    'route' => 'catalog/load-goods',
                ],
                [
                    'pattern' => 'catalog/import',
                    'route' => 'catalog/import'
                ],
                [
                    'pattern' => 'catalog/<cat:[a-zа-я0-9_\/-]+>',
                    'route' => 'catalog'
                ],


                [
                    'pattern' => 'blog',
                    'route' => 'content/view-element',
                    'defaults' => ['url' => 'blog'],
                ],
                [
                    'pattern' => 'blog/<url:[a-z0-9_\/-]+>',
                    'route' => 'content/view-element',
                    'defaults' => ['base' => 'blog'],
                ],



                [
                    'pattern' => 'apple-rabotaet-nad-novymi-akkumulyatorami-dlya-budushhih-iphone',
                    'route' => 'content/view-element',
                    'defaults' => ['url' => 'apple-rabotaet-nad-novymi-akkumulyatorami-dlya-budushhih-iphone'],
                ],
                [
                    'pattern' => 'razborka-iphone-7-plus-video',
                    'route' => 'content/view-element',
                    'defaults' => ['url' => 'razborka-iphone-7-plus-video'],
                ],
                [
                    'pattern' => 'vyshla-finalnaya-versiya-ios-10',
                    'route' => 'content/view-element',
                    'defaults' => ['url' => 'vyshla-finalnaya-versiya-ios-10'],
                ],
                [
                    'pattern' => 'kak-apple-udalos-dobitsya-vlagozashhity-iphone-7-plus',
                    'route' => 'content/view-element',
                    'defaults' => ['url' => 'kak-apple-udalos-dobitsya-vlagozashhity-iphone-7-plus'],
                ],
                [
                    'pattern' => 'zapatentovan-iphone-s-krugovym-displeem',
                    'route' => 'content/view-element',
                    'defaults' => ['url' => 'zapatentovan-iphone-s-krugovym-displeem'],
                ],
                [
                    'pattern' => 'pervaya-raspakovka-iphone-7-i-iphone-7-plus',
                    'route' => 'content/view-element',
                    'defaults' => ['url' => 'pervaya-raspakovka-iphone-7-i-iphone-7-plus'],
                ],
                [
                    'pattern' => 'ios-10-ustranila-oshibku-kamery-iphone',
                    'route' => 'content/view-element',
                    'defaults' => ['url' => 'ios-10-ustranila-oshibku-kamery-iphone'],
                ],
                [
                    'pattern' => 'data-prezentatsii-iphone-7-i-starta-prodazh',
                    'route' => 'content/view-element',
                    'defaults' => ['url' => 'data-prezentatsii-iphone-7-i-starta-prodazh'],
                ],
                [
                    'pattern' => 'pochemu-skuchnyj-iphone-7-ne-samaya-bolshaya-problema-apple',
                    'route' => 'content/view-element',
                    'defaults' => ['url' => 'pochemu-skuchnyj-iphone-7-ne-samaya-bolshaya-problema-apple'],
                ],
                [
                    'pattern' => 'huawei-nanyala-byvshego-dizajnera-apple',
                    'route' => 'content/view-element',
                    'defaults' => ['url' => 'huawei-nanyala-byvshego-dizajnera-apple'],
                ],
                [
                    'pattern' => 'v-iphone-7-poyavitsya-funktsiya-bystroj-zaryadki',
                    'route' => 'content/view-element',
                    'defaults' => ['url' => 'v-iphone-7-poyavitsya-funktsiya-bystroj-zaryadki'],
                ],


                [
                    'pattern' => 'apanel/catalog',
                    'route' => 'content/view-element',
                    'defaults' => ['url' => 'apanel/catalog'],
                ],


                [
                    'pattern' => 'basket/res',
                    'route' => 'basket/default/res',
                ],
                /*[
                    'pattern' => 'profile/info/<url:[a-z0-9_\/-]+>',
                    'route' => 'profile/info',
                ],*/

            ],
        ],
    ],
    'modules' => [
        'basket' => [
            'class' => 'common\modules\basket\Module',
        ],
        'profile' => [
            'class' => 'common\modules\profile\Module',
        ],
        'apanel' => [
            'class' => 'common\modules\apanel\Module',
        ],
    ],



    'params' => $params,
];