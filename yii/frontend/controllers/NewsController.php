<?php

namespace frontend\controllers;

use Yii;
use common\components\Ant;
use common\models\News;
use common\models\Spages;
use common\models\Settings;
use common\components\Seo;


class NewsController extends Controller
{

    public $layout = "news";
    public $newsObject;

    public function init()
    {
        $this->newsObject = new News();
        $this->newsObject->page = $this->newsObject->getPageByUrl(Ant::xurl());
        Yii::$app->params['settings'] = Settings::getSettings();
    }


    public function actionViewNews()
    {

        $page = Spages::findOne(371);
        $elements = News::find()->where(['show'=>'1'])->orderBy(['date'=>SORT_DESC])->all();

        Seo::metategs([
            'seo_description'=>$page->seo_description_lng,
            'seo_keywords'=>$page->seo_keywords_lng,
        ]);

        return $this->render('index',
            [
                'page' => $page,
                'elements' => $elements,
                'newsObject' => $this->newsObject
            ]
        );
    }

    public function actionViewNewsElement()
    {


        Seo::metategs([
            'seo_description'=>$this->newsObject->seo_description_lng,
            'seo_keywords'=>$this->newsObject->seo_keywords_lng,
        ]);

        return $this->render('element',
            [
                'newsObject' => $this->newsObject
            ]
        );
    }


}
