<?php

namespace frontend\controllers;

use Prophecy\Exception\Doubler\ClassNotFoundException;
use Yii;
use yii\helpers\Url;
use common\models\Content;
use common\models\ContentCatalog;
use common\models\Settings;
use common\components\Ant;
use yii\web\NotFoundHttpException;
use common\components\Seo;

class ContentController extends Controller
{

    public $layout = "default";
    public $contentObject;
    public $contentCatalogObject;

    public function init()
    {
        parent::init();
        $this->contentObject = new Content();
        Yii::$app->params['settings'] = Settings::getSettings();
    }


    public function getPageInfo(){
        $this->contentObject->navType = $this->contentObject->getTypeMenu($this->contentObject->page['id']);
        $this->contentObject->pageTpl = $this->contentObject->getPageTpl($this->contentObject->page['tpl']);
        $this->contentObject->parentId = $this->contentObject->getParentId($this->contentObject->page['id']);
        $this->contentObject->issetNavigation = $this->contentObject->issetNavigation($this->contentObject->parentId);
        $this->contentObject->orientation = $this->contentObject->getOrientation($this->contentObject->navType);
    }

    public function actionViewElement()
    {

        $this->contentObject->page = $this->contentObject->getPageByUrl(Content::xurl());

        if ($this->contentObject->page['tpl'] == 'catalog'){
            $this->layout = 'catalog';
        }

        if ($this->contentObject->page->url!=''){
            return $this->redirect(["{$this->contentObject->page->url}"]);
            die;
        }

        $this->getPageInfo();

        Seo::metategs([
            'seo_description'=>(trim($this->contentObject->page['seo_description_lng'])!=''?$this->contentObject->page['seo_description_lng']:$this->contentObject->page['title_lng']),
            'seo_keywords'=>(trim($this->contentObject->page['seo_keywords_lng'])!=''?$this->contentObject->page['seo_keywords_lng']:$this->contentObject->page['title_lng']),
        ]);

        if (count($this->contentObject->page)==0){
            throw new NotFoundHttpException();
        }

        $this->contentObject->catalogElements = ContentCatalog::getElements(" c.id = '{$this->contentObject->page['id']}' ");

        return $this->render('index',
            [
                'contentObject' => $this->contentObject
            ]
        );
    }

    public function actionViewCatalogElement()
    {

        $this->contentCatalogObject = new ContentCatalog();
        $this->contentCatalogObject = $this->contentCatalogObject->getElementByUrl();

        $this->contentObject->page = $this->contentObject->getPageById($this->contentCatalogObject->parent);
        $this->getPageInfo();


        Seo::metategs([
            'seo_description'=>$this->contentCatalogObject['seo_description_lng'],
            'seo_keywords'=>$this->contentCatalogObject['seo_keywords_lng'],
        ]);

        if (count($this->contentCatalogObject)==0){
            throw new NotFoundHttpException();
        }

        return $this->render('cat/catalog-element',
            [
                'contentObject' => $this->contentObject,
                'contentCatalogObject' => $this->contentCatalogObject
            ]
        );
    }

    public function actionViewCatalogStart()
    {

        echo Content::xurl();
        $this->contentObject->page = $this->contentObject->getPageByUrl(Content::xurl());
        $this->getPageInfo();

        Seo::metategs([
            'seo_description'=>$this->contentObject->page['seo_description_lng'],
            'seo_keywords'=>$this->contentObject->page['seo_keywords_lng'],
        ]);

        $this->contentObject->catalogElements = ContentCatalog::getElements();

        if (count($this->contentObject->page)==0){
            throw new NotFoundHttpException();
        }
        return $this->render('index',
            [
                'contentObject' => $this->contentObject
            ]
        );
    }

    function actionShowmorecbu(){
        echo Yii::$app->getView()->render('@frontend/views/content/tpl/_contacts_cbu_list.php');
    }





}