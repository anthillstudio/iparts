<?php

namespace frontend\controllers;

use Yii;
use common\models\User;
use common\models\Settings;

class Controller extends \yii\web\Controller
{

    public function init()
    {

        Yii::$app->params['settings'] = Settings::find()->asArray()->all();

        $session = Yii::$app->session;
        if (!$session->isActive){
            $session->open();
        }

        if (isset($_GET['city'])){
            $session['city'] = $_GET['city'];
        }



        if (isset($_GET['curr'])){
            $session['curr'] = $_GET['curr'];
        }

        //echo $session['curr'];


        /*Yii::$app->params['user'] = [];
        if (!Yii::$app->user->isGuest) {
            $user = User::findOne(Yii::$app->user->getIdentity()->id);
            Yii::$app->params['user'] = $user;
        }*/


        /*if (isset($_GET['partner_code'])) {
            Yii::$app->response->cookies->add(new \yii\web\Cookie([
                'name' => 'toplife_partner_code',
                'value' => $_GET['partner_code']
            ]));
        }*/


    }

}