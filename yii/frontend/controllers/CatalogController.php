<?php

namespace frontend\controllers;

use common\models\CatalogElementsSearch;
use Yii;
use common\models\Catalog;
use common\models\CatalogElements;
use common\models\Settings;
use common\components\Seo;
use common\components\Api;

class CatalogController extends Controller
{

    public $catalogModel;
    public $layout = "catalog";
    /**
     * Эта переменная должна быть в другом месте, но пока оставляю её тут
     * @var int
     */
    public $pageSize = 100;

    public function init()
    {



        parent::init();
        $this->catalogModel = new Catalog();
        Yii::$app->params['settings'] = Settings::getSettings();

    }

    public function actionIndex()
    {

        // Убрал каталог из под авторизации для сео продвижения
        //if (Yii::$app->user->isGuest) { return $this->redirect(['/site/login']); die; }

        if (isset($_GET['cat'])) {
            $catalog = Catalog::find()->where(['id'=>$_GET['cat']])->One();
            Seo::metategs([
                'seo_description' => (trim($catalog->seo_description) != '' ? $catalog->seo_description :
                    "Купить запчасти комплектующие ".$catalog->title." в Минске оптом. ".$catalog->title." широкий ассортимент и всегда в наличии."),
                'seo_keywords' => (trim($catalog->seo_keywords) != '' ? $catalog->seo_keywords : $catalog->title),
            ]);
        }else{
            //$catalog = Catalog::find()->One();
            $catalog = [];
            Seo::metategs([
                'seo_description' => 'Купить запчасти и комплектующие для мобильных телефонов в Минске оптом по выгодной цене. Широкий ассортимент запчастей и комплектующих для мобильных телефонов в Минске.',
                'seo_keywords' => 'Каталог',
            ]);
        }

        $catalogElementsSearch = new CatalogElementsSearch();
        $countElements = $catalogElementsSearch->countElementsForAjax(Yii::$app->request->get('cat'), Yii::$app->request->get('city'));

        return $this->render('index',
            [
                'catalog' => $catalog,
                'catalogObject' => $this->catalogModel,
                'content' => Yii::$app->getView()->render(
                    '@frontend/views/catalog/list.php',
                    [
                        'countElements' => $countElements,
                        'pageSize' => $this->pageSize,
                    ]
                ),
            ]
        );
    }

    public function actionImport()
    {
        $api = new Api();
        $api->importcats();
        $api->importgoods();
    }

    // Этот контроллер используется для загрузки товаров каталога аяксом
    public function actionLoadGoods(){

        $session = Yii::$app->session;
        if (!$session->isActive) {
            $session->open();
        }

        $catalogElementsSearch = new CatalogElementsSearch();
        $arr = $catalogElementsSearch->getArr(Yii::$app->request->get('cat'));

        // Определяем элементы постраничной навигации
        $limit = $this->pageSize;
        $page = Yii::$app->request->get('page');

        switch ($_GET['subaction']){
            case "showuse":

                $limit = 0;
                $arruse = [];
                if (isset($_SESSION['basket'])) {
                    foreach ($_SESSION['basket'] as $key => $val) {
                        $arruse[] = $val['id'];
                    }
                    $arruse = ['id'=>$arruse];
                    $limit = 1500;
                }

                $els = \common\models\CatalogElements::find()
                    ->where($arr)
                    ->andWhere(['show'=>1])
                    ->andWhere($arruse)
                    ->andWhere(['!=','parent','00-00000370'])
                    //->andWhere(['price1'<>'0.00'])
                    //->andWhere(['price2'<>'0.00'])
                    ->limit($limit)
                    //->offset(0)
                    ->orderBy(['title' => SORT_ASC])
                    ->all();

                break;
            default:
                $els = $catalogElementsSearch->findElementsForAjax(Yii::$app->request->get('cat'), Yii::$app->request->get('reg'), $limit, $page);
                break;

        }

        return Yii::$app->getView()->render('@frontend/views/catalog/_goods_list.php', ['els'=>$els,'type'=>1,'curr'=>$_GET['curr'],'reg'=>$_GET['reg']]);
    }


    public function actionViewCatalogElement()
    {

        $this->contentCatalogObject = new ContentCatalog();
        $this->contentCatalogObject = $this->contentCatalogObject->getElementByUrl();

        $this->contentObject->page = $this->contentObject->getPageById($this->contentCatalogObject->parent);
        $this->getPageInfo();


        Seo::metategs([
            'seo_description'=>$this->contentCatalogObject['seo_description_lng'],
            'seo_keywords'=>$this->contentCatalogObject['seo_keywords_lng'],
        ]);

        if (count($this->contentCatalogObject)==0){
            throw new NotFoundHttpException();
        }

        return $this->render('cat/catalog-element',
            [
                'contentObject' => $this->contentObject,
                'contentCatalogObject' => $this->contentCatalogObject
            ]
        );
    }

    public function actionDetals($el){
        $element = CatalogElements::findOne($el);
        $toReturn = Yii::$app->getView()->render('@frontend/views/catalog/_detals.php',['element'=>$element]);
        echo $toReturn;
    }

    public function actionSpecifics($el){
        $element = CatalogElements::findOne($el);

        $catalog = [];
        $title = isset($element->title) ? $element->title : "";
        Seo::metategs([
            'seo_description' => "Купить запчасти комплектующие ".$title." в Минске оптом. ".$title." широкий ассортимент и всегда в наличии.",
            'seo_keywords' => 'Каталог',
        ]);

        return $this->render('index',
            [
                'catalog' => $catalog,
                'catalogObject' => $this->catalogModel,
                'element' => $element,
                'content' => Yii::$app->getView()->render('@frontend/views/catalog/specifics.php', ['element' => $element]),
            ]
        );
    }

    public function actionViewCatalogStart()
    {

        echo Content::xurl();
        $this->contentObject->page = $this->contentObject->getPageByUrl(Content::xurl());
        $this->getPageInfo();

        Seo::metategs([
            'seo_description'=>$this->contentObject->page['seo_description_lng'],
            'seo_keywords'=>$this->contentObject->page['seo_keywords_lng'],
        ]);

        $this->contentObject->catalogElements = ContentCatalog::getElements();

        if (count($this->contentObject->page)==0){
            throw new NotFoundHttpException();
        }
        return $this->render('index',
            [
                'contentObject' => $this->contentObject
            ]
        );
    }

    function actionShowmorecbu(){
        echo Yii::$app->getView()->render('@frontend/views/content/tpl/_contacts_cbu_list.php');
    }



    public function actionSearch(){
        $elements = [];
        $resStr = "";
        $str = isset($_GET['str']) ? $_GET['str'] : "";
        if (strlen($str)>3) {
            $elements = CatalogElements::getElementsByStr($str);
        }
        if (count($elements)!=0){
            $resStr = CatalogElements::buildList($elements, $str);
        }
        return json_encode(array("str" => $str, "res" => $resStr));
    }


}