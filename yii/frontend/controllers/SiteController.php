<?php
namespace frontend\controllers;

use common\models\CatalogElements;
use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
//use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use frontend\controllers\Controller;
use common\models\Settings;
use common\models\Content;

use common\components\Ant;
use common\components\Seo;
use common\models\ContentCatalog;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            /*
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            */
            /*
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
            */
        ];
    }


    public function init()
    {
        parent::init();
        Yii::$app->params['settings'] = Settings::getSettings();
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            /*'error' => [
                'class' => 'yii\web\ErrorAction',
            ],*/
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }


    public function actionAddtopartners(){
        $toReturn = Yii::$app->getView()->render('@frontend/views/site/_partners.php');
        echo $toReturn;
    }

    public function actionRequest(){
        echo "test";
    }

    public function actionTest(){



        /*print_r("<pre>");
        print_r($user);
        print_r("</pre>");*/

        die;
        $array = ['03229057-4997-11e6-a411-086266371bb4','03229071-4997-11e6-a411-086266371bb4'];
        $elements = ContentCatalog::find()
            ->where(['in','id',$array])
            ->all();

        // $query = MyModel::find()->where(['not in','attribute',$array]);

        print_r("<pre>");
        print_r($elements);
        print_r("</pre>");

    }

    public function actionError()
    {
        $this->layout = "default";
        return $this->render('errors/404.php');
    }



    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        Seo::metategs([
            'seo_description'=>'Купить оригинальные запчасти для мобильных телефонов Apple iPhone/iPad/iPod/MacBook в Минске по доступной цене. оригинальные запчасти для мобильных телефонов Apple iPhone/iPad/iPod/MacBook широкий ассортимент.',
            'seo_keywords'=>'Купить оригинальные запчасти для мобильных телефонов Apple iPhone/iPad/iPod/MacBook в Минске по доступной цене. оригинальные запчасти для мобильных телефонов Apple iPhone/iPad/iPod/MacBook широкий ассортимент.',
        ]);
        return $this->render('index');
    }


    public function actionBuildflybox()
    {
        switch ($_GET['action']){
            case 'addUserMssg':
                echo Yii::$app->getView()->render('@frontend/views/layouts/inc/_form_usermssg.php',['url'=>$_GET['url']]);
                break;
            case 'buhOrder':
                echo Yii::$app->getView()->render('@frontend/views/layouts/inc/_form_buhorder.php');
                break;
        }
    }




    public function actionSendmail(){
        switch ($_GET['action']){
            case 'addPartnerMessages':
                $ant = new Ant();
                $emailfrom = $ant->emailfrom;
                $emails = $ant->emails['messages'][1];
                $theme = "iParts :: Заявка (партнерство)";
                $mssg = Yii::$app->getView()->render('@frontend/views/layouts/inc/_mail_partner.php',[
                    'name'=>$_GET['name'],
                    'phone'=>$_GET['phone'],
                    'email'=>$_GET['email'],
                    'addra'=>$_GET['addra'],
                    'addrb'=>$_GET['addrb'],
                    'comment'=>$_GET['comment'],
                    'type'=>$_GET['type'],
                ]);
                Yii::$app->mailer->compose()
                    ->setFrom($emailfrom)
                    ->setTo($emails)
                    ->setSubject($theme)
                    ->setHtmlBody($mssg)
                    ->send();

                break;
            case 'addDeleveryMessages':

                $ant = new Ant();
                $emailfrom = $ant->emailfrom;
                $emails = $ant->emails['messages'][1];
                $theme = "iParts :: Доставка";
                $mssg = Yii::$app->getView()->render('@frontend/views/layouts/inc/_mail_delevery.php',[
                    'name'=>$_GET['name'],
                    'company'=>$_GET['company'],
                    'mail'=>$_GET['mail'],
                    'postbox'=>$_GET['postbox'],
                    'mssg'=>$_GET['mssg'],
                ]);

                Yii::$app->mailer->compose()
                    ->setFrom($emailfrom)
                    ->setTo($emails)
                    ->setSubject($theme)
                    ->setHtmlBody($mssg)
                    ->send();

                break;
            case 'addFeedbackMessages':

                $ant = new Ant();
                $emailfrom = $ant->emailfrom;
                $emails = $ant->emails['messages'][1];
                $theme = "iParts :: Обратная связь";
                $mssg = Yii::$app->getView()->render('@frontend/views/layouts/inc/_mail_obratnaya_svyaz.php',[
                    'name'=>$_GET['name'],
                    'company'=>$_GET['company'],
                    'mail'=>$_GET['mail'],
                    'postbox'=>$_GET['postbox'],
                    'mssg'=>$_GET['mssg'],
                ]);

                Yii::$app->mailer->compose()
                    ->setFrom($emailfrom)
                    ->setTo($emails)
                    ->setSubject($theme)
                    ->setHtmlBody($mssg)
                    ->send();

                break;



        }
    }


    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {

        $this->layout = "default";
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goHome();
            //return $this->goBack('/catalog');
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        $this->layout = "default";
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $this->layout = "default";
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending your message.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $this->layout = "default";
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }
        
        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $this->layout = "default";
        $model = new PasswordResetRequestForm();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
/*
                $ant = new Ant();
                $emailfrom = $ant->emailfrom;
                $emails = [$model->email];
                $theme = "iParts ::  восстановление пароля";
                //$mssg = Yii::$app->getView()->render('@frontend/views/layouts/inc/_mail_partner.php',[
                //    'name'=>$_GET['name']                
                //]);

                if (Yii::$app->mailer->compose()
                    ->setFrom($emailfrom)
                    ->setTo($emails)
                    ->setSubject($theme)
                    ->setHtmlBody("---")
                    ->send()){
*/
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Для изменения пароля, следуйте инструкциям, отправленым на e-mail');
                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Извините, мы не можем сбросить пароль для указанного адреса электронной почты.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        $this->layout = "default";
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'Новый пароль сохранен');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }




}
