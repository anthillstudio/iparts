<?php

namespace frontend\controllers;

use Yii;
use common\models\Catalog;
use common\models\CatalogElements;
use common\models\Content;

class SitemapController extends Controller
{
    public function actionIndex()
    {
        $urls = [];

        // Загружаем новости
        // Костыльное решение, т.к. не нашёл прямого метода по получению новостей
        $list = Content::find()->where(["tpl" => "news_element", "show" => "1"])->all();
        foreach ($list as $key => $value) {
            /** @var Content $value */
            $urls[] = [
                'loc' => $value->beautiful_url,
            ];
        }

        // Загружаем категории каталога
        $list = Catalog::getAllActive();
        foreach ($list as $key => $value) {
            /** @var Catalog $value */
            $urls[] = [
                'loc' => 'catalog/' . $value->id,
            ];
        }

        // Загружаем элементы каталога
        $list = CatalogElements::getAllActive();
        foreach ($list as $key => $value) {
            /** @var CatalogElements $value */
            $urls[] = [
                'loc' => 'specifics/' . $value->id,
            ];
        }

        $xml_sitemap = Yii::$app->getView()->render('@frontend/views/sitemap/index.php', [
            'host' => Yii::$app->request->hostInfo,         // текущий домен сайта
            'urls' => $urls,                                // с генерированные ссылки для sitemap
        ]);

        echo $xml_sitemap;
    }
}