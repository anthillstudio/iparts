<?php

namespace frontend\controllers;

use Yii;
use common\models\Articles;
use common\models\ArticlesCatalogs;

class ArticlesController extends \yii\web\Controller
{

    public $layout = "default";
    public $articleObject;
    public $tagObject;

    public function init()
    {
        $this->articleObject = new Articles();
        $this->tagObject = new ArticlesCatalogs();
        Yii::$app->params['settings'] = Settings::getSettings();
    }

    public function actionViewElements()
    {
        $elements = Articles::find()->orderBy(['date'=>SORT_DESC])->all();
        return $this->render('index',
            [
                'elements' => $elements
            ]
        );
    }

    public function actionViewElement()
    {
        $element = $this->articleObject->getPageByUrl($_GET['url']);
        return $this->render('element',
            [
                'element' => $element
            ]
        );
    }


}
