<?php
use common\components\Ant;
use common\modules\xblocks\models\Iblocks;
use common\modules\xblocks\models\Imgblocks;
$page = $newsObject->page;
$this->title = $page['title_lng'];

$config = [
    'title' => $page['title_lng'],
    'notice' => $page['notice_lng'],
    'style' => $page['title_style'],
    'img' => "/{$eventObject->config['dir']}/{$page['image_file_title']}"
];

?>
<?= Yii::$app->getView()->render('@frontend/views/inc/menu/_title_box.php', ['config' => $config]); ?>
<div class="fix page-wrapper page-news">
    <ul class="bread-crumb FNTC">
        <li><a href="/">Главная</a></li>
        <li>|</li>
        <li><a href="/news">Новости</a></li>
    </ul><!-- bread-crumb -->

    <div class="fix news-content">
        <div class="news-content-left">
            <div class="deflist-items">

                <?php $date = Ant::buildDate($value->date,'php:d F, Y','H:mm'); ?>
                <div class="page-news-date"><?= $date ?></div>

                <?php if(trim($page['content_lng']!='')){ ?>
                    <div class="element-content FNTC">
                        <?php $path = "/{$newsObject->config['dir']}/{$page['image_file']}"; ?>
                        <?php if (trim($page['image_file']!='')){ ?>
                            <div class="element-content-img">
                                <img src="<?=Yii::$app->params['uploader']['domen'].$path?>">
                            </div><!-- element-content-img -->
                        <?php } ?>
                        <?= $page['content_lng']; ?>
                    </div>
                <?php } ?>
                <?=Iblocks::buildElements($page['id'],'news')?>

                <?=Imgblocks::buildElements($page['id'],'news')?>

            </div><!-- deflist-itemS -->
        </div>
    </div>


</div><!-- page-news -->