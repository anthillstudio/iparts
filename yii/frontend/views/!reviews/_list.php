<style>
.reviews-wrapper{}
.reviews-wrapper h2{font-weight: 100; font-size: 24pt; color: #b5b5b5; padding: 20px 0px}
.reviews-item{
    background-color: #f5f5f5; box-sizing: border-box; padding: 30px; margin-bottom: 10px;
    -webkit-border-radius: 5px;
    -moz-border-radius: 5px;
    border-radius: 5px;
}
.reviews-item:nth-child(2n) {
    background-color: #fff2d7;
}
.reviews-item-name{font-size: 20pt}
.reviews-item-date{font-size: 12pt; color: #888}
.reviews-item-review{font-size: 14pt; line-height: 18pt; padding: 10px 0px}
.reviews-item-answer{
    font-size: 14pt; line-height: 18pt; box-sizing: border-box; padding: 10px; margin-top: 10px; border: 1px solid #b5b5b5;
    font-style: italic;
    -webkit-border-radius: 5px;
    -moz-border-radius: 5px;
    border-radius: 5px;
}
</style>

<?php if (count($elements)!=0){ ?>
    <div class="reviews-wrapper">
    <h2 class="FNT">Отзывы:</h2>
    <?php foreach ($elements as $key => $value) { ?>
        <?php
        $insertDate = Yii::$app->formatter->asDate($value['insert_date'], 'php:d F');
        ?>
        <div class="reviews-item FNTC">
            <div class="reviews-item-name"><?=$value['name']?></div>
            <div class="reviews-item-date"><?=$insertDate?></div>
            <div class="reviews-item-review"><?=$value['review']?></div>
            <?php if (trim($value['answer'])!=''){ ?>
                <div class="reviews-item-answer"><?=$value['answer']?></div>
            <?php } ?>
        </div><!-- reviews-item -->
    <?php } ?>
    </div><!-- reviews-wrapper -->
<?php } ?>