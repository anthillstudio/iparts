<?php
use common\components\Ant;
use common\components\Api;
use yii\widgets\Breadcrumbs;
use common\widgets\contentmenu\ContentmenuWidget;
use common\models\Content;
use common\models\ContentCatalog;
use common\modules\xblocks\models\Iblocks;
use common\widgets\apanel_menu\ApanelMenuWidget;

$catmodel = new \common\models\Catalog();

$config = [
    'title' => (isset($catalog->title) ? "Купить запчасти комплектующие " . $catalog->title . " в Минске оптом" : "Купить запчасти и комплектующие для мобильных телефонов в Минске оптом"),
    'title_local' => (isset($catalog->title) ? $catalog->title : "Каталог"),
    'notice' => $catalog->notice
];

// Задаём title страницы (если открыта страница товара, то от товара, иначе от каталога)
$this->title = isset($element->title) ? "Купить запчасти комплектующие " . $element->title . " в Минске оптом" : $config['title'];

$path = \common\models\Catalog::buildPath((!empty($_GET['cat'])?$_GET['cat']:0));
?>
<?= \common\widgets\apanel_info\ApanelInfoWidget::widget() ?>

<?php if (!Yii::$app->user->isGuest) { ?>
<div class="fix content-navigation-h">
    <?= ApanelMenuWidget::widget() ?>
</div><!-- capBox -->
<?php } ?>


<div class="fix cat-title-box">
    <h1>Встречайте! Новая программа для сервисных центров <a href="https://helloclient.ru" target="_blank">https://helloclient.ru</a></h1>
</div><!-- cat-title-box -->

<div class="fix cat-title-box">
    <h1><?=$config['title_local']?></h1>
</div><!-- cat-title-box -->

<?php if (Yii::$app->user->isGuest) { ?>
<div class="fix"><div class="sales-info FNTC">После <a href="/site/login">авторизации</a> Вы сможете заказывать товары со скидкой!</div></div>
<?php } ?>

<?= Yii::$app->getView()->render('@frontend/views/inc/menu/_catalog_path.php', ['path'=>$path]); ?>

<div class="body-content-wrapper content-navigation-v fix catalog-page">
    <div class="cat-menu-box">
    <?= $catalogObject->buildMenu(); ?>
    </div><!-- cat-menu-box -->
    <div class="content-box-wrapper">
        <?=$content?>
    </div><!-- content-box-wrapper -->
    <div class="clear-both"></div>
    <br><br>
</div><!-- body-content-wrapper -->