<script src="/js/goods-list.js?v=1524623393"></script>

<?php
$session = Yii::$app->session;
if (!$session->isActive) {
    $session->open();
}
?>

<?php if (count($els)!=0){ ?>

    <table class="good-tbl">
        <tr class="good-tbl-cap">
            <td></td>
            <td>Товар</td>
            <td class="good-tbl-cap-summ">Цена <?= ($curr=='ue'?"(usd)":"(byn)")?></td>
            <td class="good-tbl-cap-count"><?=(!Yii::$app->user->isGuest)?"Кол-во":""?></td>
            <td class="good-tbl-cap-add"><?=(!Yii::$app->user->isGuest)?"Сумма ".($curr=='ue'?"(usd)":"(byn)"):""?></td>
            <td class="good-tbl-cap-del"></td>
            <td class="good-tbl-cap-del"></td>
        </tr>
        <?php foreach ($els as $key=>$value){ ?>

            <tr class="good-tbl-tr price-change<?= $value->getPriceChange() ?>" id="good-item<?= $value->id ?>">
                <td class="good-img">
                    <?php
                        $imgSrc = "/img/icons/noimg_mini.png";
                        if (trim($value->image_file_mini)!='') {
                            $imgSrc = 'data:image/jpg;base64,' . trim($value->image_file_mini);
                        }
                        else if (trim($value->image_file)!='') {
                            $imgSrc = 'data:image/jpg;base64,' . trim($value->image_file);
                        }
                    ?>
                    <a href="javascript:showDetals('<?= $value->id; ?>')"><img src='<?= $imgSrc; ?>'></a>
                </td><!-- good-img -->
                <?php $price_ue = $price = (!Yii::$app->user->isGuest?$value->price1:$value->price2); ?>
                <?php $price = ($price=='0.00'?"0.00":$price); ?>
                <?php $cssclass = ($price=='0.00'?'class-no':'class-yes'); ?>
                <?php $issetgoodtext = ($price=='0.00'?' <i>(Нет в наличии. Можно зарезервировать, добавив к заказу)</i>':''); ?>
                <td class="good-title <?=$cssclass?>">
                    <span title="<?= $value->title; ?>"><a href="javascript:showDetals('<?= $value->id; ?>')"><?= $value->title; ?></a><?=$issetgoodtext?> <?= ($value->isActionElement())?'<span class="good-action"></span>':''; ?>  </span><br>
                    <i># <a href="/specifics/<?=$value->id?>"><?= $value->ref; ?></a></i>
                    <div style="font-size: 8pt">
                        <?php
                        $toReturn = "";
                        foreach ($value->regionProduct as $xk=>$vx){
                            $toReturn .= $vx->region->name.", ";
                        }
                        $toReturn = substr($toReturn,0,-2);
                        echo $toReturn;
                        ?>
                    </div>
                </td>
                <?php
                $settings = Yii::$app->params['settings'];                        
                if ($curr!='ue'){ $price = $price*$settings['usd_currency'];}
                ?>
                <td class="item-price-ue" style="display: none"><?= $price_ue; ?></td>
                <td class="item-price <?=$cssclass?>"><?= $price; ?></td>
                <?php
                $itemCount = "";
                $itemSumm = "0.00";
                $bttn = "<a href=\"javascript:addToBasket('{$value->id}','list')\"><img src='/img/icons/basket.svg'></a>";
                $bttnReserv = "<a href=\"javascript:addToBasket('{$value->id}','list')\"><img src='/img/icons/basket.svg'></a>";
                $bttnDelete = "";
                if (isset($_SESSION['basket'])){
                    if (isset($_SESSION['basket'][$value->id])){
                        $itemCount = $_SESSION['basket'][$value->id]['count'];
                        $itemSumm = $_SESSION['basket'][$value->id]['count']*$_SESSION['basket'][$value->id]['price'];
                        $bttnDelete = "<a href=\"javascript:delFromBasket('{$value->id}')\"><img title='Удалить из корзины' alt='Удалить из корзины' src='/img/icons/delete.svg'></a>";
                    }
                }
                if ($curr!='ue'){ $itemSumm = $itemSumm*$settings['usd_currency'];}
                ?>
                <td class="good-tbl-cap-count"><?=(!Yii::$app->user->isGuest)?"<input class='item-count' id='item-count".$value->id."' maxlength='3' value='".$itemCount."' type='text'>":""?></td>
                <td class="item-summ <?=$cssclass?>"><?=(!Yii::$app->user->isGuest)?$itemSumm:""?></td>
                <td class="good-tbl-cap-add"><?=(!Yii::$app->user->isGuest)?($price=='0.00'?$bttnReserv:$bttn):""?></td>
                <td class="good-tbl-cap-delete"><div><?=(!Yii::$app->user->isGuest)?$bttnDelete:""?></div></td>
            </tr>
        <?php } ?>
    </table>
    <meta name="p:domain_verify" content="cd2c1a6fe4ea4a1d885097872472cf90"/><meta name="p:domain_verify" content="cd2c1a6fe4ea4a1d885097872472cf90"/>
<?php }else{ ?>
    Товары отсутствуют
<?php } ?>
