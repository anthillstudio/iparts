<script src="/js/zoomsl-3.0.js"></script>
<script>
    $( document ).ready(function() {

        $(".goods-detals-box-close").click(function () {
            $(".goods-detals-box-wrapper").fadeOut();
        });

        $(document).mouseup(function (e) {
            var container = $(".goods-detals-box-html");
            if (container.has(e.target).length === 0){
                $(".goods-detals-box-wrapper").fadeOut();
            }
        });

        $(".goods-detals-add").click(function () {
            $(".goods-detals-add").html("<a href='/basket'>Перейти к корзине</a>");
        });

        // Лупа для увеличения маленькой картинки
        jQuery(function(){
            $("#loopImage").imagezoomsl({
                zoomrange: [5, 5],
                magnifiersize: [300, 300]
            });
        });
    });
</script>

<div class="goods-detals-box-html-content" id="good-box-<?= $element->id; ?>">
    <div class="goods-detals-box-close FNTA">X</div>
    <div class="goods-detals-title FNTC"><?= $element->title; ?></div>
    <div class="goods-detals-ref FNTC">Артикул: <?= $element->ref; ?></div>
    <?php if (trim($element->image_file)!=''){ ?><div class="goods-detals-img"><img id="loopImage" width="150px" src='data:image/jpg;base64,<?= $element->image_file ?>'></div><?php } ?>
    <?php $price_ue = $price = (!Yii::$app->user->isGuest?$element->price1:$element->price2); ?>

    <?php

    $settings = Yii::$app->params['settings'];
    if ($_SESSION['curr']!='ue'){ $price = $price*$settings['usd_currency'];}
    ?>

    <?php $notext = "Ориентировочная дата поступления 12 марта"; ?>
    <?php $notext = ""; ?>
    <?php $price = ($price=='0.00'?"Нет в наличии. {$notext}":"{$price} ".($_SESSION['curr']=='ue'?"USD":"BYN")); ?>
    <div class="goods-detals-price FNTC"><?= $price; ?></div>

    <?php if (!Yii::$app->user->isGuest) { ?>
        <ul class='goods-detals-price-count-box'>
            <li>
                <input class="item-count" value="<?=(isset($_SESSION['basket'][$element->id])?$_SESSION['basket'][$element->id]['count']:"1")?>">
                <div class="goods-detals-price-ue" style="display: none"><?= $price_ue; ?></div>
            </li>
            <li><a href="javascript:addToBasket('<?=$element->id?>','el')"><img src="/img/icons/basket.svg"></a></li>
        </ul>
        <?php if ($price!='0.00'){ ?>
            <?php if (isset($_SESSION['basket'][$element->id])){ ?>
                <div class="goods-detals-add"><a href='/basket'>Перейти к корзине</a></div>
            <?php }else{ ?>
                <div class="goods-detals-add"><a href="javascript:addToBasket('<?= $element->id; ?>','detals')">Добавить в корзину</a></div>
            <?php } ?>
        <?php } ?>
    <?php } ?>

    <div class="goods-detals-notice FNTC"><?= $element->notice; ?></div>
</div><!-- goods-detals-box-html-content -->
<?php
//unset($_SESSION['basket']);
//print_r("<pre style='text-align: left'>");
//print_r($_SESSION['basket'][$element->id]);
//print_r("</pre>");
?>