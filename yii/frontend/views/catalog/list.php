    <script>
        $(window).scroll(function()
        {
            if  ($(window).scrollTop() > $(document).height() - $(window).height() - 300)
            {

            }
        });
        $( document ).ready(function() {
            <?php
            $subaction = (isset($_GET['subaction'])?$_GET['subaction']:"");
            switch ($subaction){
                case "showuse":
                    echo "loadgoods('showuse','{$_GET['cat']}', 0);";
                    break;
                default:
                    echo "loadgoods('showall','{$_GET['cat']}', 0);";
                    break;
            }
            ?>
        });

        /**
         * Блок с постраничной навигацией
         *
         * Использует переменные JS:
         * prefixPaginationPageElemId - префикс для ID элемента пагинации
         * activePaginationPageElemId - элемент с индексом текущей активной страницы
         * countPaginationPage - общее количество страниц пагинации
         * countVisiblePaginationPage - количество страниц пагинации, которое отображается пользователю
         * paginationCatalogParam - параметр каталога для загрузки элементов
         */
        var prefixPaginationPageElemId = 'paginationPage-';
        var activePaginationPageElemId = prefixPaginationPageElemId + '0';
        function setActivePaginationPage(curPaginationPageElemId)
        {
            var cssActiveClass = 'pagination-item-active';
            var curElem = $('#' + curPaginationPageElemId);
            $('#' + activePaginationPageElemId).removeClass(cssActiveClass);
            curElem.addClass(cssActiveClass);
            activePaginationPageElemId = curPaginationPageElemId;

            loadgoods('showall', paginationCatalogParam, curElem.val());
        }
        function setFirstPaginationPage()
        {
            var activeElem = $('#' + prefixPaginationPageElemId + '0');
            var activeVisiblePage = 0;
            var activePage = 0;

            var beginPage = activePage; // Высчитываем стартовую страницу для перерисовки
            updatePaginationVisibleList(beginPage);
            setActivePaginationPage(prefixPaginationPageElemId + activeVisiblePage); // Активируем обновлённую страницу
        }
        function setPrevPaginationPage()
        {
            var activeElem = $('#' + activePaginationPageElemId);
            var activeVisiblePage = Number(activeElem.data('id'));
            var activePage = Number(activeElem.val());

            if (activeVisiblePage <= 0) { // Если достигли первого элемента в списке видимых страниц
                if (activePage > 0) { // Если это ещё не последняя страница из всего списка
                    var beginPage = (activePage - 1); // Высчитываем стартовую страницу для перерисовки
                    updatePaginationVisibleList(beginPage);
                    setActivePaginationPage(prefixPaginationPageElemId + activeVisiblePage); // Повторно активируем обновлённую страницу
                }
            }
            else {
                setActivePaginationPage(prefixPaginationPageElemId + (activeVisiblePage - 1)); // Активируем предыдущую страницу
            }
        }
        function setNextPaginationPage()
        {
            var activeElem = $('#' + activePaginationPageElemId);
            var activeVisiblePage = Number(activeElem.data('id'));
            var activePage = Number(activeElem.val());

            if (activeVisiblePage >= (countVisiblePaginationPage - 1)) { // Если достигли последнего элемента в списке видимых страниц
                if (activePage < (countPaginationPage - 1)) { // Если это ещё не последняя страница из всего списка
                    var beginPage = (activePage + 1) - (countVisiblePaginationPage - 1); // Высчитываем стартовую страницу для перерисовки
                    updatePaginationVisibleList(beginPage);
                    setActivePaginationPage(prefixPaginationPageElemId + activeVisiblePage); // Повторно активируем обновлённую страницу
                }
            }
            else {
                setActivePaginationPage(prefixPaginationPageElemId + (activeVisiblePage + 1)); // Активируем следующую страницу
            }
        }
        function setLastPaginationPage()
        {
            var activeElem = $('#' + prefixPaginationPageElemId + Number(countVisiblePaginationPage - 1).toString());
            var activeVisiblePage = countVisiblePaginationPage - 1;
            var activePage = countPaginationPage - 1;

            var beginPage = activePage - (countVisiblePaginationPage - 1); // Высчитываем стартовую страницу для перерисовки
            updatePaginationVisibleList(beginPage);
            setActivePaginationPage(prefixPaginationPageElemId + activeVisiblePage); // Активируем обновлённую страницу
        }
        function updatePaginationVisibleList(beginPage)
        {
            for (i = 0; i < countVisiblePaginationPage; i++) {
                elemPageId = prefixPaginationPageElemId + i;
                $('#' + elemPageId).html(Number(beginPage + 1).toString());
                $('#' + elemPageId).val(Number(beginPage).toString());
                beginPage++;
            }
        }
    </script>

    <div class="clear-both"></div>

    <?= \common\widgets\search\SearchWidget::widget() ?>

    <?= \common\models\Catalog::filtersMenu(); ?>

    <!--
        View, которое отображает постраничную навигацию
    -->
    <?php
        $totalElements = (isset($countElements) ? $countElements : 0);
        $curPageSize = (isset($pageSize) ? $pageSize : 100);
        $countPages = ceil($totalElements / $curPageSize);

        // Максимальное количество кнопок с номерами страниц, которые отображаем
        $maxCountVisiblePaginationPage = 10;
        $countVisiblePaginationPage = ($countPages > $maxCountVisiblePaginationPage) ? $maxCountVisiblePaginationPage : $countPages;

        // Проверяем отображать ли кнопки вперёд и назад для списка страниц
        $showNavigationButtonsForPagination = false;
        if ($countPages > $countVisiblePaginationPage) {
            $showNavigationButtonsForPagination = true;
        }

        echo "<script>
            var countPaginationPage = {$countPages};
            var countVisiblePaginationPage = {$countVisiblePaginationPage};
            var paginationCatalogParam = '" . $_GET['cat'] ."';
        </script>";
        echo '<div class="pagination-box">';

        if ($showNavigationButtonsForPagination) {
            echo "<button onclick=\"setFirstPaginationPage()\" class='pagination-item'> << </button>";
            echo "<button onclick=\"setPrevPaginationPage()\" class='pagination-item'> < </button>";
        }

        for ($i = 0; $i < $countVisiblePaginationPage; $i++) {
            echo "<button
                onclick=\"setActivePaginationPage(this.id)\"
                class='pagination-item" . ($i == 0 ? ' pagination-item-active' : '') . "'
                id='paginationPage-{$i}'
                data-id='{$i}'
                value='{$i}'>" . ($i + 1) . "</button>";
        }

        if ($showNavigationButtonsForPagination) {
            echo "<button onclick=\"setNextPaginationPage()\" class='pagination-item'> > </button>";
            echo "<button onclick=\"setLastPaginationPage()\" class='pagination-item'> >> </button>";
        }

        echo '</div>';
    ?>
    <!--
        End view
    -->

    <div class="goods-list">
        <div class="goods-list-box"></div>
    </div><!-- goods-list -->
