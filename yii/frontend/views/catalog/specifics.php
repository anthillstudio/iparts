<?php /* ?>
<script>
    $( document ).ready(function() {

        $(".goods-detals-box-close").click(function () {
            $(".goods-detals-box-wrapper").fadeOut();
        });

        $(".goods-detals-add").click(function () {
            $(".goods-detals-add").html("<a href='/basket'>Перейти к корзине</a>");
        });

    });
</script>
<?= */ ?>

<style>
    
.specifics-title{
    font-size: 26pt; line-height: 35pt;
}
.specifics-ref{font-size: 12pt; padding: 10px 0px; display: block; color: #b5b5b5; margin-bottom: 10px}
.specifics-img{width: 40%; margin-bottom: 20px; float: left; margin-right: 20px}
.specifics-img img{display: block; max-width: 100%; border-radius: 3px;}



.specifics-box-html-content{text-align: left;}
.specifics-box-html-content .goods-detals-price-count-box{text-align: left;}
.specifics-box-html-content .goods-detals-add{text-align: left;}
.goods-detals-price-count-box li{display: inline-block;}


</style>

<div class="specifics-box-html-content" id="good-box-<?= $element->id; ?>">

    <div class="specifics-title FNTC"><?= $element->title; ?></div>
    <div class="specifics-ref FNTC">Артикул: <?= $element->ref; ?></div>

    <?php if (trim($element->image_file)!=''){ ?><div class="specifics-img"><img src='data:image/jpg;base64,<?= $element->image_file ?>'></div><?php } ?>
    
    <?php $price_ue = $price = (!Yii::$app->user->isGuest?$element->price1:$element->price2); ?>

    <?php

    $settings = Yii::$app->params['settings'];
    if ($_SESSION['curr']!='ue'){ $price = $price*$settings['usd_currency'];}
    ?>

    <?php $notext = "Ориентировочная дата поступления 12 марта"; ?>
    <?php $notext = ""; ?>
    <?php $price = ($price=='0.00'?"Нет в наличии. {$notext}":"{$price} ".($_SESSION['curr']=='ue'?"USD":"BYN")); ?>
    <div class="goods-detals-price FNTC"><?= $price; ?></div>

    <?php if (!Yii::$app->user->isGuest) { ?>
        <ul class='goods-detals-price-count-box'>
            <li>
                <input class="item-count" value="<?=(isset($_SESSION['basket'][$element->id])?$_SESSION['basket'][$element->id]['count']:"1")?>">
                <div class="goods-detals-price-ue" style="display: none"><?= $price_ue; ?></div>
            </li>
            <li><a href="javascript:addToBasket('<?=$element->id?>','el')"><img src="/img/icons/basket.svg"></a></li>
        </ul>
        <?php if ($price!='0.00'){ ?>
            <?php if (isset($_SESSION['basket'][$element->id])){ ?>
                <div class="goods-detals-add"><a href='/basket'>Перейти к корзине</a></div>
            <?php }else{ ?>
                <div class="goods-detals-add"><a href="javascript:addToBasket('<?= $element->id; ?>','detals')">Добавить в корзину</a></div>
            <?php } ?>
        <?php } ?>
    <?php } ?>

    <div class="specifics-notice FNTC"><?= $element->notice; ?></div>

</div><!-- specifics-box-html-content -->