<?php

use common\components\Ant;
$uploadsPath = Ant::uploaderInfo();

$img = "";
if (file_exists("../{$uploadsPath['dir']}{$config['img']}")) {
    $img = "<img src='{$uploadsPath['domen']}{$config['img']}'>";
}


if (!isset($config['style'])) $config['style'] = 1;

$useEventDate = false;
if ($config['style']==5){
    $useEventDate = true;
    $config['style'] = 1;
}elseif($config['style']==6){
    $useEventDate = true;
    $config['style'] = 2;
}

// echo $config['style'];
switch ($config['style']){
    case "1":
        ?>
        <div class="content-title-box-wrapper title-style<?=$config['style']?>">
            <div class="content-title-box">
                <div class="fix content-title-box-info">
                    <div class="content-title-box-info-box">
                    <h1 class="FNTC"><?= $config['title']; ?></h1>
                    <i class="FNTC"><?= $config['notice']; ?></i>
                    <?php if ($useEventDate){ ?>
                        <span class="content-title-date FNT"><?= $config['date']; ?></span>
                    <?php } ?>
                    </div>
                </div>
            </div><!-- content-title-box -->
        </div><!-- content-title-box-wrapper -->
        <?php
        break;
    case "2":
        ?>
        <div class="content-title-box-wrapper title-style<?=$config['style']?>">
            <div class="content-title-box">
                <div class="content-title-img"><span><?=$img?></span></div>
                <div class="fix content-title-box-info">

                    <div class="content-title-box-info-box">
                    <h1 class="FNT"><?= $config['title']; ?></h1>
                    <i class="FNTC"><?= $config['notice']; ?></i>
                    <?php if ($useEventDate){ ?>
                        <span class="content-title-date FNT"><?= $config['date']; ?></span>
                    <?php } ?>
                    </div>

                </div>
            </div><!-- content-title-box -->
        </div><!-- content-title-box-wrapper -->
        <?php
        break;
    case "3":
        ?>
        <div class="content-title-box-wrapper title-style<?=$config['style']?>">
            <div class="fix content-title-box">
                <h1 class="FNTC"><?= $config['title']; ?></h1>
            </div><!-- content-title-box -->
        </div><!-- content-title-box-wrapper -->
        <?php
        break;
}