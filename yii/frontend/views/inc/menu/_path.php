<?php
$arr = [];
for ($i = count($config)-1; $i>0; $i--){
    $arr[] = $config[$i];
}
?>
<ul class="fix path-menu">
    <li><a href="/">iParts.by</a></li>
    <?php if (count($config)!=0) echo "<li>/</li>"; ?>
    <?php $i=1; ?>
    <?php foreach ($arr as $key=>$value){ ?>
        <li><a href="/<?=$value['url']?>"><?=$value['title']?></a></li>
        <?php if ($i<count($config)-1) echo "<li>/</li>"; ?>
        <?php $i++ ?>
    <?php } ?>
</ul><!-- path-menu -->