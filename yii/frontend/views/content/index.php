<?php

use yii\widgets\Breadcrumbs;
use common\widgets\contentmenu\ContentmenuWidget;

$this->title = $contentObject->page['title_lng'];
if (trim($contentObject->page['seo_title_lng'])!='') $this->title = $contentObject->page['seo_title_lng'];


switch ($contentObject->page['tpl']){
    case "default":
        //echo $contentObject->page['menu_type'];
        echo $this->render("tpl/_default_{$contentObject->page['menu_type']}.php", [
            'contentObject' => $contentObject,
        ]);
        break;
    default:
        echo $this->render("tpl/_{$contentObject->page['tpl']}.php", [
            'contentObject' => $contentObject,
        ]);

        break;
}
