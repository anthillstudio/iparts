<div class="form-items FNTC">

    <form class="el-obr" name="el-obr" id="el-obr">

    <div class="box-mssg" id="box-mssg"></div>

    <div class="form-item">
        <span class="form-item-podpis">Фамилия, имя, отчество: * :</span>
        <span class="form-item-input"><input type="text" name="name" id="name" value=""></span>
    </div><!-- form-item -->

    <div class="form-item">
        <span class="form-item-podpis">Название организации (для юридических лиц):</span>
        <span class="form-item-input"><input type="text" name="company" id="company" value=""></span>
    </div><!-- form-item -->

    <div class="form-item">
        <span class="form-item-podpis">Адрес для почтовых отправлений:</span>
        <span class="form-item-input"><input type="text" name="postbox" id="postbox" value=""></span>
    </div><!-- form-item -->

    <div class="form-item">
        <span class="form-item-podpis">Адрес электронной почты * :</span>
        <span class="form-item-input"><input type="text" name="mail" id="mail" value=""></span>
    </div><!-- form-item -->

    <div class="form-item">
        <span class="form-item-podpis">Сообщение * :</span>
        <span class="form-item-textarea"><textarea name="mssg" id="mssg" value=""></textarea></span>
    </div><!-- form-item -->

    <div class="form-bttn"><a href="javascript:addMessage()">Отправить</a></div>

    </form>

</div>