<?php
use common\components\Ant;
use yii\widgets\Breadcrumbs;
use common\widgets\contentmenu\ContentmenuWidget;
use common\models\Content;
use common\models\ContentCatalog;
use common\modules\xblocks\models\Iblocks;

$page = $contentObject->page;
$orientation = $contentObject->orientation;

$config = [
    'title' => (trim($page['long_title_lng'])!=''?$page['long_title_lng']:$page['title_lng']),
    'notice' => $page['notice_lng'],
    'style' => $page['title_style'],
    'img' => "/{$contentObject->config['dir']}/{$page['image_file_title']}"
];
$path = [
    '1'=> ['url'=>'/', 'title'=>'Ссылка 1'],
    '2'=> ['url'=>'/', 'title'=>'Ссылка 2'],
    '3'=> ['url'=>'/', 'title'=>'Ссылка 3'],
];

$path = Content::getPathMenuElements($contentObject->page->id);

?>
<?= Yii::$app->getView()->render('@frontend/views/inc/menu/_title_box.php', ['config'=>$config]); ?>
<?= Yii::$app->getView()->render('@frontend/views/inc/menu/_path.php', ['config'=>$path]); ?>


<div class="content-wrapper content-navigation-h">
    <div class="body-content-wrapper fix">
        <?php $contentObject->parentId = 1421; ?>
        <?= $contentObject->buildContentMenu() ?>
        <div class="content-box-wrapper">
            <div class="obratnaya-sviaz-form-wrapper">
            <?= Yii::$app->getView()->render('@frontend/views/content/tpl/_contacts_obratnaya_svyaz_form.php'); ?>
            </div>
            <?php if (trim($page['content_lng'])!=''){ ?>
                <?php $path = "/{$contentObject->config['dir']}/{$page['image_file']}"; ?>
                <?php if (file_exists('../cloud/'.$path) && (trim($page['image_file'])!='')){ ?>
                    <div class="element-content-img">
                        <img src="<?=Yii::$app->params['uploader']['domen'].$path?>">
                    </div><!-- element-content-img -->
                <?php } ?>
                <div class="content-box-content FNTC"><?= $page['content_lng']; ?></div>
            <?php } ?>
            <div class="clear-both"></div>
            <?=Iblocks::buildElements($page['id'])?>
        </div><!-- content-box-wrapper -->

    </div><!-- body-content-wrapper -->
</div>