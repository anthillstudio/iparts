<div class="form-items FNTC">

    <form class="el-obr" name="korp-credit-order" id="korp-credit-order">

        <div class="box-mssg" id="box-mssg"></div>

        <div class="form-item">
            <span class="form-item-podpis">Фамилия, имя, отчество: * :</span>
            <span class="form-item-input"><input type="text" name="name" id="name" value=""></span>
        </div><!-- form-item -->

        <div class="form-item">
            <span class="form-item-podpis">Наименование юр. лица:</span>
            <span class="form-item-input"><input type="text" name="company" id="company" value=""></span>
        </div><!-- form-item -->

        <div class="form-item">
            <span class="form-item-podpis">Телефон * :</span>
            <span class="form-item-input"><input type="text" name="phone" id="phone" value=""></span>
        </div><!-- form-item -->

        <div class="form-item">
            <span class="form-item-podpis">Город * :</span>
            <span class="form-item-input"><input type="text" name="city" id="city" value=""></span>
        </div><!-- form-item -->

        <div class="form-item">
            <span class="form-item-podpis">Адрес электронной почты (e-mail)</span>
            <span class="form-item-textarea"><textarea name="email" id="email" value=""></textarea></span>
        </div><!-- form-item -->

        <div class="form-bttn"><a href="javascript:addKorpCreditOrder()">Отправить</a></div>

    </form>

</div>