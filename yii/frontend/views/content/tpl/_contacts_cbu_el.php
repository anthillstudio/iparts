<?php
use common\models\ContactsPlaces;
?>
<?php $element = ContactsPlaces::find()->where(['id'=>$_GET['el'],'show'=>1])->one(); ?>
<div class="contact-element-box">
    <div class="contact-element-l">
        <script type="text/javascript">
            ymaps.ready(init);
            var myMap;
            var geoObjects;
            function gotobalun(el){
                //placemark = new ymaps.Placemark(item.center, { balloonContent: item.name });
                myMap.setZoom(18);
                myMap.panTo(geoObjects[el].geometry.getCoordinates(), {
                    delay: 0,
                    callback: function () {
                        geoObjects[el].open();
                    }
                });
            }
            function init () {
                // Параметры карты можно задать в конструкторе.
                myMap = new ymaps.Map(
                    // ID DOM-элемента, в который будет добавлена карта.
                    'map',
                    // Параметры карты.
                    {
                        // Географические координаты центра отображаемой карты.
                        center: [<?=$element['coordinates']?>],
                        // Масштаб.
                        zoom: 15,
                        //controls: ['zoomControl', 'searchControl', 'typeSelector',  'fullscreenControl'],
                        controls: ['zoomControl', 'fullscreenControl'],
                        // Тип покрытия карты: "Спутник".
                        type: 'yandex#map'
                    }, {
                        // Поиск по организациям.
                        searchControlProvider: 'yandex#search'
                    }
                );
                clusterer = new ymaps.Clusterer();
                getPointData = function (city, addr, phone, time_work) {
                    return {
                        balloonContentBody: '<ul class="balloonContentBody"><li><b>' + city + ', ' + addr + '</b></li><li>Телефон: ' + phone + '</li></ul>',
                        clusterCaption: '<strong>Адрес ' + addr + '</strong>'
                    };
                };
                points = [<?=$element['coordinates']?>];
                geoObjects = [];
                geoObjects[0] = new ymaps.Placemark([<?=$element['coordinates']?>], getPointData("Минск", "", "(017) 269-23-09 (22) - приемная, (017) 269-23-39 (факс)", ""));

                clusterer.add(geoObjects);
                myMap.geoObjects.add(clusterer);

            }
        </script>
        <div id="map" style="width:100%; height:500px"></div>
    </div><!-- contact-element-l -->
    <div class="contact-element-r">
        <div><h2><?=$element['title_lng']?></h2></div>
        <br>
        <?= ($element['addr']==''? "" : "<div><b>Адрес:</b> {$element['addr']}</div>"); ?><br>
        <?= ($element['phone']==''? "" : "<div><b>Телефон:</b> {$element['phone']}</div>"); ?><br>
        <?= ($element['time_work']==''? "" : "<div><b>Время работы:</b> {$element['time_work']}</div>"); ?><br>
    </div><!-- contact-element-r -->
</div><!-- contact-element-box -->