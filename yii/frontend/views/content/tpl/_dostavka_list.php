<?php
use common\models\ContactsPlaces;
?>
<div class="cbu-box-wrapper">
    <div class="cbu-box">
        <?php $elements = ContactsPlaces::find()->where(['show'=>'1'])->all(); ?>
        <?php
        $i=0;
        foreach ($elements as $k=>$v) {
            if ($v->coordinates != '') {
                $arr[$v->city->title_lng][$i] = [
                    'title'=>"{$v->title_lng}",
                    'place' => "{$v->coordinates}",
                    'city' => "{$v->city->title_lng}",
                    'addr' => "{$v->addr}",
                    'phone' => "{$v->phone}",
                    'id' => "{$v->id}",
                    'show' => "{$v->show}"
                ];
                $i++;
            }
        }
        ?>

        <?php
        $newarr = [];
        foreach ($arr as $key=>$val){
        foreach ($val as $k=>$v) {
        if ($v['show'] == 1)
        $newarr[$key][$k] = $v;
        }
        }

        $toReturn = "";
        $toReturn .= "<div class='contact-place-items'>";
                foreach ($newarr as $cityKey=>$cityVal){
                $toReturn .= "<div class='contact-place-item'>";

                    $toReturn .= "<h3>{$cityKey}</h3>";
                    $toReturn .= "<ul>";
                        foreach ($cityVal as $key => $val) {

                        if ($val['show'] == 1) {
                        $toReturn .= "<li>";
                            $toReturn .= "<h2 class='contact-place-item-title'>{$val['title']}</h2>";
                            $toReturn .= "<div>Адрес: {$val['addr']}</div>";
                            $toReturn .= "<div>Телефон: {$val['phone']}</div>";
                            $toReturn .= "<div class='contact-place-item-map'><a href='javascript:gotobalun({$key})'>Смотреть на карте</a></div>";
                            $toReturn .= "</li>";
                        }
                        }
                        $toReturn .= "</ul>";
                    $toReturn .= "</div>";
                }
            $toReturn .= "</div>";

        echo $toReturn;
        ?>


    </div><!-- cbu-box -->
</div><!-- cbu-box-wrapper -->
<script type="text/javascript">
    ymaps.ready(init);
    var myMap;
    var geoObjects;
    function gotobalun(el){
        //placemark = new ymaps.Placemark(item.center, { balloonContent: item.name });
        nav('#map',180);
        myMap.setZoom(18);
        myMap.panTo(geoObjects[el].geometry.getCoordinates(), {
            delay: 0,
            callback: function () {
                geoObjects[el].open();
            }
        });
    }
    function init () {
        // Параметры карты можно задать в конструкторе.
        myMap = new ymaps.Map(
            // ID DOM-элемента, в который будет добавлена карта.
            'map',
            // Параметры карты.
            {
                // Географические координаты центра отображаемой карты.
                center: [53.90, 27.56],
                // Масштаб.
                zoom: 7,
                //controls: ['zoomControl', 'searchControl', 'typeSelector',  'fullscreenControl'],
                controls: ['zoomControl', 'fullscreenControl'],
                // Тип покрытия карты: "Спутник".
                type: 'yandex#map'
            }, {
                // Поиск по организациям.
                searchControlProvider: 'yandex#search'
            }
        );
        clusterer = new ymaps.Clusterer();
        getPointData = function (id, city, addr, phone) {
            return {
                balloonContentBody: '<ul class="balloonContentBody"><li><b>' + city + ', ' + addr + '</b></li><li>Телефон: ' + phone + '</li><li><a href="/bank/contacts/cbu?el=' + id + '">Подробнее</a></li></ul>',
                clusterCaption: '<strong>Адрес ' + addr + '</strong>'
            };
        };
        points = [
            <?php $place_str = ''; ?>
            <?php foreach ($arr as $key=>$val){ ?>
            <?php foreach ($val as $k=>$v){ ?>
            <?php $place_str .= "[{$v['place']}], "; ?>
            <?php } ?>
            <?php } ?>
            <?php $place_str = substr($place_str,0,-2); ?>
            <?= $place_str ?>
        ];
        geoObjects = [];
        <?php foreach ($arr as $key=>$val){ ?>
        <?php foreach ($val as $k=>$v){ ?>
        <?php
        $v['addr'] = trim($v['addr']);
        $v['addr'] = str_replace('"',"",$v['addr']);
        $v['addr'] = str_replace("'","",$v['addr']);
        ?>
        geoObjects[<?=$k?>] = new ymaps.Placemark([<?=$v['place']?>], getPointData("<?=$v['id']?>", "<?=$v['city']?>", "<?=$v['addr']?>", "<?=$v['phone']?>"));
        <?php } ?>
        <?php } ?>

        clusterer.add(geoObjects);
        myMap.geoObjects.add(clusterer);
    }
</script>
<div style="display: none">
    Новый адрес <span class="ymaps-geolink" data-bounds="[[55.63333783240489,37.486741441564136],
            [55.75433517114847,37.69017466910319]]" data-type="geo">Льва Толстого, 18Б</span>
</div>
<div id="map" style="width:100%; height:500px"></div>