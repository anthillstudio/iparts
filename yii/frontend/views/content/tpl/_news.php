<?php
use common\components\Ant;
use yii\widgets\Breadcrumbs;
use common\widgets\contentmenu\ContentmenuWidget;
use common\models\Content;
use common\models\ContentCatalog;
use common\modules\xblocks\models\Iblocks;
use common\modules\xblocks\models\Xfiles;

$page = $contentObject->page;
?>
<?php

$config = [
    'title' => (trim($page['long_title_lng'])!=''?$page['long_title_lng']:$page['title_lng']),
    'notice' => $page['notice_lng'],
    'style' => $page['title_style'],
    'img' => "/{$contentObject->config['dir']}/{$page['image_file_title']}"
];

?>
<?= Yii::$app->getView()->render('@frontend/views/inc/menu/_title_box.php', ['config'=>$config]); ?>

<div class="body-content-wrapper fix">
    <div class="content-wrapper">
        <?php $elements = Content::find()->where(['parent' => $page['id'],'show' => 1])->orderBy(['date'=>SORT_DESC])->limit(100)->all(); ?>
        <div class="news-list-items">
            <?php foreach ($elements as $key=>$value){ ?>
                <div class="news-list-item">
                    <?php $path = Yii::$app->params['uploader']['domen']."/content/{$value->image_file}"; ?>
                    <div class="news-list-item-img"><a href="/<?= $value->beautiful_url ?>"><img alt="<?= $value->title_lng ?>" title="<?= $value->title_lng ?>" src="<?= $path; ?>"></a></div>
                    <div class="news-list-item-date"><?= $value->date ?></div>
                    <div class="news-list-item-title"><a href="/<?= $value->beautiful_url ?>"><?= $value->title_lng ?></a></div>
                    <div class="news-list-item-notice"><?= $value->notice_lng ?></div>
                </div><!-- news-list-item -->
            <?php } ?>
        </div><!-- news-list-items-a -->
        <?=Iblocks::buildElements($page['id'])?>
        <?=Xfiles::buildElements($page['id'])?>
    </div><!-- content-wrapper -->
</div><!-- body-content-wrapper -->