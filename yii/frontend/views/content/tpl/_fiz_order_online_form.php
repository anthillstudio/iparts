<div class="form-items FNTC">

    <form name="fiz-credit-order" id="fiz-credit-order">

        <div class="box-mssg" id="box-mssg"></div>

        <div class="form-item">
            <span class="form-item-podpis">Фамилия, имя, отчество: * :</span>
            <span class="form-item-input"><input type="text" name="name" id="name" value=""></span>
        </div><!-- form-item -->

        <div class="form-item">
            <span class="form-item-podpis">Телефон * :</span>
            <span class="form-item-input"><input type="text" name="phone" id="phone" value=""></span>
        </div><!-- form-item -->

        <div class="form-item">
            <span class="form-item-podpis">Город * :</span>
            <span class="form-item-input"><input type="text" name="city" id="city" value=""></span>
        </div><!-- form-item -->

        <div class="form-item">
            <span class="form-item-podpis">Сумма кредита:</span>
            <span class="form-item-input"><input type="text" name="summ" id="summ" value=""></span>
        </div><!-- form-item -->

        <div class="form-item">
            <span class="form-item-podpis">Среднемесячный доход</span>
            <span class="form-item-textarea"><textarea name="average" id="average" value=""></textarea></span>
        </div><!-- form-item -->

        <div class="form-bttn"><a href="javascript:addFizCreditOrder()">Отправить</a></div>

    </form>

</div>