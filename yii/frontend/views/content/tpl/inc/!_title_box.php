<?php

use common\components\Ant;

$uploadsPath = Ant::uploaderInfo();
$path = "/content/{$page['image_file']}";
$img = "";
if (trim($page['image_file'])!='' && file_exists("../{$uploadsPath['dir']}{$path}"))
    $img = "<img src='{$uploadsPath['domen']}{$path}'>";

switch ($page['title_style']){
    case "1":
        ?>
        <div class="content-title-box-wrapper image-file-use<?=$page['image_file_use']?>">
            <div class="content-title-box">
                <div class="content-title-box-info">
                    <div>
                        <h1 class="FNTC"><?= $page['title_lng']; ?></h1>
                        <i class="FNTB"><?= $page['notice_lng']; ?></i>
                    </div>
                </div>
            </div><!-- content-title-box -->
        </div><!-- content-title-box-wrapper -->
        <?php
        break;
    case "2":
        ?>
        <div class="content-title-box-wrapper">
            <div class="content-title-box">
                <div class="content-title-img"><span><?=$img?></span></div>
                <div class="content-title-box-info">
                    <div>
                        <h1 class="FNTC"><?= $page['title_lng']; ?></h1>
                        <i class="FNTB"><?= $page['notice_lng']; ?></i>
                    </div>
                </div>
            </div><!-- content-title-box -->
        </div><!-- content-title-box-wrapper -->
        <?php
        break;
    case "3":
        ?>
        <div class="content-title-box-wrapper content-title-box-mini">
            <div class="content-title-box">
                <div class="fix"><h1 class="FNTC"><?= $page['title_lng']; ?></h1></div>
            </div><!-- content-title-box -->
        </div><!-- content-title-box-wrapper -->
        <?php
        break;
}