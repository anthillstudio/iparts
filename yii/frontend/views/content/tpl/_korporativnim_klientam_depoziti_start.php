<?php
use common\components\Ant;
use yii\widgets\Breadcrumbs;
use common\widgets\contentmenu\ContentmenuWidget;
use common\models\Content;
use common\models\ContentCatalog;
use common\modules\xblocks\models\Iblocks;
use common\modules\xblocks\models\Xfiles;

$page = $contentObject->page;
$orientation = $contentObject->orientation;

$config = [
    'title' => (trim($page['long_title_lng'])!=''?$page['long_title_lng']:$page['title_lng']),
    'notice' => $page['notice_lng'],
    'style' => $page['title_style'],
    'img' => "/{$contentObject->config['dir']}/{$page['image_file_title']}"
];

$path = Content::getPathMenuElements($contentObject->page->id);

?>
<?= Yii::$app->getView()->render('@frontend/views/inc/menu/_title_box.php', ['config'=>$config]); ?>
<?= Yii::$app->getView()->render('@frontend/views/inc/menu/_path.php', ['config'=>$path]); ?>

<div class="body-content-wrapper fix">
    <?php


    $issetNavigation = 0;
    if ($contentObject->issetNavigation){
        $issetNavigation = 1;
    }
    ?>
    <div class="content-wrapper content-tpl-<?=$contentObject->pageTpl?> content-navigation-<?=$orientation?> issetnavigation<?=$issetNavigation?>">
        <?php $contentObject->parentId = 5; ?>
        <?= $contentObject->buildContentMenu() ?>
        <div class="content-box-wrapper">
            <?php if (trim($page['content_lng'])!=''){ ?>
                <?php $path = "/{$contentObject->config['dir']}/{$page['image_file']}"; ?>
                <?php if (file_exists('../cloud/'.$path) && (trim($page['image_file'])!='')){ ?>
                    <div class="element-content-img">
                        <img src="<?=Yii::$app->params['uploader']['domen'].$path?>">
                    </div><!-- element-content-img -->
                <?php } ?>
                <div class="content-box-content FNTC"><?= $page['content_lng']; ?></div>
            <?php } ?>
            <div class="clear-both"></div>
            <?=Iblocks::buildElements($page['id'])?>
            <?=Xfiles::buildElements($page['id'])?>
        </div><!-- content-box-wrapper -->
    </div><!-- content-wrapper -->
</div><!-- body-content-wrapper -->