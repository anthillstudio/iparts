<div class="form-items FNTC">

    <form class="el-obr" name="zp-order" id="zp-order">

        <div class="box-mssg" id="box-mssg"></div>

        <div class="form-item">
            <span class="form-item-podpis">Фамилия, имя * :</span>
            <span class="form-item-input"><input type="text" name="name" id="name" value=""></span>
        </div><!-- form-item -->

        <div class="form-item">
            <span class="form-item-podpis">Название компании * :</span>
            <span class="form-item-input"><input type="text" id="company" name="company" value=""></span>
        </div><!-- form-item -->

        <div class="form-item">
            <span class="form-item-podpis">Город *:</span>
            <span class="form-item-input"><input type="text" id="city" name="city" value=""></span>
        </div><!-- form-item -->

        <div class="form-item">
            <span class="form-item-podpis">Наличие счета:</span>
            <input type='radio' name='accountx' id='account1' value='1'><label class='formLabel' for='account1'>Есть</label>
            <input type='radio' name='accountx' id='account0' value='0'><label class='formLabel' for='account0'>Нет</label>
        </div><!-- form-item -->

        <div class="form-item">
            <span class="form-item-podpis">Ежемесячный фонд оплаты труда:</span>
            <span class="form-item-input"><input type="text" id="fond" name="fond" value=""></span>
        </div><!-- form-item -->

        <div class="form-item">
            <span class="form-item-podpis">Количество сотрудников:</span>
            <span class="form-item-input"><input type="text" id="count" name="count" value=""></span>
        </div><!-- form-item -->

        <div class="form-item">
            <span class="form-item-podpis">Контактный тел:</span>
            <span class="form-item-input"><input type="text" id="phone" name="phone" value=""></span>
        </div><!-- form-item -->

        <div class="form-item">
            <span class="form-item-podpis">Электронная почта:</span>
            <span class="form-item-input"><input type="text" id="email" name="email" value=""></span>
        </div><!-- form-item -->

        <div class="form-item">
            <span class="form-item-podpis">Комментарий:</span>
            <span class="form-item-textarea"><textarea rows="5" cols="50" name="message" id="message"></textarea></span>
        </div><!-- form-item -->

    <div class="form-bttn"><a href="javascript:addZPOrder()">Отправить</a></div>

    </form>

</div>