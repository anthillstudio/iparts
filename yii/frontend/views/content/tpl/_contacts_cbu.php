<?php
use common\components\Ant;
use yii\widgets\Breadcrumbs;
use common\widgets\contentmenu\ContentmenuWidget;
use common\models\Content;
use common\models\ContentCatalog;
use common\modules\xblocks\models\Iblocks;
use common\models\ContactsCities;

$page = $contentObject->page;
$orientation = $contentObject->orientation;

$config = [
    'title' => (trim($page['long_title_lng'])!=''?$page['long_title_lng']:$page['title_lng']),
    'notice' => $page['notice_lng'],
    'style' => $page['title_style'],
    'img' => "/{$contentObject->config['dir']}/{$page['image_file_title']}"
];

$path = Content::getPathMenuElements($contentObject->page->id);

?>
<?= Yii::$app->getView()->render('@frontend/views/inc/menu/_title_box.php', ['config'=>$config]); ?>
<?= Yii::$app->getView()->render('@frontend/views/inc/menu/_path.php', ['config'=>$path]); ?>

<div class='fix' style='padding: 20px; text-align: center; margin-bottom: 20px; font-size: 14pt; font-weight: bold'>
    С вопросами о сотрудничестве обращайтесь по номеру телефону <a href='tel:+375291220077'>+375 (29) 122-00-77</a>,<br>
    отправляйте предложения на почту <a href='mailto:info@iparts.by'>info@iparts.by</a> или отправляйте online сообщение<br>через <a href='/contacts/obratnaya-svyaz'>форму обратной связи</a>
</div>
<div class="content-wrapper content-navigation-h">
    <div class="body-content-wrapper fix">
        <?php $contentObject->parentId = 1421; ?>
        <?= $contentObject->buildContentMenu() ?>
        <div class="content-box-wrapper">
            <?php if (isset($_GET['el'])){ ?>
                <?= Yii::$app->getView()->render('@frontend/views/content/tpl/_contacts_cbu_el.php'); ?>
            <?php }else{ ?>
                <?= Yii::$app->getView()->render('@frontend/views/content/tpl/_contacts_cbu_list.php'); ?>
            <?php } ?>
            <?php if (trim($page['content_lng'])!=''){ ?>
                <?php $path = "/{$contentObject->config['dir']}/{$page['image_file']}"; ?>
                <?php if (file_exists('../cloud/'.$path) && (trim($page['image_file'])!='')){ ?>
                    <div class="element-content-img">
                        <img src="<?=Yii::$app->params['uploader']['domen'].$path?>">
                    </div><!-- element-content-img -->
                <?php } ?>
                <div class="content-box-content FNTC"><?= $page['content_lng']; ?></div>
            <?php } ?>
            <div class="clear-both"></div>
            <?=Iblocks::buildElements($page['id'])?>
        </div><!-- content-box-wrapper -->
    </div><!-- body-content-wrapper -->
</div>