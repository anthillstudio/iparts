<div class="form-items FNTC">

    <form class="el-delevery" name="el-delevery" id="el-delevery">

        <h2>Заказать доставку</h2>

        <div class="box-mssg" id="box-mssg"></div>

        <div class="form-item">
            <span class="form-item-podpis">Фамилия, имя, отчество: * :</span>
            <span class="form-item-input"><input type="text" name="name" id="name" value=""></span>
        </div><!-- form-item -->

        <div class="form-item">
            <span class="form-item-podpis">Адрес электронной почты * :</span>
            <span class="form-item-input"><input type="text" name="mail" id="mail" value=""></span>
        </div><!-- form-item -->

        <div class="form-item">
            <span class="form-item-podpis">Сообщение * :</span>
            <span class="form-item-textarea"><textarea name="mssg" id="mssg" value=""></textarea></span>
        </div><!-- form-item -->

        <div class="form-bttn"><a href="javascript:addDeleveryMessage()">Отправить</a></div>

    </form>

</div>