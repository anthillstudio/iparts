<?php
use common\components\Ant;
use yii\widgets\Breadcrumbs;
use common\widgets\contentmenu\ContentmenuWidget;
use common\models\Content;
use common\models\ContentCatalog;
use common\modules\xblocks\models\Iblocks;

$page = $contentObject->page;
?>
<?php

$config = [
    'title' => (trim($page['long_title_lng'])!=''?$page['long_title_lng']:$page['title_lng']),
    'notice' => $page['notice_lng'],
    'style' => $page['title_style'],
    'img' => "/{$contentObject->config['dir']}/{$page['image_file_title']}"
];
$path = Content::getPathMenuElements($contentObject->page->id);
?>
<?= Yii::$app->getView()->render('@frontend/views/inc/menu/_title_box.php', ['config'=>$config]); ?>
<?= Yii::$app->getView()->render('@frontend/views/inc/menu/_path.php', ['config'=>$path]); ?>
<div class="body-content-wrapper fix">
    <div class="content-wrapper">



        <div id="visaoffers" style="box-sizing: border-box" visa-responsive="true"></div>

        <script>

            window.visaClientwidgetConfig = {
                apiKey: 'OWS_RRB_BY_BD00A134-9ED8-4990-B16F-CCDA349FC1B6',
                template: 'A',
                enabledFilters: ['cards','country','category'],
                cards: ['classic', 'gold', 'platinum','signature','infinite'],
                categories: ['DO','EAT','SHOP','STAY','RELAX','GO'],
                viewMode: {
                    display: 'list',
                    listFilterParam: '&sortMerchantCountry=BY'
                },
                language: 'ru',
                pageSize: 9,
                sortOptions : {order:"desc",params:"merchantCountry,featured,lastModifiedDatetime"},
                title: "Be rewarded for using your Visa card!",
                description: "Enjoy exclusive deals, discounts and offers by simply using your Visa card at Merchants worldwide.",
                header:"Visa Offers"

            };
            window.syndication.init();

        </script>



    </div><!-- content-wrapper -->
</div><!-- body-content-wrapper -->