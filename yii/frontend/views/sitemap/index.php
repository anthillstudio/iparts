<?php

/* @var $urls */
/* @var $host */

echo '<?xml version="1.0" encoding="UTF-8"?>';
?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <?php foreach($urls as $url) { ?>
        <url>
            <loc><?php echo $host . '/' . $url['loc']; ?></loc>
        </url>
    <?php } ?>
</urlset>​