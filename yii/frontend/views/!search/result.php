<div class="content-title-box-wrapper">
    <div class="content-title-box">
        <div class="fix">
            <h1>Поиск</h1>
        </div>
    </div>
</div>
<div class="fix">
    <?php if (count($elements)!=0){ ?>
        <?php foreach ($elements as $key=>$value){ ?>
            <div>
                <div><a href="/news/<?=$value->beautiful_url?>"><?=$value->title_lng?></a></div>
                <div><?=$value->date?><br></div>
            </div>
        <?php } ?>
    <?php }else{ ?>
        <div style="text-align: center; padding-bottom: 50px">По данному запросу ничего не найдено ..</div>
    <?php } ?>
</div>