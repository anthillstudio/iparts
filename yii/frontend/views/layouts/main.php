<?php

use frontend\assets\AppAsset;
use common\widgets\Alert;
use common\widgets\slider\SliderWidget;
use common\widgets\mainMenu\MainMenuWidget;
use common\widgets\about\AboutWidget;
use common\widgets\icons\IconsWidget;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<?php echo $this->render('inc/_header.php'); ?>
<?php $this->beginBody() ?>

<?= Alert::widget() ?>

<div id="cap-box">
<?= MainMenuWidget::widget() ?>
</div>

<?= SliderWidget::widget() ?>

<div class="body-box">


        <?php
        $arr = [
            '1' => [
                'title' => 'В iPhone 7 появится функция быстрой зарядки',
                'url' => '/v-iphone-7-poyavitsya-funktsiya-bystroj-zaryadki',
                'date' => '2016-10-08',
                'img' => Yii::$app->params['uploader']['domen'].'/content/m1433X.jpg'
            ],
            '2' => [
                'title' => 'Apple работает над новыми аккумуляторами для будущих iPhone',
                'url' => '/apple-rabotaet-nad-novymi-akkumulyatorami-dlya-budushhih-iphone',
                'date' => '2016-10-06',
                'img' => Yii::$app->params['uploader']['domen'].'/content/m1423X.jpg'
            ],
            '3' => [
                'title' => 'Разборка iPhone 7 Plus + Видео',
                'url' => '/razborka-iphone-7-plus-video',
                'date' => '2016-09-17',
                'img' => Yii::$app->params['uploader']['domen'].'/content/m1424X.jpg'
            ],
            '4' => [
                'title' => 'Вышла финальная версия iOS 10',
                'url' => '/vyshla-finalnaya-versiya-ios-10',
                'date' => '2016-09-13',
                'img' => Yii::$app->params['uploader']['domen'].'/content/m1425X.jpg'
            ],
            '5' => [
                'title' => 'Как Apple удалось добиться влагозащиты iPhone 7 (Plus)',
                'url' => '/kak-apple-udalos-dobitsya-vlagozashhity-iphone-7-plus',
                'date' => '2016-09-10',
                'img' => Yii::$app->params['uploader']['domen'].'/content/m1426X.jpg'
            ],
            '6' => [
                'title' => 'Запатентован iPhone с круговым дисплеем',
                'url' => '/zapatentovan-iphone-s-krugovym-displeem',
                'date' => '2016-08-17',
                'img' => Yii::$app->params['uploader']['domen'].'/content/m1427X.jpg'
            ]
        ]
        ?>

        <?= IconsWidget::widget() ?>

        <div class="news-box-wrapper FNTC">

            <div class="fix">

                <h2>Новости</h2>
                <div class="news-box-items">
                <?php foreach($arr as $key=>$value){ ?>
                    <div class="news-box-item">
                        <div class="news-box-item-img"><img src="<?=$value['img']?>"></div>
                        <div class="news-box-item-info">
                            <a href="<?=$value['url']?>"><?=$value['title']?></a>
                            <span><?=$value['date']?></span>
                        </div><!-- news-box-item-info -->
                    </div><!-- news-box-item -->
                <?php } ?>
                </div><!-- news-box-items -->
                <div class="news-box-more"><a href="/blog">Все новости</a></div>

            </div>

        </div><!-- news-box-wrapper -->


        <div class="about-box FNTC">
            <div class="fix">
                <h1>Купить запчасти для мобильных телефонов Apple в Минске</h1>
                <p>Продукция компании Apple известна по всему миру. Каждый год миллионы поклонников ждут выхода новых моделей, чтобы стать их счастливыми обладателями. Вне всякого сомнения продукция американской компании, отличается высоким уровнем качества и надёжности. Может использоваться на протяжении длительного интервала времени.</p>
                <p>Рано или поздно, но самые надёжные модели мобильных телефонов от компании Apple требуют проведения ремонтных работ. Зачастую необходимость ремонта обусловлена небрежным обращением и повреждением в результате механического воздействия. Ремонт дорогостоящего устройства нужно доверить лишь специалистам. При этом важно использовать оригинальные запасные части для мобильных телефонов. Качество ремонта в данном случае будет на высоком профессиональном уровне.</p>
            </div>
        </div><!-- about -->



</div>

<?= AboutWidget::widget() ?>
</div><!-- body-box -->

<?= $content ?>

<?php echo $this->render('inc/_footer.php'); ?>

<?php $this->endBody() ?>
<?php $this->endPage() ?>