<?php

use frontend\assets\AppAsset;
use common\widgets\Alert;
use common\models\Settings;
use common\widgets\mainMenu\MainMenuWidget;
use common\widgets\icons\IconsWidget;
use common\widgets\apanel_menu\ApanelMenuWidget;

$session = Yii::$app->session;
if (!$session->isActive) {
    $session->open();
}

$settings = Settings::getSettings();
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<?php echo $this->render('inc/_header.php'); ?>
<?php $this->beginBody() ?>

<div id="cap-box">
    <?= MainMenuWidget::widget() ?>
</div><!-- capBox -->

<br>
<?= Alert::widget() ?>
<?= $content ?>

<?= IconsWidget::widget() ?>
<?php echo $this->render('inc/_footer.php'); ?>

<?php $this->endBody() ?>
<?php $this->endPage() ?>
