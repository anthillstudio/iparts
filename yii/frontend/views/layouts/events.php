<?php


use frontend\assets\AppAsset;
use common\widgets\Alert;
use common\widgets\mainMenu\MainMenuWidget;
use common\widgets\mobMenu\MobMenuWidget;
use common\widgets\bnnrs\BnnrsWidget;
use common\widgets\sponsorBnnr\SponsorBnnrWidget;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<?php echo $this->render('inc/_header.php'); ?>
<?php $this->beginBody() ?>

<?=mainMenuWidget::widget(['type'=>'default']);?>
    <div id="global-wrapper">
        <?=mobMenuWidget::widget();?>
        <?php
        $session = Yii::$app->session;
        if (!$session->isActive) {
            $session->open();
        }

        ?>
        <div id="body-wrapper" class="default-body-wrapper">
            <?= Alert::widget() ?>



            <?= $content ?>



            <div class="fix">
                <?=bnnrsWidget::widget();?>
            </div>



            <?=sponsorBnnrWidget::widget([]);?>

        </div>
        <?php echo $this->render('inc/_footer.php'); ?>
    </div>
<?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>