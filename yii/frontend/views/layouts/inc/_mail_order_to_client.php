<?php
use common\models\ContentCatalog;
?>
<div style="font-family: arial; font-size: 10pt">
    Здравствуйте<?= (!Yii::$app->user->isGuest?"":" {$name}") ?>, Ваша заявка сформирована. Мы приняли Ваш заказ в обработку, сформированый заказ будет отправлен на Вашу почту "<?=$mail?>" в ближайше время.
    Наш менеджер свяжется с Вами!
    <?php
    $idstr = [];
    foreach ($_SESSION['basket'] as $key=>$value){
        $idstr[] = $value['id'];
    }
    $elements = \common\models\CatalogElements::find()
        ->where(['in','id',$idstr])
        ->all();

    ?>
    <br>
    <br>
    <b>Заказ</b>
    <br>
    <?php $summ = 0 ?>
    <?php foreach ($elements as $key=>$value){ ?>
        <?php $summ += $_SESSION['basket'][$value->id]['price']*$_SESSION['basket'][$value->id]['count']*1 ?>
        <div>---</div>
        <div>Товар: <?= $value->title ?></div>
        <div>Арт: <?= $value->ref ?></div>
        <div>Цена: <?= $_SESSION['basket'][$value->id]['price'] ?> USD</div>
        <div>Количество: <?= $_SESSION['basket'][$value->id]['count'] ?></div>
    <?php } ?>
    <div>---</div>
    <br>
    <div>---</div>
    <b>СУММА: <?= $summ ?> USD</b>
    <div>---</div>
    <br>
    <a href="http://iparts.by/contacts/ipartscenters">Контактная инфомрация</a>
</div>
