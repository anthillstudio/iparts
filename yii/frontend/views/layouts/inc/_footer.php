<?php
use common\widgets\settings\SettingsWidget;
use common\widgets\bnnrs\BnnrsWidget;
use common\widgets\cards\CardsWidget;
use common\widgets\news\NewsWidget;
?>



<div class="footer-wrapper">
    <div class="fix">

        <div class="footer-left">
            <h2><?= SettingsWidget::widget(['var'=>'slogan']) ?></h2>
            <p>Следите за нами в социальных сетях</p>
            <div class="smm-wrapper">
                <div class="smm-items">
                    <a rel="external" href="https://www.facebook.com/ipartsby/" class="smm-item smm-item-f"></a>
                    <a rel="external" href="https://vk.com/ipartsby" class="smm-item smm-item-v"></a>
                    <a rel="external" href="https://plus.google.com/107085044468506919189" class="smm-item smm-item-g"></a>
                    <a rel="external" href="https://t.me/ipartsby" class="smm-item smm-item-t"></a>
                </div><!-- smm-items -->
            </div><!-- smm-wrapper -->
            <div class="copy">&copy; <?php echo date("Y"); ?> <?= SettingsWidget::widget(['var'=>'copy']) ?></div>
        </div><!-- footer-left -->

        <div class="footer-right">
            <ul>
                <li>Адрес: г.Минск, ТД «Ждановичи» ул.Тимирязева 127, пав.D12</li>
                <li>Адрес: г.Минск, ТЦ «Монетка» ул. Кульман 9, сектор 20 павильон 10</li>
                <li>Адрес: г. Гомель ТЦ «Виктория», ул. Карповича 28, корп. 1, этаж 2, павильон 175</li>
                <li>Адрес: г. Гродно, ул. Антонова 4А, этаж 3, помещение 12</li>
            </ul>
            <ul>
                <li>Email: info@iparts.by</li>
                <li>Время работы: Понедельник — Воскресение 10:00 – 20:00</li>
            </ul>
            <ul class="footer-contacts-more" style="display: none">
                <li><a href="/contacts/ipartscenters">Подробная контактная информация</a></li>
            </ul><!-- footer-contacts-more -->

            <ul>
                <li><a href="javascript:addToPartners()">Подать заявку на партнерство</a></li>
            </ul>

        </div><!-- footer-right -->

    </div><!-- fix -->
</div><!-- footer-wrapper -->


<div class="flash-mssg-box FNTC">
    <div class="flash-mssg">Сообщение</div>
</div><!-- flash-mssg-box -->

<div class="load-box"><span></span></div>


<div class="popup-box-wrapper">
    <div class="popup-box">
        <div class="popup-box-html">



        </div>
    </div><!-- goods-detals-box -->
</div><!-- goods-detals-box-wrapper -->


<div class="goods-detals-box-wrapper">
    <div class="goods-detals-box">
        <div class="goods-detals-box-html"></div>
    </div><!-- goods-detals-box -->
</div><!-- goods-detals-box-wrapper -->

<div id='bigpreloader'></div>

<div class="gototop"></div>