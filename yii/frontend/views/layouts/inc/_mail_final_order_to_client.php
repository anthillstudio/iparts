<?php
use common\models\ContentCatalog;
session_start();
?>
<div style="font-family: arial; font-size: 10pt">
    Здравствуйте, Ваша заявка сформирована. Наш менеджер свяжется с Вами!
    <br>
    <br>
    <b>Заказ</b>
    <br>

    <?php $summ = 0 ?>
    <?php foreach ($re as $key=>$value){ ?>
        <?= $price = $value->price; ?>
        <?php $summ += $price*$value->count*1 ?>
        <div>---</div>
        <div>Товар: <?= $value->catalogElements->title ?></div>
        <div>Арт: <?= $value->catalogElements->ref ?></div>
        <div>Цена: <?= $price ?> USD</div>
        <div>Количество: <?= $value->count ?></div>
    <?php } ?>
    <div>---</div>
    <br>
    <div>---</div>
    <b>СУММА: <?= $summ ?> USD</b>
    <div>---</div>
    <br>
    <a href="http://iparts.by/contacts/ipartscenters">Контактная инфомрация</a>
</div>
