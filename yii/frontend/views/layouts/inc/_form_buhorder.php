<h2>Оставить заявку на услугу<br>«Онлайн - бухгалтерия»!</h2>
<div class="fly-box-form buh-form">

    <div class="fly-box-form-el">
        <div class="fly-box-form-inp">
            <span class="fly-box-form-podpis">Имя</span>
            <input id="name" name="name" type="text" value="">
        </div><!-- fly-box-form-input -->
    </div><!-- fly-box-form-el -->

    <div class="fly-box-form-el">
        <div class="fly-box-form-inp">
            <span class="fly-box-form-podpis">Телефон</span>
            <input id="phone" name="phone" type="text" value="">
        </div><!-- fly-box-form-input -->
    </div><!-- fly-box-form-el -->

    <div class="fly-box-form-bttn"><a href="javascript:addBuhOrder()">Отправить</a></div>

</div><!-- fly-box-form -->