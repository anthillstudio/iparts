<?php
use yii\helpers\Html;
?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="google-site-verification" content="R3lEXQE63tjECQ8xvmwNx5z-qZ2GfjHO5y2q_w3fh0E" />
    <meta name="yandex-verification" content="03ef3af3af883412" />

    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link rel="shortcut icon" href="/img/favicon.png?r=1" type="image/icon">
    <?php $this->head() ?>

    <link href="/css/media.css" rel="stylesheet">




    <link rel="stylesheet" href="https://yastatic.net/bootstrap/3.3.4/css/bootstrap.min.css"/>
    <script src="https://yastatic.net/jquery/2.2.3/jquery.min.js"></script>
    <script src="https://yastatic.net/bootstrap/3.3.4/js/bootstrap.min.js"></script>

    <script src="https://api-maps.yandex.ru/2.1/?load=Geolink&lang=ru_RU" type="text/javascript"></script>
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>

    <!-- Yandex.Metrika counter --> <script type="text/javascript" > (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)}; m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)}) (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym"); ym(54721411, "init", { clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true }); </script> <noscript><div><img src="https://mc.yandex.ru/watch/54721411" style="position:absolute; left:-9999px;" alt="" /></div></noscript> <!-- /Yandex.Metrika counter -->
</head>
<body>