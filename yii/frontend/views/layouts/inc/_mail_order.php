<div style="font-family: arial; font-size: 10pt">
    <div>
        <div>Клиент <?= (!Yii::$app->user->isGuest?"":"(новый)") ?>: <?= (!Yii::$app->user->isGuest?Yii::$app->user->getIdentity()->name." (".Yii::$app->user->getId().")":$name) ?></div>
        <div>Адрес электронной почты: <?=$mail?></div>
        <div>Телефон: <?=$phone?></div>
        <div>Комментарий: <?=$mssg?></div>
    </div>
    <?php
    $idstr = [];
    foreach ($_SESSION['basket'] as $key=>$value){
        $idstr[] = $value['id'];
    }
    $elements = \common\models\CatalogElements::find()
        ->where(['in','id',$idstr])
        ->all();
    ?>
    <br>
    <br>
    <b>Заказ</b>
    <br>
    <?php $summ = 0 ?>
    <?php foreach ($elements as $key=>$value){ ?>
        <?php $summ += $_SESSION['basket'][$value->id]['price']*$_SESSION['basket'][$value->id]['count']*1 ?>
        <div>---</div>
        <div>Товар: <?= $value->title ?></div>
        <div>Арт: <?= $value->ref ?></div>
        <div>Цена: <?= $_SESSION['basket'][$value->id]['price'] ?> USD</div>
        <div>Количество: <?= $_SESSION['basket'][$value->id]['count'] ?></div>
        <?php
        if (!is_null($value->regionProduct)) {
            $reg = "";
            foreach ($value->regionProduct as $k => $v) {
                $reg .= "{$v->region->name}, ";
            }
            $reg = substr($reg,0,-2);
            echo "<div>Товар есть: {$reg}</div>";
        }
        ?>
    <?php } ?>
    <div>---</div>
    <br>
    <div>---</div>
    <b>СУММА: <?= $summ ?> USD</b>
    <div>---</div>
</div>