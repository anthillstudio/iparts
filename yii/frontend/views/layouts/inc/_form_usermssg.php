<h2>Сообщите нам, если Вы нашли ошибку или неточность!</h2>
<div class="fly-box-form usermssg-form">
    <div class="fly-box-form-el">
        <span style="display: none" class="fly-box-form-podpis">Сообщение</span>
        <div class="fly-box-form-textarea">
            <textarea id="usermssg" name="usermssg" type="text" value=""></textarea>
            <input type="hidden" name="url" id="url" value="<?=$url?>">
        </div>
    </div><!-- fly-box-form-el -->
    <div class="fly-box-form-bttn"><a href="javascript:addusermssg()">Отправить</a></div>
</div><!-- fly-box-form -->