<?php

use frontend\assets\AppAsset;
use common\widgets\Alert;
use common\models\Settings;
use common\widgets\Slider\SliderWidget;
use common\widgets\mainMenu\MainMenuWidget;
use common\widgets\currency\CurrencyWidget;

$settings = Settings::getSettings();
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<?php echo $this->render('inc/_header.php'); ?>
<?php $this->beginBody() ?>

    <div>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>


<?php $this->endBody() ?>
<?php $this->endPage() ?>