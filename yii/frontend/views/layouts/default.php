<?php

use frontend\assets\AppAsset;
use common\widgets\Alert;
use common\models\Settings;
use common\widgets\mainMenu\MainMenuWidget;
use common\widgets\icons\IconsWidget;

$session = Yii::$app->session;
if (!$session->isActive) {
    $session->open();
}

/*print_r("<pre>");
print_r($_SESSION['basket']);
print_r("</pre>");*/
//unset($_SESSION['basket']);

$settings = Settings::getSettings();
AppAsset::register($this);
?>

<?php $this->beginPage() ?>
<?php echo $this->render('inc/_header.php'); ?>
<?php $this->beginBody() ?>



    <div id="cap-box">
        <?= MainMenuWidget::widget() ?>
    </div><!-- capBox -->

<?= $content ?>


<?= IconsWidget::widget() ?>
<?php echo $this->render('inc/_footer.php'); ?>

<?php $this->endBody() ?>
<?php $this->endPage() ?>