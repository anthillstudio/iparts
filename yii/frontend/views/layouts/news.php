<?php

use frontend\assets\AppAsset;
use common\widgets\Alert;
use common\models\Settings;
use common\widgets\Slider\SliderWidget;
use common\widgets\mainMenu\MainMenuWidget;


$settings = Settings::getSettings();
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<?php echo $this->render('inc/_header.php'); ?>
<?php $this->beginBody() ?>

<?php
$idcapbox = "cap-box-def";
$capbox = "cap-box-show";
?>

    <div id="<?=$idcapbox;?>" class="cap-box <?=$capbox;?>">
        <?= MainMenuWidget::widget() ?>
    </div><!-- capBox -->

    <div style="padding-top: 130px">
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>

<?php echo $this->render('inc/_footer.php'); ?>

<?php $this->endBody() ?>
<?php $this->endPage() ?>