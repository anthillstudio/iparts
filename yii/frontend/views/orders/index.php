<div style="background-color: #000; padding-top: 80px">
    <div class="fix">

    <?php

    use yii\helpers\Html;
    use yii\grid\GridView;
    use yii\widgets\Breadcrumbs;

    $this->title = 'Baskets !!!!!';
    $this->params['breadcrumbs'][] = $this->title;

    ?>

        <br><br>

    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ]) ?>



    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        Формируйте свою корзину. Вы можете добавлять, удалять и изменять количество блюд!

    <p>
        <?= Html::a('Create Basket', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'insert_date',
            'region',
            'user_id',
            'fname',
            // 'order:ntext',
            // 'summ',
            // 'phone',
            // 'pay_type',
            // 'code',
            // 'city',
            // 'region_type',
            // 'addr:ntext',
            // 'home',
            // 'porch',
            // 'stage',
            // 'corpus',
            // 'room',
            // 'user_message:ntext',
            // 'email:email',
            // 'user_ip',
            // 'use_in_om',
            // 'status',
            // 'comment:ntext',
            // 'block',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
</div>

<a href="/basket/orders/check-a">Заказать с доставкой</a>
<a href="/basket/orders/check-b">Заказать без доставки</a>