<?php

use common\components\Ant;
use common\models\Events;

$uploadsPath = Ant::uploaderInfo();
$path = "/events/{$page['image_file']}";
$img = "";
if (trim($page['image_file'])!='' && file_exists("../{$uploadsPath['dir']}{$path}"))
    $img = "<img src='{$uploadsPath['domen']}{$path}'>";

$domen = Yii::$app->params['uploader']['domen'];

$date = Events::eventDate($page);

switch ($page['image_file_use']){
    case "1":
        ?>
        <div class="content-title-box-wrapper events-title image-file-use<?=$page['image_file_use']?>">
            <div class="content-title-box">
                <div class="content-title-box-info">
                    <div>
                        <h1 class="FNTC"><?= $page['title_lng']; ?></h1>
                        <i class="FNTC"><?= $page['long_title_lng']; ?></i>
                        <span class="FNTC"><?=$date?></span>
                    </div>
                </div>
            </div><!-- content-title-box -->
        </div><!-- content-title-box-wrapper -->
        <?php
        break;
    case "2":
        ?>
        <div class="content-title-box-wrapper events-title">
            <div class="content-title-box">
                <div class="content-title-img"><?=$img?></div>
                <div class="content-title-box-info">
                    <div>
                        <h1 class="FNTC"><?= $page['title_lng']; ?></h1>
                        <i class="FNTC"><?= $page['long_title_lng']; ?></i>
                        <span class="FNTC"><?=$date?></span>
                    </div>
                </div>
            </div><!-- content-title-box -->
        </div><!-- content-title-box-wrapper -->
        <?php
        break;
    case "3":
        ?>
        <div class="content-title-box-wrapper page-events-title">
            <div class="content-title-box">
                <div class="content-title-img"><img src="<?=$domen?>/news/353X77722.jpg"></div>
                <div class="content-title-box-info">
                    <div>
                        <h1 class="FNTC"><?= $page['title_lng']; ?></h1>
                        <i class="FNTC"><?= $page['long_title_lng']; ?></i>
                    </div>
                </div>
            </div><!-- content-title-box -->
        </div><!-- content-title-box-wrapper -->
        <?php
        break;
}