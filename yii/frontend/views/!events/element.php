<?php
use common\modules\xblocks\models\Iblocks;
use common\modules\xblocks\models\Imgblocks;
use common\modules\xblocks\models\Reviewsblocks;
use common\widgets\eventsAdditional\EventsAdditionalWidget;
$page = $eventObject->page;
$this->title = $page['title_lng'];

$config = [
    'title' => $page['title_lng'],
    'notice' => $page['notice_lng'],
    'style' => $page['title_style'],
    'date' => $eventObject->eventDate($page,false),
    'img' => "/{$eventObject->config['dir']}/{$page['image_file_title']}"
];

?>
<?= Yii::$app->getView()->render('@frontend/views/inc/menu/_title_box.php', ['config'=>$config]); ?>
<div class="fix page-wrapper page-news">
    <ul class="bread-crumb FNTC">
        <li><a href="/">Главная</a></li>
        <li>|</li>
        <li><a href="/events">Мероприятия</a></li>
    </ul><!-- bread-crumb -->

    <div class="fix news-content">
        <div class="news-content-left">
            <div class="deflist-items">

                <?php if (!in_array($page['title_style'],[5,6])){ ?>
                    <?php $eventObject->eventDate($page) ?>
                    <div class="page-news-date"><?= $date ?></div>
                <?php } ?>

                <?php if(trim($page['content_lng']!='')){ ?>
                    <div class="element-content FNTC">

                        <?php $path = "/{$eventObject->config['dir']}/m{$page['image_file']}"; ?>
                        <?php if (trim($page['image_file']!='')){ ?>
                        <div class="element-content-img">
                        <img src="<?=Yii::$app->params['uploader']['domen'].$path?>">
                        </div><!-- element-content-img -->
                        <?php } ?>


                        <?= $page['content_lng']; ?>
                    </div>
                <?php } ?>
                <?=Iblocks::buildElements($page['id'],'events')?>

                <?=Imgblocks::buildElements($page['id'],'events')?>

                <?=Reviewsblocks::buildElements($page['id'],'events')?>


            </div><!-- deflist-itemS -->
        </div>
        <div class="news-content-right">
            <?=eventsAdditionalWidget::widget();?>
        </div>
    </div>




</div><!-- page-news -->