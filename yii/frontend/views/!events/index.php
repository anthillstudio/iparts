<?php
use common\widgets\eventsAdditional\EventsAdditionalWidget;
//$page = $eventObject->page;
?>

<?php
$this->title = $page['title_lng'];

$config = [
    'title' => $page['title_lng'],
    'notice' => $page['content_lng'],
    'style' => 5,
];

?>
<?= Yii::$app->getView()->render('@frontend/views/inc/menu/_title_box.php', ['config'=>$config]); ?>


<div class="fix news-content">
    <div class="news-content-left">
        <div class="deflist-items">


            <?php foreach ($elements as $key=>$value){ ?>
                <div class="deflist-item">

                    <?php $path = "/{$eventObject->config['dir']}/m".$value['image_file']; ?>
                    <?php $img = Yii::$app->params['uploader']['domen'].$path; ?>
                    <?php
                    if (!file_exists("../cloud".$path)){
                        $img = '/img/icons/noimg.jpg';
                    }
                    ?>

                    <div class="deflist-item-img">
                        <img src="<?=$img?>">
                    </div><!-- deflist-item-img -->

                    <div class="deflist-item-info">
                        <?php $date = $eventObject->eventDate($value) ?>
                        <div class="deflist-item-date FNTC"><?=$date?><br></div>
                        <div class="deflist-item-title"><a class="FNTC" href="/<?=$value->beautiful_url?>"><?=$value->title_lng?></a></div>
                        <div class="deflist-item-notice FNTC"><?=$value->notice_lng?> (<a href="/<?=$value->beautiful_url?>">Подробнее</a>)</div>
                    </div>
                </div><!-- deflist-item -->
            <?php } ?>


        </div><!-- deflist-itemS -->
    </div>
    <div class="news-content-right">
        <?=eventsAdditionalWidget::widget();?>
    </div>
</div>