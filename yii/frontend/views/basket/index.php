<?php
//use yii\widgets\Breadcrumbs;
//use common\widgets\contentmenu\ContentmenuWidget;
//use common\models\Content;
//use common\models\ContentCatalog;
?>

<?php
/*print_r("<pre>");
print_r($basketObject->elements);
print_r("</pre>");*/
?>


<div class="content-title-box-wrapper content-title-box-mini">
    <div class="content-title-box">
        <div class="fix"><h1 class="FNTC">Корзина</h1></div>
    </div><!-- content-title-box -->
</div><!-- content-title-box-wrapper -->

<div class="body-content-wrapper fix">
    <div class="content-wrapper">
        <?php
        $totalprice = 0;
        ?>
        <?php if (count($basketObject->elements)!=0){ ?>

            <table class="basket-tbl">
                <tr>
                    <td>Название</td>
                    <td>Цена</td>
                    <td>Количество</td>
                    <td>Сумма</td>
                </tr>
            <?php foreach ($basketObject->elements as $key=>$value){ ?>
                <tr>
                <td><?= $value['title_lng']; ?></td>
                <td><?= $value['price']; ?></td>
                <td><?= $value['count']; ?></td>
                <?php
                $price2 = $value['count']*$value['price'];
                ?>
                <td><?= $price2; ?></td>
                <?php
                $totalprice += $price2;
                ?>
                </tr>
            <?php } ?>

            <tr>
            <td colspan="4">Итого: <?=$totalprice?></td>
            </tr>
            </table>

            <div class="basket-order-btn"><a href="/">Заказать</a></div>

        <?php } ?>
    </div>
</div>