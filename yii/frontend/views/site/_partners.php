<script>$( document ).ready(function(){$(".popup-box-close").click(function(){$(".popup-box-wrapper").fadeOut();});});</script>
<script>
    function submitPartnerForm() {

        var name = $(".add-partner-form #partner-name").val();
        var email = $(".add-partner-form #partner-email").val();
        var phone = $(".add-partner-form #partner-phone").val();
        var addra = $(".add-partner-form #partner-addr-a").val();
        var addrb = $(".add-partner-form #partner-addr-b").val();
        var comment = $(".add-partner-form #partner-comment").val();
        var type = $(".add-partner-form #partner-type").val();

        var toreturn = true;

        if (name==''){
            toreturn = false;
            $(".add-partner-form #partner-name").focus();
            showBoxMssg('Вы не указали имя',0,'add-partner-form');
        }else if(email==''){
            toreturn = false;
            $(".add-partner-form #partner-email").focus();
            showBoxMssg('Вы не указали e-mail',0,'add-partner-form');
        }else if(phone==''){
            toreturn = false;
            $(".add-partner-form #partner-phone").focus();
            showBoxMssg('Вы не указали телефон',0,'add-partner-form');
        }else if(addra==''){
            toreturn = false;
            $(".add-partner-form #partner-addr-a").focus();
            showBoxMssg('Вы не указали фактический адрес',0,'add-partner-form');
        }else if(addrb==''){
            toreturn = false;
            $(".add-partner-form #partner-addr-b").focus();
            showBoxMssg('Вы не указали юридический адрес',0,'add-partner-form');
        }else if(type==0){
            toreturn = false;
            $(".add-partner-form #partner-type").focus();
            showBoxMssg('Вы не указали тип',0,'add-partner-form');
        }

        if (toreturn){
            $.get("/site/sendmail",{
                    action : 'addPartnerMessages',
                    name : name,
                    email : email,
                    phone: phone,
                    type: type,
                    addra: addra,
                    addrb: addrb,
                    comment: comment
                },
                function(data) {
                    showBoxMssg('Сообщение отправлено',1);
                    setTimeout(hideBoxMssg, 3000)
                    document.getElementById("partner-form").reset();
                }
            );
        }
    };
</script>
<style>
    .add-partner-form h2{font-size: 20pt}
    .add-partner-form-item{display: block; margin-bottom: 10px}
    .add-partner-form-item span{
        display: block; text-align: left; margin-bottom: 5px; font-size: 12pt;
    }
    .add-partner-form-item input{display: block; box-sizing: border-box; width: 100%; padding: 10px; border: 1px solid #dedede; font-size: 12pt;}
    .add-partner-form-item textarea{display: block; box-sizing: border-box; width: 100%; padding: 10px; border: 1px solid #dedede; font-size: 12pt;}
    .add-partner-form-item select{display: block; box-sizing: border-box; width: 100%; padding: 10px; border: 1px solid #dedede; font-size: 12pt;}
    .add-partner-text{font-size: 12pt; margin-bottom: 20px; text-align: center}
    .add-partner-form-bttn{text-align: center; margin-top: 20px}
    .add-partner-form-bttn a{
        display: inline-block; background-color: #ecd379; color: #222; padding: 20px 40px; text-decoration: none;
        font-size: 12pt;
    }
    .add-partner-form-bttn a:hover,
    .add-partner-form-bttn a:active,
    .add-partner-form-bttn a:focus{
        background-color: #e2c55b;
    }
</style>
<div class="goods-detals-box-html-content">
    <div class="popup-box-close FNTA">X</div>

    <div class="add-partner-form FNTC">
        <div class="box-mssg" id="box-mssg"></div>
        <h2 class="add-partner-title">Заявка на партнерство</h2>
        <div class="add-partner-text">Заявку могут подавать сервисные центры, предприниматели</div>
        <form id="partner-form">
            <div class="add-partner-form-item"><input placeholder="ФИО" name="partner-name" id="partner-name" type=""></div>
            <div class="add-partner-form-item"><input placeholder="E-mail" name="partner-email" id="partner-email" type=""></div>
            <div class="add-partner-form-item"><input placeholder="Телефон" name="partner-phone" id="partner-phone" type=""></div>
            <div class="add-partner-form-item"><input placeholder="Фактический адрес" name="partner-addr-a" id="partner-addr-a" type=""></div>
            <div class="add-partner-form-item"><input placeholder="Юридический адрес" name="partner-addr-b" id="partner-addr-b" type=""></div>
            <div class="add-partner-form-item">
                <textarea placeholder="Комментарий" name="partner-comment" id="partner-comment"></textarea>
            </div><!-- add-partner-form-item -->
            <div class="add-partner-form-item">
                <select name="partner-type" id="partner-type">
                    <option value="0">Тип</option>
                    <?php foreach(Yii::$app->params['partnerType'] as $key=>$val){ ?>
                        <option value="<?= $key ?>"><?= $val ?></option>
                    <?php } ?>
                </select>
            </div><!-- add-partner-form-item -->
        </form>
        <div class="add-partner-form-bttn"><a href="javascript:submitPartnerForm()">Подать заявку</a></div>
    </div><!-- add-partner-form -->

</div><!-- goods-detals-box-html-content -->