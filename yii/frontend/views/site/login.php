<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Вход';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="body-content-wrapper fix">



    <h1><?= Html::encode($this->title) ?></h1>


    <br>
    <br>

    <div style="max-width: 500px">
        <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

        <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>

        <?= $form->field($model, 'password')->passwordInput() ?>

        <?= $form->field($model, 'rememberMe')->checkbox() ?>

        <div>
            Если Вы забыли пароль <?= Html::a('перейдите по ссылке', ['site/request-password-reset']) ?>.
        </div>
        <br>
        <div class="form-group">
            <?= Html::submitButton('Войти', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
        </div>

        <?php ActiveForm::end(); ?>

<br><br>
        <b><a href="javascript:addToPartners()">Подать заявку на партнерство</a></b>. <i>Заявку могут подавать сервисные центры, предприниматели (частные мастера).</i>

    </div>
</div>
