<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

$this->title = $name;
?>
<div class="site-error">
    <?php if($exception->statusCode == '404') { $this->title = "Где-то ошибка..."; ?>
        <?= $this->render('errors/404.php') ?>
    <? }?>
</div><!-- site-error -->