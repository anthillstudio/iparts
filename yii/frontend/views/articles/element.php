<?php
use common\components\Ant;
use yii\widgets\Breadcrumbs;
use common\widgets\contentmenu\ContentmenuWidget;
use common\models\Articles;
use common\models\ArticlesCatalogs;
use common\modules\xblocks\models\Iblocks;


//$orientation = $contentObject->orientation;
?>

<div class="content-title-box-wrapper image-file-use1">
    <div class="content-title-box">
        <div class="content-title-box-info">
            <div>
                <h1 class="FNTC"><?= $element['title_lng']; ?></h1>
                <i class="FNTB"><?= $element['long_title_lng']; ?></i>
            </div>
        </div>
    </div><!-- content-title-box -->
</div><!-- content-title-box-wrapper -->


<div class="body-content-wrapper fix">
    <div class="content-wrapper content-tpl-<?=$contentObject->pageTpl?> content-navigation-<?=$orientation?> issetnavigation<?=$issetNavigation?>">
        <div class="content-box-wrapper">
            <?php if (trim($element['content_lng'])!=''){ ?>
                <div class="content-box-content FNTC"><?= $element['content_lng']; ?></div>
            <?php } ?>
            <?=Iblocks::buildElements($element['id'])?>
        </div><!-- content-box-wrapper -->
    </div><!-- content-wrapper -->
</div><!-- body-content-wrapper -->