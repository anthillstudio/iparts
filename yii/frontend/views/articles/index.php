<?php
use yii\widgets\Breadcrumbs;
$page = $newsObject->page;
?>

<?php
$page['title_lng'] = "Блог";
$page['long_title_lng'] = "Блог";
$page['image_file_use'] = 3;

?>
<?= $this->render('inc/_title_box.php', ['page' => $page]); ?>
<div class="fix">
    <div class="deflist-items">
<?php foreach ($elements as $key=>$value){ ?>
    <div class="deflist-item">
        <?php $date = Yii::$app->formatter->asDate($value['date'], 'php:d F '); ?>
        <div class="deflist-item-date FNTC"><?=$date?><br></div>
        <div class="deflist-item-title"><a class="FNTC" href="/blog/<?=$value->beautiful_url?>"><?=$value->title_lng?></a></div>
        <div class="deflist-item-notice FNTC"><?=$value->notice_lng?> (<a href="/blog/<?=$value->beautiful_url?>">Подробнее</a>)</div>
    </div><!-- deflist-item -->
<?php } ?>
    </div><!-- deflist-itemS -->
</div>
