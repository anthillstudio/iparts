<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        '/inc/swiper/dist/css/swiper.min.css',
        //'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/css/bootstrap-select.min.css',
        'https://fonts.googleapis.com/css?family=Cuprum:700|Marck+Script|Open+Sans|PT+Sans+Narrow|PT+Sans'
    ];
    public $js = [
        'js/scripts.js',
        'inc/swiper/dist/js/swiper.min.js',
        //'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.min.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

    public $jsOptions = ['position' => \yii\web\View::POS_HEAD];

}
