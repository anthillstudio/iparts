<?php

use common\components\Ant;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\BnnrsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Баннеры';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bnnrs-index">
    <script>
        function moveRecord(el){
            var val = $("#jumpElement"+el).val();
            if (val==0){
                alert("ID не указан!");
            }else{
                if (confirm("Вы действительно хотите переместить элемент?")){
                    location.href='/bnnrs/sort?action=jumpelement&val='+val+'&el='+el;
                }
            }
        }
    </script>
    <p>
        <?= Html::a('Добавить баннер', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'options' => ['style'=>'white-space: normal'],
        'columns' => [
            [
                'attribute' => 'id',
                'options' => ['style'=>'width: 70px']
            ],
            [
                'attribute' => 'IMG',
                'format' => 'raw',
                'options' => ['style'=>'width: 70px'],
                'value' => function ($models) {
                    $toReturn = "";
                    $uploaderInfo = Ant::uploaderInfo();
                    $path = "/{$models->config['dir']}/{$models->image_file}";
                    if (trim($models->image_file)!='') {
                        $toReturn .= Html::img(Url::to("{$uploaderInfo['domen']}{$path}"), [
                            'alt' => '',
                            'style' => 'width:100px;'
                        ]);
                    }else{
                        $toReturn .= '-';
                    }
                    return $toReturn;
                },
            ],
            'title_lng',
            'show',
            [
                'attribute' => '',
                'format' => 'raw',
                'options' => ['style'=>'width: 70px'],
                'value' => function ($model, $key, $index, $column) {
                    $toReturn = "<input style='width: 50px' id='jumpElement{$model->id}' type='text'><a href=\"javascript:moveRecord('{$model->id}')\"> >> </a>";
                    return $toReturn;
                },
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
