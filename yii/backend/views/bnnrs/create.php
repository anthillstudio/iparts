<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Bnnrs */

$this->title = 'Добавить баннер';
$this->params['breadcrumbs'][] = ['label' => 'Баннеры', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bnnrs-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
