<?php

use common\components\Ant;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$ant = new Ant();

/* @var $this yii\web\View */
/* @var $model common\models\Bnnrs */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bnnrs-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title_lng')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'show')->checkbox() ?>

    <div>
        <div style="border: 1px solid #b5b5b5; padding: 10px; display: inline-block; margin-bottom: 20px">
            <?php
            if (!$model->isNewRecord){
                $uploadsPath = Ant::uploaderInfo();
                $path = "/{$model->config['dir']}/{$model->image_file}";
                if (file_exists("../{$uploadsPath['dir']}{$path}") && $model->image_file!='') {
                    $img = \yii\helpers\Html::tag(
                        "img",
                        "Баннеры",
                        [
                            "style" => "width: 100px; margin-bottom: 10px",
                            "src" => "{$uploadsPath['domen']}{$path}"
                        ]
                    );
                    echo $img;
                }
            }
            ?>
            <?= $form->field($model, 'image_file')->fileInput()->label('Изображение') ?>
            <?= $form->field($model, 'imageFileDelete')->checkbox(['label'=>'Удалить'])->label(false) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
