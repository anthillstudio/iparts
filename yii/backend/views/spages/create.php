<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Spages */

$this->title = 'Добавить страницу';
$this->params['breadcrumbs'][] = ['label' => 'Spages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="spages-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
