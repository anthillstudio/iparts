<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\SpagesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Страницы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="spages-index">

    <p>
        <?= Html::a('Создать страницу', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'options' => ['style'=>'white-space: normal'],
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],
            //'id',
            'title_lng',
            'content_lng',
            'seo_title_lng',
            'seo_description_lng:ntext',
            'seo_keywords_lng',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
