<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Spages */

$this->title = 'Обновить страницу: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Spages', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Обновить';
?>
<div class="spages-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
