<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ArticlesCatalogs */

$this->title = 'Обновить тег: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Articles Catalogs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="articles-catalogs-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
