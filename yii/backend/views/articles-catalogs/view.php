<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ArticlesCatalogs */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Articles Catalogs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="articles-catalogs-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'insert_date',
            'update_date',
            'user_id',
            'seo_title_lng',
            'seo_description_lng:ntext',
            'seo_keywords_lng',
            'beautiful_url:url',
            'title_lng',
            'title_long_lng',
            'content_lng:ntext',
            'brand_id',
            'temporary',
            'tomenu',
            'main',
            'img_url:url',
            'img_title',
            'ext',
            'show',
            'level',
        ],
    ]) ?>

</div>
