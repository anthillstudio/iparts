<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ArticlesCatalogs */

$this->title = 'Добавить тег';
$this->params['breadcrumbs'][] = ['label' => 'Articles Catalogs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="articles-catalogs-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
