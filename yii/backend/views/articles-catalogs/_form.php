<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;
use vova07\imperavi\Widget;
use common\components\Ant;

use common\models\ArticlesCatalogs;


?>

<div class="articles-catalogs-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title_lng')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'content_lng')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'seo_title_lng')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'seo_description_lng')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'seo_keywords_lng')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'beautiful_url')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'image_title')->textInput(['maxlength' => true]) ?>

    <?php
    if (!$model->isNewRecord){

        $uploadsPath = Ant::uploaderInfo();
        $path = "/{$model->config['dir']}/{$model->image_file}";
        if (file_exists("../{$uploadsPath['dir']}{$path}") && $model->image_file!='') {
            $img = \yii\helpers\Html::tag(
                "img",
                "Страницы",
                [
                    "style" => "width: 100px; margin-bottom: 10px",
                    "src" => "{$uploadsPath['domen']}{$path}"
                ]
            );
            echo $img;
        }
    }
    ?>
    <?= $form->field($model, 'image_file')->fileInput()->label(false) ?>

    <?= $form->field($model, 'show')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
