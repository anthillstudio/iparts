<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\SettingsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Настройки';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="settings-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'options' => ['style'=>'white-space: normal'],
        'columns' => [
            [
                'attribute' => 'title',
                'options' => ['style'=>'width: 200px']
            ],
            'key',
            'value:ntext',
            [
                'attribute' => '',
                'format' => 'raw',
                'options' => ['style'=>'width: 10px'],
                'value' => function ($model, $key, $index, $column) {
                    $toReturn = "";
                    $toReturn .=  Html::a('<span class="glyphicon glyphicon-pencil"></span>', "/settings/update?id={$model->id}&type={$_GET['type']}", [
                        'title' => Yii::t('yii', 'Update'),
                    ]);
                    return $toReturn;
                },
            ],
        ],
    ]); ?>
</div>
