<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

use common\components\Ant;
use common\modules\xblocks\controllers\XblocksController;

/* @var $this yii\web\View */
/* @var $searchModel common\models\EventsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Мероприятия';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="events-index">

    <p>
        <?= Html::a('Добавить мероприятие', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'options' => ['style'=>'white-space: normal'],
        'columns' => [
            [
                'attribute' => 'IMG',
                'format' => 'raw',
                'options' => ['style'=>'width: 70px'],
                'value' => function ($models) {
                    $toReturn = "";
                    $uploaderInfo = Ant::uploaderInfo();
                    $path = "/{$models->config['dir']}/{$models->image_file}";
                    if (file_exists("../{$uploaderInfo['dir']}{$path}") && trim($models->image_file)!='') {
                        $toReturn .= Html::img(Url::to("{$uploaderInfo['domen']}{$path}"), [
                            'alt' => '',
                            'style' => 'width:100px;'
                        ]);
                    }else{
                        $toReturn .= '-';
                    }
                    return $toReturn;
                },
            ],
            [
                'attribute' => 'id',
                'options' => ['style'=>'width: 50px'],
                'label' => 'ID'
            ],
            [
                'attribute' => 'date_start',
                'options' => ['style'=>'width: 70px'],
                'label' => 'Дата начала'
            ],
            [
                'attribute' => 'date_end',
                'options' => ['style'=>'width: 70px'],
                'label' => 'Дата завершения'
            ],
            [
                'attribute' => 'title_lng',
                'options' => ['style'=>'width: 370px'],
                'label' => 'Название'
            ],
            [
                'attribute' => 'notice_lng',
                'options' => ['style'=>'width: 500px'],
                'label' => 'Кратко'
            ],
            [
                'format' => 'raw',
                'options' => ['style'=>'width: 10px'],
                'value' => function ($model) {
                    $modparent = ""; if (isset($_GET['parent'])) $modparent = "&modparent=".$_GET['parent'];
                    return \yii\helpers\Html::tag(
                        'a',
                        "Инфоблоки",
                        [
                            "class" => "label label-" . ('success'),
                            "href" => "/xblocks/iblocks/?model=events&parent={$model->id}{$modparent}"
                        ]
                    );
                },
            ],
            [
                'attribute' => '',
                'format' => 'raw',
                'options' => ['style'=>'width: 70px'],
                'value' => function ($model, $key, $index, $column) {
                    $modparent = ""; if (isset($_GET['parent'])) $modparent = "&modparent=".$_GET['parent'];
                    return \yii\helpers\Html::tag(
                        'a',
                        "Отзывы",
                        [
                            "class" => "label label-" . ('success'),
                            "href" => "/xblocks/reviewsblocks/?model=events&parent={$model->id}{$modparent}"
                        ]
                    );
                },
            ],
            [
                'format' => 'raw',
                'options' => ['style'=>'width: 10px'],
                'value' => function ($model) {
                    $modparent = ""; if (isset($_GET['parent'])) $modparent = "&modparent=".$_GET['parent'];
                    return \yii\helpers\Html::tag(
                        'a',
                        "Изображения",
                        [
                            "class" => "label label-" . ('success'),
                            "href" => "/xblocks/imgblocks/?model=events&parent={$model->id}{$modparent}"
                        ]
                    );
                },
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
