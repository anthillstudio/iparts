<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\UserInfo */

$this->title = 'Добавить пользователя';
$this->params['breadcrumbs'][] = ['label' => 'User Infos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-info-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
