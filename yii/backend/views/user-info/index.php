<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

use common\components\Ant;

/* @var $this yii\web\View */
/* @var $searchModel common\models\UserInfoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Партнеры';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-info-index">
    <p>
        <?= Html::a('Добавить пользователя', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'IMG',
                'format' => 'raw',
                'options' => ['style'=>'width: 70px'],
                'value' => function ($models) {
                    $toReturn = "";
                    $uploaderInfo = Ant::uploaderInfo();
                    $path = "/".\common\models\UserInfo::$config['dir']."/{$models->image_file}";
                    if (file_exists("../{$uploaderInfo['dir']}{$path}") && trim($models->image_file)!='') {
                        $toReturn .= Html::img(Url::to("{$uploaderInfo['domen']}{$path}"), [
                            'alt' => '',
                            'style' => 'width:100px;'
                        ]);
                    }else{
                        $toReturn .= '-';
                    }
                    return $toReturn;
                },
            ],
            [
                'attribute' => 'id',
                'options' => ['style'=>'width: 10px']
            ],
            'code',
            'name',
            'phone',
            /*[
                'format' => 'raw',
                'options' => ['style'=>'width: 10px'],
                'value' => function ($model) {
                    $modparent = ""; if (isset($_GET['parent'])) $modparent = "&modparent=".$_GET['parent'];
                    return \yii\helpers\Html::tag(
                        'a',
                        "Настройки",
                        [
                            "class" => "label label-" . ('success'),
                            "href" => "/user-settings/update?user={$model->id}"
                        ]
                    );
                },
            ],*/
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
