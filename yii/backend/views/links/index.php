<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\LinksSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ссылки';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="links-index">

    <script>
        function moveRecord(el){
            var val = $("#jumpElement"+el).val();
            if (val==0){
                alert("ID не указан!");
            }else{
                if (confirm("Вы действительно хотите переместить элемент?")){
                    location.href='/links/sort?action=jumpelement&val='+val+'&el='+el+'&parent=<?=$_GET['parent']?>';
                }
            }
        }
    </script>

    <p>
        <?= Html::a('Добавление ссылок', ['create?parent='.$_GET['parent']], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'cat_id',
            'title_lng',
            'url:url',
            'show',
            //'level',
            [
                'attribute' => '',
                'format' => 'raw',
                'options' => ['style'=>'width: 70px'],
                'value' => function ($model) {
                    $toReturn = "<input style='width: 50px' id='jumpElement{$model->id}' type='text'><a href=\"javascript:moveRecord('{$model->id}')\"> >> </a>";
                    return $toReturn;
                },
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
