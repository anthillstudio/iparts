<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\CatalogElementsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Catalog Elements';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="catalog-elements-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Catalog Elements', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'parent',
            'insert_date',
            'update_date',
            'user_id',
            // 'title_lng',
            // 'ref',
            // 'price1',
            // 'price2',
            // 'beautiful_url:url',
            // 'notice_lng',
            // 'content_lng:ntext',
            // 'image_file',
            // 'tpl',
            // 'hit',
            // 'new',
            // 'seo_title',
            // 'seo_description:ntext',
            // 'seo_keywords',
            // 'show',
            // 'level',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
