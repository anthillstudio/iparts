<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\LinksCatSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Категории ссылок';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="links-cat-index">

    <script>
        function moveRecord(el){
            var val = $("#jumpElement"+el).val();
            if (val==0){
                alert("ID не указан!");
            }else{
                if (confirm("Вы действительно хотите переместить элемент?")){
                    location.href='/links-cat/sort?action=jumpelement&val='+val+'&el='+el;
                }
            }
        }
    </script>

    <p>
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'id',
                'options' => ['style'=>'width: 70px']
            ],
            'title_lng',
            'show',
            [
                'label' => 'Ссылки',
                'attribute' => '',
                'format' => 'raw',
                'filter' => [0 => 'No', 1 => 'Yes'],
                'options' => ['style'=>'width: 70px'],
                'value' => function ($model, $key, $index, $column) {
                    return \yii\helpers\Html::tag(
                        'a',
                        "Ссылки",
                        [
                            "class" => "label label-" . ('success'),
                            "href" => "/links/?parent={$model->id}"
                        ]
                    );
                },
            ],
            //'level',
            [
                'attribute' => '',
                'format' => 'raw',
                'options' => ['style'=>'width: 70px'],
                'value' => function ($model) {
                    $toReturn = "<input style='width: 50px' id='jumpElement{$model->id}' type='text'><a href=\"javascript:moveRecord('{$model->id}')\"> >> </a>";
                    return $toReturn;
                },
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
