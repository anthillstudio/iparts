<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\LinksCat */

$this->title = 'Обновить: ' . $model->title_lng;
$this->params['breadcrumbs'][] = ['label' => 'Links Cats', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="links-cat-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
