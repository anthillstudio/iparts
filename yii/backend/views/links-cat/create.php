<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\LinksCat */

$this->title = 'Добавить';
$this->params['breadcrumbs'][] = ['label' => 'Категория ссылок', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="links-cat-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
