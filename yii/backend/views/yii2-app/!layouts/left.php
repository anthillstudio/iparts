
<aside class="main-sidebar">

    <section class="sidebar">

        <br>

        <!-- search form -->
        <form action="#" method="get" style="display: none" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->

        <?php
        $projects = Yii::$app->params['projects'];
        $menuCatalogs = array();
        foreach ($projects as $key=>$value){
            $menuCatalogs[] = ['label' => $value['title'], 'icon' => 'fa fa-image', 'url' => ['/'.$value['url'].'/'.$value['key']]];
        }
        $menuCatalogs[] = ['label' => 'БАЗА', 'icon' => 'fa fa-image', 'url' => '/catalogbase',];
        $menuCatalogs[] = ['label' => 'Упаковка', 'icon' => 'fa fa-image', 'url' => '/catalog-boxes'];
        ?>

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    [
                        'label' => 'МЕНЮ БЛЮД',
                        'icon' => 'fa fa-coffee',
                        'url' => '#',
                        'items' => $menuCatalogs,
                    ],
                    //['label' => '', 'options' => ['class' => 'header']],
                    ['label' => 'КОРЗИНА', 'icon' => 'fa fa-birthday-cake', 'url' => ['/basket']],
                    ['label' => 'Акции', 'icon' => 'fa fa-birthday-cake', 'url' => ['/actions']],
                    ['label' => 'Пиццерии', 'icon' => 'fa fa-birthday-cake', 'url' => ['/place']],
                    ['label' => 'Новости', 'icon' => 'fa fa-newspaper-o', 'url' => ['/news']],
                    ['label' => 'Афиша', 'icon' => 'fa fa-music', 'url' => ['/afisha']],
                    ['label' => 'Фото галерея', 'icon' => 'fa fa-photo', 'url' => ['/event']],
                    ['label' => 'Контент', 'icon' => 'fa fa-newspaper-o', 'url' => ['/content']],
                    ['label' => 'Настройки', 'icon' => 'fa fa-newspaper-o', 'url' => ['/settings']],
                    //['label' => 'Баннеры (боковые)', 'icon' => 'fa fa-image', 'url' => ['/catalog-pages']],
                    //['label' => 'Баннеры (основные)', 'icon' => 'fa fa-image', 'url' => ['/mainbnnrs']],
                    ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                    /*[
                        'label' => 'Каталог',
                        'icon' => 'fa fa-circle-o',
                        'url' => '#',
                    ],*/
                    [
                        'label' => 'Баннеры',
                        'icon' => 'fa fa-image',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Основные', 'icon' => 'fa fa-image', 'url' => ['/mainbnnrs'],],
                            [
                                'label' => 'Боковые',
                                'icon' => 'fa fa-image',
                                'url' => '/bnnrs',
                            ],
                        ],
                    ],
                    [
                        'label' => 'Дополнительно',
                        'icon' => 'fa fa-share',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Gii', 'icon' => 'fa fa-file-code-o', 'url' => ['/gii'],],
                            ['label' => 'Debug', 'icon' => 'fa fa-dashboard', 'url' => ['/debug'],],
                            /*[
                                'label' => 'Level One',
                                'icon' => 'fa fa-circle-o',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Level Two', 'icon' => 'fa fa-circle-o', 'url' => '#',],
                                    [
                                        'label' => 'Level Two',
                                        'icon' => 'fa fa-dashboard',
                                        'url' => '#',
                                        'items' => [
                                            ['label' => 'Level Three', 'icon' => 'fa fa-circle-o', 'url' => '#',],
                                            ['label' => 'Level Three', 'icon' => 'fa fa-circle-o', 'url' => '#',],
                                        ],
                                    ],
                                ],
                            ],*/
                        ],
                    ],
                ],
            ]
        ) ?>

    </section>

</aside>
