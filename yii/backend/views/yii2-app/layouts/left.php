<aside class="main-sidebar">

    <section class="sidebar" style="padding-top: 20px">

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    ['label' => 'Контент', 'icon' => 'fa fa-birthday-cake', 'url' => ['/content']],
                    ['label' => 'Заказы', 'icon' => 'fa fa-birthday-cake', 'url' => ['/basket']],
                    ['label' => 'Новости', 'icon' => 'fa fa-birthday-cake', 'url' => ['/news']],
                    ['label' => 'Мероприятия', 'icon' => 'fa fa-birthday-cake', 'url' => ['/events']],
                    ['label' => 'Слайдер', 'icon' => 'fa fa-birthday-cake', 'url' => ['/slider']],
                    ['label' => 'Блог', 'icon' => 'fa fa-birthday-cake', 'url' => ['/articles']],
                    ['label' => 'Блог (рубрики)', 'icon' => 'fa fa-birthday-cake', 'url' => ['/articles-catalogs']],
                    ['label' => 'Настройки', 'icon' => 'fa fa-newspaper-o', 'url' => ['/settings?type=0']],
                    ['label' => 'Самостоятельные страницы', 'icon' => 'fa fa-newspaper-o', 'url' => ['/spages']],
                    ['label' => 'Ссылки', 'icon' => 'fa fa-newspaper-o', 'url' => ['/links-cat']],
                    ['label' => 'Баннеры', 'icon' => 'fa fa-newspaper-o', 'url' => ['/bnnrs']],
                    ['label' => 'Партнеры', 'icon' => 'fa fa-newspaper-o', 'url' => ['/user-info']],
                    [
                        'label' => 'Еще ...',
                        'icon' => 'share',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Выход', 'icon' => 'fa fa-newspaper-o', 'url' => ['/site/logout']],
                            ['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii'],],
                            ['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/debug'],],
                        ],
                    ],
                ],
            ]
        ) ?>

    </section>

</aside>
