<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ContactsCitiesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="contacts-cities-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'insert_date') ?>

    <?= $form->field($model, 'update_date') ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'region_id') ?>

    <?php // echo $form->field($model, 'title_lng') ?>

    <?php // echo $form->field($model, 'top') ?>

    <?php // echo $form->field($model, 'left') ?>

    <?php // echo $form->field($model, 'beautiful_url') ?>

    <?php // echo $form->field($model, 'seo_title_lng') ?>

    <?php // echo $form->field($model, 'seo_description_lng') ?>

    <?php // echo $form->field($model, 'seo_keywords_lng') ?>

    <?php // echo $form->field($model, 'show') ?>

    <?php // echo $form->field($model, 'level') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
