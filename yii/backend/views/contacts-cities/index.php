<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ContactsCitiesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Contacts Cities';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contacts-cities-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Contacts Cities', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'insert_date',
            'update_date',
            'user_id',
            'region_id',
            // 'title_lng',
            // 'top',
            // 'left',
            // 'beautiful_url:url',
            // 'seo_title_lng',
            // 'seo_description_lng:ntext',
            // 'seo_keywords_lng',
            // 'show',
            // 'level',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
