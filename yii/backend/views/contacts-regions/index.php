<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ContactsRegionsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Contacts Regions';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contacts-regions-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Contacts Regions', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title_lng',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
