<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ContactsRegions */

$this->title = 'Create Contacts Regions';
$this->params['breadcrumbs'][] = ['label' => 'Contacts Regions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contacts-regions-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
