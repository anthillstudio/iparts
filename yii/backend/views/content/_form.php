<?php


use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;
use vova07\imperavi\Widget;
use common\components\Ant;

$ant = new Ant();

/* @var $this yii\web\View */
/* @var $model common\models\Content */
/* @var $form yii\widgets\ActiveForm */

//print_r();

?>





<div class="content-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'parent')->hiddenInput(['value'=>$parent])->label(false) ?>

    <?= $form->field($model, 'date')->textInput()->widget(DateTimePicker::className(), [
        'name' => 'date',
        'value' => date("Y-m-d H:i:00"),
        'convertFormat' => true,
        'pluginOptions' => [
            'format' => 'yyyy-MM-dd HH:i:00',
            'todayHighlight' => true
        ]
    ]);?>


    <?= $form->field($model, 'user_id')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'title_lng')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'long_title_lng')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'beautiful_url')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'notice_lng')->textarea(['rows' => 6])->widget(Widget::className(), [
        'settings' => [
            'lang' => 'ru',
            'minHeight' => 200,
            'plugins' => [
                'clips',
                'fullscreen'
            ],
            'imageUpload' => Url::to(['/content/image-upload']),
            'fileUpload' => Url::to(['/content/file-upload'])
        ]
    ]);
    ?>

    <?= $form->field($model, 'content_lng')->textarea(['rows' => 6])->widget(Widget::className(), [
        'settings' => [
            'lang' => 'ru',
            'minHeight' => 200,
            'plugins' => [
                'clips',
                'fullscreen'
            ],
            'imageUpload' => Url::to(['/content/image-upload']),
            'fileUpload' => Url::to(['/content/file-upload'])
        ]
    ]);
    ?>


    <?= $form->field($model, 'menu_type')->dropDownList($contentObject->menuType,['style'=>'width: auto']) ?>

    <?= $form->field($model, 'title_style')->dropDownList($ant->titleStyle,['style'=>'width: auto']) ?>

    <?= $form->field($model, 'tpl')->dropDownList($contentObject->tpls,['style'=>'width: auto']) ?>

    <?= $form->field($model, 'parent')->hiddenInput(['value' => $_GET['parent'],'maxlength' => true])->label(false) ?>

    <?= $form->field($model, 'seo_title_lng')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'seo_description_lng')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'seo_keywords_lng')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'show')->checkbox() ?>


    <div>
    <div style="border: 1px solid #b5b5b5; padding: 10px; display: inline-block; margin-bottom: 20px">
    <?php
    if (!$model->isNewRecord){
        $uploadsPath = Ant::uploaderInfo();
        $path = "/{$model->config['dir']}/{$model->image_file}";
        if (file_exists("../{$uploadsPath['dir']}{$path}") && $model->image_file!='') {
            $img = \yii\helpers\Html::tag(
                "img",
                "Страницы",
                [
                    "style" => "width: 100px; margin-bottom: 10px",
                    "src" => "{$uploadsPath['domen']}{$path}"
                ]
            );
            echo $img;
        }
    }
    ?>
    <?= $form->field($model, 'image_file')->fileInput()->label('Изображение') ?>
    <?= $form->field($model, 'imageFileDelete')->checkbox(['label'=>'Удалить'])->label(false) ?>
    </div>
    </div>

    <div>
    <div style="border: 1px solid #b5b5b5; padding: 10px; display: inline-block; margin-bottom: 20px">
    <?php
    if (!$model->isNewRecord){
        $uploadsPath = Ant::uploaderInfo();
        $path = "/{$model->config['dir']}/{$model->image_file_title}";
        if (file_exists("../{$uploadsPath['dir']}{$path}") && $model->image_file_title!='') {
            $img = \yii\helpers\Html::tag(
                "img",
                "Страницы",
                [
                    "style" => "width: 100px; margin-bottom: 10px",
                    "src" => "{$uploadsPath['domen']}{$path}"
                ]
            );
            echo $img;
        }
    }
    ?>
    <?= $form->field($model, 'image_file_title')->fileInput()->label('Изображение для заголовка') ?>
    <?= $form->field($model, 'imageFileTitleDelete')->checkbox(['label'=>'Удалить'])->label(false) ?>
    </div>
    </div>




    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>




<script type="text/javascript" src="http://cloud.rrb.dev/redactor/lib/jquery-1.7.min.js"></script>
<link rel="stylesheet" href="http://cloud.rrb.dev/redactor/redactor/css/redactor.css" />
<script src="http://cloud.rrb.dev/redactor/redactor/redactor.js"></script>

<script type="text/javascript">
    $(document).ready(
        function()
        {
            $('.redactor').redactor({
                imageUpload: '../cloud/redactor/scripts/image_upload.php',
                fileUpload: '../cloud/redactor/scripts/file_upload.php',
                imageGetJson: '../cloud/redactor/scripts/data_json.php'
            });
        }
    );
</script>