<?php

use yii\helpers\Html;

$this->title = 'Обновить страницу: ' . $model->title_lng;
$this->params['breadcrumbs'][] = ['label' => 'Contents', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="content-update">

    <?= $this->render('_form', [
        'model' => $model,
        'contentObject' => $contentObject
    ]) ?>

</div>
