<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

use common\components\Ant;
use common\modules\xblocks\controllers\XblocksController;


$this->title = 'Страницы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-index">

    <script>
        function moveRecord(el){
            var val = $("#jumpElement"+el).val();
            if (val==0){
                alert("ID не указан!");
            }else{
                if (confirm("Вы действительно хотите переместить элемент?")){
                    location.href='/content/sort?action=jumpelement&val='+val+'&el='+el;
                }
            }
        }
    </script>

        <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'options' => ['style'=>'white-space: normal'],
        'columns' => [
            [
                'attribute' => 'id',
                'options' => ['style'=>'width: 50px']
            ],
            [
                'attribute' => 'IMG',
                'format' => 'raw',
                'options' => ['style'=>'width: 50px'],
                'value' => function ($models) {
                    $toReturn = "";
                    $uploaderInfo = Ant::uploaderInfo();
                    $path = "/{$models->config['dir']}/{$models->image_file}";
                    if (file_exists("../{$uploaderInfo['dir']}{$path}") && trim($models->image_file)!='') {
                        $toReturn .= Html::img(Url::to("{$uploaderInfo['domen']}{$path}"), [
                            'alt' => '',
                            'style' => 'width:100px;'
                        ]);
                    }else{
                        $toReturn .= '-';
                    }
                    return $toReturn;
                },
            ],
            [
                'attribute' => 'level',
                'options' => ['style'=>'width: 50px'],
                'label' => 'Уровень'
            ],
            [
                'attribute' => 'title_lng',
                'options' => ['style'=>'width: 100%'],
                'label' => 'Название'
            ],
            [
                'label' => false,
                'attribute' => '',
                'format' => 'raw',
                'filter' => [0 => 'No', 1 => 'Yes'],
                'options' => ['style'=>'width: 70px'],
                'value' => function ($model) {
                    return \yii\helpers\Html::tag(
                        'a',
                        "Страницы",
                        [
                            "class" => "label label-" . ('success'),
                            "href" => "/content/?parent={$model->id}"
                        ]
                    );
                },
            ],
            [
                'format' => 'raw',
                'options' => ['style'=>'width: 70px'],
                'value' => function ($model) {
                    $modparent = ""; if (isset($_GET['parent'])) $modparent = "&modparent=".$_GET['parent'];
                    return \yii\helpers\Html::tag(
                        'a',
                        "Инфоблоки",
                        [
                            "class" => "label label-" . ('success'),
                            "href" => "/xblocks/iblocks/?model=content&parent={$model->id}{$modparent}"
                        ]
                    );
                },
            ],
            [
                'format' => 'raw',
                'options' => ['style'=>'width: 70px'],
                'value' => function ($model) {
                    $modparent = ""; if (isset($_GET['parent'])) $modparent = "&modparent=".$_GET['parent'];
                    return \yii\helpers\Html::tag(
                        'a',
                        "Изображения",
                        [
                            "class" => "label label-" . ('success'),
                            "href" => "/xblocks/imgblocks/?model=content&parent={$model->id}{$modparent}"
                        ]
                    );
                },
            ],
            [
                'format' => 'raw',
                'options' => ['style'=>'width: 70px'],
                'value' => function ($model) {
                    $modparent = ""; if (isset($_GET['parent'])) $modparent = "&modparent=".$_GET['parent'];
                    return \yii\helpers\Html::tag(
                        'a',
                        "Файлы",
                        [
                            "class" => "label label-" . ('success'),
                            "href" => "/xblocks/xfiles/?model=content&parent={$model->id}{$modparent}"
                        ]
                    );
                },
            ],
            [
                'attribute' => '',
                'format' => 'raw',
                'options' => ['style'=>'width: 70px'],
                'value' => function ($model, $key, $index, $column) {
                    $toReturn = "<input style='width: 50px' id='jumpElement{$model->id}' type='text'><a href=\"javascript:moveRecord('{$model->id}')\"> >> </a>";
                    return $toReturn;
                },
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'options' => ['style'=>'width: 10px']
            ],
        ],
    ]); ?>

</div>
<p>
    <?php
    $parent = 0;
    $request = Yii::$app->request->get();
    if (isset($request['parent'])) $parent = $request['parent'];
    ?>
    <?= Html::a('Создать страницу', ['create?parent='.$parent], ['class' => 'btn btn-success']) ?>
</p>