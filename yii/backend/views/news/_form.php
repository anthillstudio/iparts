<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

use kartik\datetime\DateTimePicker;
use vova07\imperavi\Widget;
use common\components\Ant;

$ant = new Ant();

/* @var $this yii\web\View */
/* @var $model common\models\News */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="news-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php if ($model->isNewRecord) $model->date = date("Y-m-d H:i:00"); ?>

    <?= $form->field($model, 'date')->textInput()->widget(DateTimePicker::className(), [
        'name' => 'date',
        'value' => date("Y-m-d H:i:00"),
        //'options' => ['placeholder' => 'Select operating time ...'],
        'convertFormat' => true,
        'pluginOptions' => [
            'format' => 'yyyy-MM-dd HH:i:00',
            //'startDate' => $date,
            //'format' => 'yyyy-m-dd hh:ii',
            'todayHighlight' => true
        ]
    ]);?>

    <?= $form->field($model, 'user_id')->hiddenInput(['value'=>'1'])->label(false) ?>

    <?= $form->field($model, 'title_lng')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'beautiful_url')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'notice_lng')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'content_lng')->textarea(['rows' => 6])->widget(Widget::className(), [
        'settings' => [
            'lang' => 'ru',
            'minHeight' => 200,
            'plugins' => [
                'clips',
                'fullscreen'
            ],
            'imageUpload' => Url::to(['/content/image-upload'])
        ]
    ]);
    ?>

    <?= $form->field($model, 'title_style')->dropDownList($ant->titleStyle,['style'=>'width: auto']) ?>



    <div>
        <div style="border: 1px solid #b5b5b5; padding: 10px; display: inline-block; margin-bottom: 20px">
            <?php
            if (!$model->isNewRecord){
                $uploadsPath = Ant::uploaderInfo();
                $path = "/{$model->config['dir']}/{$model->image_file}";
                if (file_exists("../{$uploadsPath['dir']}{$path}") && $model->image_file!='') {
                    $img = \yii\helpers\Html::tag(
                        "img",
                        "Страницы",
                        [
                            "style" => "width: 100px; margin-bottom: 10px",
                            "src" => "{$uploadsPath['domen']}{$path}"
                        ]
                    );
                    echo $img;
                }
            }
            ?>
            <?= $form->field($model, 'image_file')->fileInput()->label('Изображение') ?>
            <?= $form->field($model, 'imageFileDelete')->checkbox(['label'=>'Удалить'])->label(false) ?>
        </div>
    </div>

    <div>
        <div style="border: 1px solid #b5b5b5; padding: 10px; display: inline-block; margin-bottom: 20px">
            <?php
            if (!$model->isNewRecord){
                $uploadsPath = Ant::uploaderInfo();
                $path = "/{$model->config['dir']}/{$model->image_file_title}";
                if (file_exists("../{$uploadsPath['dir']}{$path}") && $model->image_file_title!='') {
                    $img = \yii\helpers\Html::tag(
                        "img",
                        "Страницы",
                        [
                            "style" => "width: 100px; margin-bottom: 10px",
                            "src" => "{$uploadsPath['domen']}{$path}"
                        ]
                    );
                    echo $img;
                }
            }
            ?>
            <?= $form->field($model, 'image_file_title')->fileInput()->label('Изображение для заголовка') ?>
            <?= $form->field($model, 'imageFileTitleDelete')->checkbox(['label'=>'Удалить'])->label(false) ?>
        </div>
    </div>


    <?= $form->field($model, 'seo_title_lng')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'seo_description_lng')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'seo_keywords_lng')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'show')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
