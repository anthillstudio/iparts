<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ContentCatalog */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Content Catalogs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-catalog-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'parent',
            'insert_date',
            'update_date',
            'user_id',
            'title_lng',
            'long_title_lng',
            'beautiful_url:url',
            'notice_lng',
            'content_lng:ntext',
            'image_file',
            'tpl',
            'seo_title_lng',
            'seo_description_lng:ntext',
            'seo_keywords_lng',
            'show',
            'level',
        ],
    ]) ?>

</div>
