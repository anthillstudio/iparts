<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ContentCatalog */

$this->title = 'Добавление';
$this->params['breadcrumbs'][] = ['label' => 'Каталог', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-catalog-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
