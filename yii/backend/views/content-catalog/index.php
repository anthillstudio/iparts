<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\components\Ant;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ContentCatalogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Товары';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-catalog-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить', ['create?parent='.$_GET['parent']], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'options' => ['style'=>'white-space: normal'],
        'columns' => [
            // ['class' => 'yii\grid\SerialColumn'],
            //'parent',
            //'insert_date',
            //'update_date',
            //'user_id',
            [
                'attribute' => 'IMG',
                'format' => 'raw',
                'options' => ['style'=>'width: 70px'],
                'value' => function ($models) {
                    $toReturn = "";
                    $uploaderInfo = Ant::uploaderInfo();
                    $path = "/{$models->config['dir']}/{$models->image_file}";
                    if (file_exists("../{$uploaderInfo['dir']}{$path}") && trim($models->image_file)!='') {
                        $toReturn .= Html::img(Url::to("{$uploaderInfo['domen']}{$path}"), [
                            'alt' => '',
                            'style' => 'width:100px;'
                        ]);
                    }else{
                        $toReturn .= '-';
                    }
                    return $toReturn;
                },
            ],
            [
                'attribute' => 'id',
                'options' => ['style'=>'width: 10px']
            ],
            [
                'attribute' => 'title_lng',
                'options' => ['style'=>'width: 170px'],
                'label' => 'Название'
            ],
            // 'long_title_lng',
            // 'beautiful_url:url',
            [
                'attribute' => 'notice_lng',
                'options' => ['style'=>'width: 470px'],
                'label' => 'Кратко'
            ],
            'price',
            'price_pv',
            // 'image_file',
            // 'tpl',
            // 'seo_title_lng',
            // 'seo_description_lng:ntext',
            // 'seo_keywords_lng',
            // 'show',
            // 'level',
            [
                'attribute' => '',
                'format' => 'raw',
                'options' => ['style'=>'width: 70px'],
                'value' => function ($model, $key, $index, $column) {
                    $modparent = ""; if (isset($_GET['parent'])) $modparent = "&modparent=".$_GET['parent'];
                    return \yii\helpers\Html::tag(
                        'a',
                        "Отзывы",
                        [
                            "class" => "label label-" . ('success'),
                            "href" => "/xblocks/reviewsblocks/?model=content_catalog&parent={$model->id}{$modparent}"
                        ]
                    );
                },
            ],
            [
                'format' => 'raw',
                'options' => ['style'=>'width: 100px'],
                'value' => function ($model) {
                    $modparent = "";
                    if (isset($_GET['modparent'])) $modparent = "&modparent=".$_GET['modparent'];
                    return \yii\helpers\Html::tag(
                        'a',
                        "Изображения",
                        [
                            'class' => 'btn btn-sm mybutton',
                            "href" => "/xblocks/iblocks/?model=iblocks&xmodel={$_GET['model']}&parent={$model->id}{$modparent}"
                        ]
                    );
                },
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
