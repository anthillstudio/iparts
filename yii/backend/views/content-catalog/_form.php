<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;
use vova07\imperavi\Widget;
use common\components\Ant;

/* @var $this yii\web\View */
/* @var $model common\models\ContentCatalog */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="content-catalog-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'parent')->hiddenInput(['value'=>$_GET['parent']])->label(false) ?>

    <?= $form->field($model, 'insert_date')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'update_date')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'user_id')->hiddenInput(['value'=>1])->label(false) ?>

    <?= $form->field($model, 'title_lng')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ref')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'price_pv')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'long_title_lng')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'beautiful_url')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'notice_lng')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'content_lng')->textarea(['rows' => 6])->widget(Widget::className(), [
        'settings' => [
            'lang' => 'ru',
            'minHeight' => 200,
            'plugins' => [
                'clips',
                'fullscreen'
            ],
            'imageUpload' => Url::to(['/content/image-upload'])
        ]
    ]);
    ?>

    <?= $form->field($model, 'seo_title_lng')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'seo_description_lng')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'seo_keywords_lng')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'new')->checkbox() ?>

    <?= $form->field($model, 'hit')->checkbox() ?>

    <?= $form->field($model, 'show')->checkbox() ?>

    <?= $form->field($model, 'level')->hiddenInput(['value'=>10])->label(false) ?>

    <?php
    if (!$model->isNewRecord){
        $uploadsPath = Ant::uploaderInfo();
        $path = "/{$model->config['dir']}/{$model->image_file}";
        if (file_exists("../{$uploadsPath['dir']}{$path}") && $model->image_file!='') {
            $img = \yii\helpers\Html::tag(
                "img",
                "Страницы",
                [
                    "style" => "width: 100px; margin-bottom: 10px",
                    "src" => "{$uploadsPath['domen']}{$path}"
                ]
            );
            echo $img;
        }
    }
    ?>
    <?= $form->field($model, 'image_file')->fileInput()->label('Основное изображение') ?>


    <?php
    if (!$model->isNewRecord){
        $uploadsPath = Ant::uploaderInfo();
        $path = "/{$model->config['dir']}/{$model->image_file_title}";
        if (file_exists("../{$uploadsPath['dir']}{$path}") && $model->image_file_title!='') {
            $img = \yii\helpers\Html::tag(
                "img",
                "Страницы",
                [
                    "style" => "width: 100px; margin-bottom: 10px",
                    "src" => "{$uploadsPath['domen']}{$path}"
                ]
            );
            echo $img;
        }
    }
    ?>
    <?= $form->field($model, 'image_file_title')->fileInput()->label('Изображение для шапки') ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
