<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ContactsPlacesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="contacts-places-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'city_id') ?>

    <?= $form->field($model, 'insert_date') ?>

    <?= $form->field($model, 'update_date') ?>

    <?= $form->field($model, 'user_id') ?>

    <?php // echo $form->field($model, 'title_lng') ?>

    <?php // echo $form->field($model, 'content_lng') ?>

    <?php // echo $form->field($model, 'addr') ?>

    <?php // echo $form->field($model, 'phone') ?>

    <?php // echo $form->field($model, 'time_work') ?>

    <?php // echo $form->field($model, 'cash_service') ?>

    <?php // echo $form->field($model, 'row_num') ?>

    <?php // echo $form->field($model, 'boss') ?>

    <?php // echo $form->field($model, 'boss_post') ?>

    <?php // echo $form->field($model, 'type') ?>

    <?php // echo $form->field($model, 'ext') ?>

    <?php // echo $form->field($model, 'img_url') ?>

    <?php // echo $form->field($model, 'show') ?>

    <?php // echo $form->field($model, 'level') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
