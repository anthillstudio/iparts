<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ContactsPlaces */

$this->title = 'Create Contacts Places';
$this->params['breadcrumbs'][] = ['label' => 'Contacts Places', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contacts-places-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
