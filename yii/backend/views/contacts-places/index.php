<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ContactsPlacesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Contacts Places';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contacts-places-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Contacts Places', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'city_id',
            'insert_date',
            'update_date',
            'user_id',
            // 'title_lng',
            // 'content_lng:ntext',
            // 'addr:ntext',
            // 'phone',
            // 'time_work:ntext',
            // 'cash_service:ntext',
            // 'row_num',
            // 'boss',
            // 'boss_post',
            // 'type',
            // 'ext',
            // 'img_url:url',
            // 'show',
            // 'level',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
