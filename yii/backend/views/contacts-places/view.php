<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ContactsPlaces */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Contacts Places', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contacts-places-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'city_id',
            'insert_date',
            'update_date',
            'user_id',
            'title_lng',
            'content_lng:ntext',
            'addr:ntext',
            'phone',
            'time_work:ntext',
            'cash_service:ntext',
            'row_num',
            'boss',
            'boss_post',
            'type',
            'ext',
            'img_url:url',
            'show',
            'level',
        ],
    ]) ?>

</div>
