<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;
use vova07\imperavi\Widget;
use common\components\Ant;

use common\models\ArticlesCatalogs;


$ant = new Ant();

?>

<div class="articles-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'date')->textInput()->widget(DateTimePicker::className(), [
        'name' => 'date',
        'convertFormat' => true,
        'pluginOptions' => [
            'format' => 'yyyy-MM-dd HH:i:s',
            'todayHighlight' => true
        ]
    ]);?>

    <?= $form->field($model, 'title_lng')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'beautiful_url')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'notice_lng')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'content_lng')->textarea(['rows' => 6])->widget(Widget::className(), [
        'settings' => [
            'lang' => 'ru',
            'minHeight' => 200,
            'plugins' => [
                'clips',
                'fullscreen'
            ],
            'imageUpload' => Url::to(['/content/image-upload'])
        ]
    ]);
    ?>

    <?php
    if (!$model->isNewRecord){

        $uploadsPath = Ant::uploaderInfo();
        $path = "/{$model->config['dir']}/{$model->image_file}";
        if (file_exists("../{$uploadsPath['dir']}{$path}") && $model->image_file!='') {
            $img = \yii\helpers\Html::tag(
                "img",
                "Страницы",
                [
                    "style" => "width: 100px; margin-bottom: 10px",
                    "src" => "{$uploadsPath['domen']}{$path}"
                ]
            );
            echo $img;
        }
    }
    ?>
    <?= $form->field($model, 'image_file')->fileInput()->label(false) ?>

    <?= $form->field($model, 'image_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'seo_title_lng')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'seo_description_lng')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'seo_keywords_lng')->textInput(['maxlength' => true]) ?>

    <?php
    $model->tags = ArticlesCatalogs::getActiveTags($model->id);
    echo $form->field($model, 'tags')->checkboxList(
        $model->taglist,
        ['separator' => '<br>']
    )->label(false);
    ?>

    <?= $form->field($model, 'level')->textInput() ?>

    <?= $form->field($model, 'show')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
