<?php

use yii\helpers\Html;
use yii\grid\GridView;

use yii\helpers\Url;

use common\components\Ant;
use common\modules\xblocks\controllers\XblocksController;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ArticlesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Блог';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="articles-index">

    <p>
        <?= Html::a('Добавить статью', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'options' => ['style'=>'white-space: normal'],
        'columns' => [
            [
                'attribute' => 'id',
                'options' => ['style'=>'width: 10px']
            ],
            [
                'attribute' => 'level',
                'options' => ['style'=>'width: 10px']
            ],
            [
                'attribute' => 'IMG',
                'format' => 'raw',
                'options' => ['style'=>'width: 70px'],
                'value' => function ($models) {
                    $toReturn = "";
                    $uploaderInfo = Ant::uploaderInfo();
                    $path = "/{$models->config['dir']}/{$models->image_file}";
                    if (file_exists("../{$uploaderInfo['dir']}{$path}") && trim($models->image_file)!='') {
                        $toReturn .= Html::img(Url::to("{$uploaderInfo['domen']}{$path}"), [
                            'alt' => '',
                            'style' => 'width:100px;'
                        ]);
                    }else{
                        $toReturn .= '-';
                    }
                    return $toReturn;
                },
            ],
            'date',
            'title_lng',
            [
                'format' => 'raw',
                'options' => ['style'=>'width: 10px'],
                'value' => function ($model) {
                    $modparent = ""; if (isset($_GET['parent'])) $modparent = "&modparent=".$_GET['parent'];
                    return \yii\helpers\Html::tag(
                        'a',
                        "Инфоблоки",
                        [
                            "class" => "label label-" . ('success'),
                            "href" => "/xblocks/iblocks/?model=articles&parent={$model->id}{$modparent}"
                        ]
                    );
                },
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
