<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Articles */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Articles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="articles-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'insert_date',
            'update_date',
            'date',
            'date_prev',
            'user_id',
            'seo_title_lng',
            'seo_description_lng:ntext',
            'seo_keywords_lng',
            'title_lng',
            'techTitle',
            'ref:ntext',
            'index',
            'video_id:ntext',
            'brand_id',
            'beautiful_url:url',
            'notice_lng:ntext',
            'content_lng:ntext',
            'source:ntext',
            'characteristics_table:ntext',
            'show_compare_block',
            'ext',
            'img_url:url',
            'img_title',
            'show',
            'popular',
            'mega',
            'tostart',
            'gallery_type',
            'isupdate',
            'popularity',
            'level',
        ],
    ]) ?>

</div>
