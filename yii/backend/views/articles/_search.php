<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ArticlesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="articles-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'insert_date') ?>

    <?= $form->field($model, 'update_date') ?>

    <?= $form->field($model, 'date') ?>

    <?= $form->field($model, 'date_prev') ?>

    <?php // echo $form->field($model, 'user_id') ?>

    <?php // echo $form->field($model, 'seo_title_lng') ?>

    <?php // echo $form->field($model, 'seo_description_lng') ?>

    <?php // echo $form->field($model, 'seo_keywords_lng') ?>

    <?php // echo $form->field($model, 'title_lng') ?>

    <?php // echo $form->field($model, 'techTitle') ?>

    <?php // echo $form->field($model, 'ref') ?>

    <?php // echo $form->field($model, 'index') ?>

    <?php // echo $form->field($model, 'video_id') ?>

    <?php // echo $form->field($model, 'brand_id') ?>

    <?php // echo $form->field($model, 'beautiful_url') ?>

    <?php // echo $form->field($model, 'notice_lng') ?>

    <?php // echo $form->field($model, 'content_lng') ?>

    <?php // echo $form->field($model, 'source') ?>

    <?php // echo $form->field($model, 'characteristics_table') ?>

    <?php // echo $form->field($model, 'show_compare_block') ?>

    <?php // echo $form->field($model, 'ext') ?>

    <?php // echo $form->field($model, 'img_url') ?>

    <?php // echo $form->field($model, 'img_title') ?>

    <?php // echo $form->field($model, 'show') ?>

    <?php // echo $form->field($model, 'popular') ?>

    <?php // echo $form->field($model, 'mega') ?>

    <?php // echo $form->field($model, 'tostart') ?>

    <?php // echo $form->field($model, 'gallery_type') ?>

    <?php // echo $form->field($model, 'isupdate') ?>

    <?php // echo $form->field($model, 'popularity') ?>

    <?php // echo $form->field($model, 'level') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
