<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Basket */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Baskets', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="basket-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'insert_date',
            'region',
            'user_id',
            'fname',
            'order:ntext',
            'summ',
            'phone',
            'pay_type',
            'code',
            'city',
            'region_type',
            'addr:ntext',
            'home',
            'porch',
            'stage',
            'corpus',
            'room',
            'user_message:ntext',
            'email:email',
            'user_ip',
            'use_in_om',
            'status',
            'comment:ntext',
            'block',
        ],
    ]) ?>

</div>
