<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\User;
use common\modules\basket\models\BasketElements;

/* @var $this yii\web\View */
/* @var $searchModel common\models\BasketSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Заказы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="basket-index">

    <p style="display: none">
        <?= Html::a('Добавить заказ', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'options' => ['style'=>'white-space: normal'],
        'columns' => [
            [
                'attribute' => 'id',
                'options' => ['style'=>'width: 10px']
            ],
            'insert_date',
            [
                'label' => 'Покупатель',
                'attribute' => 'user_id',
                'format' => 'raw',
                'options' => ['style'=>'width: 200px'],
                'value' => function ($model) {
                    $usr = User::findOne($model->user_id)->userInfo;
                    $toReturn = $usr->name. " (" . $usr->code . ")";
                    return $toReturn;
                },
            ],
            [
                'label' => 'Заказ',
                'attribute' => 'order',
                'format' => 'raw',
                'options' => ['style'=>'width: 400px'],
                'value' => function ($model) {
                    $basket = BasketElements::find()->with('catalogElement')->where(['order_id'=>$model->id])->all();
                    $toReturn = "";
                    foreach ($basket as $key=>$value){
                        foreach ($value->catalogElement as $k=>$v) {
                            $toReturn .= "
                            <div style='margin-bottom: 5px'>
                            {$v->title_lng}<br>
                            Количество: {$value->count}, цена: ({$value->price}Р/{$value->price_pv}PV)
                            </div>
                            ";
                        }
                    }
                    return $toReturn;
                },
            ],
            'fname',
            'phone',
            'email',
            'stock_id',
            'comment',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
