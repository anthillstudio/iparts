<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\PasswordResetRequestForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Восстановление пароля';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="body-content-wrapper fix">
    <h1><?= Html::encode($this->title) ?></h1>

    <br>
    <p>Пожалуйста, укажите свой адрес электронной почты. Вам будет отправлена ссылка для сброса пароля</p>
    <br>

    <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>

    <div style="max-width: 500px">
    <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
