<?php

namespace backend\controllers;

use Yii;
use common\components\Ant;
use common\models\Bnnrs;
use common\models\BnnrsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use yii\imagine\Image;
use yii\web\UploadedFile;

use common\components\LElement;
use yii\filters\AccessControl;

/**
 * BnnrsController implements the CRUD actions for Bnnrs model.
 */
class BnnrsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Bnnrs models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BnnrsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Bnnrs model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Bnnrs model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Bnnrs();

        if ($model->load(Yii::$app->request->post())) {

            $model->insert_date = date("Y-m-d H:i:00");
            $model->level = $model->getMaxLevel()+10;

            $imageFile = UploadedFile::getInstance($model, 'image_file');
            if (isset($imageFile)){
                $uploaderInfo = Ant::uploaderInfo();
                $model->image_file = 'EL'.Ant::buildFileName($model->id,$imageFile);
                if ($model->validate()) {
                    $path = "../{$uploaderInfo['dir']}/{$model->config['dir']}/";
                    $imageFile->saveAs("{$path}{$model->image_file}");
                    Image::thumbnail("{$path}{$model->image_file}", 400, 400)
                        ->save(Yii::getAlias("{$path}m{$model->image_file}"), ['quality' => 100]);
                }
            }

            $model->save();

            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Bnnrs model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $post = Yii::$app->request->post();
        if ($model->load(Yii::$app->request->post())) {


            $model->update_date = date("Y-m-d H:i:00");

            /*
             * Добавление / удаление
             * изображения
            */
            $uploaderInfo = Ant::uploaderInfo();
            $path = "../{$uploaderInfo['dir']}/{$model->config['dir']}/";
            if ($model->image_file == '') unset($model->image_file);
            if ($post['Bnnrs']['imageFileDelete']==1){
                unlink($path.$model['oldAttributes']['image_file']);
                unlink($path."m".$model['oldAttributes']['image_file']);
                $model->image_file = '';
                $model->save();
            }
            $imageFile = UploadedFile::getInstance($model, 'image_file');
            if (isset($imageFile)){
                $model->image_file = Ant::buildFileName($model->id,$imageFile);
                if ($model->validate()) {
                    $imageFile->saveAs("{$path}{$model->image_file}");
                    Image::thumbnail("{$path}{$model->image_file}", 400, 400)
                        ->save(Yii::getAlias("{$path}m{$model->image_file}"), ['quality' => 100]);
                }
            }

            $model->save();

            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Bnnrs model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Bnnrs model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Bnnrs the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Bnnrs::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionSort(){
        LElement::moveAfterIdRecord(new Bnnrs);
        return $this->redirect(["/bnnrs"]);
    }

}
