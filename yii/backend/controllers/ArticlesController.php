<?php

namespace backend\controllers;

use Yii;
use common\models\Articles;
use common\models\ArticlesSearch;
use common\models\ArticlesCatalogs;
use common\models\ArticlesWithArticlesCatalogs;
use common\components\Ant;
use yii\web\Controller;
use yii\web\NotFoundHttpException;


use yii\imagine\Image;
use yii\web\UploadedFile;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

/**
 * ArticlesController implements the CRUD actions for Articles model.
 */
class ArticlesController extends Controller
{


    public $articleObject;
    public $tagObject;

    public function init(){
        $this->articleObject = new Articles();
        $this->tagObject = new ArticlesCatalogs();
    }


    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Articles models.
     * @return mixed
     */
    public function actionIndex()
    {


        $searchModel = new ArticlesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Articles model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Articles model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Articles();

        $post = Yii::$app->request->post();

        if ($model->load($post)) {

            $imageFile = UploadedFile::getInstance($model, 'image_file');
            $ext = '.' . $imageFile->extension;

            $model->level = $model->getMaxLevel()+10;
            $model->save();

            $elementId = $model->getPrimaryKey();

            $imageFile = UploadedFile::getInstance($model, 'image_file');
            if (isset($imageFile)){
                $uploaderInfo = Ant::uploaderInfo();
                $model->image_file = 'EL'.Ant::buildFileName($model->id,$imageFile);
                if ($model->validate()) {
                    $path = "../{$uploaderInfo['dir']}/{$model->config['dir']}/";
                    $imageFile->saveAs("{$path}{$model->image_file}");
                    Image::thumbnail("{$path}{$model->image_file}", 400, 400)
                        ->save(Yii::getAlias("{$path}m{$model->image_file}"), ['quality' => 100]);
                }
            }

            Ant::beautyURL($model);

            ArticlesWithArticlesCatalogs::addTags($elementId,$post['Articles']['tags']);

            /*print_r("<pre>");
            print_r($model);
            print_r("</pre>");
            die;*/

            $model->save();

            return $this->redirect(['/articles']);

        } else {
            return $this->render('create', [
                'model' => $model
                //'contentObject' => $this->contentObject
            ]);
        }
    }

    /**
     * Updates an existing Articles model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {

        $model = $this->findModel($id);


        $post = Yii::$app->request->post();

        if ($model->load($post)) {

            //$post = ;
            //$tags = $post['Articles']['tags'];

            $model->update_date = date("Y-m-d H:i:00");

            if ($model->image_file == '') unset($model->image_file);

            $imageFile = UploadedFile::getInstance($model, 'image_file');
            if (isset($imageFile)){

                $uploaderInfo = Ant::uploaderInfo();
                $model->image_file = Ant::buildFileName($model->id,$imageFile);

                if ($model->validate()) {
                    $path = "../{$uploaderInfo['dir']}/{$model->config['dir']}/";
                    $imageFile->saveAs("{$path}{$model->image_file}");
                    Image::thumbnail("{$path}{$model->image_file}", 400, 400)
                        ->save(Yii::getAlias("{$path}m{$model->image_file}"), ['quality' => 100]);
                }

            }

            ArticlesWithArticlesCatalogs::addTags($id,$post['Articles']['tags']);

            Ant::beautyURL($model);

            $model->save();

            return $this->redirect(['/articles']);
        } else {

            /*print_r($model);
            print_r("");*/

            return $this->render('update', [
                'model' => $model,
                //'contentObject' => $this->contentObject
            ]);
        }
    }

    /**
     * Deletes an existing Articles model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Articles model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Articles the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Articles::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
