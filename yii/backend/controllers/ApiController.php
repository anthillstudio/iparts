<?php

namespace backend\controllers;

use common\external_api\managers\LoadManager;
use Yii;
use yii\web\Controller;
use common\models\User;
use common\components\Ant;
//use common\models\Catalog;
use common\external_api\access\TokenManager;
use common\external_api\mappers\MapperInterface;
use common\external_api\mappers\CatalogMapper;
use common\external_api\mappers\CurrencyMapper;
use common\external_api\mappers\PartnerMapper;
use common\external_api\mappers\OrderMapper;
use common\external_api\mappers\BalanceMapper;
use common\external_api\mappers\FinalOrderMapper;
use common\external_api\mappers\PriceMapper;
use common\external_api\mappers\RegionMapper;
use common\external_api\mappers\RegionProductMapper;
use common\external_api\mappers\StatementMapper;
use common\external_api\mappers\RegistrationnoticeMapper;
use common\external_api\mappers\ReturnsMapper;
use common\external_api\mappers\PurchaseMapper;

use common\external_api\services\ServiceInterface;
use common\external_api\services\CatalogService;
use common\external_api\services\CurrencyService;
use common\external_api\services\PartnerService;
use common\external_api\services\OrderService;
use common\external_api\services\BalanceService;
use common\external_api\services\FinalOrderService;
use common\external_api\services\PriceService;
use common\external_api\services\RegionProductService;
use common\external_api\services\RegionService;
use common\external_api\services\StatementService;
use common\external_api\services\RegistrationnoticeService;
use common\external_api\services\ReturnsService;
use common\external_api\services\PurchaseService;

/**
 * BnnrsController implements the CRUD actions for Bnnrs model.
 */
class ApiController extends Controller
{

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    /**
     * @inheritdoc
     */
    /*public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }*/

    /*public function actionImport()
    {
        Catalog::importcatalog();
    }*/
    /**
     * Lists all Bnnrs models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BnnrsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    protected function importData($filePrefix, MapperInterface $mapper, ServiceInterface $service)
    {
        if ($this->validateToken()) {
            $xml = $this->processFile($filePrefix);

            $manager = new LoadManager($mapper, $service);
            $manager->loadFromXml($xml);

            //Возвращаем ответ об успешной загрузке
            echo "success";
        }
        else {
            //Возвращаем ответ, что токен не валиден
            echo "error";
        }
    }

    public function actionImportCatalog()
    {
        $filePrefix = "catalog";
        $mapper = new CatalogMapper('product');
        $service = new CatalogService();

        $this->importData($filePrefix, $mapper, $service);
    }

    public function actionImportPartner()
    {
        $filePrefix = "partner";
        $mapper = new PartnerMapper('partner');
        $service = new PartnerService();

        $this->importData($filePrefix, $mapper, $service);
    }


    public function actionRegistrationNotice()
    {

        // Получаем идентификатор пользователя методом post
        // Меняем пароль и отправляем письмо

            $id = $_POST['id'];

            $user = User::findOne($id);

            $ant = new Ant;
            $emailfrom = $ant->emailfrom;        

            // Устанавливаем пароль
            $pwd = rand(10000,99999);
            $user->setPassword($pwd);
            $user->generateAuthKey();
            $user->save();
            // Отправляем письмо с паролем $pwd и емейлом $user->email

            $emails = $user->email;
            $theme = "iParts :: регистрация";
            $mssg = Yii::$app->getView()->render('@frontend/views/layouts/inc/_registration.php',[
                'name'=>$user->username,
                'pwd'=>$pwd,
                'email'=>$user->email
            ]);

            Yii::$app->mailer->compose()
                ->setFrom($emailfrom)
                ->setTo($emails)
                ->setSubject($theme)
                ->setHtmlBody($mssg)
                ->send();

            echo "Уведомление отправлено";

    }

    public function actionExportNewOrders()
    {
        if ($this->validateToken()) {
            $orderService = new OrderService();
            $newOrderList = $orderService->getNewOrders();

            $orderMapper = new OrderMapper();
            $xml = $orderMapper->asXml($newOrderList);
            echo $xml;
            //Возвращаем ответ об успешной загрузке
        }
        else {
            //Возвращаем ответ, что токен не валиден
            echo "error";
        }
    }

    public function actionImportOrders()
    {
        $filePrefix = "order";
        $mapper = new OrderMapper('order');
        $service = new OrderService();

        $this->importData($filePrefix, $mapper, $service);
    }

    public function actionImportCurrency()
    {
        $filePrefix = "currency";
        $mapper = new CurrencyMapper('currency_rate');
        $service = new CurrencyService();

        $this->importData($filePrefix, $mapper, $service);
    }

    public function actionImportRegion()
    {
        $filePrefix = "region";
        $mapper = new RegionMapper('region');
        $service = new RegionService();

        $this->importData($filePrefix, $mapper, $service);
    }

    public function actionImportRegionProduct()
    {
        $filePrefix = "region_product";
        $mapper = new RegionProductMapper('region');
        $service = new RegionProductService();

        $this->importData($filePrefix, $mapper, $service);
    }

    public function actionExportStatementRequests()
    {
        if ($this->validateToken()) {
            $service = new StatementService();
            $list = $service->getStatementRequests();

            $mapper = new StatementMapper();
            $xml = $mapper->asXml($list);
            echo $xml;
            //Возвращаем ответ об успешной загрузке
        }
        else {
            //Возвращаем ответ, что токен не валиден
            echo "error";
        }
    }

    public function actionImportStatement()
    {
        $filePrefix = "account_statement";
        $mapper = new StatementMapper('account_statement');
        $service = new StatementService();

        $this->importData($filePrefix, $mapper, $service);
    }





    public function actionImportUpdate()
    {

        //$fd = fopen("../yii/backend/uploads/external_api/pre_".date('YmdHis')."_{$res}.txt", 'w') or die("не удалось создать файл");
        //$str = "hi";
        //fwrite($fd, $str);
        //fclose($fd);

        if ($this->validateToken()) {
            $xml = $this->processFile('update');
            //$xml = file_get_contents("https://iparts.by/update_20181017192853.xml");
            //$xml = file_get_contents(__DIR__ . "/../uploads/external_api/update_to_test.xml");
            //$xml = "";

            // TODO: Список этих обработчиков должен быть явно не тут, но пока не придумал лучшего и более простого варианта
            $updates = array(
                'returns_upd' => new LoadManager(new ReturnsMapper(array('purchase_returns_upd', 'partner')), new ReturnsService()),
                'purchase_upd' => new LoadManager(new PurchaseMapper(array('purchase_upd', 'partner')), new PurchaseService()),
                'orders_upd' => new LoadManager(new FinalOrderMapper(array('orders_upd', 'order')), new FinalOrderService()),
                'partners_upd' => new LoadManager(new PartnerMapper(array('partners_upd', 'partner')), new PartnerService()),
                'partners' => new LoadManager(new PartnerMapper('partner'), new PartnerService()),
                'catalog_upd' => new LoadManager(new CatalogMapper(array('catalog_upd', 'product')), new CatalogService()),
                'balances_upd' => new LoadManager(new BalanceMapper(array('balances_upd', 'partner')), new BalanceService()),
                'prices_upd' => new LoadManager(new PriceMapper(array('prices_upd', 'product')), new PriceService()),
            );       

            foreach ($updates as $key => $value) {           
                $value->loadFromXml($xml);
            }

            //Возвращаем ответ об успешной загрузке
            echo $res = "success";
        }
        else {
            //Возвращаем ответ, что токен не валиден
            echo $res = "error";
        }

        //$fd = fopen("../yii/backend/uploads/external_api/hello_".date('YmdHis')."_{$res}.txt", 'w') or die("не удалось создать файл");
        //$str = "Привет мир";
        //fwrite($fd, $str);
        //fclose($fd);

    }

    protected function validateToken()
    {
        $isValidToken = false;

        //Получаем токен из реквеста
        $token = Yii::$app->request->get("param_from_get");

        $tokenManager = new TokenManager();

        if ($tokenManager->isValidToken($token)) {
            $isValidToken = true;
        }

        return $isValidToken;
    }

    protected function processFile($fileName)
    {
        // Получение файла
        $xml = file_get_contents('php://input');

        // Базовый путь к папке логов
        $baseDirPath = __DIR__ . "/../uploads/external_api/";

        // Папка для сохранения лога
        $fullDirName = $baseDirPath . $fileName;

        // Конечная папка для сохранения лога
        $destinationFolder = $baseDirPath;
        if (file_exists($fullDirName) || mkdir($fullDirName)) {
            $destinationFolder = $fullDirName . "/";
        }

        // Сохраняем файл на сервер для истории
        $destinationFileName = $fileName . "_" . date("YmdHis") . ".xml";
        $fullFileName = $destinationFolder . $destinationFileName;
        file_put_contents($fullFileName, $xml, FILE_APPEND);

        // Архивируем файл с логом
        $archiveName = $fileName . "_" . date("Ym") . "_archive.zip";
        $cmd = "cd $destinationFolder && zip -m {$archiveName} {$destinationFileName}";   //"m" for delete source file
        exec($cmd);

        return $xml;
    }

}
