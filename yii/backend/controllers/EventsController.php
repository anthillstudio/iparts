<?php

namespace backend\controllers;

use Yii;
use common\components\Ant;
use common\models\Events;
use common\models\EventsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use yii\imagine\Image;
use yii\web\UploadedFile;
use yii\filters\AccessControl;

/**
 * EventsController implements the CRUD actions for Events model.
 */
class EventsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Events models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EventsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Events model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Events model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Events();




        //$model->load(Yii::$app->request->post());


        if ($model->load(Yii::$app->request->post())) {



            Ant::beautyURL($model);

            $imageFile = UploadedFile::getInstance($model, 'image_file');
            $ext = '.' . $imageFile->extension;

            $model->save();

            //$elementId = $model->getPrimaryKey();


            /*
             * Добавление / удаление
             * изображения
            */
            $uploaderInfo = Ant::uploaderInfo();
            $path = "../{$uploaderInfo['dir']}/{$model->config['dir']}/";
            if ($model->image_file == '') unset($model->image_file);

            $imageFile = UploadedFile::getInstance($model, 'image_file');
            if (isset($imageFile)){
                $model->image_file = Ant::buildFileName($model->id,$imageFile);
                if ($model->validate()) {
                    $imageFile->saveAs("{$path}{$model->image_file}");
                    Image::thumbnail("{$path}{$model->image_file}", 600, 400)
                        ->save(Yii::getAlias("{$path}m{$model->image_file}"), ['quality' => 100]);
                }
            }


            /*
             * Добавление / удаление
             * изображения для заголовка
            */
            $uploaderInfo = Ant::uploaderInfo();
            $path = "../{$uploaderInfo['dir']}/{$model->config['dir']}/";

            $imageFileTitle = UploadedFile::getInstance($model, 'image_file_title');
            if (isset($imageFileTitle)){


                $model->image_file_title = 'T'.Ant::buildFileName($model->id,$imageFileTitle);
                if ($model->validate()) {


                    $imageFileTitle->saveAs("{$path}{$model->image_file_title}");

                    Image::thumbnail("{$path}{$model->image_file_title}", 600, 400)
                        ->save(Yii::getAlias("{$path}m{$model->image_file_title}"), ['quality' => 100]);

                }
            }

            $model->save();

            return $this->redirect(['/events']);

        } else {
            return $this->render('create', [
                'model' => $model
            ]);
        }
    }

    /**
     * Updates an existing Events model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {

        $model = $this->findModel($id);


        $post = Yii::$app->request->post();
        if ($model->load(Yii::$app->request->post())) {

            Ant::beautyURL($model);

            $model->update_date = date("Y-m-d H:i:00");
            if ($model->image_file == '') unset($model->image_file);



            /*
             * Добавление / удаление
             * изображения
            */
            $uploaderInfo = Ant::uploaderInfo();
            $path = "../{$uploaderInfo['dir']}/{$model->config['dir']}/";
            if ($model->image_file == '') unset($model->image_file);
            if ($post['Events']['imageFileDelete']==1){
                unlink($path.$model['oldAttributes']['image_file']);
                unlink($path.'m'.$model['oldAttributes']['image_file']);
                $model->image_file = '';
                $model->save();
            }
            $imageFile = UploadedFile::getInstance($model, 'image_file');
            if (isset($imageFile)){
                $model->image_file = Ant::buildFileName($model->id,$imageFile);
                if ($model->validate()) {
                    $imageFile->saveAs("{$path}{$model->image_file}");
                    Image::thumbnail("{$path}{$model->image_file}", 600, 400)
                        ->save(Yii::getAlias("{$path}m{$model->image_file}"), ['quality' => 100]);
                }
            }


            /*
             * Добавление / удаление
             * изображения для заголовка
            */
            $uploaderInfo = Ant::uploaderInfo();
            $path = "../{$uploaderInfo['dir']}/{$model->config['dir']}/";
            if ($model->image_file_title == '') unset($model->image_file_title);
            if ($post['Events']['imageFileTitleDelete']==1){
                unlink($path.''.$model['oldAttributes']['image_file_title']);
                unlink($path.'m'.$model['oldAttributes']['image_file_title']);
                $model->image_file_title = '';
                $model->save();
            }
            $imageFileTitle = UploadedFile::getInstance($model, 'image_file_title');
            if (isset($imageFileTitle)){

                $model->image_file_title = 'T'.Ant::buildFileName($model->id,$imageFileTitle);
                if ($model->validate()) {

                    $imageFileTitle->saveAs("{$path}{$model->image_file_title}");

                    Image::thumbnail("{$path}{$model->image_file_title}", 600, 400)
                        ->save(Yii::getAlias("{$path}m{$model->image_file_title}"), ['quality' => 100]);

                }
            }

            if ($model->save()) {
                return $this->redirect(['/events']);
            }

        }

        return $this->render('update', [
            'model' => $model
        ]);


    }

    /**
     * Deletes an existing Events model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Events model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Events the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Events::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
