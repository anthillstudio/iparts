<?php

namespace backend\controllers;

use Yii;
use common\models\ContentCatalog;
use common\components\Ant;
use common\models\ContentCatalogSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\components\LElement;

use common\models\Content;
use yii\imagine\Image;
use yii\web\UploadedFile;
use yii\filters\AccessControl;

/**
 * ContentCatalogController implements the CRUD actions for ContentCatalog model.
 */
class ContentCatalogController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionSort(){
        LElement::moveAfterIdRecord(new Content);
        return $this->redirect(["/content"]);
    }

    /**
     * Lists all ContentCatalog models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ContentCatalogSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ContentCatalog model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ContentCatalog model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {

        $model = new ContentCatalog();

        $model->load(Yii::$app->request->post());

        $parent = 0;
        if (isset($_GET['parent'])) $parent = $_GET['parent'];

        if ($model->load(Yii::$app->request->post())) {

            $model->level = $model->getMaxLevel()+10;
            $model->save();
            $elementId = $model->getPrimaryKey();

            $imageFile = UploadedFile::getInstance($model, 'image_file');
            if (isset($imageFile)){
                $uploaderInfo = Ant::uploaderInfo();
                $model->image_file = 'EL'.Ant::buildFileName($model->id,$imageFile);
                if ($model->validate()) {
                    $path = "../{$uploaderInfo['dir']}/{$model->config['dir']}/";
                    $imageFile->saveAs("{$path}{$model->image_file}");
                    Image::thumbnail("{$path}{$model->image_file}", 400, 400)
                        ->save(Yii::getAlias("{$path}m{$model->image_file}"), ['quality' => 100]);
                }
            }

            $imageFileCap = UploadedFile::getInstance($model, 'image_file_cap');
            if (isset($imageFileCap)){
                $uploaderInfo = Ant::uploaderInfo();
                $model->image_file_cap = 'ELCAP'.Ant::buildFileName($model->id,$imageFileCap);
                if ($model->validate()) {
                    $path = "../{$uploaderInfo['dir']}/{$model->config['dir']}/";
                    $imageFileCap->saveAs("{$path}{$model->image_file_cap}");
                }
            }

            $model->baseUrlPath = Content::buildBaseUrl($model->parent);
            Ant::beautyURL($model);

            $model->save();

            return $this->redirect(['/content-catalog', 'parent' => $model->parent]);

        } else {
            return $this->render('create', [
                'model' => $model,
                'parent' => $parent,
                //'contentObject' => $this->contentObject
            ]);
        }
    }

    /**
     * Updates an existing ContentCatalog model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);



        if ($model->load(Yii::$app->request->post())) {

            $model->update_date = date("Y-m-d H:i:00");
            if ($model->image_file == '') unset($model->image_file);

            $imageFile = UploadedFile::getInstance($model, 'image_file');
            if (isset($imageFile)){
                $uploaderInfo = Ant::uploaderInfo();
                $model->image_file = Ant::buildFileName($model->id,$imageFile);
                if ($model->validate()) {
                    $path = "../{$uploaderInfo['dir']}/{$model->config['dir']}/";
                    $imageFile->saveAs("{$path}{$model->image_file}");
                    Image::thumbnail("{$path}{$model->image_file}", 400, 400)
                        ->save(Yii::getAlias("{$path}m{$model->image_file}"), ['quality' => 100]);
                }
            }

            $imageFileCap = UploadedFile::getInstance($model, 'image_file_cap');

            if (isset($imageFileCap)){
                $uploaderInfo = Ant::uploaderInfo();
                $model->image_file_cap = Ant::buildFileName($model->id,$imageFileCap);
                if ($model->validate()) {
                    $path = "../{$uploaderInfo['dir']}/{$model->config['dir']}/";
                    $imageFileCap->saveAs("{$path}{$model->image_file_cap}");
                }
            }

            $model->baseUrlPath = Content::buildBaseUrl($model->parent);
            Ant::beautyURL($model);

            $model->save();

            return $this->redirect(['/content-catalog', 'parent' => $model->parent]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ContentCatalog model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $parent = $model->parent;

        $model->delete();

        return $this->redirect(['index?parent='.$parent]);
    }

    /**
     * Finds the ContentCatalog model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ContentCatalog the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ContentCatalog::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
