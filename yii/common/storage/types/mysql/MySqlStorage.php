<?php

namespace common\storage\types\mysql;

use Yii;
use common\storage\types\StorageInterface;

/**
 * This is the model class for table "{{%storage}}".
 *
 * @property int    $id
 * @property string $user_id
 * @property string $type
 * @property text   $data
 */

/* TODO: Добавить время жизни данных в хранилище */

class MySqlStorage extends \yii\db\ActiveRecord implements StorageInterface
{
    const BASKET_TYPE = '__BASKET__';

    public static function tableName()
    {
        return '{{%storage}}';
    }

    public function getBasket($userId)
    {
        $basket = self::find()->where([
            'user_id'   => $userId,
            'type'      => self::BASKET_TYPE
        ])->one();

        $basketString = '';
        if (isset($basket->data)) {
            $basketString = $basket->data;
        }

        return $basketString;
    }

    public function setBasket($userId, $basketString)
    {
        $basket = self::find()->where([
            'user_id'   => $userId,
            'type'      => self::BASKET_TYPE
        ])->one();

        if (is_null($basket)) {
            $basket = new MySqlStorage();
            $basket->user_id = $userId;
            $basket->type = self::BASKET_TYPE;
        }
        $basket->data = $basketString;
        $basket->save();
    }
}