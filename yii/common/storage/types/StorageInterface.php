<?php

namespace common\storage\types;

/**
 * Interface StorageInterface
 * @package common\modules\storage
 *
 * Идея в том, что хранилище содержит в одном классе все методы по работе с ним.
 * Сейчас это только содержимое корзины, но в будущем могут добавиться ещё данные.
 */

interface StorageInterface
{
    /**
     * @param $userId string Id пользователя, для которого надо подтянуть данных корзины
     * @return string
     */
    public function getBasket($userId);

    /**
     * @param $userId string Id пользователя, для которого надо сохранить данные корзины
     * @param $basketString string данные корзины
     */
    public function setBasket($userId, $basketString);
}