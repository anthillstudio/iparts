<?php

namespace common\storage;

use common\storage\types\mysql\MySqlStorage;

class StorageFactory
{
    /**
     * @return StorageInterface
     */
    public function getStorage()
    {
        return new MySqlStorage();
    }
}