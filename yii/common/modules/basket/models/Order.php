<?php

namespace common\modules\basket\models;

use Yii;

/**
 * This is the model class for table "{{%basket}}".
 *
 * @property integer $id
 * @property string $insert_date
 * @property string $region
 * @property integer $user_id
 * @property string $fname
 * @property string $order
 * @property string $summ
 * @property string $phone
 * @property integer $pay_type
 * @property string $code
 * @property string $city
 * @property integer $region_type
 * @property string $addr
 * @property integer $home
 * @property string $porch
 * @property string $stage
 * @property string $corpus
 * @property string $room
 * @property string $user_message
 * @property string $email
 * @property string $user_ip
 * @property integer $use_in_om
 * @property integer $status
 * @property string $comment
 * @property integer $block
 */
class Order extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%basket}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['insert_date', 'region', 'order', 'summ', 'phone', 'code', 'city', 'region_type', 'addr', 'home', 'porch', 'stage', 'corpus', 'room', 'email'], 'required'],
            [['insert_date'], 'safe'],
            [['user_id', 'pay_type', 'region_type', 'home', 'use_in_om', 'status', 'block'], 'integer'],
            [['order', 'addr', 'user_message', 'comment'], 'string'],
            [['region', 'code', 'porch', 'stage', 'user_ip'], 'string', 'max' => 50],
            [['fname', 'summ', 'phone', 'city', 'corpus', 'email'], 'string', 'max' => 255],
            [['room'], 'string', 'max' => 11],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'insert_date' => 'Insert Date',
            'region' => 'Region',
            'user_id' => 'User ID',
            'fname' => 'Fname',
            'order' => 'Order',
            'summ' => 'Summ',
            'phone' => 'Phone',
            'pay_type' => 'Pay Type',
            'code' => 'Code',
            'city' => 'City',
            'region_type' => 'Region Type',
            'addr' => 'Addr',
            'home' => 'Home',
            'porch' => 'Porch',
            'stage' => 'Stage',
            'corpus' => 'Corpus',
            'room' => 'Room',
            'user_message' => 'User Message',
            'email' => 'Email',
            'user_ip' => 'User Ip',
            'use_in_om' => 'Use In Om',
            'status' => 'Status',
            'comment' => 'Comment',
            'block' => 'Block',
        ];
    }
}
