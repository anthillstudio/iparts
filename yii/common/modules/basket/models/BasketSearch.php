<?php

namespace common\modules\basket\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\basket\models\Basket;
use common\models\UserInfo;

/**
 * BasketSearch represents the model behind the search form about `common\models\Basket`.
 */
class BasketSearch extends Basket
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id'], 'integer'],
            [['insert_date', 'fname', 'phone', 'addr', 'email', 'comment'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {

        $query = Basket::find()->orderBy(['insert_date'=>SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'insert_date' => $this->insert_date,
            'user_id' => $this->user_id,
        ]);

        $query->andFilterWhere(['like', 'fname', $this->fname])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'comment', $this->comment]);

        return $dataProvider;
    }

}
