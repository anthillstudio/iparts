<?php

namespace common\modules\basket\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\basket\models\Order;

/**
 * BasketSearch represents the model behind the search form about `app\models\Basket`.
 */
class OrderSearch extends Order
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'pay_type', 'region_type', 'home', 'use_in_om', 'status', 'block'], 'integer'],
            [['insert_date', 'region', 'fname', 'order', 'summ', 'phone', 'code', 'city', 'addr', 'porch', 'stage', 'corpus', 'room', 'user_message', 'email', 'user_ip', 'comment'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Order::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'insert_date' => $this->insert_date,
            'user_id' => $this->user_id,
            'pay_type' => $this->pay_type,
            'region_type' => $this->region_type,
            'home' => $this->home,
            'use_in_om' => $this->use_in_om,
            'status' => $this->status,
            'block' => $this->block,
        ]);

        $query->andFilterWhere(['like', 'region', $this->region])
            ->andFilterWhere(['like', 'fname', $this->fname])
            ->andFilterWhere(['like', 'order', $this->order])
            ->andFilterWhere(['like', 'summ', $this->summ])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'code', $this->code])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'addr', $this->addr])
            ->andFilterWhere(['like', 'porch', $this->porch])
            ->andFilterWhere(['like', 'stage', $this->stage])
            ->andFilterWhere(['like', 'corpus', $this->corpus])
            ->andFilterWhere(['like', 'room', $this->room])
            ->andFilterWhere(['like', 'user_message', $this->user_message])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'user_ip', $this->user_ip])
            ->andFilterWhere(['like', 'comment', $this->comment]);

        return $dataProvider;
    }
}
