<?php

namespace common\modules\basket\models;

use Yii;
use common\models\ContentCatalog;

class BasketElements extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%basket_elements}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id','catalog_element_id'], 'required'],
            [['order_id','count'], 'integer'],
            [['price','catalog_element_id'], 'string', 'max' => 255]
            //[['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Basket::className(), 'targetAttribute' => ['order_id' => 'id']],
            //[['catalog_element_id'], 'exist', 'skipOnError' => true, 'targetClass' => ContentCatalog::className(), 'targetAttribute' => ['catalog_element_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Номер заказа',
            'catalog_element_id' => 'Номер товара',
            'price' => 'Цена',
            'count' => 'Количество',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Basket::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCatalogElement()
    {
        return $this->hasOne(ContentCatalog::className(), ['id' => 'catalog_element_id']);
    }

}
