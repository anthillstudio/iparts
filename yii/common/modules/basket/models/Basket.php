<?php

namespace common\modules\basket\models;


use common\storage\StorageFactory;
use Yii;
use common\components\Ant;
use common\models\CatalogCats;
use common\modules\basket\models\BasketElements;
use common\components\Db;
use common\models\User;

class Basket extends \yii\db\ActiveRecord
{

    const EXPORTED_STATUS_NO = 0; // Необходимо передать в 1С для получения ID заказа
    const EXPORTED_STATUS_YES = 1; // ID заказа из 1С получено
    const EXPORTED_STATUS_IN_PROCESS = 2; // Находится на обработке в 1С

    public $order;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%basket}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['insert_date'], 'required'],
            [['insert_date'], 'safe'],
            [['comment'], 'string'],
            [['phone', 'user_id', 'email'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'insert_date' => 'Дата добавления',
            'user_id' => 'Покупатель',
            'phone' => 'Телефон',
            'email' => 'E-mail',
            'comment' => 'Комментарий'
        ];
    }

    public function basketPlus($el){
        $session = Yii::$app->session;
        if (!$session->isActive) {
            $session->open();
        }
        $basket = $_SESSION['basket'];
        $basket[$el]['count']++;
        $_SESSION['basket'] = $basket;

        self::saveToStorage($_SESSION['basket']);
    }

    public function basketMinus($el){
        $session = Yii::$app->session;
        if (!$session->isActive) {
            $session->open();
        }
        $basket = $_SESSION['basket'];
        $basket[$el]['count']--;
        if ($basket[$el]['count']==0) unset($basket[$el]);
        $_SESSION['basket'] = $basket;

        self::saveToStorage($_SESSION['basket']);
    }

    public static function getElements(){

        $session = Yii::$app->session;
        if (!$session->isActive) {
            $session->open();
        }
        $elements = [];


        if (!empty($session['basket']) && count($session['basket'])!=0){
            $elsstr = "";
            foreach ($session['basket'] as $item){
                $elsstr .= "'{$item['id']}',";
            }
            $elsstr = substr($elsstr,0,-1);

            if (trim($elsstr)!=''){
                $query = new \yii\db\Query;
                $elements = $query
                    ->select('cc.*, c.id as id, c.ref as ref, c.price1 as price1, c.price2 as price2, c.beautiful_url as c_url, c.title as title')
                    ->from('{{%catalog}} cc')
                    ->leftJoin('{{%catalog_elements}} c', 'c.parent = cc.id')
                    ->where("cc.show = 1 AND c.id IN ({$elsstr})")
                    ->all();

                $newElements = [];
                foreach ($elements as $key=>$value){
                    $newElements[$value['id']] = $value;
                    $newElements[$value['id']]['cnt'] = $session['basket'][$value['id']]['count'];
                }
                $elements = $newElements;
                //print_r($elements);

            }

        }

        return $elements;
    }



    public static function getBasketSumm(){

        $elements = Basket::getElements();

        $summ = 0;
        foreach($elements as $key=>$value){
            $summ += $value['price']*$value['cnt'];
        }
        $summ = number_format($summ[0], 2, '.','');
        return $summ;

    }

    public function buildOrderInfo(){

    }

    public function addBasketElements($id)
    {
        $session = Yii::$app->session;
        if (!$session->isActive) {
            $session->open();
        }
        $toReturn = "";

        foreach ($_SESSION['basket'] as $key=>$value){
            $basketElements = new BasketElements;
            $basketElements->order_id = $id;
            $basketElements->catalog_element_id = $value['id'];
            $basketElements->count = $value['count'];
            $basketElements->price = $value['price'];
            $basketElements->save();
        }

        return $toReturn;
    }

    public static function buildBasketBox(){
        $session = Yii::$app->session;
        if (!$session->isActive) {
            $session->open();
        }
        $sum = 0.00;

        if (isset($_SESSION['basket'])) {
            foreach ($_SESSION['basket'] as $key => $value) {
                $sum += $value['price'] * $value['count'];
            }
        }

        if (isset($_SESSION['curr'])){        
            if ($_SESSION['curr']!='ue'){
                $sum = $sum*Yii::$app->params['settings']['usd_currency'];
            }
        }
                
        return $sum;
    }

    public static function buildElement($el){
        $toReturn = "<tr id='good-item{$el['id']}' class='FNTC good-item'>";
            $toReturn .= "<td valign='top'>";
                $toReturn .= "<div><b>{$el['title']}</b></div>";
                $toReturn .= "<div>Артикул: {$el['ref']}</div>";
                $toReturn .= "<div>{$el['notice']}</div>";
            $toReturn .= "</td>";
            $toReturn .= "<td valign='top' class='item-price'>{$el['price']}</td>";
            $itemCount = $_SESSION['basket'][$el['id']]['count'];
            $itemSumm = $_SESSION['basket'][$el['id']]['count']*$_SESSION['basket'][$el['id']]['price'];

            //$toReturn .= "<td valign='top' class='good-tbl-cap-count'><input class='basket-item-count' id='basket-item-count{$el['id']}' maxlength='3' value='{$itemCount}' type='text'></td>";
            $toReturn .= "<td valign='top' class='good-tbl-cap-count'>{$itemCount}</td>";
            $toReturn .= "<td valign='top' class='item-summ'>{$itemSumm}</td>";
            $toReturn .= "<td valign='top'><div class='basket-item-del'><a href='/basket/orders/deletefrombasket?el={$el['id']}')>X</a></div></td>";
        $toReturn .= "</tr>";
        return $toReturn;
    }

    public static function addToBasket($el,$count,$price,$curr){

        $session = Yii::$app->session;
        if (!$session->isActive) {
            $session->open();
        }

        $basket = [];
        if (isset($_SESSION['basket']))
        $basket = $_SESSION['basket'];

        if (isset($basket[$el])){
            $basket[$el]['count'] = $count;
        }else{
            $basket[$el] = [
                'id'=>$el,
                'count'=>$count,
                'price'=>$price
            ];
        }
        $_SESSION['basket'] = $basket;
        self::saveToStorage($_SESSION['basket']);
        echo $basket[$el]['count'];
    }


    public static function deleteElFromBasket($el){
        $session = Yii::$app->session;
        if (!$session->isActive) {
            $session->open();
        }
        $basket = $_SESSION['basket'];
        unset($basket[$el]);
        $_SESSION['basket'] = $basket;
        self::saveToStorage($_SESSION['basket']);
    }

    public static function deleteFromBasket($el){
        $session = Yii::$app->session;
        if (!$session->isActive) {
            $session->open();
        }
        unset($_SESSION['basket'][$el]);
        self::saveToStorage($_SESSION['basket']);
    }

    public function getBasketElements()
    {
        return $this->hasMany(BasketElements::className(), ['order_id' => 'id']);
    }

    public static function addOrder($post){
        $model = new Basket();

        $model->insert_date = date("Y-m-d H:i:00");
        $model->phone = $post['phone'];
        $model->email = $post['mail'];
        $model->user_id = Yii::$app->user->id;
        $model->comment = $post['mssg'];

        $model->save();

        $id = Yii::$app->db->lastInsertID;

        Basket::addBasketElements($id);
    }



    public function actionIndex()
    {
        $model = new Basket();

        $model->insert_date = date("Y-m-d H:i:00");
        $model->phone = $_POST['phone'];
        $model->fname = $_POST['name'];
        $model->email = $_POST['email'];
        $model->stock_id = $_POST['stockid'];
        $model->user_id = $_POST['userid'];
        $model->comment = $_POST['comment'];

        $model->save();
        $id = Yii::$app->db->lastInsertID;

        Basket::addBasketElements($id);
    }


    public function order($post)
    {
        /******************************
         * Отправляем
         ******************************/

        // Администратору всегда отправляем письмо
        Basket::sendOrderMail($post);

        // Пользователю только если стоит галочка в настройках
        if (isset(Yii::$app->user->id)) {
            $user = User::findIdentity(Yii::$app->user->id);
            if (!empty($user) && $user->isSendEmail()) {
                Basket::sendOrderMailToClient($post);
            }
        }

        /******************************
         * Пишем в БД
         ******************************/
        Basket::addOrder($post);
        unset($_SESSION['basket']);
        self::clearStorage();
    }

    public static function sendOrderMail($post){

        $ant = new Ant();
        $emailfrom = $ant->emailfrom;
        $emails = $ant->emails['messages'][1];
        $theme = "iParts :: Заказ Для администратора";
        $mssg = Yii::$app->getView()->render('@frontend/views/layouts/inc/_mail_order.php',[
            'name'=>$post['name'],
            'mail'=>$post['mail'],
            'mssg'=>$post['mssg'],
            'phone'=>$post['phone'],
        ]);
        Yii::$app->mailer->compose()
        ->setFrom($emailfrom)
        ->setTo($emails)
        ->setSubject($theme)
        ->setHtmlBody($mssg)
        ->send();
    }

    public static function sendOrderMailToClient($post){

        $ant = new Ant();
        $emailfrom = $ant->emailfrom;
        $theme = "iParts :: Заказ";
        $mssg = Yii::$app->getView()->render('@frontend/views/layouts/inc/_mail_order_to_client.php',[
            'name'=>$post['name'],
            'mail'=>$post['mail'],
            'mssg'=>$post['mssg'],
            'phone'=>$post['phone'],
        ]);

        Yii::$app->mailer->compose()
            ->setFrom($emailfrom)
            ->setTo([$post['mail']])
            //->setTo(['shablouski@gmail.com'])
            ->setSubject($theme)
            ->setHtmlBody($mssg)
            ->send();
    }

    /**
     * Выгружаем все заказы, которые помечены как не выгруженные в 1С
     * @return mixed
     */
    public static function getNewOrders()
    {
        $orders = self::findAll(['is_exported' => self::EXPORTED_STATUS_NO]);
        return $orders;
    }

    /**
     * Помечаем заказ как находящиеся на обработке в 1С
     * @return mixed
     */
    public function markAsInProcess()
    {
        $this->is_exported = self::EXPORTED_STATUS_IN_PROCESS;
        $this->save();
    }

    /**
     * Помечаем заказ как выгруженный в 1С
     * @return mixed
     */
    public function markAsExported()
    {
        $this->is_exported = self::EXPORTED_STATUS_YES;
        $this->save();
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    // Не хотел делать статическую функцию, но приложение очень активно их использует, поэтому проще
    // сделать статическую чем переписать большую часть приложения
    public static function saveToStorage($basketArray)
    {
        if (isset(Yii::$app->user->id)) {
            $basketString = '';
            if (!empty($basketArray)) {
                $basketString = serialize($basketArray);
            }

            $storageFactory = new StorageFactory();
            $storage = $storageFactory->getStorage();
            $storage->setBasket(Yii::$app->user->id, $basketString);
        }
    }

    // Не хотел делать статическую функцию, но приложение очень активно их использует, поэтому проще
    // сделать статическую чем переписать большую часть приложения
    public static function loadFromStorage()
    {
        if (isset(Yii::$app->user->id)) {
            $userId = Yii::$app->user->id;
            $session = Yii::$app->session;
            if (!$session->isActive) {
                $session->open();
            }

            if (!isset($_SESSION["userForLoadedBasketFromStorage"]) || ($_SESSION["userForLoadedBasketFromStorage"] != $userId)) {
                $storageFactory = new StorageFactory();
                $storage = $storageFactory->getStorage();
                $basketString = $storage->getBasket($userId);
                if (!empty($basketString)) {
                    $_SESSION["basket"] = unserialize($basketString);
                }
            }
        }
    }

    /**
     * Очищаем хранилище
     *
     * Не хотел делать статическую функцию, но приложение очень активно их использует, поэтому проще
     * сделать статическую чем переписать большую часть приложения
     */
    public static function clearStorage()
    {
        // Записываем пустой массив для очистки хранилища
        self::saveToStorage(array());
    }

}
