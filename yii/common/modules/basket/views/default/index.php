<?php
use Yii;
use common\components\Ant;
use common\models\Content;
use common\modules\basket\models\Basket;

$currencyTypeHeader = '(usd)';
if (isset($_SESSION['curr'])){
    if ($_SESSION['curr']!='ue'){
        $currencyTypeHeader = '(byn)';
    }
}

?>

<?= \common\widgets\apanel_info\ApanelInfoWidget::widget() ?>



<script src="/js/goods-list.js?v=1524623393"></script>




<div class="fix cat-title-box"><h1>Корзина</h1></div>
<br><br>


<div class="goods-list bascket-page">
    <div class="goods-list-box">


<div class="fix">
<?php if (count($elements)!=0){ ?>
    <table class="good-tbl">
        <tbody><tr class="good-tbl-cap">
            <td></td>
            <td></td>
            <td>Товар</td>
            <td class="good-tbl-cap-summ">Цена <?=$currencyTypeHeader?></td>
            <td class="good-tbl-cap-count">Кол-во</td>
            <td class="good-tbl-cap-add">Сумма <?=$currencyTypeHeader?></td>
            <td class="good-tbl-cap-del"></td>
        </tr>
        <?php
        $msumm = 0;
        if (!empty($_SESSION['basket']) && count($_SESSION['basket'])!=0) {
            foreach ($elements as $item) {
                $summ = 0;
                $price = (!Yii::$app->user->isGuest?$item['price1']:$item['price2']);
                $summ += $_SESSION['basket'][$item['id']]['count'] * $price;

                $txt.= "<tr class='good-tbl-tr' id='good-item{$item['id']}'>";
                $txt.= "<td class='good-info-i-box' valign='top'>";
                $txt.= "<div class='good-info-i' id='good-info-i{$item['id']}'></div>";
                $txt.= "</td>";
                $txt.= "<td class='good-img'>";
                $txt.= "<img src='/img/icons/noimg.png'>";
                $txt.= "</td><!-- good-img -->";
                $txt.= "<td class='good-title class-yes'>";
                $txt.= "<span title='Контроллер заряда для Xiaomi BQ24296M'><a href=\"javascript:showDetals('{$item['id']}')\">{$item['title']}</a></span><br>";
                $txt.= "<i># {$item['ref']}</i>";
                $txt.= "</td>";

                $priceShow = $price;
                $summShow = $summ;
                if (isset($_SESSION['curr'])){
                    if ($_SESSION['curr']!='ue'){                                        
                        $priceShow = $price*Yii::$app->params['settings']['usd_currency'];
                        $summShow = $summ*Yii::$app->params['settings']['usd_currency'];                        
                    }
                }

                $txt.= "<td class='item-price class-yes'>{$priceShow}</td>";
                $txt.= "<td class='good-tbl-cap-count'><input class='item-count' id='item-count{$item['id']}' maxlength='3' value='{$_SESSION['basket'][$item['id']]['count']}' type='text'></td>";

                

                $txt.= "<td class='item-summ class-yes'>{$summShow}</td>";
                //$txt.= "<td class='good-tbl-cap-add'><a href=\"javascript:addToBasket('{$item['id']}','list')\"><img src='/img/icons/basket.svg'></a></td>";
                $txt.= "<td class='good-tbl-cap-delete'><a href='/basket/orders/deletefrombasket?el={$item['id']}'><img src='/img/icons/delete.svg'></a></td>";
                $txt.= "</tr>";
                $msumm += $summ;
            }
        }
        echo $txt;
        ?>
        </tbody>
    </table>
<?php } ?>
</div>


<?php
/*print_r("<pre>");
print_r($elements);
print_r("</pre>");*/

//unset($_SESSION['basket']);

/*print_r("<pre>");
print_r($_SESSION['basket']);
print_r("</pre>");*/



if (count($elements)!=0){


    $msummShow = $msumm;
    $currencyType = ' USD';
    if (isset($_SESSION['curr'])){
        if ($_SESSION['curr']!='ue'){
            $msummShow = $msumm*Yii::$app->params['settings']['usd_currency'];
            $currencyType = ' BYN';
        }
    }


    $toReturn = "";
    $toReturn .= "<div class='basket-main-summ-info FNTC'>";
    $toReturn .= "<div>Сумма: <span class='basket-main-summ-price'>{$msummShow}</span>";
    $toReturn .= $currencyType;
    $toReturn .= "</div>";
    $toReturn .= "</div>";

    $toReturn .= "<div style='text-align: center'>";


    $toReturn .= "<div class='form-items FNTC'>";
    $toReturn .= "<form class='el-obr' name='el-obr' id='el-obr'>";

    $toReturn .= "<div class='box-mssg' id='box-mssg'></div>";

//print_r(Yii::$app->user->identity);

    if(Yii::$app->user->isGuest) {

        $toReturn .= "<input type='hidden' name='isGuest' id='isGuest' value='1'></span>";

        $toReturn .= "<div class='form-item'>";
        $toReturn .= "<span class='form-item-podpis'>ФИО * :</span>";
        $toReturn .= "<span class='form-item-input'><input type='text' name='name' id='name' value=''></span>";
        $toReturn .= "</div><!-- form-item -->";

        $toReturn .= "<div class='form-item'>";
        $toReturn .= "<span class='form-item-podpis'>Город * :</span>";
        $toReturn .= "<span class='form-item-input'><input type='text' name='city' id='city' value=''></span>";
        $toReturn .= "</div><!-- form-item -->";

        $toReturn .= "<div class='form-item'>";
        $toReturn .= "<span class='form-item-podpis'>Адрес электронной почты * :</span>";
        $toReturn .= "<span class='form-item-input'><input type='text' name='mail' id='mail' value=''></span>";
        $toReturn .= "</div><!-- form-item -->";

        $toReturn .= "<div class='form-item'>";
        $toReturn .= "<span class='form-item-podpis'>Контактный телефон * :</span>";
        $toReturn .= "<span class='form-item-input'><input type='text' name='phone' id='phone' value=''></span>";
        $toReturn .= "</div><!-- form-item -->";

    }else{

        $toReturn .= "<input type='hidden' name='name' id='name' value=''>";
        $toReturn .= "<input type='hidden' name='city' id='city' value=''>";
        $toReturn .= "<input type='hidden' name='mail' id='mail' value='".Yii::$app->user->getIdentity()->email."'>";
        $toReturn .= "<input type='hidden' name='isGuest' id='isGuest' value='0'>";

        $toReturn .= "<div class='form-item'>";
        $toReturn .= "<span class='form-item-podpis'>Контактный телефон * :</span>";
        $toReturn .= "<span class='form-item-input'><input type='text' name='phone' id='phone' value=''></span>";
        $toReturn .= "</div><!-- form-item -->";

    }

    $toReturn .= "<div class='form-item'>";
    $toReturn .= "<span class='form-item-podpis'>Комментарий * :</span>";
    $toReturn .= "<span class='form-item-textarea'><textarea name='mssg' id='mssg' value=''></textarea></span>";
    $toReturn .= "</div><!-- form-item -->";

    $toReturn .= "<div class='form-bttn'><a href='javascript:order()'>Оформить заказ</a></div>";

    $toReturn .= "</form>";
    $toReturn .= "</div>";

    $toReturn .= "</div>";
    echo $toReturn;

    echo "</div>";

}else{
    echo "<div style='text-align: center; font-size: 12pt; padding: 50px' class='FNTC'>Ваша корзина пуста</div>";
}

?>
</div>
<br><br><br>