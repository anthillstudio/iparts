<?php
use common\components\Ant;
use common\models\Content;
use common\modules\basket\models\Basket;

$page = $contentObject->page;
$orientation = $contentObject->orientation;
$config = [
    'title' => Корзина,
    'notice' => $page['notice_lng'],
    'style' => $page['title_style'],
    'img' => "/{$contentObject->config['dir']}/{$page['image_file_title']}"
];
$path = Content::getPathMenuElements($contentObject->page->id);

$path =  [
    1 => [
        'url' => 'basket/res?type=2',
        'title' => 'Оформление заказа',
    ],
    2 => [
        'url' => 'basket',
        'title' => 'Корзина',
    ],
    3 => [
        'url' => '/',
        'title' => 'iParts.by',
    ],
];
?>

<?= \common\widgets\apanel_info\ApanelInfoWidget::widget() ?>

    <div class="fix cat-title-box">
        <h1><?=$config['title']?></h1>
    </div><!-- cat-title-box -->
<?= Yii::$app->getView()->render('@frontend/views/inc/menu/_path.php', ['config'=>$path]); ?>
    </div><!-- content-title-box-wrapper -->

<?php

Basket::getElements();

?>

<?php if (!empty($_SESSION['basket']) && (count($_SESSION['basket'])!=0)){ ?>


<?php
    /*print_r("<pre>");
    print_r(Yii::$app->params['userinfo']);
    print_r("</pre>");
    die();*/
?>

    <div class="form-items FNTC">

        <form class="el-obr" name="el-obr" id="el-obr">

            <div class="box-mssg" id="box-mssg"></div>

            <div class="form-item">
                <span class="form-item-podpis">Фамилия, имя, отчество: * :</span>
                <span class="form-item-input"><input type="text" name="name" id="name" value=""></span>
            </div><!-- form-item -->

            <div class="form-item">
                <span class="form-item-podpis">Адрес электронной почты * :</span>
                <span class="form-item-input"><input type="text" name="mail" id="mail" value=""></span>
            </div><!-- form-item -->

            <div class="form-item">
                <span class="form-item-podpis">Сообщение * :</span>
                <span class="form-item-textarea"><textarea name="mssg" id="mssg" value=""></textarea></span>
            </div><!-- form-item -->

            <div class="form-bttn"><a href="javascript:addFeedbackMessage()">Отправить</a></div>

        </form>

    </div>

<?php }else{ ?>
    <div style="padding: 20px; font-size: 11pt; text-align: center">Корзина пуста</div>
<?php } ?>