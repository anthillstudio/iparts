<?php

namespace common\modules\basket\controllers;

use Yii;
use common\components\Ant;
use common\modules\basket\models\Order;
use common\modules\basket\models\OrderSearch;
use common\modules\basket\models\Basket;
use common\modules\basket\models\BasketElements;
use common\models\Settings;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\Place;

/**
 * BasketController implements the CRUD actions for Basket model.
 */
class OrdersController extends Controller
{
    /**
     * @inheritdoc
     */

    public $layout = '@app/views/layouts/default.php';
    public $elements;
    public $enableCsrfValidation;

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    function init(){
        $this->elements = Basket::getElements();
        Yii::$app->params['settings'] = Settings::getSettings();
    }

    /**
     * Lists all Basket models.
     * @return mixed
     */
    public function actionIndex()
    {

        

        if (count($_POST)!=0) {

            $ant = new Ant();
            $model = new Basket();

            $model->insert_date = date("Y-m-d H:i:00");
            $model->phone = $_POST['phone'];
            $model->fname = $_POST['name'];
            $model->email = $_POST['email'];
            $model->stock_id = $_POST['stockid'];
            $model->user_id = $_POST['userid'];
            $model->comment = $_POST['comment'];

            $model->save();
            $id = Yii::$app->db->lastInsertID;

            Basket::addBasketElements($id);
            Basket::buildOrderInfo();



            $esupport = $ant->emails['support'];
            $emailfrom = $ant->emailfrom;
            $eadmin = $ant->emails['admin'];



            $boxtypetxt = "Доставка";
            if ($_POST['boxtype']==2) $boxtypetxt = "Самовывоз";
            $theme = "Заказ № {$id}" . ", " . $model->fname . " (". $boxtypetxt .")";

            $mssg = "";
            $mssg .= "<style>";
            $mssg .= ".mail-mssg-order{font: 9pt/12pt arial}";
            $mssg .= ".mail-mssg-order td{padding: 0px}";
            $mssg .= ".mail-mssg-order li{display: block}";
            $mssg .= ".mail-mssg-order i{display: block; margin-bottom: 20px; font-size: 8pt}";
            $mssg .= "</style>";


            $mssg .= "<table style='width: 100%;'>";
            $mssg .= "<tr>";
            $mssg .= "<td style='text-align: center; padding: 20px;'>";
            $mssg .= "<table class='mail-mssg-order' style='padding: 20px; width: 600px; border: 2px solid #333;'>";
            $mssg .= "<tr>";
            $mssg .= "<td style='padding: 20px'>";
            $mssg .= "{$theme}<br>";
            $mssg .= "---<br>";
            $mssg .= "<div><b>{$model->fname}</b></div>";
            $mssg .= "---<br>";
            $mssg .= "<div><i>{$model->insert_date}</i></div>";
            $mssg .= "<div><b>Телефон: </b> {$model->phone}</div>";
            $mssg .= "<div><b>E-mail:</b> {$model->email}</div>";

            //$mssg .= "<div><b>Склад:</b> {$model->place}</div>";

            $mssg .= "</td>";
            $mssg .= "</tr>";
            $mssg .= "<tr><td style='padding: 20px'><b>Заказ:</b> {$model->order}</td></tr>";
            $mssg .= "<tr><td style='padding: 20px'><b>Комментарий:</b> {$model->comment}</td></tr>";
            $mssg .= "</table>";
            $mssg .= "</td>";
            $mssg .= "</tr>";
            $mssg .= "</table>";

            /*Yii::$app->mailer->compose()
                ->setFrom($emailfrom)
                ->setTo($eadmin)
                ->setSubject($theme)
                ->setHtmlBody($mssg)
                ->send();*/

            unset($_SESSION['basket']);

        }else{
            return $this->goHome();
        }



    }




    /**
     * Displays a single Basket model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {

        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Basket model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Order();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Basket model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Basket model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {

        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionDeletefrombasket($el){
        Basket::deleteElFromBasket($el);
        return $this->redirect('/basket');
    }

    public function actionBasket(){


        //$this->enableCsrfValidation = false; //отключаем проверку Csrf

        $toReturn = "";

        if (isset($_GET['action'])) {
            switch ($_GET['action']) {
                case "order":
                    Basket::order($_GET);
                    break;
                case "add":
                    Basket::addToBasket($_GET['el'], $_GET['count'], $_GET['price'], $_GET['curr']);
                    break;
                case "del":
                    Basket::deleteFromBasket($_GET['el']);
                    break;
                case "plus":
                    Basket::basketPlus($_GET['el']);
                    break;
                case "minus":
                    Basket::basketMinus($_GET['el']);
                    break;
                case "buildres":
                    $toReturn = Basket::buildResElement();
                    break;
                case "buildBasketBox":
                    $toReturn = Basket::buildBasketBox();
                    break;
            }
        }
        return $toReturn;
    }



    /**
     * Finds the Basket model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Basket the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Order::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }





}
