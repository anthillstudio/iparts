<?php
namespace common\modules\basket\controllers;

use Yii;
use yii\web\Controller;
use common\modules\basket\models\Basket;
use common\models\User;
use common\models\Settings;

/**
 * Default controller for the `basket` module
 */
class DefaultController extends Controller
{

    //public $layout = '@app/views/layouts/default-dark.php';
    public $elements;

    function init(){
        parent::init();
        Yii::$app->params['settings'] = Settings::getSettings();
        $this->elements = Basket::getElements();
    }

    public function actionIndex()
    {
        // Закрыл корзину от неавторизованных пользователей
        if (Yii::$app->user->isGuest) { return $this->redirect(['/site/login']); die; }

        return $this->render('index',[
            'elements'=>$this->elements,
        ]);
    }

    public function actionRes()
    {

        Yii::$app->params['userinfo'] = User::userInfo(Yii::$app->user->id);

        return $this->render('res',[
            'elements'=>$this->elements
        ]);
    }



}
