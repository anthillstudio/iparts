<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\apanel\models\CurrencyPosition */

$this->title = 'Update Currency Position: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Currency Positions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="currency-position-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
