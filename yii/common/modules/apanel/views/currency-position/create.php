<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\modules\apanel\models\CurrencyPosition */

$this->title = 'Create Currency Position';
$this->params['breadcrumbs'][] = ['label' => 'Currency Positions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="currency-position-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
