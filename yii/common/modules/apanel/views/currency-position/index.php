<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\apanel\models\CurrencyPositionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Currency Positions';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="currency-position-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Currency Position', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title_lng',
            'notice_lng:ntext',
            'type',
            'level',
            // 'show',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
