<?php
use common\components\Ant;
use yii\widgets\Breadcrumbs;
use common\modules\basket\models\Basket;
use common\models\CatalogCats;

$session = Yii::$app->session;
if (!$session->isActive) {
    $session->open();
}

$this->params['breadcrumbs'][] = ['label' => 'Новости', 'url' => ["/news"]];

//unset($_SESSION['basket']);
?>

<div class="content-title-box-wrapper shop-page-title-box image-file-use1">
    <div class="content-title-box">
        <div class="content-title-box-info">
            <div>
                <h1 class="FNTC">Личный кабинет</h1>
            </div>
        </div>
        <div class="title-box-dec"></div>
    </div><!-- content-title-box -->
</div><!-- content-title-box-wrapper -->

<div class="fix content-navigation-h FNTC">

    <div class="content-menu-wrapper">
        <div class="content-menu">
            <ul class="content-menu-level1 content-menu-parent331">
                <li class="content-menu-item "><a href="/profile">Анкета</a></li>
                <li class="content-menu-item "><a href="/profile/business">Бизнес</a></li>
            </ul>
        </div>
    </div>


    <style>
        .xxxul{text-align: center}
        .xxxul ul{padding-left: 30px;}
        .xxxul li{display: block; text-align: left}
        .xxxul > li{display: inline-block; vertical-align: top; margin-right: 30px}

        .ddxul{display: block; border-bottom: 1px solid #ededed; margin-top: 14px}
        .ddxul li{display: block; border-top: 1px solid #ededed; padding: 4px 0px; font-size: 11pt}
    </style>

    <div style="width: 500px; box-sizing: border-box; padding: 30px; border: 2px double #333; margin: 0px auto; margin-bottom: 30px">
        <h2 style="text-align: center">Директор 3 уровня</h2>
        <ul class="ddxul">
            <li>Оборот: 60040 PW</li>
            <li>Партнеров (1LV): 4</li>
            <li>Партнеров (2LV): 18</li>
            <li>Партнеров (3LV): 28</li>
            <li>Партнеров (4LV): 158</li>
        </ul>
    </div>

    <center><h2>Дерево партнеров</h2></center><br>
    <ul class="xxxul">
        <li>
            <b>Иванов (400PW)</b>
            <ul>
                <li>
                    Иванов (30PW)

                </li>
                <li>
                    Петров (800PW)

                </li>
                <li>
                    Сидаров (130PW)

                </li>
                <li>
                    Лобанов (250PW)
                    <ul>
                        <li>
                            <span style="color: red">Иванов (0PW)</span>

                        </li>
                        <li>
                            Петров

                        </li>
                        <li>
                            Сидаров (30PW)
                            <ul>
                                <li>
                                    Иванов (30PW)

                                </li>
                                <li>
                                    Петров (30PW)

                                </li>
                                <li>
                                    Сидаров (30PW)


                                </li>
                                <li>
                                    Лобанов (30PW)

                                </li>
                            </ul>
                        </li>
                        <li>
                            Лобанов (30PW)

                        </li>
                    </ul>
                </li>
            </ul>
        </li>
        <li>
            <b>Петров (760PW)</b>
            <ul>
                <li>
                    Иванов (30PW)

                </li>
            </ul>
        </li>
        <li>
            <b>Сидаров (30PW)</b>
            <ul>
                <li>
                    Иванов (30PW)

                </li>
                <li>
                    <span style="color: red">Петров (0PW)</span>

                </li>
            </ul>
        </li>
        <li>
            <b>Лобанов (70PW)</b>
            <ul>
                <li>
                    Иванов (30PW)

                </li>
                <li>
                    Петров (30PW)

                </li>
                <li>
                    <span style="color: red">Сидаров (0PW)</span>

                </li>
                <li>
                    Лобанов (30PW)

                </li>
            </ul>
        </li>
    </ul>


    <br><br><br>


</div>