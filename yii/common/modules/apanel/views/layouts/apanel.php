<?php

use frontend\assets\AppAsset;
use common\widgets\Alert;
use common\models\Settings;
use common\widgets\Slider\SliderWidget;
use common\widgets\mainMenu\MainMenuWidget;
use common\widgets\apanel_info\ApanelInfoWidget;
use common\widgets\apanel_menu\ApanelMenuWidget;


$settings = Settings::getSettings();
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<?php echo $this->render('@frontend/views/layouts/inc/_header.php'); ?>
<?php $this->beginBody() ?>

<?php
$idcapbox = "cap-box-def";
$capbox = "cap-box-show";
?>



    <div id="<?=$idcapbox;?>" class="cap-box <?=$capbox;?>">
        <?= MainMenuWidget::widget() ?>
    </div><!-- capBox -->

    <div class="apanel-content">

        <br><br><br><br><br><br><br><br>

        <div class="fix content-navigation-h">

            <div class="content-menu-wrapper FNTC">
                <div class="content-menu">
                    <ul class="content-menu-level1 content-menu-parent331">
                        <li class="content-menu-item "><a href="/apanel">Синхронизация пользователей</a></li>
                        <li class="content-menu-item "><a href="/apanel">Синхронизация товаров</a></li>
                        <li class="content-menu-item "><a href="/apanel">Настройки</a></li>
                    </ul>
                </div>
            </div>

            <?= Alert::widget() ?>
            <?= $content ?>
        </div><!-- content-navigation-h -->
    </div><!-- apanel-content -->

<?php echo $this->render('@frontend/views/layouts/inc/_footer.php'); ?>

<?php $this->endBody() ?>
<?php $this->endPage() ?>