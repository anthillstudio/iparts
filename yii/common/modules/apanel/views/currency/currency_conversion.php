<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use common\components\Ant;
//use vova07\imperavi\Widget;
//use common\modules\xblocks\models\Xfiles;
//$filesObject = new Xfiles();

use common\modules\apanel\models\CurrencyExchangeLoad;
$currency = CurrencyExchangeLoad::find()
    ->where(['id_place'=>2])
    ->orderBy(['id'=>SORT_DESC])
    ->all();

$config = [
    'title' => 'Курсы конверсий',
    'style' => 3,
];
?>
<?= Yii::$app->getView()->render('@frontend/views/inc/menu/_title_box.php', ['config'=>$config]); ?>
<?php echo $this->render('_menu.php'); ?>

<div class="currency-uploader-wrapper">
    <div class="currency-uploader-box FNTC">

        <?php $model = new CurrencyExchangeLoad; ?>
        <?php $form = ActiveForm::begin(['action' => ['/apanel/currency/loadfile'], 'options' => ['enctype' => 'multipart/form-data']]); ?>
        <?php $model->action = $_GET['action']; ?>
        <?= $form->field($model, 'action')->hiddenInput(['maxlength' => true])->label(false) ?>
        <?= $form->field($model, 'file')->fileInput()->label('Загрузить курсы отделений из xls-файла') ?>
        <div class="form-group"><?= Html::submitButton('Добавить', ['class' => 'currency-load-file-bttn']) ?></div>
        <?php ActiveForm::end(); ?>

    </div><!-- currency-uploader-box -->
</div><!-- currency-uploader-wrapper -->


<a class="currency-uploader-items-bttn-add" href="/apanel/currency/<?=$_GET['action']?>/adduploader">+ Добавить курсы</a>
<div class="currency-uploader-items FNTC">
    <?php foreach ($currency as $key=>$value){ ?>
        <div class="currency-uploader-item" id="currency-uploader-item<?=$value->id?>">
            <div class="currency-uploader-item-title">
                <h2>Загрузка №<?=$value->id?> от <?=$value->date_load?></h2>
                <div class="currency-uploader-item-bttns"><a href="javascript:showCurrencyDetals('<?=$value->id?>')">Подробнее</a> <a href="javascript:deleteCurrencyAllUpload('<?=$value->id?>')">Удалить</a></div>
            </div><!-- currency-uploader-item-title -->
            <div class="currency-uploader-detals-items"></div>
        </div>
    <?php } ?>
</div>