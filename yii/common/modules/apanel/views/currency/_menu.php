<script>

    function changeDateLoad(upload){
        var date_load = $("#date_load"+upload).val();
        showbigpreloader();
        $.get("/apanel/currency/<?=$_GET['action']?>/changedate",{
                action : '<?=$_GET['action']?>',
                upload : upload,
                date_load : date_load
            },
            function(data) {
                hidebigpreloader();
                $("#date_load"+upload).val(date_load);
            }
        );
    }

    function deleteCurrencyAllUpload(upload){
        if (confirm('Вы действительно жедаете удалить запись '+upload)){
            showbigpreloader();
            $.get("/apanel/currency/<?=$_GET['action']?>/deleteupload",{
                    action : '<?=$_GET['action']?>',
                    upload : upload
                },
                function(data) {
                    hidebigpreloader();
                    $('#currency-uploader-item'+upload).css('display','none');
                }
            );
        }
    }

    function deleteCurrencyUpload(upload,place){
        if (confirm('Вы действительно жедаете удалить запись '+upload)){
            showbigpreloader();
            $.get("/apanel/currency/<?=$_GET['action']?>/delete",{
                    action : '<?=$_GET['action']?>',
                    upload : upload,
                    place : place
                },
                function(data) {
                    hidebigpreloader();
                    showCurrencyDetals(upload);
                }
            );
        }
    }



    function showCurrencyDetals(id) {
        showbigpreloader();
        $.get("/apanel/currency/<?=$_GET['action']?>/detals", {
                action: '<?=$_GET['action']?>',
                id: id
            },
            function (data) {
                hidebigpreloader();
                $('#currency-uploader-item' + id + ' .currency-uploader-detals-items').html(data);
            }
        );
    }
    
    function saveCardsCurrency(id) {
        var b_card_curr41 = $(".currency_cards #b_card_curr41").val();
        var s_card_curr41 = $(".currency_cards #s_card_curr41").val();
        var b_card_curr42 = $(".currency_cards #b_card_curr42").val();
        var s_card_curr42 = $(".currency_cards #s_card_curr42").val();
        if (
            b_card_curr41 == '' ||
            b_card_curr42 == '' ||
            s_card_curr41 == '' ||
            s_card_curr42 == ''
        ){
            alert('Заполните все поля');
        }else {
            showbigpreloader();
            $.get("/apanel/currency/<?=$_GET['action']?>/addcards", {
                    action: '<?=$_GET['action']?>',
                    s_card_curr41: s_card_curr41,
                    s_card_curr42: s_card_curr42,
                    b_card_curr41: b_card_curr41,
                    b_card_curr42: b_card_curr42,
                    add_place: 36,
                    upload: id
                },
                function (data) {
                    hidebigpreloader();
                    showCurrencyDetals(id);
                }
            );

        }
    }


    function saveNBRBCurrency(id) {
        var b_nbrb_curr51 = $("#b_nbrb_curr51").val();
        var b_nbrb_curr52 = $("#b_nbrb_curr52").val();
        var b_nbrb_curr53 = $("#b_nbrb_curr53").val();
        if (
            b_nbrb_curr51 == '' ||
            b_nbrb_curr52 == '' ||
            b_nbrb_curr53 == ''
        ){
            alert('Заполните все поля');
        }else {
            showbigpreloader();
            $.get("/apanel/currency/<?=$_GET['action']?>/addnbrb", {
                    action: '<?=$_GET['action']?>',
                    b_nbrb_curr51: b_nbrb_curr51,
                    b_nbrb_curr52: b_nbrb_curr52,
                    b_nbrb_curr53: b_nbrb_curr53,
                    add_place: 0,
                    upload: id
                },
                function (data) {
                    hidebigpreloader();
                    showCurrencyDetals(id);
                }
            );
        }
    }


    function saveBCSECurrency(id) {
        var b_bcse_curr61 = $("#b_bcse_curr61").val();
        var b_bcse_curr62 = $("#b_bcse_curr62").val();
        var b_bcse_curr63 = $("#b_bcse_curr63").val();
        if (
            b_bcse_curr61 == '' ||
            b_bcse_curr62 == '' ||
            b_bcse_curr63 == ''
        ){
            alert('Заполните все поля');
        }else {
            showbigpreloader();
            $.get("/apanel/currency/<?=$_GET['action']?>/addbcse", {
                    action: '<?=$_GET['action']?>',
                    b_bcse_curr61: b_bcse_curr61,
                    b_bcse_curr62: b_bcse_curr62,
                    b_bcse_curr63: b_bcse_curr63,
                    add_place: 0,
                    upload: id
                },
                function (data) {
                    hidebigpreloader();
                    showCurrencyDetals(id);
                }
            );
        }
    }


    function addConversionCurrency(id){
        var b_curr21 = $(".addCurrency #b_curr21").val();
        var s_curr21 = $(".addCurrency #s_curr21").val();
        var b_curr22 = $(".addCurrency #b_curr22").val();
        var s_curr22 = $(".addCurrency #s_curr22").val();
        var b_curr23 = $(".addCurrency #b_curr23").val();
        var s_curr23 = $(".addCurrency #s_curr23").val();
        var b_curr24 = $(".addCurrency #b_curr24").val();
        var s_curr24 = $(".addCurrency #s_curr24").val();
        var add_place = $(".addCurrency #add_place").val();
        if (
            b_curr21 == '' ||
            s_curr21 == '' ||
            b_curr22 == '' ||
            s_curr22 == '' ||
            b_curr23 == '' ||
            s_curr23 == '' ||
            b_curr24 == '' ||
            s_curr24 == ''
        ){
            alert('Заполните все поля');
        }else {
            showbigpreloader();
            $.get("/apanel/currency/<?=$_GET['action']?>/add", {
                    action: '<?=$_GET['action']?>',
                    b_curr21: b_curr21,
                    s_curr21: s_curr21,
                    b_curr22: b_curr22,
                    s_curr22: s_curr22,
                    b_curr23: b_curr23,
                    s_curr23: s_curr23,
                    b_curr24: b_curr24,
                    s_curr24: s_curr24,
                    add_place: add_place,
                    upload: id
                },
                function (data) {
                    hidebigpreloader();
                    showCurrencyDetals(id);
                }
            );
        }
    }


    function addBeznalCurrency(id){
        var b_curr31 = $(".addCurrency #b_curr31").val();
        var s_curr31 = $(".addCurrency #s_curr31").val();
        var b_curr32 = $(".addCurrency #b_curr32").val();
        var s_curr32 = $(".addCurrency #s_curr32").val();
        var b_curr33 = $(".addCurrency #b_curr33").val();
        var s_curr33 = $(".addCurrency #s_curr33").val();
        var add_place = $(".addCurrency #add_place").val();
        if (
            b_curr31 == '' ||
            s_curr31 == '' ||
            b_curr32 == '' ||
            s_curr32 == '' ||
            b_curr33 == '' ||
            s_curr33 == ''
        ){
            alert('Заполните все поля');
        }else {
            showbigpreloader();
            $.get("/apanel/currency/<?=$_GET['action']?>/add", {
                    action: '<?=$_GET['action']?>',
                    b_curr31: b_curr31,
                    s_curr31: s_curr31,
                    b_curr32: b_curr32,
                    s_curr32: s_curr32,
                    b_curr33: b_curr33,
                    s_curr33: s_curr33,
                    add_place: add_place,
                    upload: id
                },
                function (data) {
                    hidebigpreloader();
                    showCurrencyDetals(id);
                }
            );
        }
    }

    function addCurrency(id){
        var b_curr11 = $(".addCurrency #b_curr11").val();
        var s_curr11 = $(".addCurrency #s_curr11").val();
        var b_curr12 = $(".addCurrency #b_curr12").val();
        var s_curr12 = $(".addCurrency #s_curr12").val();
        var b_curr13 = $(".addCurrency #b_curr13").val();
        var s_curr13 = $(".addCurrency #s_curr13").val();
        var add_place = $(".addCurrency #add_place").val();
        if (
            b_curr11 == '' ||
            s_curr11 == '' ||
            b_curr12 == '' ||
            s_curr12 == '' ||
            b_curr13 == '' ||
            s_curr13 == ''
        ){
            alert('Заполните все поля');
        }else {
            showbigpreloader();
            $.get("/apanel/currency/<?=$_GET['action']?>/add", {
                    action: '<?=$_GET['action']?>',
                    b_curr11: b_curr11,
                    s_curr11: s_curr11,
                    b_curr12: b_curr12,
                    s_curr12: s_curr12,
                    b_curr13: b_curr13,
                    s_curr13: s_curr13,
                    add_place: add_place,
                    upload: id
                },
                function (data) {
                    hidebigpreloader();
                    showCurrencyDetals(id);
                }
            );
        }
    }
</script>

<div class="content-menu">
<ul class="content-menu-level1 content-menu-parent33">
    <li class="content-menu-item"><a class="FNTC" href="/apanel/currency/currency-disposal">Курсы валют</a></li>
    <li class="content-menu-item"><a class="FNTC" href="/apanel/currency/currency-disposal-beznal">Курсы по безналичным расчётам</a></li>
    <li class="content-menu-item"><a class="FNTC" href="/apanel/currency/currency-conversion">Курсы конверсий</a></li>
    <li class="content-menu-item"><a class="FNTC" href="/apanel/currency/currency-nbrb">Курсы НБРБ</a></li>
    <li class="content-menu-item"><a class="FNTC" href="/apanel/currency/currency-bcse">Курсы БВФБ</a></li>
</ul>
</div>