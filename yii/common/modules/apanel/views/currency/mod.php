<?php
use common\modules\apanel\models\CurrencyExchange;
?>
<div class="currency-box" id="currency-disposal">
<?php $elements = CurrencyExchange::getCurrency('disposal'); ?>
<h2>Курсы валют всех отделений (<?=$elements['date']?>)</h2>
    <table>
        <tr class="currency-tbl-cap">
            <td>Отделение</td>
            <td colspan="2">USD/BYN</td>
            <td colspan="2">EUR/BYN</td>
            <td colspan="2">100RUB/BYN</td>
        </tr>
        <?php foreach ($elements['elements'] as $key=>$value){ ?>
        <tr>
            <td><?=$value['title']?></td>
            <?php foreach ($value['curr'] as $k=>$v){ ?>
                <?php if (!in_array($k,[41,42])){ ?>
                <td><?=$v['buy']?></td>
                <td><?=$v['sale']?></td>
                <?php } ?>
            <?php } ?>
        </tr>
        <?php } ?>
    </table>

    <table>
        <tr>
            <td colspan="4"><b>Курсы по платёжным картам</b></td>
        </tr>
        <tr class="currency-tbl-cap">
            <td colspan="2">USD</td>
            <td colspan="2">EUR</td>
        </tr>
        <?php foreach ($elements['elements'] as $key=>$value){ ?>
            <tr>
                <?php foreach ($value['curr'] as $k=>$v){ ?>
                    <?php if (in_array($k,[41,42])){ ?>
                        <td><?=$v['buy']?></td>
                        <td><?=$v['sale']?></td>
                    <?php } ?>
                <?php } ?>
            </tr>
        <?php } ?>
    </table>
</div>


<div class="currency-box" id="currency-conversion">
    <?php $elements = CurrencyExchange::getCurrency('conversion'); ?>
    <h2>Курсы конверсий (<?=$elements['date']?>)</h2>
    <table>
        <tr class="currency-tbl-cap">
            <td>Отделение</td>
            <td colspan="1">USD/RUB</td>
            <td colspan="1">RUB/USD</td>
            <td colspan="1">USD/EUR</td>
            <td colspan="1">EUR/USD</td>
        </tr>
        <?php foreach ($elements['elements'] as $key=>$value){ ?>
            <tr>
                <td><?=$value['title']?></td>
                <?php foreach ($value['curr'] as $k=>$v){ ?>
                    <td><?=$v['buy']?></td>
                <?php } ?>
            </tr>
        <?php } ?>
    </table>
</div>

<div class="currency-box" id="currency-disposal-beznal">
    <?php $elements = CurrencyExchange::getCurrency('beznal'); ?>
    <h2>Курсы по безналичным расчётам (<?=$elements['date']?>)</h2>
    <table>
        <tr class="currency-tbl-cap">
            <td>Отделение</td>
            <td colspan="2">USD/BYN</td>
            <td colspan="2">EUR/BYN</td>
            <td colspan="2">100RUB/BYN</td>
        </tr>
        <?php foreach ($elements['elements'] as $key=>$value){ ?>
            <tr>
                <td><?=$value['title']?></td>
                <?php foreach ($value['curr'] as $k=>$v){ ?>
                    <td><?=$v['buy']?></td>
                    <td><?=$v['sale']?></td>
                <?php } ?>
            </tr>
        <?php } ?>
    </table>
</div>

<div class="currency-box" id="currency-nbrb">
    <?php $elements = CurrencyExchange::getCurrency('nbrb'); ?>
    <h2>Курсы НБРБ (<?=$elements['date']?>)</h2>
    <table>
        <tr class="currency-tbl-cap">
            <td>USD/BYN</td>
            <td>EUR/BYN</td>
            <td>100RUB/BYN</td>
        </tr>
        <?php foreach ($elements['elements'] as $key=>$value){ ?>
            <tr>

                <?php foreach ($value['curr'] as $k=>$v){ ?>
                    <td><?=$v['buy']?></td>
                <?php } ?>
            </tr>
        <?php } ?>
    </table>
</div>

<div class="currency-box" id="currency-bcse">
    <?php $elements = CurrencyExchange::getCurrency('bcse'); ?>
    <h2>Курсы БВФБ (<?=$elements['date']?>)</h2>
    <table>
        <tr class="currency-tbl-cap">
            <td>USD/BYN</td>
            <td>EUR/BYN</td>
            <td>100RUB/BYN</td>
        </tr>
        <?php foreach ($elements['elements'] as $key=>$value){ ?>
            <tr>

                <?php foreach ($value['curr'] as $k=>$v){ ?>
                    <td><?=$v['buy']?></td>
                <?php } ?>
            </tr>
        <?php } ?>
    </table>
</div>