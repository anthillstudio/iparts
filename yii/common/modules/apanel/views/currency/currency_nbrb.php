<?php

use common\modules\apanel\models\CurrencyExchangeLoad;
$currency = CurrencyExchangeLoad::find()
    ->where(['id_place'=>5])
    ->orderBy(['id'=>SORT_DESC])
    ->all();

$config = [
    'title' => 'Курсы НБРБ',
    'style' => 3,
];
?>
<?= Yii::$app->getView()->render('@frontend/views/inc/menu/_title_box.php', ['config'=>$config]); ?>
<?php echo $this->render('_menu.php'); ?>

<div class="currency-uploader-wrapper" style="display: none">
    <div class="currency-uploader-box FNTC">
        <form>
            <h2>Загрузить курсы отделений из xls-файла</h2>
            <input type="file" id="currency-uploader" name="currency-uploader">
        </form>
    </div><!-- currency-uploader-box -->
</div><!-- currency-uploader-wrapper -->


<a class="currency-uploader-items-bttn-add" href="/apanel/currency/<?=$_GET['action']?>/adduploader">+ Добавить курсы</a>
<div class="currency-uploader-items FNTC">
    <?php foreach ($currency as $key=>$value){ ?>
        <div class="currency-uploader-item" id="currency-uploader-item<?=$value->id?>">
            <div class="currency-uploader-item-title">
                <h2>Загрузка №<?=$value->id?> от <?=$value->date_load?></h2>
                <div class="currency-uploader-item-bttns"><a href="javascript:showCurrencyDetals('<?=$value->id?>')">Подробнее</a> <a href="javascript:deleteCurrencyAllUpload('<?=$value->id?>')">Удалить</a></div>
            </div><!-- currency-uploader-item-title -->
            <div class="currency-uploader-detals-items"></div>
        </div>
    <?php } ?>
</div>