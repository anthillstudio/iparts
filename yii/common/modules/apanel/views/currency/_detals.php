<div class="currency-uploader-date"><input type="text" id="date_load<?=$uploadInfo->id?>" value="<?=$uploadInfo->date_load?>"><a href="javascript:changeDateLoad(<?=$uploadInfo->id?>)">Изменить</a></div>
<?php
$placesuse = [];
?>
<?php if (count($elements)!=0){ ?>
    <table>
        <tr>
            <td colspan="5">Головной офис и другие отделения банка</td>
        </tr>
        <tr>
            <td>ЦБУ</td>
            <td>USD/BYN</td>
            <td>EUR/BYN</td>
            <td>100RUB/BYN</td>
            <td colspan="2"></td>
        </tr>
    <?php foreach ($elements as $key=>$value){ ?>
        <tr>
            <td class="currency-uploader-detals-title"><?=$value['title']?></td>
            <?php for($i=11;$i<=13;$i++){ ?>
                <?php $placesuse[$value['curr'][$i]['id_place']] = $value['curr'][$i]['id_place'] ?>
                <td class="currency-uploader-detals-curr">
                    <div>B <input value="<?=$value['curr'][$i]['buy']?>"></div>
                    <div>S <input value="<?=$value['curr'][$i]['sale']?>"></div>
                </td>
            <?php } ?>
            <td style="display: none" class="currency-uploader-detals-bttn"><a href="">Изменить</a></td>
            <td class="currency-uploader-detals-bttn"><a href="javascript:deleteCurrencyUpload(<?=$upload?>,<?=$value['curr'][11]['id_place']?>)">Удалить</a></td>
        </tr>
    <?php } ?>
    </table>
<?php } ?>


<?php if (count($placesuse) < count($places)){ ?>
<table class="addCurrency">
    <tr>
        <td>ЦБУ</td>
        <td>USD/BYN</td>
        <td>EUR/BYN</td>
        <td>100RUB/BYN</td>
        <td colspan="2"></td>
    </tr>
        <tr>
            <td class="currency-uploader-detals-title">
                <select name="add_place" id="add_place">
                    <?php foreach ($places as $k=>$v){ ?>
                        <?php if (!in_array($k,$placesuse)){ ?>
                        <option value="<?=$k?>"><?=$k?> :: <?=$v['title']?>, <?=$v['addr']?></option>
                        <?php } ?>
                    <?php } ?>
                </select>
            </td><!-- currency-uploader-detals-title -->
            <?php for($i=11;$i<=13;$i++){ ?>
            <td class="currency-uploader-detals-curr">
                <div>B <input type="text" name="b_curr<?=$i?>" id="b_curr<?=$i?>" value=""></div>
                <div>S <input type="text" name="s_curr<?=$i?>" id="s_curr<?=$i?>" value=""></div>
            </td><!-- currency-uploader-detals-curr -->
            <?php } ?>
            <td class="currency-uploader-detals-bttn"><a href="javascript:addCurrency(<?=$upload?>)">Добавить</a></td>
        </tr>
</table>
<?php } ?>



<?php if ($_GET['action']=='currency-disposal'){ ?>
<table class="currency_cards">
    <tr>
        <td colspan="3">Платежные карты</td>
    </tr>
    <tr>
        <td>USD</td>
        <td>EUR</td>
        <td colspan="2"></td>
    </tr>
    <tr>
        <?php for($i=41;$i<=42;$i++){ ?>
        <td class="currency-uploader-detals-curr">
            <div>B <input name="b_card_curr<?=$i?>" id="b_card_curr<?=$i?>" value="<?=(isset($elements[36]['curr'][$i]['buy'])?$elements[36]['curr'][$i]['buy']:"")?>"></div>
            <div>S <input name="s_card_curr<?=$i?>" id="s_card_curr<?=$i?>" value="<?=(isset($elements[36]['curr'][$i]['sale'])?$elements[36]['curr'][$i]['sale']:"")?>"></div>
        </td>
        <?php } ?>
        <td class="currency-uploader-detals-bttn"><a href="javascript:saveCardsCurrency('<?=$uploadInfo->id?>')">Изменить</a></td>
    </tr>
</table>
<?php } ?>