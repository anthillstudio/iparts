<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\apanel\models\CurrencyPlaceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Currency Places';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="currency-place-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Currency Place', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title_lng',
            'show',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
