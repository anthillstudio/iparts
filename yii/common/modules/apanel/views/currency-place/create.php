<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\modules\apanel\models\CurrencyPlace */

$this->title = 'Create Currency Place';
$this->params['breadcrumbs'][] = ['label' => 'Currency Places', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="currency-place-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
