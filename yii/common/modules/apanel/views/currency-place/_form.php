<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\modules\apanel\models\CurrencyPlace */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="currency-place-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title_lng')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'show')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
