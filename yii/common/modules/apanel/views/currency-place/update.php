<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\apanel\models\CurrencyPlace */

$this->title = 'Update Currency Place: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Currency Places', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="currency-place-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
