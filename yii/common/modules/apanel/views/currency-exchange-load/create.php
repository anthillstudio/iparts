<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\modules\apanel\models\CurrencyExchangeLoad */

$this->title = 'Create Currency Exchange Load';
$this->params['breadcrumbs'][] = ['label' => 'Currency Exchange Loads', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="currency-exchange-load-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
