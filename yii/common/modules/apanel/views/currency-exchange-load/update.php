<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\apanel\models\CurrencyExchangeLoad */

$this->title = 'Update Currency Exchange Load: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Currency Exchange Loads', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="currency-exchange-load-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
