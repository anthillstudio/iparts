<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\modules\apanel\models\CurrencyExchangeLoad */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="currency-exchange-load-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'date_load')->textInput() ?>

    <?= $form->field($model, 'id_place')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
