<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\apanel\models\CurrencyExchangeLoadSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Currency Exchange Loads';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="currency-exchange-load-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Currency Exchange Load', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'date_load',
            'id_place',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
