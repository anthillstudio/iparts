<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\modules\apanel\models\CurrencyExchangeSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="currency-exchange-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'date_actual') ?>

    <?= $form->field($model, 'id_upload') ?>

    <?= $form->field($model, 'id_position') ?>

    <?= $form->field($model, 'id_place') ?>

    <?php // echo $form->field($model, 'buy') ?>

    <?php // echo $form->field($model, 'sale') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
