<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\apanel\models\CurrencyExchangeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Currency Exchanges';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="currency-exchange-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Currency Exchange', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'date_actual',
            'id_upload',
            'id_position',
            'id_place',
            // 'buy',
            // 'sale',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
