<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\apanel\models\CurrencyExchange */

$this->title = 'Update Currency Exchange: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Currency Exchanges', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="currency-exchange-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
