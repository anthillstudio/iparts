<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\modules\apanel\models\CurrencyExchange */

$this->title = 'Create Currency Exchange';
$this->params['breadcrumbs'][] = ['label' => 'Currency Exchanges', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="currency-exchange-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
