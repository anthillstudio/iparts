<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\modules\apanel\models\CurrencyExchange */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="currency-exchange-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'date_actual')->textInput() ?>

    <?= $form->field($model, 'id_upload')->textInput() ?>

    <?= $form->field($model, 'id_position')->textInput() ?>

    <?= $form->field($model, 'id_place')->textInput() ?>

    <?= $form->field($model, 'buy')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sale')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
