<?php

namespace common\modules\apanel\models;

use Yii;

/**
 * This is the model class for table "{{%currency_position}}".
 *
 * @property integer $id
 * @property string $title_lng
 * @property string $notice_lng
 * @property integer $type
 * @property integer $level
 * @property integer $show
 */
class CurrencyPosition extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%currency_position}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['notice_lng'], 'string'],
            [['type', 'level', 'show'], 'integer'],
            [['title_lng'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title_lng' => 'Title Lng',
            'notice_lng' => 'Notice Lng',
            'type' => 'Type',
            'level' => 'Level',
            'show' => 'Show',
        ];
    }
}
