<?php

namespace common\modules\apanel\models;

use Yii;

/**
 * This is the model class for table "{{%currency_exchange_load}}".
 *
 * @property integer $id
 * @property string $date_load
 * @property integer $id_place
 *
 * @property CurrencyExchange[] $currencyExchanges
 */
class CurrencyExchangeLoad extends \yii\db\ActiveRecord
{

    public $file;
    public $action;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%currency_exchange_load}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_load'], 'safe'],
            [['id_place'], 'integer'],
            [['file'], 'file', 'extensions' => 'xls', 'maxSize' => 11024000, 'tooBig' => 'Не больше 500KB'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_load' => 'Date Load',
            'id_place' => 'Id Place',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrencyExchanges()
    {
        return $this->hasMany(CurrencyExchange::className(), ['id_upload' => 'id']);
    }
}
