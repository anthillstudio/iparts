<?php

namespace common\modules\apanel\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\apanel\models\CurrencyExchange;

/**
 * CurrencyExchangeSearch represents the model behind the search form about `common\modules\apanel\models\CurrencyExchange`.
 */
class CurrencyExchangeSearch extends CurrencyExchange
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_upload', 'id_position', 'id_place'], 'integer'],
            [['date_actual'], 'safe'],
            [['buy', 'sale'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CurrencyExchange::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'date_actual' => $this->date_actual,
            'id_upload' => $this->id_upload,
            'id_position' => $this->id_position,
            'id_place' => $this->id_place,
            'buy' => $this->buy,
            'sale' => $this->sale,
        ]);

        return $dataProvider;
    }
}
