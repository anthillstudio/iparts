<?php

namespace common\modules\apanel\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\apanel\models\CurrencyExchangeLoad;

/**
 * CurrencyExchangeLoadSearch represents the model behind the search form about `common\modules\apanel\models\CurrencyExchangeLoad`.
 */
class CurrencyExchangeLoadSearch extends CurrencyExchangeLoad
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_place'], 'integer'],
            [['date_load'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CurrencyExchangeLoad::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'date_load' => $this->date_load,
            'id_place' => $this->id_place,
        ]);

        return $dataProvider;
    }
}
