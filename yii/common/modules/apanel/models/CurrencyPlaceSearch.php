<?php

namespace common\modules\apanel\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\apanel\models\CurrencyPlace;

/**
 * CurrencyPlaceSearch represents the model behind the search form about `common\modules\apanel\models\CurrencyPlace`.
 */
class CurrencyPlaceSearch extends CurrencyPlace
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'show'], 'integer'],
            [['title_lng'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CurrencyPlace::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'show' => $this->show,
        ]);

        $query->andFilterWhere(['like', 'title_lng', $this->title_lng]);

        return $dataProvider;
    }
}
