<?php

namespace common\modules\apanel\models;

use Yii;
use common\models\ContactsPlaces;

use yii\imagine\Image;
use yii\web\UploadedFile;
use common\components\excel\Spreadsheetexcelreader;

/**
 * This is the model class for table "{{%currency_exchange}}".
 *
 * @property integer $id
 * @property integer $id_upload
 * @property integer $id_position
 * @property integer $id_place
 * @property string $buy
 * @property string $sale
 *
 * @property CurrencyExchangeLoad $idUpload
 * @property ContactsPlaces $idPlace
 * @property CurrencyPosition $idPosition
 */
class CurrencyExchange extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%currency_exchange}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_upload', 'id_position', 'id_place'], 'integer'],
            [['buy', 'sale'], 'number'],
            [['id_upload'], 'exist', 'skipOnError' => true, 'targetClass' => CurrencyExchangeLoad::className(), 'targetAttribute' => ['id_upload' => 'id']],
            //[['id_place'], 'exist', 'skipOnError' => true, 'targetClass' => ContactsPlaces::className(), 'targetAttribute' => ['id_place' => 'id']],
            [['id_position'], 'exist', 'skipOnError' => true, 'targetClass' => CurrencyPosition::className(), 'targetAttribute' => ['id_position' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_upload' => 'Id Upload',
            'id_position' => 'Id Position',
            'id_place' => 'Id Place',
            'buy' => 'Buy',
            'sale' => 'Sale',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUpload()
    {
        return $this->hasOne(CurrencyExchangeLoad::className(), ['id' => 'id_upload']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdPlace()
    {
        return $this->hasOne(ContactsPlaces::className(), ['id' => 'id_place']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdPosition()
    {
        return $this->hasOne(CurrencyPosition::className(), ['id' => 'id_position']);
    }


    public function buildCurrency(){
        /*$id_place = 0;
        switch ($_GET['action']) {
            case 'currency-disposal': $viewdetals = '_detals'; break;
            case 'currency-disposal-beznal': $viewdetals = '_detals_beznal'; break;
            case 'currency-conversion': $viewdetals = '_detals_conversion'; break;
            case 'currency-nbrb': $viewdetals = '_detals_nbrb'; break;
            case 'currency-bcse': $viewdetals = '_detals_bcse'; break;
        }

        $elements = CurrencyExchange::find()->where(['id_upload'=>$_GET['id']])->orderBy(['id'=>SORT_ASC])->all();
        $uploadInfo = CurrencyExchangeLoad::findOne($_GET['id']);

        $placesRows = ContactsPlaces::find()->where(['show'=>1])->all();
        $places = [];
        foreach ($placesRows as $k=>$v){
            $places[$v->id]['id'] = $v->id;
            $places[$v->id]['title'] = $v->title_lng;
            $places[$v->id]['addr'] = $v->addr;
        }

        $arr = [];
        foreach ($elements as $key=>$value){
            $contact = $places[$value->id_place];
            $arr[$value->id_place]['title'] = $contact['title'];
            $arr[$value->id_place]['curr'][$value->id_position]['buy'] = $value->buy;
            $arr[$value->id_place]['curr'][$value->id_position]['sale'] = $value->sale;
            $arr[$value->id_place]['curr'][$value->id_position]['id_position'] = $value->id_position;
            $arr[$value->id_place]['curr'][$value->id_position]['id_place'] = $value->id_place;
        }*/

        echo Yii::$app->getView()->render('@common/modules/apanel/views/currency/mod.php');

    }

    public function getCurrency($type){


        $contacts = Yii::$app->db->createCommand("
            SELECT cp.*, cc.title_lng as city
            FROM {{%contacts_places}} as cp 
            LEFT JOIN {{%contacts_cities}} as cc ON cc.id = cp.city_id
            ORDER BY city DESC
        ")->queryAll();

        $cbu = [];
        foreach ($contacts as $k=>$v){
            $cbu[$v['id']] = "{$v['city']}, {$v['title_lng']}";
        }

        $toReturn = [];
        switch ($type){
            case 'disposal':
                $elements = CurrencyExchangeLoad::find()
                    ->where(['id_place'=>1])
                    ->andWhere(['<', 'date_load', date("Y-m-d H:i:00")])
                    ->orderBy(['date_load'=>SORT_DESC])
                    ->limit(10)
                    ->All();
                foreach ($elements as $key=>$value){
                    $date = $value->date_load;
                    $currel = CurrencyExchange::find()
                        ->where(['id_upload'=>$value->id])
                        ->all();
                    if (count($currel)!=0) break;
                }
                $curr = [];
                foreach ($currel as $k=>$v){
                    $curr['date'] = $date;
                    $curr['elements'][$v->id_place]['title'] = $cbu[$v->id_place];
                    $curr['elements'][$v->id_place]['curr'][$v->id_position]['buy'] = $v->buy;
                    $curr['elements'][$v->id_place]['curr'][$v->id_position]['sale'] = $v->sale;
                }
                $toReturn = $curr;
                break;
            case 'conversion':
                $elements = CurrencyExchangeLoad::find()
                    ->where(['id_place'=>2])
                    ->andWhere(['<', 'date_load', date("Y-m-d H:i:00")])
                    ->orderBy(['date_load'=>SORT_DESC])
                    ->limit(10)
                    ->All();
                foreach ($elements as $key=>$value){
                    $date = $value->date_load;
                    $currel = CurrencyExchange::find()->where(['id_upload'=>$value->id])->all();
                    if (count($currel)!=0) break;
                }
                $curr = [];
                foreach ($currel as $k=>$v){
                    $curr['date'] = $date;
                    $curr['elements'][$v->id_place]['title'] = $cbu[$v->id_place];
                    $curr['elements'][$v->id_place]['curr'][$v->id_position]['buy'] = $v->buy;
                    $curr['elements'][$v->id_place]['curr'][$v->id_position]['sale'] = $v->sale;
                }
                $toReturn = $curr;
                break;
            case 'beznal':
                $elements = CurrencyExchangeLoad::find()
                    ->where(['id_place'=>3])
                    ->andWhere(['<', 'date_load', date("Y-m-d H:i:00")])
                    ->orderBy(['date_load'=>SORT_DESC])
                    ->limit(10)
                    ->All();
                foreach ($elements as $key=>$value){
                    $date = $value->date_load;
                    $currel = CurrencyExchange::find()->where(['id_upload'=>$value->id])->all();
                    if (count($currel)!=0) break;
                }
                $curr = [];
                foreach ($currel as $k=>$v){
                    $curr['date'] = $date;
                    $curr['elements'][$v->id_place]['title'] = $cbu[$v->id_place];
                    $curr['elements'][$v->id_place]['curr'][$v->id_position]['buy'] = $v->buy;
                    $curr['elements'][$v->id_place]['curr'][$v->id_position]['sale'] = $v->sale;
                }
                $toReturn = $curr;
                break;
            case 'nbrb':
                $elements = CurrencyExchangeLoad::find()
                    ->where(['id_place'=>5])
                    ->andWhere(['<', 'date_load', date("Y-m-d H:i:00")])
                    ->orderBy(['date_load'=>SORT_DESC])
                    ->limit(10)
                    ->All();
                foreach ($elements as $key=>$value){
                    $date = $value->date_load;
                    $currel = CurrencyExchange::find()->where(['id_upload'=>$value->id])->all();
                    if (count($currel)!=0) break;
                }
                $curr = [];
                foreach ($currel as $k=>$v){
                    $curr['date'] = $date;
                    $curr['elements'][$v->id_place]['curr'][$v->id_position]['buy'] = $v->buy;
                }
                $toReturn = $curr;
                break;
            case 'bcse':
                $elements = CurrencyExchangeLoad::find()
                    ->where(['id_place'=>6])
                    ->andWhere(['<', 'date_load', date("Y-m-d H:i:00")])
                    ->orderBy(['date_load'=>SORT_DESC])
                    ->limit(10)
                    ->All();
                foreach ($elements as $key=>$value){
                    $date = $value->date_load;
                    $currel = CurrencyExchange::find()->where(['id_upload'=>$value->id])->all();
                    if (count($currel)!=0) break;
                }
                $curr = [];
                foreach ($currel as $k=>$v){
                    $curr['date'] = $date;
                    $curr['elements'][$v->id_place]['curr'][$v->id_position]['buy'] = $v->buy;
                }
                $toReturn = $curr;
                break;
        }

        return $toReturn;
    }



    public function loadfileBeznal()
    {

        $post = Yii::$app->request->post();
        $action = $post['CurrencyExchangeLoad']['action'];

        $model = new CurrencyExchangeLoad;
        if ($model->load(Yii::$app->request->post())) {

            $file = UploadedFile::getInstance($model, 'file');

            if (isset($file)){
                if ($file->extension=='xls') {
                    $filename = 'beznal_' . date('YdmHis') . '.' . $file->extension;
                    $path = "../cloud/currency/{$filename}";
                    if ($model->validate()) {
                        $file->saveAs("{$path}");
                    }
                    $id_place = 3;

                    $data = \moonland\phpexcel\Excel::import($path, [
                        'setFirstRecordAsKeys' => false,
                    ]);

                    $importarr['currency-disposal'] = [
                        '36' => [
                            'xlskey' => 15,
                            'usd' => [31, 'D', 'E'],
                            'euro' => [32, 'F', 'G'],
                            'rub' => [33, 'H', 'I'],
                        ],
                        '41' => [
                            'xlskey' => 16,
                            'usd' => [31, 'D', 'E'],
                            'euro' => [32, 'F', 'G'],
                            'rub' => [33, 'H', 'I'],
                        ],
                        '63' => [
                            'xlskey' => 18,
                            'usd' => [31, 'D', 'E'],
                            'euro' => [32, 'F', 'G'],
                            'rub' => [33, 'H', 'I'],
                        ],
                        '59' => [
                            'xlskey' => 19,
                            'usd' => [31, 'D', 'E'],
                            'euro' => [32, 'F', 'G'],
                            'rub' => [33, 'H', 'I'],
                        ],
                    ];

                    $importarr['datetime'] = [
                        'key' => 9,
                        'time' => 'E',
                        'date' => 'H'
                    ];

                    // Пишем в таблицу iparts_currency_exchange_load
                    $date = $data[0][$importarr['datetime']['key']]['E'];
                    $datearr = explode('-', $date);
                    $date = "{$datearr[2]}-{$datearr[0]}-{$datearr[1]}";
                    $time = $data[0][$importarr['datetime']['key']]['H'];
                    $date = date("Y-m-d", strtotime($date)) . " " . $time . ":00";

                    $exchangeloadmodel = new CurrencyExchangeLoad();
                    $exchangeloadmodel->date_load = $date;
                    $exchangeloadmodel->id_place = $id_place;
                    $exchangeloadmodel->save();
                    $elementId = $exchangeloadmodel->getPrimaryKey();

                    // Массивом пишем в таблицу iparts_currency_exchange
                    // id_upload
                    // id_position
                    // id_place
                    // buy
                    // sale
                    foreach ($importarr['currency-disposal'] as $key => $value) {

                        foreach ($value as $k => $v) {
                            $b = $data[0][$value['xlskey']][$value[$k][1]];
                            $s = 0;
                            if (
                                trim($s) != '' &&
                                trim($b) != ''
                            ) {
                                if ($k != 'xlskey') {
                                    Yii::$app->db->createCommand()->insert('{{%currency_exchange}}', [
                                        'id_upload' => $elementId,
                                        'id_position' => $v[0],
                                        'id_place' => $key,
                                        'buy' => $b,
                                        'sale' => $s
                                    ])->execute();
                                }
                            }
                        }
                    }
                    Yii::$app->session->addFlash('success', 'Информация добавлена');
                }else {
                    Yii::$app->session->addFlash('success', 'Не верный формат');
                }
            }
        }

    }



    public function loadfileConversion()
    {


        $post = Yii::$app->request->post();
        $action = $post['CurrencyExchangeLoad']['action'];

        $model = new CurrencyExchangeLoad;
        if ($model->load(Yii::$app->request->post())) {

            $file = UploadedFile::getInstance($model, 'file');

            if (isset($file)){
                if ($file->extension=='xls') {
                    $filename = "conversion_" . date('YdmHis') . '.' . $file->extension;
                    $path = "../cloud/currency/{$filename}";
                    if ($model->validate()) {
                        $file->saveAs("{$path}");
                    }
                    $id_place = 2;

                    $data = \moonland\phpexcel\Excel::import($path, [
                        'setFirstRecordAsKeys' => false,
                    ]);

                    $importarr['currency'] = [
                        '36' => [
                            'xlskey' => 17,
                            'usdrub' => [21, 'F', 'E'],
                            'rubusd' => [22, 'H', 'G'],
                            'usdeur' => [23, 'J', 'I'],
                            'eurusd' => [24, 'L', 'I'],
                        ],
                        '41' => [
                            'xlskey' => 19,
                            'usdrub' => [21, 'F', 'E'],
                            'rubusd' => [22, 'H', 'G'],
                            'usdeur' => [23, 'J', 'I'],
                            'eurusd' => [24, 'L', 'I'],
                        ],
                        '51' => [
                            'xlskey' => 23,
                            'usdrub' => [21, 'F', 'E'],
                            'rubusd' => [22, 'H', 'G'],
                            'usdeur' => [23, 'J', 'I'],
                            'eurusd' => [24, 'L', 'I'],
                        ],
                        '63' => [
                            'xlskey' => 25,
                            'usdrub' => [21, 'F', 'E'],
                            'rubusd' => [22, 'H', 'G'],
                            'usdeur' => [23, 'J', 'I'],
                            'eurusd' => [24, 'L', 'I'],
                        ],
                        '59' => [
                            'xlskey' => 26,
                            'usdrub' => [21, 'F', 'E'],
                            'rubusd' => [22, 'H', 'G'],
                            'usdeur' => [23, 'J', 'I'],
                            'eurusd' => [24, 'L', 'I'],
                        ],
                        '76' => [
                            'xlskey' => 28,
                            'usdrub' => [21, 'F', 'E'],
                            'rubusd' => [22, 'H', 'G'],
                            'usdeur' => [23, 'J', 'I'],
                            'eurusd' => [24, 'L', 'I'],
                        ],
                    ];

                    $importarr['datetime'] = [
                        'key' => 11,
                        'time' => 'B',
                        'date' => 'E'
                    ];

                    // Пишем в таблицу iparts_currency_exchange_load
                    $date = $data[0][$importarr['datetime']['key']]['B'];
                    $datearr = explode('-', $date);
                    $date = "{$datearr[2]}-{$datearr[0]}-{$datearr[1]}";
                    $time = $data[0][$importarr['datetime']['key']]['E'];
                    $date = date("Y-m-d", strtotime($date)) . " " . $time . ":00";

                    $exchangeloadmodel = new CurrencyExchangeLoad();
                    $exchangeloadmodel->date_load = $date;
                    $exchangeloadmodel->id_place = $id_place;
                    $exchangeloadmodel->save();
                    $elementId = $exchangeloadmodel->getPrimaryKey();

                    // Массивом пишем в таблицу iparts_currency_exchange
                    // id_upload
                    // id_position
                    // id_place
                    // buy
                    // sale



                        foreach ($importarr['currency'] as $key=>$value){

                            foreach ($value as $k=>$v){

                                    if ($k!='xlskey'){

                                        $b = $data[0][$value['xlskey']][$value[$k][1]];
                                        $s = 0;

                                        Yii::$app->db->createCommand()->insert('{{%currency_exchange}}', [
                                            'id_upload' => $elementId,
                                            'id_position' => $v[0],
                                            'id_place' => $key,
                                            'buy' => $b,
                                            'sale' => $s
                                        ])->execute();
                                    }

                            }

                    }
                    Yii::$app->session->addFlash('success', 'Информация добавлена');
                }else {
                    Yii::$app->session->addFlash('success', 'Не верный формат');
                }
            }
        }

    }


    public function loadFile(){
        $model = new CurrencyExchangeLoad;
        if ($model->load(Yii::$app->request->post())) {

            $file = UploadedFile::getInstance($model, 'file');

            if (isset($file)){
                if ($file->extension=='xls') {
                    $filename = date('YdmHis') . '.' . $file->extension;
                    $path = "../cloud/currency/{$filename}";
                    if ($model->validate()) {
                        $file->saveAs("{$path}");
                    }

                    $id_place = 1;

                    $data = \moonland\phpexcel\Excel::import($path,[
                        'setFirstRecordAsKeys' => false,
                    ]);

                    $importarr['currency-disposal'] = [
                        '36' => [
                            'xlskey' => 14,
                            'usd' => [11,'D','E'],
                            'euro' => [12,'F','G'],
                            'rub' => [13,'H','I'],
                        ],
                        '76' => [
                            'xlskey' => 15,
                            'usd' => [11,'D','E'],
                            'euro' => [12,'F','G'],
                            'rub' => [13,'H','I'],
                        ],
                        '41' => [
                            'xlskey' => 16,
                            'usd' => [11,'D','E'],
                            'euro' => [12,'F','G'],
                            'rub' => [13,'H','I'],
                        ],
                        '51' => [
                            'xlskey' => 17,
                            'usd' => [11,'D','E'],
                            'euro' => [12,'F','G'],
                            'rub' => [13,'H','I'],
                        ],
                        '59' => [
                            'xlskey' => 18,
                            'usd' => [11,'D','E'],
                            'euro' => [12,'F','G'],
                            'rub' => [13,'H','I'],
                        ],
                        '57' => [
                            'xlskey' => 19,
                            'usd' => [11,'D','E'],
                            'euro' => [12,'F','G'],
                            'rub' => [13,'H','I'],
                        ],
                        '55' => [
                            'xlskey' => 20,
                            'usd' => [11,'D','E'],
                            'euro' => [12,'F','G'],
                            'rub' => [13,'H','I'],
                        ],
                        '63' => [
                            'xlskey' => 21,
                            'usd' => [11,'D','E'],
                            'euro' => [12,'F','G'],
                            'rub' => [13,'H','I'],
                        ],
                    ];

                    $importarr['currency-cards'] = [
                        'usd' => [
                            'xlskey' => 33,
                            'usd' => [41,'C','E'],
                            'euro' => [42,'C','E'],
                        ],
                        'euro' => [
                            'xlskey' => 34,
                            'usd' => [41,'C','E'],
                            'euro' => [42,'C','E'],
                        ],
                    ];

                    $importarr['datetime'] = [
                        'key' => 9,
                        'time' => 'E',
                        'date' => 'H'
                    ];

                    // Пишем в таблицу iparts_currency_exchange_load
                    $date = $data[0][$importarr['datetime']['key']]['E'];
                    $datearr = explode('-',$date);
                    $date = "{$datearr[2]}-{$datearr[0]}-{$datearr[1]}";
                    $time = $data[0][$importarr['datetime']['key']]['H'];
                    $date = date("Y-m-d", strtotime($date))." ".$time.":00";

                    $exchangeloadmodel = new CurrencyExchangeLoad();
                    $exchangeloadmodel->date_load = $date;
                    $exchangeloadmodel->id_place = $id_place;
                    $exchangeloadmodel->save();
                    $elementId = $exchangeloadmodel->getPrimaryKey();

                    // Массивом пишем в таблицу iparts_currency_exchange
                    // id_upload
                    // id_position
                    // id_place
                    // buy
                    // sale
                    foreach ($importarr['currency-disposal'] as $key=>$value){

                        foreach ($value as $k=>$v){
                            $b = $data[0][$value['xlskey']][$value[$k][1]];
                            $s = $data[0][$value['xlskey']][$value[$k][2]];
                            if (
                                trim($s)!='' &&
                                trim($b)!=''
                            ){
                                if ($k!='xlskey'){
                                    Yii::$app->db->createCommand()->insert('{{%currency_exchange}}', [
                                        'id_upload' => $elementId,
                                        'id_position' => $v[0],
                                        'id_place' => $key,
                                        'buy' => $b,
                                        'sale' => $s
                                    ])->execute();
                                }
                            }
                        }
                    }

                    // КАРТЫ
                    foreach ($importarr['currency-cards'] as $key=>$value){
                        $b = $data[0][$value['xlskey']][$value[$key][1]];
                        $s = $data[0][$value['xlskey']][$value[$key][2]];
                        if ($k!='xlskey') {
                            if (
                                trim($s) != '' &&
                                trim($b) != ''
                            ) {
                                Yii::$app->db->createCommand()->insert('{{%currency_exchange}}', [
                                    'id_upload' => $elementId,
                                    'id_position' => $value[$key][0],
                                    'id_place' => 36,
                                    'buy' => $b,
                                    'sale' => $s
                                ])->execute();
                            }
                        }
                    }
                    Yii::$app->session->addFlash('success', 'Информация добавлена');
                }else{
                    Yii::$app->session->addFlash('success', 'Не верный формат');
                }
            }
        }
    }


}
