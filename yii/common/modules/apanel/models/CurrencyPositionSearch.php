<?php

namespace common\modules\apanel\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\apanel\models\CurrencyPosition;

/**
 * CurrencyPositionSearch represents the model behind the search form about `common\modules\apanel\models\CurrencyPosition`.
 */
class CurrencyPositionSearch extends CurrencyPosition
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'type', 'level', 'show'], 'integer'],
            [['title_lng', 'notice_lng'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CurrencyPosition::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'type' => $this->type,
            'level' => $this->level,
            'show' => $this->show,
        ]);

        $query->andFilterWhere(['like', 'title_lng', $this->title_lng])
            ->andFilterWhere(['like', 'notice_lng', $this->notice_lng]);

        return $dataProvider;
    }
}
