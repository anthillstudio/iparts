<?php

namespace common\modules\apanel\models;

use Yii;

/**
 * This is the model class for table "{{%currency_place}}".
 *
 * @property integer $id
 * @property string $title_lng
 * @property integer $show
 */
class CurrencyPlace extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%currency_place}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['show'], 'integer'],
            [['title_lng'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title_lng' => 'Title Lng',
            'show' => 'Show',
        ];
    }
}
