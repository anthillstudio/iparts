<?php

namespace common\modules\xblocks\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\xblocks\models\Reviewsblocks;

/**
 * ReviewsSearch represents the model behind the search form about `common\models\Reviews`.
 */
class ReviewsblocksSearch extends Reviewsblocks
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'parent', 'user_id', 'show'], 'integer'],
            [['insert_date', 'update_date', 'model', 'name', 'email', 'phone', 'answer_date', 'review', 'answer'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Reviewsblocks::find()
            ->where(['model'=>$params['model']])
            ->andWhere(['parent'=>$params['parent']])
            ->orderBy(['insert_date'=>SORT_DESC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'insert_date' => $this->insert_date,
            'update_date' => $this->update_date,
            'parent' => $this->parent,
            'answer_date' => $this->answer_date,
            'user_id' => $this->user_id,
            'show' => $this->show,
        ]);

        $query->andFilterWhere(['like', 'model', $this->model])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'review', $this->review])
            ->andFilterWhere(['like', 'answer', $this->answer]);

        return $dataProvider;
    }
}
