<?php

namespace common\modules\xblocks\models;

use Yii;
use common\components\Ant;

/**
 * This is the model class for table "{{%iblocks}}".
 *
 * @property integer $id
 * @property string $insert_date
 * @property string $update_date
 * @property integer $user_id
 * @property string $box
 * @property string $title_lng
 * @property string $content
 * @property string $tpl
 * @property string $type
 * @property string $image_file
 * @property integer $show
 * @property integer $level
 */
class Xfiles extends \yii\db\ActiveRecord
{

    public $modparent;

    public $config = [
        'dir' => 'xblocks/files',
    ];


    public function getMaxLevel(){
        return Ant::getMaxLevel($this->tableName());
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%files}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title_lng'], 'required'],
            [['insert_date', 'update_date'], 'safe'],
            [['user_id', 'parent', 'show', 'level'], 'integer'],
            [['model', 'title_lng', 'link', 'file'], 'string', 'max' => 255],
        ];
    }


    public function getElementsByParentId($el,$model){
        $elements = Yii::$app->db->createCommand("
          SELECT *
          FROM {{%files}}
          WHERE `parent` = '{$el}' AND `model` = '{$model}' AND `show` = '1'
          ORDER BY `level` DESC
        ")->queryAll();

        return $elements;
    }

    public function buildElements($el,$model='content')
    {
        $elements = Xfiles::getElementsByParentId($el,$model);
        $toReturn = $this->render('@common/modules/xblocks/views/xfiles/inc/_1.php', ['elements' => $elements]);
        return $toReturn;
    }



    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'insert_date' => 'Дата добавления',
            'update_date' => 'Дата обновления',
            'user_id' => 'ID пользователя',
            'model' => 'Модель',
            'parent' => 'Parent ID',
            'file' => 'Изображение',
            'link' => 'Ссылка',
            'show' => 'Отображать',
            'level' => 'Уровень',
        ];
    }
}
