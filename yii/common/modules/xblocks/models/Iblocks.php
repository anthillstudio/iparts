<?php

namespace common\modules\xblocks\models;

use Yii;
use common\modules\xblocks\models\Imgblocks;
use common\components\Ant;

/**
 * This is the model class for table "{{%iblocks}}".
 *
 * @property integer $id
 * @property string $insert_date
 * @property string $update_date
 * @property integer $user_id
 * @property string $box
 * @property string $title_lng
 * @property string $content
 * @property string $tpl
 * @property string $type
 * @property string $image_file
 * @property integer $show
 * @property integer $level
 */
class Iblocks extends \yii\db\ActiveRecord
{

    public $modparent;

    public $config = [
        'dir' => 'xblocks/info',
    ];

    public $tpls = [
        '1'=>'Default (текст + изображение)',
        '2'=>'Слайдер изображений (текст + слайдер изображений)',
        '3'=>'Матрица изображений (текст + матрица изображений)',
        '4'=>'Видео (текст + видео)',
        '5'=>'Уникальный (верстка)',
        '6'=>'Default (текст + изображение / обтекание)',
        '7'=>'Слайдер изображений (текст + слайдер изображений / обтекание)',
        '8'=>'Баннер',
        '9'=>'CODE',
        '10'=>'Контакты',
        '11'=>'Заявка на кредит для физлиц',
        '12'=>'Заявка на кредит для юрлиц',
    ];

    public $tplStyles = [
        '0'=>'DEFAULT',
        '1'=>'Default TPL / Default (обтекание) | ICONS | Info',
        '2'=>'Default TPL / Default (обтекание) | ICONS | Warning',
        '3'=>'Default TPL / Default (обтекание) | ICONS | Contacts',
    ];

    public $textPositions = [
        '1'=>'Слева',
        '2'=>'Справа',
        '3'=>'Снизу',
        '4'=>'Сверху',
        '5'=>'Наложение',
    ];
    public $titleType = [
        '1'=>'Отображать',
        '0'=>'Скрывать',
    ];

    public function getMaxLevel(){
        return Ant::getMaxLevel($this->tableName());
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%iblocks}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title_lng'], 'required'],
            [['insert_date', 'update_date'], 'safe'],
            [['user_id', 'text_position', 'title_type', 'tpl_style', 'fix', 'parent', 'show', 'level'], 'integer'],
            [['content', 'js', 'html', 'link', 'styles', 'text_align'], 'string'],
            [['model', 'title_lng', 'vidget', 'tpl', 'image_file'], 'string', 'max' => 255],
        ];
    }

    public function actions()
    {
        return [
            'image-upload' => [
                'class' => 'vova07\imperavi\actions\UploadAction',
                'url' => Yii::$app->params['uploader']['domen'].'/imperavi/',
                'path' => '../cloud/imperavi/'
            ],
            'file-upload' => [
                'class' => 'vova07\imperavi\actions\UploadAction',
                'url' => Yii::$app->params['uploader']['domen'].'/imperavi/files/',
                'path' => '../cloud/imperavi/files/',
                'uploadOnlyImage' => false,
            ],
        ];
    }

    public static function getElementsByParentId($el,$model){
        $elements = Yii::$app->db->createCommand("
          SELECT *
          FROM {{%iblocks}}
          WHERE `parent` = '{$el}' AND `model` = '{$model}' AND `show` = '1'
          ORDER BY `level` ASC
        ")->queryAll();

        return $elements;
    }

    public function buildElements($el,$model='content')
    {

        $elements = Iblocks::getElementsByParentId($el,$model);
        $toReturn = "";

        if (count($elements)!=0) {
            foreach ($elements as $key => $value) {
                $images = Imgblocks::find()
                    ->where(['parent'=>$value['id']])
                    ->andWhere(['model'=>'iblocks'])
                    ->all();
                //if (file_exists("@common/modules/xblocks/views/iblocks/inc/_{$value['tpl']}.php")) {
                    $toReturn .= $this->render('@common/modules/xblocks/views/iblocks/inc/_' . $value['tpl'] . '.php', [
                        'element' => $value,
                        'images' => $images
                    ]);
                //}
            }
        }
        return $toReturn;
    }



    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'insert_date' => 'Дата добавления',
            'update_date' => 'Дата обновления',
            'user_id' => 'ID пользователя',
            'model' => 'Модель',
            'parent' => 'Parent ID',
            'title_lng' => 'Название',
            'content' => 'Контент',
            'styles' => 'Стили',
            'js' => 'Js',
            'html' => 'HTML',
            'link' => 'Ссылка',
            'vidget' => 'Виджет',
            'tpl' => 'Шаблон',
            'tpl_style' => 'Стиль шаблона',
            'text_align' => 'Выравнивание текста',
            'fix' => 'Фиксированный',
            'text_position' => 'Положение текста',
            'title_type' => 'Тип заголовка',
            'image_file' => 'Изображение',
            'show' => 'Отображать',
            'level' => 'Уровень',
        ];
    }
}
