<?php

namespace common\modules\xblocks\models;

use Yii;

/**
 * This is the model class for table "{{%reviews}}".
 *
 * @property integer $id
 * @property string $insert_date
 * @property string $update_date
 * @property string $answer_date
 * @property integer $user_id
 * @property string $review
 * @property string $ansewr
 * @property integer $show
 */
class Reviews extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%reviews}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['insert_date', 'update_date', 'answer_date'], 'safe'],
            [['user_id', 'show'], 'integer'],
            [['review', 'answer'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'insert_date' => 'Insert Date',
            'update_date' => 'Update Date',
            'answer_date' => 'Answer Date',
            'user_id' => 'User ID',
            'review' => 'Отзыв',
            'answer' => 'Отват',
            'show' => 'Отображать',
        ];
    }


    public function getElementsByParentId($el,$model){
        $elements = Yii::$app->db->createCommand("
          SELECT *
          FROM {{%reviews}}
          WHERE `parent` = '{$el}' AND `model` = '{$model}'
          ORDER BY `insert_date` DESC
        ")->queryAll();
        return $elements;
    }

    public function buildElements($el,$model='content')
    {

        $elements = Reviews::getElementsByParentId($el,$model);
        $toReturn = "";
        $toReturn .= $this->render('@frontend/views/reviews/_list.php', [
            'elements'=>$elements,
        ]);
        return $toReturn;
    }



}