<?php

namespace common\modules\xblocks\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\xblocks\models\Imgblocks;

/**
 * ImgblocksSearch represents the model behind the search form about `common\modules\xblocks\models\Imgblocks`.
 */
class ImgblocksSearch extends Imgblocks
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'parent', 'show', 'level'], 'integer'],
            [['insert_date', 'update_date', 'model', 'title_lng', 'image_file'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Imgblocks::find()
            ->where(['model'=>$params['model']])
            ->andWhere(['parent'=>$params['parent']])
            ->orderBy(['level'=>SORT_DESC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'insert_date' => $this->insert_date,
            'update_date' => $this->update_date,
            'user_id' => $this->user_id,
            'model' => $this->model,
            'parent' => $this->parent,
            'show' => $this->show,
            'level' => $this->level,
        ]);

        $query->andFilterWhere(['like', 'model', $this->model])
            ->andFilterWhere(['like', 'title_lng', $this->title_lng])
            ->andFilterWhere(['like', 'image_file', $this->image_file]);

        return $dataProvider;
    }
}
