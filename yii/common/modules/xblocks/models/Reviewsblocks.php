<?php

namespace common\modules\xblocks\models;

use Yii;

/**
 * This is the model class for table "{{%reviews}}".
 *
 * @property integer $id
 * @property string $insert_date
 * @property string $update_date
 * @property string $answer_date
 * @property integer $user_id
 * @property string $review
 * @property string $ansewr
 * @property integer $show
 */
class Reviewsblocks extends \yii\db\ActiveRecord
{

    public $modparent;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%reviews}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['insert_date', 'update_date', 'answer_date'], 'safe'],
            [['user_id', 'parent', 'show'], 'integer'],
            [['model', 'name', 'review', 'answer'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'insert_date' => 'Insert Date',
            'update_date' => 'Update Date',
            'answer_date' => 'Answer Date',
            'user_id' => 'User ID',
            'name' => 'Имя',
            'review' => 'Отзыв',
            'answer' => 'Ответ',
            'show' => 'Отображать',
        ];
    }


    public function getElementsByParentId($el,$model){
        $elements = Yii::$app->db->createCommand("
          SELECT *
          FROM {{%reviews}}
          WHERE `parent` = '{$el}' AND `model` = '{$model}' AND `show` = '1'
          ORDER BY `insert_date` DESC
        ")->queryAll();
        return $elements;
    }

    public function buildElements($el,$model='content')
    {

        $elements = Reviewsblocks::getElementsByParentId($el,$model);
        $toReturn = "";
        $toReturn .= $this->render('@frontend/views/reviews/_list.php', [
            'elements'=>$elements,
        ]);
        return $toReturn;
    }



}