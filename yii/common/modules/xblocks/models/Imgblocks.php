<?php

namespace common\modules\xblocks\models;

use Yii;
use common\components\Ant;

/**
 * @property integer $id
 * @property string $insert_date
 * @property string $update_date
 * @property integer $user_id
 * @property string $box
 * @property integer $parent
 * @property string $title_lng
 * @property string $image_file
 * @property integer $show
 * @property integer $level
 */
class Imgblocks extends \yii\db\ActiveRecord
{

    public $modparent;
    public $xmodel;

    public $config = [
        'dir' => 'xblocks/img',
        'model' => 'xblocks',
    ];

    public function getMaxLevel(){
        return Ant::getMaxLevel($this->tableName());
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%imgblocks}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title_lng'], 'required'],
            [['insert_date', 'update_date'], 'safe'],
            [['user_id', 'parent', 'show', 'level'], 'integer'],
            [['model', 'title_lng', 'image_file'], 'string', 'max' => 255],
        ];
    }

    public function getElementsByParentId($el,$model){
        $elements = Yii::$app->db->createCommand("
          SELECT *
          FROM {{%imgblocks}}
          WHERE `parent` = '{$el}' AND `model` = '{$model}'
          ORDER BY `level` ASC
        ")->queryAll();


        return $elements;
    }


    public function buildElements($el,$model='content')
    {
        $elements = Imgblocks::getElementsByParentId($el,$model);
        $toReturn = "";

        if (count($elements)!=0) {
            $toReturn .= $this->render('@common/modules/xblocks/views/imgblocks/inc/_matrix.php', [
                'elements' => $elements
            ]);
        }
        return $toReturn;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'insert_date' => 'Дата добавления',
            'update_date' => 'Дата обновления',
            'user_id' => 'Пользователь',
            'model' => 'Модель',
            'parent' => 'Parent',
            'title_lng' => 'Название',
            'image_file' => 'Изображение',
            'show' => 'Отображать',
            'level' => 'Уровень',
        ];
    }
}
