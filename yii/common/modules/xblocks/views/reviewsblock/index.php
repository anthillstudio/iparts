<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ReviewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Отзывы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reviews-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= Html::a('Создать отзыв', ['create?xpar='.$_GET['xpar'].'&xurl='.$_GET['xurl']], ['class' => 'btn btn-success']) ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'options' => ['style'=>'white-space: normal'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            //'insert_date',
            //'update_date',
            //'model',
            'parent',
            // 'name',
            // 'email:email',
            // 'phone',
            // 'answer_date',
            // 'user_id',
             'review:ntext',
             'answer:ntext',
            // 'show',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
