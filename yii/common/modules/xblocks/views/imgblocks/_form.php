<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use common\components\Ant;
use vova07\imperavi\Widget;
use common\modules\xblocks\models\Imgblocks;
$iblocksObject = new Imgblocks();
?>

<div class="iblocks-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'model')->hiddenInput(['value' => $_GET['model']])->label(false) ?>

    <?= $form->field($model, 'xmodel')->hiddenInput(['value' => $_GET['xmodel']])->label(false) ?>

    <?= $form->field($model, 'parent')->hiddenInput(['value' => $_GET['parent']])->label(false) ?>

    <?= $form->field($model, 'modparent')->hiddenInput(['value' => $_GET['modparent']])->label(false) ?>

    <?= $form->field($model, 'insert_date')->hiddenInput(['value' => '0000-00-00 00:00:00'])->label(false) ?>

    <?= $form->field($model, 'update_date')->hiddenInput(['value' => '0000-00-00 00:00:00'])->label(false) ?>

    <?= $form->field($model, 'user_id')->hiddenInput(['value'=>1])->label(false) ?>

    <?= $form->field($model, 'title_lng')->textInput(['maxlength' => true]) ?>


    <?php
    if (!$model->isNewRecord){
        $uploadsPath = Ant::uploaderInfo();
        $path = "/xblocks/info/{$model->image_file}";
        if (
            trim($model->image_file)!='' &&
            file_exists("../{$uploadsPath['dir']}{$path}")
        )
        {
            $img = \yii\helpers\Html::tag(
                "img",
                "Страницы",
                [
                    "style" => "width: 100px; margin-bottom: 10px",
                    "src" => "{$uploadsPath['domen']}{$path}"
                ]
            );
            echo $img;
        }
    }
    ?>
    <?= $form->field($model, 'image_file')->fileInput()->label(false) ?>


    <?php if (!isset($model->show)) $model->show = true ?>
    <?= $form->field($model, 'show')->checkbox(['checked' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>