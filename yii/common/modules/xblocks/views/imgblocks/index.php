<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

use common\components\Ant;
use common\modules\xblocks\controllers\XblocksController;

$this->title = 'Изображения';
$this->params['breadcrumbs'][] = $this->title;
?>

    <style>
        .backbttn{
            padding: 7px 20px;
            margin-bottom: 20px;
            display: inline-block;
            color: #000;
            background-color: #fff
        }
    </style>

    <div class="iblocks-index">



        <?php
        $modparent = "";
        if (isset($_GET['modparent'])) $modparent = "?parent=".$_GET['modparent'];
        ?>
        <a class="backbttn" href="/<?=$parentModel['model'].$modparent?>"><?=$parentModel['title_lng']?></a>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'options' => ['style'=>'white-space: normal'],
            'columns' => [
                //['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute' => 'id',
                    'options' => ['style'=>'width: 10px'],
                    'filter' => false
                ],
                [
                    'attribute' => 'IMG',
                    'format' => 'raw',
                    'options' => ['style'=>'width: 70px'],
                    'value' => function ($models) {
                        $toReturn = "";
                        $uploaderInfo = Ant::uploaderInfo();
                        $path = "/{$models->config['dir']}/{$models->image_file}";
                        if (file_exists("../{$uploaderInfo['dir']}{$path}") && trim($models->image_file)!='') {
                            $toReturn .= Html::img(Url::to("{$uploaderInfo['domen']}{$path}"), [
                                'alt' => '',
                                'style' => 'width:100px;'
                            ]);
                        }else{
                            $toReturn .= '-';
                        }
                        return $toReturn;
                    },
                ],
                [
                    'attribute' => 'title_lng',
                    'options' => ['style'=>'width: 250px'],
                    //'filter' => false
                ],
                'level',
                [
                    'format' => 'raw',
                    'options' => ['style'=>'width: 100px'],
                    'value' => function ($model) {
                        $modparent = "";
                        $xmodel = "";
                        if (isset($_GET['modparent'])) $modparent = "&modparent=".$_GET['modparent'];
                        if (isset($_GET['xmodel'])) $xmodel = "&xmodel=".$_GET['xmodel'];
                        return \yii\helpers\Html::a(
                            'Обновить',["update?id={$model->id}&model={$_GET['model']}{$xmodel}&parent={$_GET['parent']}{$modparent}"],
                            [
                                'class' => 'btn btn-sm mybutton',
                            ]
                        );
                    },
                ],
                [
                    'format' => 'raw',
                    'options' => ['style'=>'width: 100px'],
                    'value' => function ($model) {
                        return "".
                        Html::beginForm(['delete','id'=>$model->id,'xpar'=>$_GET['xpar']], 'post').
                        Html::submitButton('Удалить',
                            [
                                'class' => 'btn btn-sm',
                                'style' => 'display: inline-block',
                                'data' => ['confirm' => 'Вы действительно хотите?']
                            ]
                        ).
                        Html::endForm();
                    },
                ],

            ],
        ]); ?>
    </div>
<?php
$modparent = "";
$xmodel = "";
if (isset($_GET['modparent'])) $modparent = "&modparent=".$_GET['modparent'];
if (isset($_GET['xmodel'])) $xmodel = "&xmodel=".$_GET['xmodel'];
?>
<a class="btn btn-success" href="create?model=<?=$_GET['model']?><?=$xmodel?>&parent=<?=$_GET['parent']?><?=$modparent?>">Создать</a>