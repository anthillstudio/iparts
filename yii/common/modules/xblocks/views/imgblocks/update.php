<?php

use yii\helpers\Html;

$this->title = $model->title_lng;
/*$this->params['breadcrumbs'][] = ['label' => 'Iblocks', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';*/
?>
<div class="iblocks-update">
    <?= $this->render('_form', [
        'model' => $model,
        'ibObject' => $ibObject,
        'parentbox' => $parentbox
    ]) ?>
</div>
