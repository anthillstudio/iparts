<?php
$uploadsPath = Yii::$app->params['uploader'];
?>
<?php if (count($elements)!=0){ ?>
    <div class="img-matrix">
        <?php foreach ($elements as $key=>$value){ ?>
            <div class="img-matrix-item">
                <?php $path = "/xblocks/img/{$value['image_file']}"; ?>
                <a rel="fancybox<?=$element['id']?>" href="<?=$uploadsPath['domen']?><?=$path?>"><img src='<?=$uploadsPath['domen']?><?=$path?>'></a>
            </div><!-- img-matrix-item -->
        <?php } ?>
    </div><!-- img-matrix -->
<?php } ?>
<?php
echo newerton\fancybox\FancyBox::widget([
    'target' => "a[rel=fancybox{$element['id']}]",
    'helpers' => true,
    'mouse' => true,
    'config' => [
        'maxWidth' => '90%',
        'maxHeight' => '90%',
        'playSpeed' => 7000,
        'padding' => 0,
        'fitToView' => false,
        'width' => '70%',
        'height' => '70%',
        'autoSize' => false,
        'closeClick' => false,
        'openEffect' => 'elastic',
        'closeEffect' => 'elastic',
        'prevEffect' => 'elastic',
        'nextEffect' => 'elastic',
        'closeBtn' => false,
        'openOpacity' => true,
        'helpers' => [
            'title' => ['type' => 'float'],
            'buttons' => [],
            'thumbs' => ['width' => 68, 'height' => 50],
            'overlay' => [
                'css' => [
                    'background' => 'rgba(0, 0, 0, 0.8)'
                ]
            ]
        ],
    ]
]);
?>
