<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use common\components\Ant;
use vova07\imperavi\Widget;
use common\modules\xblocks\models\Iblocks;
$iblocksObject = new Iblocks();

?>

<div class="iblocks-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'model')->hiddenInput(['value' => $_GET['model']])->label(false) ?>

    <?= $form->field($model, 'parent')->hiddenInput(['value' => $_GET['parent']])->label(false) ?>

    <?= $form->field($model, 'modparent')->hiddenInput(['value' => $_GET['modparent']])->label(false) ?>

    <?= $form->field($model, 'insert_date')->hiddenInput(['value' => '0000-00-00 00:00:00'])->label(false) ?>

    <?= $form->field($model, 'update_date')->hiddenInput(['value' => '0000-00-00 00:00:00'])->label(false) ?>

    <?= $form->field($model, 'user_id')->hiddenInput(['value'=>1])->label(false) ?>

    <?= $form->field($model, 'title_lng')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'link')->textInput(['maxlength' => true]) ?>



    <?= $form->field($model, 'content')->textarea(['rows' => 6])->widget(Widget::className(), [
        'settings' => [
            'lang' => 'ru',
            'minHeight' => 200,
            'plugins' => [
                'clips',
                'fullscreen'
            ],
            'imageUpload' => Url::to(['/content/image-upload']),
            'fileUpload' => Url::to(['/content/file-upload'])
        ]
    ]);
    ?>


    <?= $form->field($model, 'styles')->textarea(['rows' => 6])->label('Стили') ?>

    <?= $form->field($model, 'js')->textarea(['rows' => 6])->label('Js code') ?>

    <?= $form->field($model, 'html')->textarea(['rows' => 6])->label('Верстка') ?>

    <?= $form->field($model, 'vidget')->textInput(['maxlength' => true])->label('Виджет') ?>

    <?= $form->field($model, 'tpl')->dropDownList($iblocksObject->tpls,['style'=>'width: auto']) ?>

    <?= $form->field($model, 'tpl_style')->dropDownList($iblocksObject->tplStyles,['style'=>'width: auto']) ?>

    <?= $form->field($model, 'text_position')->dropDownList($iblocksObject->textPositions,['style'=>'width: auto']) ?>

    <?= $form->field($model, 'title_type')->dropDownList($iblocksObject->titleType,['style'=>'width: auto']) ?>

    <?= $form->field($model, 'text_align')->dropDownList([
        'left' => 'Лево',
        'right' => 'Право',
        'center'=>'Центр'
    ],['style'=>'width: auto'])?>

    <?php
    if (!$model->isNewRecord){
        $uploadsPath = Ant::uploaderInfo();
        $path = "/xblocks/info/{$model->image_file}";
        if (
            trim($model->image_file)!='' &&
            file_exists("../{$uploadsPath['dir']}{$path}")
        )
        {
            $img = \yii\helpers\Html::tag(
                "img",
                "Страницы",
                [
                    "style" => "width: 100px; margin-bottom: 10px",
                    "src" => "{$uploadsPath['domen']}{$path}"
                ]
            );
            echo $img;
        }
    }
    ?>
    <?= $form->field($model, 'image_file')->fileInput()->label(false) ?>

    <?php if (!isset($model->show)) $model->show = true ?>
    <?= $form->field($model, 'show')->checkbox(['checked' => true]) ?>

    <?php /*
    <?php if (!isset($model->fix)) $model->fix = true ?>
    <?= $form->field($model, 'fix')->checkbox(['checked' => true]) ?>
    */ ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>