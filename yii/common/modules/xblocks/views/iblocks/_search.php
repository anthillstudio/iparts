<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\modules\xblocks\models\IblocksSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="iblocks-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'insert_date') ?>

    <?= $form->field($model, 'update_date') ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'box') ?>

    <?php // echo $form->field($model, 'title_lng') ?>

    <?php // echo $form->field($model, 'content') ?>

    <?php // echo $form->field($model, 'tpl') ?>

    <?php // echo $form->field($model, 'type') ?>

    <?php // echo $form->field($model, 'image_file') ?>

    <?php // echo $form->field($model, 'show') ?>

    <?php // echo $form->field($model, 'level') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
