<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;



use common\components\Ant;
use common\modules\xblocks\controllers\XblocksController;

$this->title = 'Информационные блоки';
$this->params['breadcrumbs'][] = $this->title;

?>

<style>
.backbttn{
    padding: 7px 20px;
    margin-bottom: 20px;
    display: inline-block;
    color: #000;
    background-color: #fff
}
</style>

    <script>
        function moveRecord(el){
            var val = $("#jumpElement"+el).val();
            if (val==0){
                alert("ID не указан!");
            }else{
                if (confirm("Вы действительно хотите переместить элемент?")){
                    location.href='/xblocks/iblocks/sort?action=jumpelement&val='+val+'&el='+el;
                }
            }
        }
    </script>

<div class="iblocks-index">





    <?php
    $modparent = "";
    if (isset($_GET['modparent'])) $modparent = "?parent=".$_GET['modparent'];
    ?>
    <a class="backbttn" href="/<?=$parentModel['model'].$modparent?>"><?=$parentModel['title_lng']?></a>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'options' => ['style'=>'white-space: normal'],
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'id',
                'options' => ['style'=>'width: 10px'],
                'filter' => false
            ],
            [
                'attribute' => 'level',
                'options' => ['style'=>'width: 10px'],
                'filter' => false
            ],
            [
                'attribute' => 'IMG',
                'format' => 'raw',
                'options' => ['style'=>'width: 70px'],
                'value' => function ($models) {
                    $toReturn = "";
                    $uploaderInfo = Ant::uploaderInfo();
                    $path = "/{$models->config['dir']}/{$models->image_file}";
                    if (file_exists("../{$uploaderInfo['dir']}{$path}") && trim($models->image_file)!='') {
                        $toReturn .= Html::img(Url::to("{$uploaderInfo['domen']}{$path}"), [
                            'alt' => '',
                            'style' => 'width:100px;'
                        ]);
                    }else{
                        $toReturn .= '-';
                    }
                    return $toReturn;
                },
            ],
            [
                'attribute' => 'title_lng',
                'options' => ['style'=>'width: 250px'],
            ],
            'level',
            'content',
            [
                'format' => 'raw',
                'options' => ['style'=>'width: 100px'],
                'value' => function ($model) {

                    $modparent = "";
                    if (isset($_GET['modparent'])) $modparent = "&modparent=".$_GET['modparent'];
                    return \yii\helpers\Html::tag(
                        'a',
                        "Обновить",
                        [
                            "class" => "label label-" . ('success'),
                            "href" => "/xblocks/iblocks/update?model={$_GET['model']}&id={$model->id}&parent={$_GET['parent']}{$modparent}"
                        ]
                    );
                },
            ],
            [
                'format' => 'raw',
                'options' => ['style'=>'width: 100px'],
                'value' => function ($model) {
                    $modparent = "";
                    if (isset($_GET['modparent'])) $modparent = "&modparent=".$_GET['modparent'];
                    return \yii\helpers\Html::tag(
                        'a',
                        "Изображения",
                        [
                            'class' => 'btn btn-sm mybutton',
                            "href" => "/xblocks/imgblocks/?model=iblocks&xmodel={$_GET['model']}&parent={$model->id}{$modparent}"
                        ]
                    );
                },
            ],
            [
                'attribute' => '',
                'format' => 'raw',
                'options' => ['style'=>'width: 70px'],
                'value' => function ($model, $key, $index, $column) {
                    $toReturn = "<input style='width: 50px' id='jumpElement{$model->id}' type='text'><a href=\"javascript:moveRecord('{$model->id}')\"> >> </a>";
                    return $toReturn;
                },
            ],
            [
                'format' => 'raw',
                'options' => ['style'=>'width: 100px'],
                'value' => function ($model) {
                    $modparent = "";
                    if (isset($_GET['modparent'])) $modparent = "&modparent=".$_GET['modparent'];
                    return Html::beginForm(["delete?model={$_GET['model']}&id={$model->id}&parent={$_GET['parent']}{$modparent}"], 'post').
                    Html::submitButton('Удалить',
                        [
                            'class' => 'btn btn-sm',
                            'style' => 'display: inline-block',
                            'data' => ['confirm' => 'Вы действительно хотите?']
                        ]
                    ).
                    Html::endForm();
                },
            ],

        ],
    ]); ?>
    </form>
    <?php
    $modparent = "";
    if (isset($_GET['modparent'])) $modparent = "&modparent=".$_GET['modparent'];
    ?>
    <a class="btn btn-success" href="create?model=<?=$_GET['model']?>&parent=<?=$_GET['parent']?><?=$modparent?>">Создать</a>

</div>