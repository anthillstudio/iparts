<?php

// Default (текст + изображение)

use common\components\Ant;

?>

<?php if (trim($element['js'])!='') echo "<script>{$element['js']}</script>"; ?>
<?php if (trim($element['styles'])!='') echo "<style>{$element['styles']};</style>"; ?>

<div class="ib-item-wrapper ibtpl<?= $element['tpl'] ?> txt-align-<?=$element['text_align']?> tpl-style<?=$element['tpl_style']?>" id="ib<?=$element['id']?>">

        <?php

        $uploadsPath = Ant::uploaderInfo();
        $path = "/xblocks/info/{$element['image_file']}";


        if (trim($element['image_file'])!='' && file_exists("../{$uploadsPath['dir']}{$path}")) {
            $imgbox .= "<img src='{$uploadsPath['domen']}{$path}'>";
        }

        ?>


                <div id="ib-item<?= $element['id'] ?>" class="ib-item ib-item-txt-position<?= $element['text_position'] ?>">
                    <?=$imgbox?>
                    <?php if($element['title_type']==1){ ?>
                    <div class="FNTC ib-item-title ib-item-title-type<?= $element['title_type'] ?>"><?= $element['title_lng'] ?></div>
                    <?php } ?>
                    <div class="FNTC ib-item-content"><?=$element['content']?></div>
                </div><!-- ib-item -->



</div><!-- ib-wrapper -->