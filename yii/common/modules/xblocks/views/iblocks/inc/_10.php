<div class="ib-contacts">
    <div class="ib-contacts-info-line">
        <h3>ИНФОЛИНИЯ:</h3>
        <ul>
            <li>Тел.: <a href="tel:8 017 306-02-02">8 017 306-02-02</a></li>
            <li><b>226</b> – С МОБИЛЬНЫХ <i>(ПОНЕДЕЛЬНИК – ПЯТНИЦА С 9-00 ДО 19-00)</i></li>
        </ul>
    </div><!-- ib-contacts-info-line -->
    <div class="ib-contacts-links">
        <ul>
            <li><a href="/bank/contacts/cbu">Адреса центров банковских услуг</a></li>
            <li><a href="/bank/contacts/obratnaya-svyaz">Отправить сообщение</a></li>
        </ul>
    </div><!-- ib-contacts-lnks -->
</div>