<?php

// Default (текст + изображение)

use common\components\Ant;

?>
<?php
$fix = "rubber";
if ($element['fix']==1){
    //$fix = "fix";
}

?>

<?php if (trim($element['js'])!='') echo "<script>{$element['js']}</script>"; ?>
<?php if (trim($element['styles'])!='') echo "<style>{$element['styles']};</style>"; ?>

<div class="ib-item-wrapper ibtpl<?= $element['tpl'] ?> txt-align-<?=$element['text_align']?>" id="ib<?=$element['id']?>">
    <div class="<?=$fix?>">
    <?php


    $uploadsPath = Ant::uploaderInfo();
    $path = "/xblocks/info/{$element['image_file']}";


    if (trim($element['image_file'])!='' && file_exists("../{$uploadsPath['dir']}{$path}")) {
        $imgbox = "<div class='ib-item-img'>";
        $imgbox .= "<img src='{$uploadsPath['domen']}{$path}'>";
        $imgbox .= "</div><!-- ib-item-img -->";
    }


    if (count($images)!=0){
        $imgbox = "<div class='ib-item-img'>";
        $imgbox .= "<div class='swiper-container' id='ibimgsbox{$element['id']}'>";
        $imgbox .= "<div class='swiper-wrapper'>";



        foreach ($images as $key=>$value){
            $imgbox .= "<div class='swiper-slide'>";
            $path = "/xblocks/info/{$element['image_file']}";
            $imgbox .= "<div class=''><img src='{$uploadsPath['domen']}{$path}'></div>";
            $imgbox .= "</div><!-- swiper-slide -->";
        }
        $imgbox .= "</div><!-- swiper-wrapper -->";
        $imgbox .= "</div><!-- swiper-container -->";
        $imgbox .= "<div id='ibimgsbox-pagination{$element['id']}' class='swiper-pagination'></div>";
        $imgbox .= "<script>";
        $imgbox .= "var swiper = new Swiper('#ibimgsbox{$element['id']}', {";
        $imgbox .= "pagination: '#ibimgsbox-pagination{$element['id']}',";
        $imgbox .= "slidesPerView: 1,";
        $imgbox .= "spaceBetween: 0,";
        $imgbox .= "paginationClickable: true,";
        $imgbox .= "nextButton: '#swiper-button-next',";
        $imgbox .= "prevButton: '#swiper-button-prev',";
        $imgbox .= "freeMode: true";
        $imgbox .= "});";
        $imgbox .= "</script>";
        $imgbox .= "</div><!-- ib-item-img -->";
    }


    $content = explode('~~',$element['content']);

    switch($element['text_position']){
        case "1": // Слева
            ?>

            <div id="ib-item<?= $element['id'] ?>" class="ib-item ib-item-txt-position<?= $element['text_position'] ?> tpl-style<?= $element['tpl_style'] ?>">
                <div class="ib-item-info">
                    <?php if ($element['title_type']==1){ ?>
                    <div class="FNTC ib-item-title ib-item-title-type<?= $element['title_type'] ?>"><?= $element['title_lng'] ?></div>
                    <?php } ?>
                    <div class="FNTC ib-item-content">
                        <div id="swiper-container-modbox<?=$element['id']?>" class="swiper-container">
                            <div class="swiper-wrapper">
                                <?php foreach ($content as $item) { ?>
                                <div class="swiper-slide"><?=$item?></div>
                                <?php } ?>
                            </div><!-- swiper-wrapper -->
                        </div><!-- swiper-container -->
                        <?php if (count($content)>1){ ?>
                        <div id="swiper-modbox<?=$element['id']?>-pagination" class="swiper-pagination"></div>
                        <?php } ?>
                        <?php if (trim($element['link'])!=''){ ?>
                            <div class="ib-more"><a href="<?=$element['link']?>">Подробнее</a></div>
                        <?php } ?>
                    </div><!-- ib-item-content -->
                </div><!-- ib-item-info -->
                <?= $imgbox ?>
            </div><!-- ib-item -->

            <?php
            break;
        case "2": // Справа
            ?>

            <div class="ib-item ib-item-txt-position<?= $element['text_position'] ?> tpl-style<?= $element['tpl_style'] ?>">
                <?= $imgbox ?>
                <div class="ib-item-info">
                    <?php if ($element['title_type']==1){ ?>
                    <div class="FNTC ib-item-title ib-item-title-type<?= $element['title_type'] ?>"><?= $element['title_lng'] ?></div>
                    <?php } ?>
                    <div class="FNTC ib-item-content">
                        <div id="swiper-container-modbox<?=$element['id']?>" class="swiper-container">
                            <div class="swiper-wrapper">
                                <?php foreach ($content as $item){ ?>
                                <div class="swiper-slide"><?=trim($item)?></div>
                                <?php } ?>
                            </div><!-- swiper-wrapper -->
                        </div><!-- swiper-container -->
                        <?php if (count($content)>1){ ?>
                            <div id="swiper-modbox<?=$element['id']?>-pagination" class="swiper-pagination"></div>
                        <?php } ?>
                        <?php if (trim($element['link'])!=''){ ?>
                            <div class="ib-more"><a href="<?=$element['link']?>">Подробнее</a></div>
                        <?php } ?>
                    </div><!-- ib-item-content -->
                </div><!-- ib-item-info -->
            </div><!-- ib-item -->

            <?php
            break;
        case "3": // Снизу
            ?>
            <div class="ib-item ib-item-txt-position<?= $element['text_position'] ?> tpl-style<?= $element['tpl_style'] ?>">
                <?= $imgbox ?>
                <div class="ib-item-info">
                    <?php if ($element['title_type']==1){ ?>
                    <div class="FNTC ib-item-title ib-item-title-type<?= $element['title_type'] ?>"><?= $element['title_lng'] ?></div>
                    <?php } ?>
                    <div class="FNTC ib-item-content">
                        <div id="swiper-container-modbox<?=$element['id']?>" class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide"><?=$element['content']?></div>
                            </div><!-- swiper-wrapper -->
                        </div><!-- swiper-container -->
                        <?php if (count($content)>1){ ?>
                            <div id="swiper-modbox<?=$element['id']?>-pagination" class="swiper-pagination"></div>
                        <?php } ?>
                        <?php if (trim($element['link'])!=''){ ?>
                            <div class="ib-more"><a href="<?=$element['link']?>">Подробнее</a></div>
                        <?php } ?>
                    </div><!-- ib-item-content -->
                </div><!-- ib-item-info -->
            </div><!-- ib-item -->

            <?php
            break;
        case "4": // Сверху
            ?>

            <div class="ib-item ib-item-txt-position<?= $element['text_position'] ?> tpl-style<?= $element['tpl_style'] ?>">
                <div class="ib-item-info">
                    <?php if ($element['title_type']==1){ ?>
                    <div class="FNTC ib-item-title ib-item-title-type<?= $element['title_type'] ?>"><?= $element['title_lng'] ?></div>
                    <?php } ?>
                    <div class="FNTC ib-item-content">
                        <div id="swiper-container-modbox<?=$element['id']?>" class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide"><?=$element['content']?></div>
                            </div><!-- swiper-wrapper -->
                        </div><!-- swiper-container -->
                        <?php if (count($content)>1){ ?>
                            <div id="swiper-modbox<?=$element['id']?>-pagination" class="swiper-pagination"></div>
                        <?php } ?>
                        <?php if (trim($element['link'])!=''){ ?>
                        <div class="ib-more"><a href="<?=$element['link']?>">Подробнее</a></div>
                        <?php } ?>
                    </div><!-- ib-item-content -->
                </div><!-- ib-item-info -->
                <?= $imgbox ?>
            </div><!-- ib-item -->

            <?php
            break;
        case "5": // Сверху
            ?>

            <div class="ib-item ib-item-txt-position<?= $element['text_position'] ?> tpl-style<?= $element['tpl_style'] ?>">
                <?= $imgbox ?>
                <div class="ib-item-info">
                    <div class="ib-item-info-box">
                    <?php if ($element['title_type']==1){ ?>
                    <div class="FNTC ib-item-title ib-item-title-type<?= $element['title_type'] ?>"><?= $element['title_lng'] ?></div>
                    <?php } ?>
                    <div class="FNTC ib-item-content">
                        <div id="swiper-container-modbox<?=$element['id']?>" class="swiper-container">
                            <div class="swiper-wrapper">
                                <?php foreach ($content as $item) { ?>
                                    <div class="swiper-slide"><?=$item?></div>
                                <?php } ?>
                            </div><!-- swiper-wrapper -->
                        </div><!-- swiper-container -->
                        <?php if (count($content)>1){ ?>
                            <div id="swiper-modbox<?=$element['id']?>-pagination" class="swiper-pagination"></div>
                        <?php } ?>
                        <?php if (trim($element['link'])!=''){ ?>
                            <div class="ib-more"><a href="<?=$element['link']?>">Подробнее</a></div>
                        <?php } ?>
                    </div><!-- ib-item-content -->
                    </div><!-- ib-item-info-box -->
                </div><!-- ib-item-info -->
            </div><!-- ib-item -->

            <?php
            break;
    }
    ?>

            <script>
                var swiper = new Swiper('#swiper-container-modbox<?=$element['id']?>', {
                    pagination: '#swiper-modbox<?=$element['id']?>-pagination',
                    paginationClickable: true,
                    nextButton: '#swiper-modbox<?=$element['id']?>-button-next',
                    prevButton: '#swiper-modbox<?=$element['id']?>-button-prev',
                    autoplay: 5000,
                    speed: 300,
                    slidesPerView: 1,
                    spaceBetween: 0,
                });
            </script>

    </div>
</div><!-- ib-wrapper -->