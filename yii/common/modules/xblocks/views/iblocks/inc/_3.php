<?php

// Матрица изображений (текст + матрица изображений)

use common\components\Ant;
use common\modules\xblocks\models\Imgblocks;
$uploadsPath = Ant::uploaderInfo();

$toReturn = "";
$images = Imgblocks::find()
    ->where(['parent'=>$element['id']])
    ->andWhere(['model'=>'iblocks'])
    ->all();

?>

<style>
    .img-matrix{text-align: center}
    .img-matrix-item{
        width: 250px; height: 250px; border: 1px solid #fff; display: inline-block; vertical-align: top;
        padding: 10px; box-sizing: border-box; margin: 10px;
        -webkit-box-shadow: 0px 0px 12px -2px rgba(0,0,0,0.5);
        -moz-box-shadow: 0px 0px 12px -2px rgba(0,0,0,0.5);
        box-shadow: 0px 0px 12px -2px rgba(0,0,0,0.5);
    }
    .img-matrix-item img{display: block; max-width: 100%; max-height: 100%}
</style>

<?php
$imgbox = $this->render('@common/modules/xblocks/views/imgblocks/inc/_matrix.php', ['images'=>$images,'element'=>$element]);
?>

<?php
switch($element['text_position']){
    case "1": // Слева
        ?>

        <div id="ib-item<?= $element['id'] ?>" class="ib-item ib-item-txt-position<?= $element['text_position'] ?> tpl-style<?= $element['tpl_style'] ?>">
            <div class="ib-item-info">
                <?php if ($element['title_type']==1){ ?>
                    <div class="FNTC ib-item-title ib-item-title-type<?= $element['title_type'] ?>"><?= $element['title_lng'] ?></div>
                <? } ?>
                <div class="FNTC ib-item-content"><?= $element['content'] ?></div>
            </div><!-- ib-item-info -->
            <?= $imgbox ?>
        </div><!-- ib-item -->

        <?php
        break;
    case "2": // Справа
        ?>

        <div class="ib-item ib-item-txt-position<?= $element['text_position'] ?> tpl-style<?= $element['tpl_style'] ?>">
            <?= $imgbox ?>
            <div class="ib-item-info">
                <?php if ($element['title_type']==1){ ?>
                    <div class="FNTC ib-item-title ib-item-title-type<?= $element['title_type'] ?>"><?= $element['title_lng'] ?></div>
                <? } ?>
                <div class="FNTC ib-item-content"><?= $element['content'] ?></div>
            </div><!-- ib-item-info -->
        </div><!-- ib-item -->

        <?php
        break;
    case "3": // Снизу
        ?>

        <div class="ib-item ib-item-txt-position<?= $element['text_position'] ?> tpl-style<?= $element['tpl_style'] ?>">
            <?= $imgbox ?>
            <div class="ib-item-info">
                <?php if ($element['title_type']==1){ ?>
                    <div class="FNTC ib-item-title ib-item-title-type<?= $element['title_type'] ?>"><?= $element['title_lng'] ?></div>
                <? } ?>
                <div class="FNTC ib-item-content"><?= $element['content'] ?></div>
            </div><!-- ib-item-info -->
        </div><!-- ib-item -->

        <?php
        break;
    case "4": // Сверху
        ?>

        <div class="ib-item ib-item-txt-position<?= $element['text_position'] ?> tpl-style<?= $element['tpl_style'] ?>">
            <div class="ib-item-info">
                <?php if ($element['title_type']==1){ ?>
                    <div class="FNTC ib-item-title ib-item-title-type<?= $element['title_type'] ?>"><?= $element['title_lng'] ?></div>
                <? } ?>
                <div class="FNTC ib-item-content"><?= $element['content'] ?></div>
            </div><!-- ib-item-info -->
            <?= $imgbox ?>
        </div><!-- ib-item -->

        <?php
        break;
}
?>
