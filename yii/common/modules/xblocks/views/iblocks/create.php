<?php

use yii\helpers\Html;

$this->title = 'Добавить информационный блок';
$this->params['breadcrumbs'][] = ['label' => 'Iblocks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="iblocks-create">

    <?= $this->render('_form', [
        'model' => $model,
        'parentModel' => $parentModel
    ]) ?>

</div>
