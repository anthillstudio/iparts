<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\modules\xblocks\models\Iblocks */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Iblocks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="iblocks-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'insert_date',
            'update_date',
            'user_id',
            'box',
            'title_lng',
            'content:ntext',
            'tpl',
            'type',
            'image_file',
            'show',
            'level',
        ],
    ]) ?>

</div>
