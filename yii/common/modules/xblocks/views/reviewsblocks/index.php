<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ReviewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Отзывы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reviews-index">


    <style>
        .backbttn{
            padding: 7px 20px;
            margin-bottom: 20px;
            display: inline-block;
            color: #000;
            background-color: #fff
        }
    </style>


    <?php
    $modparent = "";
    if (isset($_GET['modparent'])) $modparent = "?parent=".$_GET['modparent'];
    ?>
    <a class="backbttn" href="/<?=$parentModel['model'].$modparent?>"><?=$parentModel['title_lng']?></a>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'options' => ['style'=>'white-space: normal'],
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            //'id',
            //'insert_date',
            //'update_date',
            //'model',
            //'parent',
              'name',
            // 'email:email',
            // 'phone',
            // 'answer_date',
            // 'user_id',
             'review:ntext',
             'answer:ntext',
            // 'show',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php
    $modparent = "";
    if (isset($_GET['modparent'])) $modparent = "&modparent=".$_GET['modparent'];
    ?>
    <a class="btn btn-success" href="create?model=<?=$_GET['model']?>&parent=<?=$_GET['parent']?><?=$modparent?>">Создать</a>

</div>
