<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;



use common\components\Ant;
use common\modules\xblocks\controllers\XblocksController;

$this->title = 'Файлы';
$this->params['breadcrumbs'][] = $this->title;

?>

<style>
.backbttn{
    padding: 7px 20px;
    margin-bottom: 20px;
    display: inline-block;
    color: #000;
    background-color: #fff
}
</style>

    <script>
        function moveRecord(el){
            var val = $("#jumpElement"+el).val();
            if (val==0){
                alert("ID не указан!");
            }else{
                if (confirm("Вы действительно хотите переместить элемент?")){
                    location.href='/xblocks/xfiles/sort?action=jumpelement&val='+val+'&el='+el;
                }
            }
        }
    </script>

<div class="iblocks-index">



    <?php
    $modparent = "";
    if (isset($_GET['modparent'])) $modparent = "?parent=".$_GET['modparent'];

    ?>
    <a class="backbttn" href="/<?=$parentModel['model'].$modparent?>"><?=$parentModel['title_lng']?></a>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'options' => ['style'=>'white-space: normal'],
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'id',
                'options' => ['style'=>'width: 10px'],
                'filter' => false
            ],
            [
                'attribute' => 'level',
                'options' => ['style'=>'width: 10px'],
                'filter' => false
            ],
            [
                'attribute' => 'title_lng',
                'options' => ['style'=>'width: 250px'],
            ],
            [
                'format' => 'raw',
                'label' => 'Ссылка',
                'attribute' => 'link',
                'options' => ['style'=>'width: 300px'],
                'value' => function ($model) {
                    return ($model->link==''?'':$model->link);
                },
            ],
            [
                'format' => 'raw',
                'label' => 'Ссылка',
                'attribute' => 'link',
                'options' => ['style'=>'width: 300px'],
                'value' => function ($model) {
                    $domen = Yii::$app->params['uploader']['domen'];
                    return "<input type='text' style='width: 100%' value='/xblocks/files/{$model->file}'>";
                },
            ],
            [
                'format' => 'raw',
                'options' => ['style'=>'width: 100px'],
                'value' => function ($model) {

                    $modparent = "";
                    if (isset($_GET['modparent'])) $modparent = "&modparent=".$_GET['modparent'];
                    return \yii\helpers\Html::tag(
                        'a',
                        "Обновить",
                        [
                            "class" => "label label-" . ('success'),
                            "href" => "/xblocks/xfiles/update?model={$_GET['model']}&id={$model->id}&parent={$_GET['parent']}{$modparent}"
                        ]
                    );
                },
            ],
            [
                'format' => 'raw',
                'options' => ['style'=>'width: 100px'],
                'value' => function ($model) {
                    $modparent = "";
                    if (isset($_GET['modparent'])) $modparent = "&modparent=".$_GET['modparent'];
                    return Html::beginForm(["delete?model={$_GET['model']}&id={$model->id}&parent={$_GET['parent']}{$modparent}"], 'post').
                    Html::submitButton('Удалить',
                        [
                            'class' => 'btn btn-sm',
                            'style' => 'display: inline-block',
                            'data' => ['confirm' => 'Вы действительно хотите?']
                        ]
                    ).
                    Html::endForm();
                },
            ],

        ],
    ]); ?>
    </form>
    <?php
    $modparent = "";
    if (isset($_GET['modparent'])) $modparent = "&modparent=".$_GET['modparent'];
    ?>
    <a class="btn btn-success" href="create?model=<?=$_GET['model']?>&parent=<?=$_GET['parent']?><?=$modparent?>">Создать</a>

</div>