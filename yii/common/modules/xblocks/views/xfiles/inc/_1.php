<?php
use common\components\Ant;
?>
<?php if (count($elements)!=0){ ?>
<ul class="add-files-box FNTC">
<?php foreach ($elements as $key=>$value){ ?>
    <?php
    $path = "/xblocks/files/{$value['file']}";
    $fileType = strtolower(end(explode(".", $value['file'])));
    if (trim($value['link'])!=''){
        $path = $value['link'];
        $fileType = strtolower(end(explode(".", $value['link'])));
    }
    $fileSize = round(filesize("../cloud".$path)/1024,1) . " Kb";
    ?>
    <li class="add-files-item file-type<?=$fileType?>"><a href="<?=Yii::$app->params['uploader']['domen']?><?=$path?>?r=<?=rand(1,100)?>" target="_blank"><?=$value['title_lng']?> (.<?=$fileType?>, <?=$fileSize?>)</a></li>
<?php } ?>
</ul>
<?php } ?>

