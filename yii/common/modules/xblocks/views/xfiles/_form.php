<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use common\components\Ant;
use vova07\imperavi\Widget;
use common\modules\xblocks\models\Xfiles;
$filesObject = new Xfiles();

?>

<div class="iblocks-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'model')->hiddenInput(['value' => $_GET['model']])->label(false) ?>

    <?= $form->field($model, 'parent')->hiddenInput(['value' => $_GET['parent']])->label(false) ?>

    <?= $form->field($model, 'modparent')->hiddenInput(['value' => $_GET['modparent']])->label(false) ?>

    <?= $form->field($model, 'insert_date')->hiddenInput(['value' => '0000-00-00 00:00:00'])->label(false) ?>

    <?= $form->field($model, 'update_date')->hiddenInput(['value' => '0000-00-00 00:00:00'])->label(false) ?>

    <?= $form->field($model, 'user_id')->hiddenInput(['value'=>1])->label(false) ?>

    <?= $form->field($model, 'title_lng')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'link')->textInput(['maxlength' => true]) ?>

    <?php
    if (!$model->isNewRecord){
        $uploadsPath = Ant::uploaderInfo();
        $path = "/xblocks/files/{$model->file}";
        if (
            trim($model->file)!='' &&
            file_exists("../{$uploadsPath['dir']}{$path}")
        )
        {
            echo "<div style='display: inline-block; padding: 10px 20px; border: 1px solid #555; margin: 10px 0px;'>{$uploadsPath['domen']}{$path}</div>";
        }
    }
    ?>
    <?= $form->field($model, 'file')->fileInput()->label(false) ?>

    <?php if (!isset($model->show)) $model->show = true ?>
    <?= $form->field($model, 'show')->checkbox(['checked' => true]) ?>



    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>