<?php

use yii\helpers\Html;

$this->title = 'Файл';
$this->params['breadcrumbs'][] = ['label' => 'Files', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="iblocks-create">

    <?= $this->render('_form', [
        'model' => $model,
        'parentModel' => $parentModel
    ]) ?>

</div>
