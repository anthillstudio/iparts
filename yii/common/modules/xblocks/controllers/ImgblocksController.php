<?php

namespace common\modules\xblocks\controllers;

use Yii;
use common\components\Ant;
use common\modules\xblocks\models\Imgblocks;
use common\modules\xblocks\models\ImgblocksSearch;
use common\modules\xblocks\controllers\XblocksController;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use yii\imagine\Image;
use yii\web\UploadedFile;

/**
 * ImgblocksController implements the CRUD actions for Imgblocks model.
 */
class ImgblocksController extends Controller
{

    public $parentModel;

    public function init(){
        $this->parentModel = XblocksController::getParentModel();
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    /**
     * Сортировка элементов
     * перемещение
     */
    public function actionSort(){
        LElement::moveAfterIdRecord(new Iblocks);
        return $this->redirect(["/xblocks/imgblocks"]);
    }
    /**
     * Lists all Imgblocks models.
     * @return mixed
     */
    public function actionIndex()
    {

        $searchModel = new ImgblocksSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'parentModel' => $this->parentModel
        ]);

    }


    /**
     * Displays a single Imgblocks model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Imgblocks model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {

        $model = new Imgblocks();
        $post = Yii::$app->request->post();

        if ($model->load(Yii::$app->request->post())) {

            $model->update_date = date("Y-m-d H:i:00");
            if ($model->image_file == '') unset($model->image_file);

            $imageFile = UploadedFile::getInstance($model, 'image_file');
            if (isset($imageFile)){
                $uploaderInfo = Ant::uploaderInfo();
                $model->image_file = 'EL'.Ant::buildFileName($model->id,$imageFile);
                if ($model->validate()) {
                    $path = "../{$uploaderInfo['dir']}/{$model->config['dir']}/";
                    $imageFile->saveAs("{$path}{$model->image_file}");
                    Image::thumbnail("{$path}{$model->image_file}", 400, 400)
                        ->save(Yii::getAlias("{$path}m{$model->image_file}"), ['quality' => 100]);
                }
            }
            $model->level = $model->getMaxLevel()+10;
            $model->save();

            $modparent = "";
            $xmodel = "";
            if ($_POST['Imgblocks']['modparent']!=''){
                $modparent = "&modparent=".$_POST['Imgblocks']['modparent'];
            }
            if ($_POST['Imgblocks']['xmodel']!=''){
                $xmodel = "&xmodel=".$_POST['Imgblocks']['xmodel'];
            }

            return $this->redirect(["/xblocks/imgblocks/?model={$model->model}{$xmodel}&parent={$model->parent}{$modparent}"]);

        } else {
            return $this->render('create', [
                'model' => $model,
                'parentModel' => $this->parentModel
            ]);
        }
    }

    /**
     * Updates an existing Imgblocks model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $post = Yii::$app->request->post();

        if ($model->load(Yii::$app->request->post())) {

            $model->update_date = date("Y-m-d H:i:00");
            if ($model->image_file == '') unset($model->image_file);

            $imageFile = UploadedFile::getInstance($model, 'image_file');
            if (isset($imageFile)){
                $uploaderInfo = Ant::uploaderInfo();
                $model->image_file = 'EL'.Ant::buildFileName($model->id,$imageFile);
                if ($model->validate()) {
                    $path = "../{$uploaderInfo['dir']}/{$model->config['dir']}/";
                    $imageFile->saveAs("{$path}{$model->image_file}");
                    Image::thumbnail("{$path}{$model->image_file}", 400, 400)
                        ->save(Yii::getAlias("{$path}m{$model->image_file}"), ['quality' => 100]);
                }
            }

            $model->save();

            $modparent = "";
            $xmodel = "";
            if ($_POST['Imgblocks']['modparent']!=''){
                $modparent = "&modparent=".$_POST['Imgblocks']['modparent'];
            }
            if ($_POST['Imgblocks']['xmodel']!=''){
                $xmodel = "&xmodel=".$_POST['Imgblocks']['xmodel'];
            }

            return $this->redirect(["/xblocks/imgblocks/?model={$model->model}{$xmodel}&parent={$model->parent}{$modparent}"]);

        } else {
            return $this->render('update', [
                'model' => $model,
                'parentModel' => $this->parentModel
            ]);
        }
    }

    /**
     * Deletes an existing Imgblocks model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Imgblocks model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Imgblocks the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Imgblocks::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
