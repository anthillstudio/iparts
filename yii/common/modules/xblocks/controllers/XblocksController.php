<?php

namespace common\modules\xblocks\controllers;

use Yii;
use common\components\Ant;
use yii\web\Controller;

class XblocksController extends Controller
{


    public function init(){

    }

    public function getParentModelByUrlX(){

        // Получаем информацию о родительском элементе

        $toReturn = [];
        if (isset($_GET['xpar'])) {

            $xpar = explode('-', $_GET['xpar']);
            $element = [];
            switch ($xpar[0]) {
                default:
                    $element = Yii::$app->db->createCommand("
                      SELECT *
                      FROM {{%{$xpar[0]}}}
                      WHERE `id` = '{$xpar[1]}'
                      ORDER BY `id` DESC
                    ")->queryOne();
                    break;
            }

            $toReturn = [
                'parent' => $xpar[1],
                'model' => $xpar[0],
                'url' => $_GET['xurl'],
                'title' => $element['title_lng']
            ];

        }

        return $toReturn;
    }

    public function getParentModel(){

        $element = [];

        if (
            !empty($_GET['model']) &&
            !empty($_GET['parent'])
        ){

            $element = Yii::$app->db->createCommand("
            SELECT *
            FROM {{%{$_GET['model']}}}
            WHERE `id` = '{$_GET['parent']}'
            ORDER BY `id` DESC
            ")->queryOne();
            $element['model'] = $_GET['model'];
        }

        return $element;
    }

    function urlToSTR($url){

        // Преобразуем ссылку в строку для передачи по GET

        $toReturn = "url-{$url}";
        if (isset($_GET['parent'])) {
            $toReturn .= "~parent-{$_GET['parent']}";
        }
        return $toReturn;
    }

    function strToURL($url){

        // Преобразуем строку в ссылку для перехода к модели

        $arr = [];
        foreach (explode('~',$url['url']) as $key=>$value){
            $urlarr = explode('-',$value);
            $arr[$urlarr[0]] = $urlarr[1];
        }

        $toReturn = "{$arr['url']}";
        if (isset($arr['parent'])) $toReturn .= "?parent={$arr['parent']}";

        return $toReturn;
    }


}
