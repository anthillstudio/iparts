<?php

namespace common\modules\xblocks\controllers;

use Yii;
use common\components\Ant;
use common\components\LElement;
use common\modules\xblocks\models\Iblocks;
use common\modules\xblocks\models\IblocksSearch;
use common\modules\xblocks\controllers\XblocksController;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use yii\imagine\Image;
use yii\web\UploadedFile;

/**
 * IblocksController implements the CRUD actions for Iblocks model.
 */
class IblocksController extends Controller
{

    public $parentModel;

    public function init(){
        $this->parentModel = XblocksController::getParentModel();
    }

    public function actions()
    {
        return [
            'image-upload' => [
                'class' => 'vova07\imperavi\actions\UploadAction',
                'url' => Yii::$app->params['uploader']['domen'].'/imperavi/',
                'path' => '../cloud/imperavi/'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Сортировка элементов
     * перемещение
     */
    public function actionSort(){
        LElement::moveAfterIdRecord(new Iblocks);
        return $this->redirect(["/xblocks/iblocks"]);
    }

    /**
     * Lists all Iblocks models.
     * @return mixed
     */
    public function actionIndex()
    {

        $searchModel = new IblocksSearch();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'parentModel' => $this->parentModel
        ]);
    }

    /**
     * Displays a single Iblocks model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Iblocks model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {

        $model = new Iblocks();

        if ($model->load(Yii::$app->request->post())) {

            $model->update_date = date("Y-m-d H:i:00");
            if ($model->image_file == '') unset($model->image_file);


            $model->save();

            $imageFile = UploadedFile::getInstance($model, 'image_file');
            if (isset($imageFile)){
                $uploaderInfo = Ant::uploaderInfo();
                $model->image_file = 'EL'.Ant::buildFileName($model->id,$imageFile);
                if ($model->validate()) {
                    $path = "../{$uploaderInfo['dir']}/{$model->config['dir']}/";
                    $imageFile->saveAs("{$path}{$model->image_file}");
                    Image::thumbnail("{$path}{$model->image_file}", 400, 400)
                        ->save(Yii::getAlias("{$path}m{$model->image_file}"), ['quality' => 100]);
                }
            }

            $model->level = $model->getMaxLevel()+10;
            $model->save();

            $modparent = "";
            if ($_POST['Iblocks']['modparent']!=''){
                $modparent = "&modparent=".$_POST['Iblocks']['modparent'];
            }

            return $this->redirect(["/xblocks/iblocks/?model={$model->model}&parent={$model->parent}{$modparent}"]);

        } else {
            return $this->render('create', [
                'model' => $model,
                'parentModel' => $this->parentModel
            ]);
        }
    }

    /**
     * Updates an existing Iblocks model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $post = Yii::$app->request->post();

        if ($model->load(Yii::$app->request->post())) {


            $model->update_date = date("Y-m-d H:i:00");
            if ($model->image_file == '') unset($model->image_file);

            $imageFile = UploadedFile::getInstance($model, 'image_file');
            if (isset($imageFile)){
                $uploaderInfo = Ant::uploaderInfo();
                $model->image_file = 'EL'.Ant::buildFileName($model->id,$imageFile);
                if ($model->validate()) {
                    $path = "../{$uploaderInfo['dir']}/{$model->config['dir']}/";
                    $imageFile->saveAs("{$path}{$model->image_file}");
                    Image::thumbnail("{$path}{$model->image_file}", 400, 400)
                        ->save(Yii::getAlias("{$path}m{$model->image_file}"), ['quality' => 100]);
                }
            }

            $model->save();


            $modparent = "";
            if ($_POST['Iblocks']['modparent']!=''){
                $modparent = "&modparent=".$_POST['Iblocks']['modparent'];
            }

            return $this->redirect(["/xblocks/iblocks/?model={$model->model}&parent={$model->parent}{$modparent}"]);


        } else {
            return $this->render('update', [
                'model' => $model,
                //'ibObject' => $this->ibObject,
                'parentModel' => $this->parentModel
            ]);
        }
    }

    /**
     * Deletes an existing Iblocks model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        $modparent = "";
        if (isset($_GET['modparent'])) $modparent = "&modparent=".$_GET['modparent'];
        return $this->redirect(["/xblocks/iblocks/?model={$_GET['model']}&parent={$_GET['parent']}{$modparent}"]);
    }

    /**
     * Finds the Iblocks model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Iblocks the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Iblocks::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
