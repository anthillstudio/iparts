<?php

namespace common\modules\xblocks\controllers;

use Yii;
use common\modules\xblocks\models\Reviewsblocks;
use common\modules\xblocks\models\ReviewsblocksSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\modules\xblocks\controllers\XblocksController;

/**
 * ReviewsController implements the CRUD actions for Reviews model.
 */
class ReviewsblocksController extends Controller
{

    public $parentModel;

    public function init(){
        $this->parentModel = XblocksController::getParentModel();
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Reviews models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ReviewsblocksSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'parentModel' => $this->parentModel
        ]);
    }

    /**
     * Displays a single Reviews model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Reviews model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Reviewsblocks();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            $modparent = "";
            if ($_POST['Iblocks']['modparent']!=''){
                $modparent = "&modparent=".$_POST['Iblocks']['modparent'];
            }

            return $this->redirect(["/xblocks/reviewsblocks/?model={$model->model}&parent={$model->parent}{$modparent}"]);

        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Reviews model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            return $this->redirect(["/xblocks/reviewsblocks/?model={$model->model}&parent={$model->parent}{$modparent}"]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Reviews model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $el = $model;
        $model->delete();

        return $this->redirect(["/xblocks/reviewsblocks/?model={$el->model}&parent={$el->parent}"]);
    }

    /**
     * Finds the Reviews model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Reviews the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Reviewsblocks::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
