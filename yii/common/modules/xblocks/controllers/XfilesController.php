<?php

namespace common\modules\xblocks\controllers;

use Yii;
use common\components\Ant;
use common\components\LElement;
use common\modules\xblocks\models\Xfiles;
use common\modules\xblocks\models\XfilesSearch;
use common\modules\xblocks\controllers\XblocksController;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

//use yii\imagine\Image;
use yii\web\UploadedFile;

/**
 * IblocksController implements the CRUD actions for Iblocks model.
 */
class XfilesController extends Controller
{

    public $parentModel;

    public function init(){
        $this->parentModel = XblocksController::getParentModel();
    }

    public function actions()
    {
        return [
            'image-upload' => [
                'class' => 'vova07\imperavi\actions\UploadAction',
                'url' => Yii::$app->params['uploader']['domen'].'/imperavi/',
                'path' => '../cloud/imperavi/'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Сортировка элементов
     * перемещение
     */
    public function actionSort(){
        LElement::moveAfterIdRecord(new Xfiles);
        return $this->redirect(["/xblocks/files"]);
    }

    /**
     * Lists all Iblocks models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new XfilesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'parentModel' => $this->parentModel
        ]);
    }

    /**
     * Displays a single Iblocks model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Iblocks model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {

        $model = new Xfiles();

        if ($model->load(Yii::$app->request->post())) {

            $model->update_date = date("Y-m-d H:i:00");
            if ($model->file == '') unset($model->file);

            $model->save();

            $file = UploadedFile::getInstance($model, 'file');
            if (isset($file)){
                $uploaderInfo = Ant::uploaderInfo();
                $model->file = 'EL'.Ant::buildFileName($model->id,$file);
                if ($model->validate()) {
                    $path = "../{$uploaderInfo['dir']}/{$model->config['dir']}/";
                    $file->saveAs("{$path}{$model->file}");
                }
            }

            $model->level = $model->getMaxLevel()+10;
            $model->save();

            $modparent = "";
            if ($_POST['Xfiles']['modparent']!=''){
                $modparent = "&modparent=".$_POST['Xfiles']['modparent'];
            }

            return $this->redirect(["/xblocks/xfiles/?model={$model->model}&parent={$model->parent}{$modparent}"]);

        } else {
            return $this->render('create', [
                'model' => $model,
                'parentModel' => $this->parentModel
            ]);
        }
    }

    /**
     * Updates an existing Iblocks model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $post = Yii::$app->request->post();

        if ($model->load(Yii::$app->request->post())) {

            $model->update_date = date("Y-m-d H:i:00");
            if ($model->file == '') unset($model->file);

            $file = UploadedFile::getInstance($model, 'file');
            if (isset($file)){
                $uploaderInfo = Ant::uploaderInfo();
                $model->file = 'EL'.Ant::buildFileName($model->id,$file);
                if ($model->validate()) {
                    $path = "../{$uploaderInfo['dir']}/{$model->config['dir']}/";
                    $file->saveAs("{$path}{$model->file}");
                }
            }

            $model->save();

            $modparent = "";
            if ($_POST['Xfiles']['modparent']!=''){
                $modparent = "&modparent=".$_POST['Xfiles']['modparent'];
            }

            return $this->redirect(["/xblocks/xfiles/?model={$model->model}&parent={$model->parent}{$modparent}"]);


        } else {
            return $this->render('update', [
                'model' => $model,
                //'ibObject' => $this->ibObject,
                'parentModel' => $this->parentModel
            ]);
        }
    }

    /**
     * Deletes an existing Iblocks model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        $modparent = "";
        if (isset($_GET['modparent'])) $modparent = "&modparent=".$_GET['modparent'];
        return $this->redirect(["/xblocks/xfiles/?model={$_GET['model']}&parent={$_GET['parent']}{$modparent}"]);
    }

    /**
     * Finds the Iblocks model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Iblocks the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Xfiles::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
