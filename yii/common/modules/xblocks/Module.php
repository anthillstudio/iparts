<?php

namespace common\modules\xblocks;

/**
 * basket module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'common\modules\xblocks\controllers';
    //public $layout = '@app/views/layouts/default.php';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
