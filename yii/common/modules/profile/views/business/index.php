<?php
use common\components\Ant;
use yii\widgets\Breadcrumbs;
use common\modules\basket\models\Basket;
use common\models\CatalogCats;

$session = Yii::$app->session;
if (!$session->isActive) {
    $session->open();
}

$this->params['breadcrumbs'][] = ['label' => 'Новости', 'url' => ["/news"]];


?>

<div class="content-title-box-wrapper content-title-box-mini">
    <div class="content-title-box">
        <div class="fix"><h1 class="FNTC">Личный кабинет</h1></div>
    </div><!-- content-title-box -->
</div><!-- content-title-box-wrapper -->

<div class="fix content-navigation-h">

    <?= $toReturn .= $this->render('@common/modules/profile/views/inc/_menu.php'); ?>

    <div style="text-align: center; padding: 50px">У Вас пока нет личных партнеров</div>

</div>