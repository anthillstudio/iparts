<?php

use common\models\Basket;

$elements = Basket::find()
    ->where(['user_id'=>Yii::$app->user->id])
    ->andWhere("`insert_date`>='{$from} 00:00:00'")
    ->andWhere("`insert_date`<='{$to} 59:59:00'")
    ->orderBy(['insert_date'=>SORT_DESC])
    ->all();


if (is_null($elements)){
    echo "Данные отсутствуют";
}else {

    echo "<table class='stock-table'>";
    $msumm = 0.00;
    foreach ($elements as $key => $value) {
        echo "<tr class='table-tr-status{$value->status_id}'>";
        echo "<td>";
        echo "Заказ № {$value->insert_date}";
        if ($value->is_exported==1){
            echo " (обработан)";
        }
        echo "</td>";
        echo "</tr>";
        echo "<tr class='table-tr-status{$value->status_id}'>";
        echo "<td colspan='3'>";
        $i = 1;
        echo "<table>";
        echo "<tr>";
        echo "<td>#</td>";
        echo "<td>Название</td>";
        echo "<td>Кол-во</td>";
        echo "<td>Цена, USD</td>";
        //echo "<td>Цена, BYN</td>";
        echo "</tr>";
        $summ = 0;
        if (!is_null($value->basketElements)) {
            foreach ($value->basketElements as $k => $v) {
                if (trim($v->catalogElements->title) != '') {
                    echo "<tr>";
                    echo "<td>{$i}</td>";
                    echo "<td><a href=\"javascript:showDetals('{$v->catalogElements->id}')\">{$v->catalogElements->title}</a><br>Артикул: {$v->catalogElements->ref}</td>";
                    echo "<td>{$v->count}</td>";
                    echo "<td>{$v->price}</td>";
                    echo "</tr>";
                    $i++;
                    $summ += $v->price*$v->count;
                }
            }
        }
        $summ = sprintf("%.2f", $summ);
        $msumm += $summ;
        echo "<tr>";
        if ($summ != 0.00) echo "<td colspan='5'>Итого: {$summ} USD</td>";
        echo "</tr>";
        /*if (trim($value->reportOrder->comment)!='') {
            echo "<tr>";
            echo "<td class='ordeer-comment' colspan='5'>{$value->reportOrder->comment}</td>";
            echo "</tr>";;
            echo "<tr>";
        }*/
        echo "</table>";
        echo "</td>";
        echo "</tr>";
    }

    echo "<tr>";
    $msumm = sprintf("%.2f", $msumm);
    if ($msumm != 0.00) echo "<td colspan='1' style='font-weight: bold; text-align: center'>Итого: {$msumm} USD</td>";
    echo "</tr>";
    echo "</table>";

}