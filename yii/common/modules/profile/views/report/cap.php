<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>



<script>

    /* Russian (UTF-8) initialisation for the jQuery UI date picker plugin. */
    /* Written by Andrew Stromnov (stromnov@gmail.com). */
    ( function( factory ) {
        if ( typeof define === "function" && define.amd ) {

            // AMD. Register as an anonymous module.
            define( [ "../widgets/datepicker" ], factory );
        } else {

            // Browser globals
            factory( jQuery.datepicker );
        }
    }( function( datepicker ) {

        datepicker.regional.ru = {
            closeText: "Закрыть",
            prevText: "&#x3C;Пред",
            nextText: "След&#x3E;",
            currentText: "Сегодня",
            monthNames: [ "Январь","Февраль","Март","Апрель","Май","Июнь",
                "Июль","Август","Сентябрь","Октябрь","Ноябрь","Декабрь" ],
            monthNamesShort: [ "Янв","Фев","Мар","Апр","Май","Июн",
                "Июл","Авг","Сен","Окт","Ноя","Дек" ],
            dayNames: [ "воскресенье","понедельник","вторник","среда","четверг","пятница","суббота" ],
            dayNamesShort: [ "вск","пнд","втр","срд","чтв","птн","сбт" ],
            dayNamesMin: [ "Вс","Пн","Вт","Ср","Чт","Пт","Сб" ],
            weekHeader: "Нед",
            //dateFormat: "dd.mm.yy",
            //firstDay: 1,
            //isRTL: false,
            //showMonthAfterYear: false,
            //yearSuffix: ""
        };
        datepicker.setDefaults( datepicker.regional.ru );

        return datepicker.regional.ru;

    } ) );


    $( function() {
        var dateFormat = "mm/dd/yy",
            from = $( "#from" )
                .datepicker({
                    language: 'ru',
                    defaultDate: "+1w",
                    changeMonth: true,
                    numberOfMonths: 1,
                    //monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
                    //dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
                    dateFormat: "yy-mm-dd"
                })
                .on( "change", function() {
                    to.datepicker( "option", "minDate", getDate( this ) );
                }),
            to = $( "#to" ).datepicker({
                language: 'ru',
                defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 1,
                //monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
                //dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
                dateFormat: "yy-mm-dd"
            })
                .on( "change", function() {
                    from.datepicker( "option", "maxDate", getDate( this ) );
                });

        function getDate( element ) {
            var date;
            try {
                date = $.datepicker.parseDate( dateFormat, element.value );
            } catch( error ) {
                date = null;
            }

            return date;
        }

        $( ".cal-box #from" ).datepicker( "setDate", new Date());
        $( ".cal-box #to" ).datepicker( "setDate", new Date());
    } );
</script>



<div class="profile-menu">
    <ul>
        <li><a href="/profile/report/order">История заказов</a></li>
        <li style="display: none"><a href="javascript:selectOrders(2)">История закупок</a></li>
        <li><a href="/profile/report/purchase">История покупок</a></li>
        <li><a href="/profile/report/returns">История возвратов</a></li>
        <li><a href="/profile/report/statement">Ведомость</a></li>
    </ul>
</div>

<div class="cal-box" style="padding-bottom: 7px; text-align: center; border-bottom: 1px solid #f7f7f7; margin-bottom: 0px">

    <div class="cal-box-item">
        <label for="from">С</label>
        <input type="text" id="from" name="from">
    </div>
    <div class="cal-box-item">
        <label for="to">по</label>
        <input type="text" id="to" name="to">
    </div>

</div>
<div class="clear-both"></div>


<style>
    .stock-table{margin: 0px auto; width: 100%; background-color: #f7f7f7}
    .stock-table td{padding: 10px; border: 1px solid #dedede}
    .table-tr-status0 td{background-color: #f7f7f7; color: #222}
    .table-tr-status1 td{background-color: red; color: #fff}
    .table-tr-status2 td{background-color: blue; color: #fff}
    .table-tr-status3 td{background-color: green; color: #fff}
    .table-tr-status4 td{background-color: #DE8E30; color: #fff}
    .stock-table tr:nth-child(2n) td{background-color: #fff}

    .stock-table table{width: 100%; background-color: #fff}
    .stock-table table td{background-color: #fff}
    .stock-table table td:nth-child(1){width: 40px}
    .stock-table table td:nth-child(3){width: 100px}
    .stock-table table td:nth-child(4){width: 100px}

    .getReportBttn{text-align: center; margin-bottom: 20px; padding-top: 20px; display: block; clear: none}
    .getReportBttn a{display: inline-block; padding: 10px 20px; background-color: orange; color: #fff; font-size: 12pt}
    .getReportBttn a:hover, .getReportBttn a:focus, .getReportBttn a:active{background-color: #333; color: #fff; text-decoration: none}


    .stock-table-elements{display: none}

</style>

