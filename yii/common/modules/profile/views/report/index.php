<?php
use common\components\Ant;
use yii\widgets\Breadcrumbs;
use common\modules\basket\models\Basket;
use common\models\CatalogCats;

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;
use vova07\imperavi\Widget;
use common\models\UserInfo;

$ant = new Ant();

$session = Yii::$app->session;
if (!$session->isActive) {
    $session->open();
}

$this->params['breadcrumbs'][] = ['label' => 'Новости', 'url' => ["/news"]];

//unset($_SESSION['basket']);
?>


<div class="fix cat-title-box"><h1>Отчеты</h1></div>



<div class="profile-content-wrapper">


    <?php echo $this->render('cap.php'); ?>


    <div class="profile-content">

    </div>

</div><!-- profile-content -->