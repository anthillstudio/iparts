<?php

use common\models\Purchase;


$elements = Purchase::find()
    ->where([
        'partner_id'=>Yii::$app->user->id,
        'status' => '1'
    ])
    ->andWhere("`date`>='{$from} 00:00:00'")
    ->andWhere("`date`<='{$to} 59:59:00'")
    ->orderBy(['date'=>SORT_DESC])
    ->limit(100)
    ->all();


if (is_null($elements)){
    echo "Данные отсутствуют";
}else {

    echo "<table class='stock-table'>";
    $msumm = 0.00;
    foreach ($elements as $key => $value) {

        echo "<tr>";
        echo "<td>";
        echo "Дата покупки: {$value->date}, документ: {$value->doc_id}, место покупки: {$value->region->name}";
        echo "</td>";
        echo "</tr>";
        echo "<tr>";
        echo "<td colspan='3'>";
        $i = 1;
        echo "<table>";
        echo "<tr>";
        echo "<td>#</td>";
        echo "<td>Название</td>";
        echo "<td>Кол-во</td>";
        echo "<td>Цена, USD</td>";
        echo "</tr>";
        $summ = 0;

        foreach ($value->purchaseElements as $k => $v) {

            if (trim($v->catalogElements->title) != '') {
                echo "<tr>";
                echo "<td>{$i}</td>";
                echo "<td><a href=\"javascript:showDetals('{$v->catalogElements->id}')\">{$v->catalogElements->title}</a><br>Артикул: {$v->catalogElements->ref}<br></td>";
                echo "<td>{$v->count}</td>";
                echo "<td>{$v->price}</td>";
                echo "</tr>";
                $i++;
                $summ += $v->price*$v->count;
            }

        }

        $summ = sprintf("%.2f", $summ);

        $msumm += $summ;
        if ($summ != 0.00) echo "<td colspan='5'>Итого: {$summ} USD</td>";
        echo "</tr>";

        echo "</table>";
        echo "</td>";
        echo "</tr>";
    }

    echo "<tr>";
    $msumm = sprintf("%.2f", $msumm);
    if ($msumm != 0.00) echo "<td colspan='1' style='font-weight: bold; text-align: center'>Итого: {$msumm} USD</td>";
    echo "</tr>";
    echo "</table>";

}
