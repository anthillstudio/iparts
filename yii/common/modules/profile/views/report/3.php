<?php

use common\models\Returns;

$user = Yii::$app->user->id;

$elements = Returns::find()
    ->where([
        'partner_id' => Yii::$app->user->id,
        'status' => '1'
    ])
    ->andWhere("`date`>='{$from} 00:00:00'")
    ->andWhere("`date`<='{$to} 59:59:00'")
    ->orderBy(['date'=>SORT_DESC])
    ->limit(100)
    ->all();

if (is_null($elements)){
    echo "Данные отсутствуют";
}else {
    if (count($elements)!=0) {
        echo "<table class='stock-table'>";
        foreach ($elements as $key => $value) {
            echo "<tr>";
            echo "<td>";
            echo "Дата покупки: {$value->date}, документ: {$value->doc_id}, место покупки: {$value->region->name}";
            echo "</td>";
            echo "</tr>";
            echo "<tr>";
            echo "<td>";
            echo "<table class='stock-table'>";
            echo "<tr>";
            echo "<td>#</td>";
            echo "<td>Товар</td>";
            echo "<td>Количество</td>";
            echo "<td>Цена, USD</td>";
            echo "</tr>";
            foreach ($value->returnsElements as $k => $v) {
                echo "<tr>";
                echo "<td>{$v->id}</td>";
                echo "<td><a href=\"javascript:showDetals('{$v->catalogElements->id}')\">{$v->catalogElements->title}</a></td>";
                echo "<td>{$v->count}</td>";
                echo "<td>{$v->price}</td>";
                echo "</tr>";
            }
            echo "</table>";
            echo "</td>";
            echo "</tr>";
        }
        echo "</table>";
    }
}