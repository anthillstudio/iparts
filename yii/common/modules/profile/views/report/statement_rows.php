<script>
    function showStatementDetals(el){
        $(".stock-table-elements").css('display','none');
        $("#stock-table-elements"+el).css('display','table-row');
    }
</script>
<table class='stock-table'>
    <tr>
        <td>Период (с)</td>
        <td>Период (по)</td>
        <td>Стартовый баланс</td>
        <td>Конечный баланс</td>
        <td></td>
    </tr>
    <?php foreach ($elements as $key=>$value){ ?>
        <tr>
            <td><?= $value->start_date ?></td>
            <td><?= $value->end_date ?></td>
            <td><?= $value->start_balance ?></td>
            <td><?= $value->end_balance ?></td>
            <td><a href="javascript:showStatementDetals(<?= $value->statement_request_id ?>)">Подробнее</a></td>
        </tr>
        <tr class="stock-table-elements" id="stock-table-elements<?= $value->statement_request_id ?>">
            <td colspan="6">
                <b>Запрос №<?= $value->statement_request_id ?></b>
                <table>
                    <tr>
                        <td style="width: 40%">Описание</td>
                        <td style="width: 30%">Дата операции</td>
                        <td>Баланс</td>
                    </tr>
                    <?php foreach ($value->statementElement as $k=>$v){ ?>
                        <tr>
                            <td><?= $v->descr ?></td>
                            <td><?= $v->date ?></td>
                            <td><?= $v->summ ?></td>
                        </tr>
                    <?php } ?>
                </table>
            </td>
        </tr>
    <?php } ?>
</table>