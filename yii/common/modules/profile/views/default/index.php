<?php
use common\components\Ant;
use yii\widgets\Breadcrumbs;
use common\modules\basket\models\Basket;
use common\models\CatalogCats;

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;
use vova07\imperavi\Widget;
use common\models\UserInfo;

$ant = new Ant();

$session = Yii::$app->session;
if (!$session->isActive) {
    $session->open();
}
$this->params['breadcrumbs'][] = ['label' => 'Новости', 'url' => ["/news"]];
?>

<div class="profile-content-wrapper">

    <div class="profile-content">

        <div class="fix content-navigation-h">
            <?= $toReturn = $this->render('@common/modules/profile/views/inc/_menu.php'); ?>
        </div>
    </div>

</div><!-- profile-content -->