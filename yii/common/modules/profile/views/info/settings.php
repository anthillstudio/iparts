<?php
use common\components\Ant;
use yii\widgets\Breadcrumbs;
use common\models\CatalogCats;

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;
use vova07\imperavi\Widget;

/** @var User $model */

$ant = new Ant();

$session = Yii::$app->session;
if (!$session->isActive) {
    $session->open();
}

$this->params['breadcrumbs'][] = ['label' => 'Новости', 'url' => ["/news"]];

?>

<div class="fix cat-title-box"><h1>Настройки</h1></div>

<div class="profile-content-wrapper">
    
    <div class="profile-info-content">

        <div class="fix content-navigation-h">
            <?= $toReturn = $this->render('@common/modules/profile/views/inc/_menu.php'); ?>
        </div>
        <?=Html::beginForm(['/profile/info/settings'], 'post')?>
            <table class='good-tbl'>
                <tr class='good-tbl-tr'>
                    <td>
                        Номер карты лояльности
                    </td>
                    <td>
                        <?=$model->cards?>
                    </td>
                </tr>
                <tr class='good-tbl-tr'>
                    <td>
                        Адрес электронной почты
                    </td>
                    <td>
                        <?=$model->email?>
                    </td>
                </tr>
                <tr class='good-tbl-tr'>
                    <td>
                        Получать рассылки с сайта
                    </td>
                    <td>
                        <?=Html::activeCheckbox($model, 'send_emails', ['label' => ''])?>
                    </td>
                </tr>
                <tr class='good-tbl-tr'>
                    <td colspan="2">
                        <?=Html::submitButton('Сохранить', ['class' => 'btn btn-primary'])?>
                    </td>
                </tr>
            </table>
        <?=Html::endForm()?>

        <div class="clear-both"></div>
        <div class="clear-both"></div>
    </div>

</div><!-- profile-content -->