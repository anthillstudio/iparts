<?php
use common\components\Ant;
use yii\widgets\Breadcrumbs;
use common\modules\basket\models\Basket;
use common\models\CatalogCats;
use common\models\UserInfo;

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;

$userInfo = new UserInfo();

$session = Yii::$app->session;
if (!$session->isActive) {
    $session->open();
}

$this->params['breadcrumbs'][] = ['label' => 'Новости', 'url' => ["/news"]];


?>

<div class="content-title-box-wrapper content-title-box-mini">
    <div class="content-title-box">
        <div class="fix"><h1 class="FNTC">Личный кабинет</h1></div>
    </div><!-- content-title-box -->
</div><!-- content-title-box-wrapper -->

<div class="fix content-navigation-h">

    <?= $toReturn .= $this->render('@common/modules/profile/views/inc/_menu.php'); ?>

    <div style="max-width: 600px; margin: 0px auto" class="FNTC">

        <i>
            Можно установить один раз каждый отчетный период не позднее 5 числа. В случае отсутствия выбора в определенный период, вы сможете получить средства наличными в офисе.
            Для того чтобы осуществить вывод средств, необходимо заполнить форму на вывод средств. Заявка рассматривается не более 7 дней.<br><br>
        </i>

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
        <?php
        $cashingTypes = [];
        foreach ($userInfo->cashingTypes as $key=>$value){
            $cashingTypes[$key] = $value['title'];
        }
        ?>
    <?= $form->field($model->userInfoObject, 'cashing_type')->dropDownList($cashingTypes,['style'=>'width: 100%'])->label('Вывод средств') ?>
    <div class="form-group">
        <?= Html::submitButton($model->userInfoObject->isNewRecord ? 'Добавить' : 'Обновить', ['class' => $model->userInfoObject->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    </form><?php ActiveForm::end(); ?>
        <br><br>
</div>


</div>