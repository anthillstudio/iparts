<?php

use frontend\assets\AppAsset;
use common\widgets\Alert;
use common\models\Settings;
use common\widgets\mainMenu\MainMenuWidget;
use common\widgets\apanel_menu\ApanelMenuWidget;
use common\widgets\apanel_info\ApanelInfoWidget;


$settings = Settings::getSettings();
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<?= $this->render('@frontend/views/layouts/inc/_header.php'); ?>
<?php $this->beginBody() ?>


    <div id="cap-box">
        <?= MainMenuWidget::widget() ?>
    </div><!-- capBox -->


    <?= ApanelInfoWidget::widget() ?>
    <div class="fix content-navigation-h">
        <?= ApanelMenuWidget::widget() ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div><!-- content-navigation-h -->


<?= $this->render('@frontend/views/layouts/inc/_footer.php'); ?>

<?php $this->endBody() ?>
<?php $this->endPage() ?>