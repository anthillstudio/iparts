<?php
use common\components\Ant;
use yii\widgets\Breadcrumbs;
use common\modules\basket\models\Basket;
use common\models\CatalogCats;

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;
use vova07\imperavi\Widget;
use common\models\UserInfo;
use common\models\ContentCatalog;

$ant = new Ant();

$session = Yii::$app->session;
if (!$session->isActive) {
    $session->open();
}

$this->params['breadcrumbs'][] = ['label' => 'Новости', 'url' => ["/news"]];

//unset($_SESSION['basket']);
?>

<div class="profile-content-wrapper">

    <div class="profile-actions-content">

        <div class="fix content-navigation-h">
            <?= $toReturn = $this->render('@common/modules/profile/views/inc/_menu.php'); ?>
        </div>

        <?php $els = ContentCatalog::find()->limit(5)->orderBy(['id'=>rand()])->all(); ?>
        <?= Yii::$app->getView()->render('@frontend/views/content/cat/_goods_list.php', ['els'=>$els,'type'=>1]); ?>

    </div>

</div><!-- profile-content -->