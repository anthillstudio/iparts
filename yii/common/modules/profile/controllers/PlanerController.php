<?php

namespace common\modules\profile\controllers;

use common\components\Db;
use common\models\UserInfo;
use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use common\models\User;
use common\components\Ant;
use common\models\Settings;

/**
 * Default controller for the `admin` module
 */
class PlanerController extends Controller
{

    public function init(){
        Yii::$app->params['settings'] = Settings::getSettings();
    }

    public function behaviors()
    {

    }


    public function actionIndex()
    {
        Db::irequest('planer');
    }

}
