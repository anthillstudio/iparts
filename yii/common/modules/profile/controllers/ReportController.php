<?php

namespace common\modules\profile\controllers;

use common\models\Statement;
use common\models\StatementElement;
use common\models\StatementRequest;
use common\models\UserInfo;
use common\modules\basket\models\Basket;
use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use common\models\User;
use common\components\Ant;

use yii\imagine\Image;
use yii\web\UploadedFile;

use common\models\Settings;
use common\components\Db;

/**
 * Default controller for the `admin` module
 */

class ReportController extends Controller
{

    public $layout = "apanel";

    public function init(){
        if(Yii::$app->user->isGuest){
            return $this->redirect(['/site/login']);
        }
        Yii::$app->params['settings'] = Settings::getSettings();
    }

    /*public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                //'only' => ['login', 'logout', 'signup'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['login', 'signup'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }*/

    protected function findModel($id)
    {
        $model = User::findOne($id);
        return $model;
    }



    public function actionBalance()
    {
        Db::irequest('get_balance');
    }







    public function actionOrders()
    {

        switch ($_GET['type']){

            case"1": // История заказов
                echo Yii::$app->getView()->render("@common/modules/profile/views/report/{$_GET['type']}.php",[
                    'type'=>$_GET['type'], 'from'=>$_GET['from'], 'to'=>$_GET['to'],
                ]);
                break;
            case"2": // История закупок
                echo Yii::$app->getView()->render("@common/modules/profile/views/report/{$_GET['type']}.php",[
                    'type'=>$_GET['type'], 'from'=>$_GET['from'], 'to'=>$_GET['to'],
                ]);
                break;
            case"3": // История возвратов
                echo Yii::$app->getView()->render("@common/modules/profile/views/report/{$_GET['type']}.php",[
                    'type'=>$_GET['type'], 'from'=>$_GET['from'], 'to'=>$_GET['to'],
                ]);
                break;
            case"4": // Ведомость
                $toReturn = StatementRequest::addToRequest($_GET['from'],$_GET['to']);
                print json_encode(array("id"=>$toReturn));
                break;
        }
        die;

    }


    public function actionCheckStat()
    {
        $statementRequest = StatementRequest::findOne($_GET['el']);
        print json_encode(array("state"=>$statementRequest->state));
        die;
    }

    public function actionElements()
    {
        $elements = Statement::find()->where(['statement_request_id'=>$_GET['el']])->all();
        echo Yii::$app->getView()->render("@common/modules/profile/views/report/statement_rows.php",[
            'elements'=>$elements,
        ]);
        die;
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $user = $this->findModel(Yii::$app->user->id);
        return $this->render('index',['model'=>$user]);
    }


    public function actionStatement()
    {
        return $this->render('statement');
    }

    public function actionOrder()
    {
        return $this->render('orders');
    }

    public function actionPurchase()
    {
        return $this->render('purchases');
    }

    public function actionReturns()
    {
        return $this->render('returns');
    }


    public function actionBusiness()
    {
        return $this->render('business');
    }

}
