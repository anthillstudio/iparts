<?php

namespace common\modules\profile\controllers;

use Yii;
use yii\web\Controller;
use common\modules\basket\models\Basket;
use common\modules\basket\models\BasketElements;
use yii\filters\AccessControl;
use common\models\User;
use common\models\UserInfo;
use common\components\Ant;
use common\models\Settings;


/**
 * Default controller for the `admin` module
 */
class StockController extends Controller
{

    public function init(){
        Yii::$app->params['settings'] = Settings::getSettings();
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                //'only' => ['login', 'logout', 'signup'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['login', 'signup'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    protected function findModel($id)
    {
        $model = User::findOne($id);
        $model->userInfoObject = UserInfo::find()->one();
        return $model;
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {


        $user = $this->findModel(Yii::$app->user->id);
        //$elements = Basket::find()->with('basketElements')->where(['user_id'=>$user->id])->all();
        $elements = Yii::$app->db->createCommand("
            SELECT be.*, b.insert_date, cc.title_lng
            FROM {{%basket}} as b
            LEFT JOIN {{%basket_elements}} as be ON be.order_id = b.id
            LEFT JOIN {{%content_catalog}} as cc ON be.catalog_element_id = cc.id
        ")->queryAll();

        return $this->render('index',[
            'user'=>$user,
            'elements'=>$elements
        ]);
    }




}
