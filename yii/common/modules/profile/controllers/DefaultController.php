<?php

namespace common\modules\profile\controllers;

use common\models\UserInfo;
use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use common\models\User;
use common\components\Ant;

use yii\imagine\Image;
use yii\web\UploadedFile;

use common\models\Settings;

/**
 * Default controller for the `admin` module
 */
class DefaultController extends Controller
{

    public $layout = "apanel";

    public function init(){
        Yii::$app->params['settings'] = Settings::getSettings();
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                //'only' => ['login', 'logout', 'signup'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['login', 'signup'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    protected function findModel($id)
    {
        $model = User::findOne($id);
        return $model;
    }

    public function actionPlanner(){
        die('d');
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {


        $user = $this->findModel(Yii::$app->user->id);







        if ($user->load(Yii::$app->request->post())) {

            $user->userInfo->load(Yii::$app->request->post());


            /*
             * Добавление / удаление
             * изображения
            */
            $post = Yii::$app->request->post();
            $uploaderInfo = Ant::uploaderInfo();
            $path = "../{$uploaderInfo['dir']}/".UserInfo::$config['dir']."/";
            if ($user->image_file == '') unset($user->userInfo->image_file);

            if (isset($post['UserInfo']['imageFileDelete']) && $post['UserInfo']['imageFileDelete']==1){
                unlink($path.$user['oldAttributes']['image_file']);
                $user->image_file = '';
                $user->save();
            }
            $imageFile = UploadedFile::getInstance($user->userInfo, 'image_file');
            if (isset($imageFile)){

                $user->userInfo->image_file = Ant::buildFileName($user->userInfo->id,$imageFile);
                if ($user->validate()) {
                    $imageFile->saveAs("{$path}{$user->userInfo->image_file}");
                    Image::thumbnail("{$path}{$user->userInfo->image_file}", 400, 400)
                        ->save(Yii::getAlias("{$path}m{$user->userInfo->image_file}"), ['quality' => 100]);
                }
            }

            $user->userInfo->save();
            Yii::$app->session->addFlash('success', 'Информация обновлена');

        }

        return $this->render('index',['model'=>$user]);
    }





    public function actionBusiness()
    {
        die;
        return $this->render('business');
    }

}
