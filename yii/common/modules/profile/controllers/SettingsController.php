<?php

namespace common\modules\profile\controllers;

use Yii;
use common\models\UserInfo;
use yii\web\Controller;
use yii\filters\AccessControl;
use common\models\User;
use common\components\Ant;

use common\models\UserSettings;
use common\models\UserSettingsValues;

use yii\imagine\Image;
use yii\web\UploadedFile;

use common\models\Settings;

/**
 * Default controller for the `admin` module
 */
class SettingsController extends Controller
{

    public function init(){
        Yii::$app->params['settings'] = Settings::getSettings();
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                //'only' => ['login', 'logout', 'signup'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['login', 'signup'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    protected function findModel($id)
    {
        $model = User::findOne($id);
        $model->userInfoObject = UserInfo::find()->one();
        return $model;
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {

        $user = $this->findModel(Yii::$app->user->id);
        if ($user->userInfoObject->load(Yii::$app->request->post())) {
            $user->userInfoObject->save();
            Yii::$app->session->addFlash('success', 'Информация обновлена');

        }

        return $this->render('index',['model'=>$user]);
    }




}
