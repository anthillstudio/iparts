<?php
namespace common\widgets\about;
use yii\web\AssetBundle;
class AboutAsset extends AssetBundle
{
    public $publishOptions = ['forceCopy' => true];
	public $sourcePath = '@common/widgets/about/assets';
	public $css = ['css/style.css','css/media.css'];
	public $js = ['js/js.js'];
	public $depends = [
		//'yii\web\YiiAsset',
		//'yii\bootstrap\BootstrapAsset',
	];
}