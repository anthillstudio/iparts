<?php
namespace common\widgets\about;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use common\components\Ant;
use common\widgets\about\AboutAsset;


class AboutWidget extends Widget
{

    public function init()
    {
        parent::init();
        $this->registerAssets();
    }

    public function run()
    {
        return $this->render('index',[

        ]);
    }

    public function registerAssets()
    {
        $view = $this->getView();
        AboutAsset::register($view);
    }


}