<?php

use common\models\Content;
use yii\helpers\Html;

$contentObject = new Content;
?>

    <div class="fix">

        <div class="aaa-box-wrapper">
        <div class="aaa-box">
            <?php
            if (!Yii::$app->user->isGuest) {
                $menuItems = "<ul>";
                $menuItems .= "<li class='aaa-menu-item-name-box'>";

                    $menuItems .= "<a class='aaa-menu-item-name' href='/profile'><span>" . Yii::$app->user->getIdentity()->email . "</span></a>";

                    $menuItems .= "<div class='aaa-sub-menu-wrapper'>";
                    $menuItems .= "<div class='aaa-sub-menu'>";
                        $menuItems .= "<div class='aaa-sub-menu-items'>";
                            //$menuItems .= "<div class='aaa-sub-menu-item'><a href='/profile'>Информация</a></div>";
                            $menuItems .= "<div class='aaa-sub-menu-item'><a href='/profile/report'>Отчеты</a></div>";
                            
                            $menuItems .= "<div class='aaa-sub-menu-item'><a href='/catalog'>Каталог</a></div>";

                            $menuItems .= "<div class='aaa-sub-menu-item'><a href='/profile/info/guaranty'>Условия гарантии</a></div>";
                            $menuItems .= "<div class='aaa-sub-menu-item'><a href='/profile/info/settings'>Настройки</a></div>";
                            $menuItems .= "<div class='aaa-sub-menu-item'><a target='_blank' href='http://iparts.by/".Yii::$app->params['price']."'>Прайс-лист</a></div>";
                            $menuItems .= "<div class='aaa-sub-menu-item'>";
                                $menuItems .= Html::beginForm(['/site/logout'], 'post');
                                $menuItems .= Html::submitButton('Выйти', ['class' => 'btn btn-link logout']);
                                $menuItems .= Html::endForm();
                            $menuItems .= "</div>";
                        $menuItems .= "</div><!-- aaa-sub-menu-items -->";
                    $menuItems .= "</div><!-- aaa-sub-menu -->";
                    $menuItems .= "</div><!-- aaa-sub-menu-wrapper -->";

                    $menuItems .= "</li>";
                $menuItems .= "</ul>";
            }else{
                $menuItems = "<ul>";
                    $menuItems .= "<li><a class='aaa-menu-item-name' href='/profile'>Войти</a></li>";
                $menuItems .= "</ul>";
            }
            echo $menuItems;
            ?>
        </div><!-- aaa-box -->
        <div class="item-mob-menu-bttn"><a href="javascript:showMobMenu()"></a></div>
        </div>

        <ul id="mainMenu" class="mainMenu mainMenuShow FNTC">
            <li id="menu2" class="item item-def hide-sub-menu"><a class="item-def-a" href="/">ГЛАВНАЯ</a><div class="item-arrow"></div></li>
            <li id="menu2" class="item item-def item-catalog hide-sub-menu"><a class="item-def-a" href="/catalog<?= \common\models\Catalog::filters() ?>">КАТАЛОГ ТОВАРОВ</a><div class="item-arrow"></div></li>
            <li id="menu2" class="item item-def item-catalog hide-sub-menu"><a target='_blank' class="item-def-a" href="https://iparts.by/<?= Yii::$app->params['price']?>">ПРАЙС (ОПТ)</a><div class="item-arrow"></div></li>
            <li id="menu2" class="item item-def hide-sub-menu"><a class="item-def-a" href="/blog">НОВОСТИ</a><div class="item-arrow"></div></li>
            <li id="menu2" class="item item-def hide-sub-menu"><a class="item-def-a" href="/contacts/ipartscenters">КОНТАКТЫ</a><div class="item-arrow"></div></li>
            <li class="clear-both"></li>
        </ul><!-- mainMenu -->

        <div class="main-logo" onClick="location.href='/'">
            <a class="iparts-w" href="/"><img src="../../img/logos/logo.svg "></a>
        </div><!-- main-logo -->

    </div>


<style>
    .btn-link{color: #fff}
    .btn-link:hover,.btn-link:active,.btn-link:focus{color: #fff}
</style>



<div id="mainMobMenu" class="mainMobMenu">
    <div class="mainMobMenuBox">
        <a class="mob-close FNTC" href='javascript:closeMobMenu()'></a>
        <ul class="mob-menu-main FNTC">
            <li class="mob-menu-main-item"><a href="/">ГЛАВНАЯ</a></li>
            <li class="mob-menu-main-item"><a href="/dostavka-i-oplata/">ДОСТАВКА И ОПЛАТА</a></li>
            <li class="mob-menu-main-item"><a href="/catalog">КАТАЛОГ</a></li>
            <li class="mob-menu-main-item"><a href="/blog/">НОВОСТИ</a></li>
            <li class="mob-menu-main-item"><a href="/contacts/ipartscenters">КОНТАКТЫ</a></li>
            <?php if (!Yii::$app->user->isGuest) { ?>
            <li class="mob-menu-main-item">
                <a href='/profile'><span><?= Yii::$app->user->getIdentity()->email ?></span></a>
                <div style="text-align: center">
                <?php
                echo Html::beginForm(['/site/logout'], 'post');
                echo Html::submitButton('Выйти', ['class' => 'btn btn-link logout']);
                echo Html::endForm();
                ?>
                </div>

            </li>
            <?php }else{ ?>
                <li class="mob-menu-main-item"><a href="/profile">ВОЙТИ</a></li>
            <?php } ?>
        </ul>
        <br>
        <ul class="mob-menu-plus FNTC">
            <li class="mob-menu-plus-item xls-file"><a href="http://iparts.by/<?= Yii::$app->params['price']?>">Прайс-лист</a></li>
        </ul>
    </div><!-- mainMobMenuBox -->
</div><!-- mainMobMenu -->