<?php
namespace common\widgets\mainMenu;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use common\components\Ant;
use common\widgets\mainMenu\MainMenuAsset;
use common\models\Content;


class MainMenuWidget extends Widget
{

    public $type;
    public $contentObject;

    public function init()
    {
        parent::init();
        $this->contentObject = new Content();
        $this->registerAssets();
    }

    public function run()
    {
        $this->type = "";
        $parentId = "";
        if (isset($_GET['pageurl'])) {
            $page = $this->contentObject->getPageByUrl($_GET['pageurl']);
            $parentId = $this->contentObject->getParentId($page['id']);
        }
        return $this->render('index', [
            'type' => $this->type,
            'parentId' => $parentId
        ]);
    }

    public function registerAssets()
    {
        $view = $this->getView();
        MainMenuAsset::register($view);
    }


}