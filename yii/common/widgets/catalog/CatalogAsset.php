<?php
namespace common\widgets\catalog;
use yii\web\AssetBundle;
class CatalogAsset extends AssetBundle
{
    public $publishOptions = ['forceCopy' => true];
	public $sourcePath = '@common/widgets/catalog/assets';
	public $css = ['css/style.css','css/media.css'];
	public $js = ['js/js.js'];
	public $depends = [
		//'yii\web\YiiAsset',
		//'yii\bootstrap\BootstrapAsset',
	];
}