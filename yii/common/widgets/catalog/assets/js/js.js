$(document).ready(function() {
    $(".widget-catalog-item").hover(function () {
        id = $(this).attr('id');
        $("#"+id).addClass('widget-catalog-item-active');
    }, function () {
        id = $(this).attr('id');
        $("#"+id).removeClass('widget-catalog-item-active');
    });

    resizecatbox();

});

function resizecatbox(){
    $(".widget-catalog-item-about").height($(".widget-catalog-items").height()/2);
}

$( window ).resize(function() {
    resizecatbox();
});