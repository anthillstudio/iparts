<?php
use common\models\Settings;
$settings = Settings::getSettings();
?>

<div id="widget-catalog-box" class="fix">
    <div class="widget-catalog-items">
        <?php $i=1; ?>
        <?php foreach ($elements as $key=>$value){ ?>
        <div id="widget-catalog-item<?=$value['id']?>" class="widget-catalog-item cat-style<?=$value['id']?>">
            <div class="widget-catalog-item-img"><img src="img/cliparts/a<?=$i?>.png"></div>
            <div href="/catalog/<?=$value['beautiful_url']?>" class="widget-catalog-item-title catBgColor1">
                <span>
                <b class="FNTC"><?=$value['title_lng']?></b>
                <i class="FNTA">Подробнее</i>
                </span>
            </div><!-- widget-catalog-item-title -->
        </div><!-- widget-catalog-item -->
        <?php $i++; ?>
        <?php } ?>
        <div class="widget-catalog-item widget-catalog-item-about">
            <div class="widget-catalog-item-txt FNT">
                <?=$settings['mod_catalog_info']?>
                <div class="widget-catalog-more FNTC"><a href="/content/catalog">Подробнее</a></div>
            </div><!-- widget-catalog-item-txt -->
        </div><!-- widget-catalog-item -->
    </div><!-- widget-catalog-items -->
    <div class="widget-catalog-bnnr">
        <img src="img/cliparts/d1.png">
        <div class="widget-catalog-bnnr-txt FNT">
            <b>5 лет</b>
            <i>на рынке</i>
            <b class="counter2">0</b>
            <i>Партнеров</i>
            <span>МЫ РАБОТАЕМ В 7 СТРАНАХ!</span>
        </div><!-- widget-catalog-bnnr-txt -->
        <div class="widget-catalog-bnnr-more FNT"><a href="/"> > </a></div>
    </div><!-- widget-catalog-bnnr -->
    <div class="clear-both"></div>
</div><!-- widget-catalog-box -->