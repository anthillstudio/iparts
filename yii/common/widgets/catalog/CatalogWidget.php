<?php
namespace common\widgets\catalog;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use common\components\Ant;
use common\widgets\catalog\CatalogAsset;
use common\models\Catalog;


class CatalogWidget extends Widget
{

    public $key = 331;

    public function init()
    {
        parent::init();
        $this->registerAssets();
    }

    public function run()
    {
        $elements = Yii::$app->db->createCommand("
          SELECT l2.*
          FROM {{%content}} as l1
          LEFT JOIN  {{%content}} as l2 ON l1.id = l2.parent
          LEFT JOIN  {{%content_catalog}} as lc ON l1.id = lc.parent
          WHERE l1.id = '{$this->key}'        
          LIMIT 3
        ")->queryAll();

        return $this->render('index',[
            'elements'=>$elements,
        ]);
    }

    public function registerAssets()
    {
        $view = $this->getView();
        CatalogAsset::register($view);
    }


}