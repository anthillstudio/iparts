<?php
namespace common\widgets\slider;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use common\components\Ant;
use common\widgets\slider\SliderAsset;
use common\models\Slider;


class SliderWidget extends Widget
{

    public function init()
    {
        parent::init();
        $this->registerAssets();
    }

    public function run()
    {
        $elements = Slider::find()->where(['show'=>1])->all();
        return $this->render('index',[
            'elements'=>$elements,
        ]);
    }

    public function registerAssets()
    {
        $view = $this->getView();
        SliderAsset::register($view);
    }


}