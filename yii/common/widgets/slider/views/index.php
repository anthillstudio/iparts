<div class="main-media-wrapper">
    <div id="swiper-container-slider">
        <div class="swiper-wrapper">
            <div class="swiper-slide"><div><span><img src="/img/cliparts/1.jpg"></span></div></div>
            <div class="swiper-slide"><div><span><img src="/img/cliparts/3.jpg"></span></div></div>
        </div><!-- swiper-wrapper -->
    </div><!-- swiper-container-slider -->
    <div class="main-media-info-box">
        <div class="FNTC">
            <p class="fix">
                <h2>Запчасти и аксессуары для Apple iPhone / iPad / MacBook / Xiaomi и других брендов оптом</h2>
                <ul>
                    <li class="main-media-telegram"><a rel="external" href="https://t.me/ipartsby">Telegram</a></li>
                    <li class="main-media-viber"><a rel="external" href="https://invite.viber.com/?g2=AQBWV2TeoQglcElcuysljEnWxCXUmcVMEFJWZPvtNHfECJQ0Xo7TSL%2F%2B9MiETps%2F&lang=ru">Viber</a></li>                    
                </ul>
            </p>
            <p class="fix FNTC main-media-telegram-txt">Подписывайтесь на наш аккаунт в <a rel="external" href="https://t.me/ipartsby">Telegram</a> для получения оперативной информации по свежим приходам и изменениям цен</p>

            <div class="icons-box-wrapper">
                <ul class="fix icons-box-items FNTC">
                    <li class="icons-box-item">
                        <img src="/img/icons/i1.png">
                        <h3>Скидки до 10%</h3>
                    </li><!-- icons-box-item -->
                    <li class="icons-box-item">
                        <img src="/img/icons/i3.png">
                        <h3>Широкий ассортимент</h3>
                    </li><!-- icons-box-item -->
                    <li class="icons-box-item">
                        <img src="/img/icons/i2.png">
                        <h3>Товары всегда в наличии</h3>
                    </li><!-- icons-box-item -->
                </ul><!-- icons-box-items -->
            </div><!-- icons-box-wrapper -->

        </div>
    </div><!-- main-media-info-box -->



</div><!-- main-media-wrapper -->
<script>
    var swiper = new Swiper('#swiper-container-slider', {
        pagination: '#swiper-pagination-slider',
        slidesPerView: 1,
        paginationClickable: true,
        effect: 'fade',
        spaceBetween: 0,
        freeMode: true,
        autoplay: 3000,
        autoplayDisableOnInteraction: false
    });
</script>