<?php
namespace common\widgets\links;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use common\components\Ant;
use common\widgets\links\LinksAsset;
use common\models\Links;


class LinksWidget extends Widget
{

    public $type;
    public $key;

    public function init()
    {
        parent::init();
        $this->registerAssets();
    }

    public function run()
    {

        $elements = Yii::$app->db->createCommand("
          SELECT l.*, lc.title_lng as lc_title_lng FROM {{%links_cat}} as lc
          LEFT JOIN  {{%links}} as l ON lc.id = l.cat_id
          WHERE lc.id = '{$this->key}'
        ")->queryAll();


        return $this->render('inc/_'.$this->type,[
            'elements'=>$elements,
        ]);
    }

    public function registerAssets()
    {
        $view = $this->getView();
        LinksAsset::register($view);
    }

}