<?php
namespace common\widgets\search;
use yii\web\AssetBundle;
class SearchAsset extends AssetBundle
{
    public $publishOptions = ['forceCopy' => true];
	public $sourcePath = '@common/widgets/search/assets';
	public $css = ['css/style.css','css/media.css'];
	public $js = ['js/js.js'];
	public $depends = [
		//'yii\web\YiiAsset',
		//'yii\bootstrap\BootstrapAsset',
	];
}