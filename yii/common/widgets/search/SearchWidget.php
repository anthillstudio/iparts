<?php
namespace common\widgets\search;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use common\components\Ant;
use common\widgets\search\SearchAsset;

class SearchWidget extends Widget
{

    public function init()
    {
        parent::init();
        $this->registerAssets();
    }

    public function run()
    {
        return $this->render('index');
    }

    public function registerAssets()
    {
        $view = $this->getView();
        SearchAsset::register($view);
    }

}