<?php
namespace common\widgets\news;

use common\models\Content;
use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use common\components\Ant;


class NewsWidget extends Widget
{

    public function init()
    {
        parent::init();
        $this->registerAssets();
    }

    public function run()
    {
        $elements = Content::find()->where(['parent'=> 129])->orderBy(['date' => SORT_DESC])->limit(4)->all();
        return $this->render('index',[
            'elements' => $elements
        ]);
    }

    public function registerAssets()
    {
        $view = $this->getView();
        NewsAsset::register($view);
    }


}