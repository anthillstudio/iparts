<div class="news-box">
    <div class="fix">
        <?php foreach ($elements as $key=>$value){ ?>
        <div class="news-box-item">
            <span><?=$value->date ?></span>
            <a href="/<?=$value->beautiful_url ?>"><?=$value->title_lng ?></a>
            <i><?=$value->notice_lng ?></i>
        </div><!-- news-box-item -->
        <?php } ?>
    </div>
</div><!-- news-box -->
