<?php
namespace common\widgets\apanel_menu;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use common\components\Ant;
use common\widgets\apanel_menu\ApanelMenuAsset;

class ApanelMenuWidget extends Widget
{

    public function init()
    {
        parent::init();
        $this->registerAssets();
    }

    public function run()
    {
        return $this->render('index');
    }

    public function registerAssets()
    {
        $view = $this->getView();
        ApanelMenuAsset::register($view);
    }

}