<?php
namespace common\widgets\apanel_menu;
use yii\web\AssetBundle;
class ApanelMenuAsset extends AssetBundle
{
    public $publishOptions = ['forceCopy' => true];
	public $sourcePath = '@common/widgets/apanel_menu/assets';
	public $css = ['css/style.css','css/media.css'];
	public $js = ['js/js.js'];
	public $depends = [
		//'yii\web\YiiAsset',
		//'yii\bootstrap\BootstrapAsset',
	];
}