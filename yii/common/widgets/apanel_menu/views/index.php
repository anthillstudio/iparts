<div class="content-menu-wrapper FNTC">
    <div class="content-menu">
        <ul class="content-menu-level1 content-menu-parent331">
            <?php
            $arr = [
                //'1' => ['url'=>'/profile','title'=>'Информация'],
                '2' => ['url'=>'/profile/report','title'=>'Отчеты'],
                '3' => ['url'=>'/catalog','title'=>'Каталог'],
                '4' => ['url'=>'/profile/info/guaranty','title'=>'Условия гарантии'],
                //'5' => ['url'=>'/profile/actions','title'=>'Акции'],
                '6' => ['url'=>'/profile/info/settings','title'=>'Настройки'],
            ];
            ?>
            <?php foreach ($arr as $key=>$value) { ?>
                <?php $class = ($_SERVER['REQUEST_URI'] == $value['url'] ? "content-menu-item-active" : "") ?>
                <li class="content-menu-item <?=$class?>"><a href="<?=$value['url']?>"><?=$value['title']?></a></li>
            <?php } ?>
        </ul>
        <div class="sales-box" style="display: block">
            <div>
            <h2>Система скидок:</h2>
            <ul>
                <li>200 y.e. - 500 y.e. : 3%</li>
                <li>501 y.e. - 2000 y.e. : 5%</li>
            </ul>
            <ul>
                <li>2001 y.e. - 3000 y.e. : 7%</li>
                <li>> 3001 y.e. : 10%</li>
            </ul>
            </div>
        </div>
        <span style="font-size: 8pt; text-align: center; padding: 10px; display: block">Расчет скидок производится по итогам закупок за прошлый месяц</span>
    </div>
</div>