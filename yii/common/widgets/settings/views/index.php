<?php
if (isset($settings)) {
    switch ($settings->key) {
        case "advantages":
            $advantages = explode('~', $settings->value);
            echo "<ul>";
            foreach ($advantages as $key => $value) {
                echo "<li>" . trim($value) . "</li>";
            }
            echo "</ul>";
            break;
        default:
            echo $settings->value;
            break;
    }
}