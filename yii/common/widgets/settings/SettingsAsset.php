<?php
namespace common\widgets\settings;
use yii\web\AssetBundle;
class SettingsAsset extends AssetBundle
{
    public $publishOptions = [
        'forceCopy' => true
    ];
	public $sourcePath = '@common/widgets/settings/assets';
	public $css = [
		'css/style.css'
	];
	
	public $js = [
        'js/js.js'
	];
	public $depends = [
		//'yii\web\YiiAsset',
		//'yii\bootstrap\BootstrapAsset',
	];
}