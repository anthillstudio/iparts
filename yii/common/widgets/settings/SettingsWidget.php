<?php
namespace common\widgets\settings;

use Yii;
use common\components\Ant;
use yii\base\Widget;
use yii\helpers\Html;
use common\widgets\settings\SettingsAsset;
use common\models\Settings;

class SettingsWidget extends Widget
{

    public $var;

    public function init()
    {
        parent::init();
        $this->registerAssets();
    }

    public function run()
    {
        $settings = Settings::getSettingsByKey($this->var);
        return $this->render('index',['settings'=>$settings]);
    }

    public function registerAssets()
    {
        $view = $this->getView();
        SettingsAsset::register($view);
    }


}