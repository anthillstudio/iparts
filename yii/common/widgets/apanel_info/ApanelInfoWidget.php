<?php
namespace common\widgets\apanel_info;

use common\models\Settings;
use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use common\components\Ant;
use common\widgets\apanel_info\ApanelInfoAsset;

class ApanelInfoWidget extends Widget
{

    public function init()
    {
        parent::init();
        $this->registerAssets();
    }

    public function run()
    {
        $currency = Settings::find()->where(['key'=>'usd_currency'])->one();
        return $this->render('index',['currency'=>$currency]);
    }

    public function registerAssets()
    {
        $view = $this->getView();
        ApanelInfoAsset::register($view);
    }

}