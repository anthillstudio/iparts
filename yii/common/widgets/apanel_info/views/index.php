<?php
use Yii;
use common\modules\basket\models\Basket;
use common\components\Db;
use common\models\User;
$summ = Basket::buildBasketBox();
$currencyType = '(usd)';
$balans = Yii::$app->user->getIdentity()->balance;
$sale = User::getSale();


if (isset($_SESSION['curr'])){
    if ($_SESSION['curr']!='ue'){
        $currencyType = '(byn)';
    }
}


?>
<div class="fix">
    <div class="info-box">
    	<?php if (!Yii::$app->user->isGuest) { ?>
        <div class="currency-box"><span>1 usd = <b><?= $currency->value; ?></b> byn</span></div>
        <div class="basket-box" onclick="location.href='/basket'"><span>Корзина: <b><?=$summ?></b> <?=$currencyType?></span></div>
        <div class="balans-box""><span>Баланс: <b><?=($balans==0?0:$balans*(-1))?></b> usd</span></div>
        <div class="sale-box""><span>Скидка: <b><?=$sale?>%</b></span></div>
        <?php } ?>
    </div><!-- info-box -->
</div>