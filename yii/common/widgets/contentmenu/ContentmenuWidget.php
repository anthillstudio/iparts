<?php
namespace common\widgets\contentmenu;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use common\components\Ant;
use common\widgets\contentmenu\ContentmenuAsset;
//use common\models\Content;


class ContentmenuWidget extends Widget
{

    public $page;
    public $navType;
    public $parent;

    public function init()
    {
        parent::init();
        $this->registerAssets();
    }

    public function run()
    {
        return $this->render('index',[
            'page'=>$this->page,
            'navType'=>$this->navType,
            'parent'=>$this->parent
        ]);
    }

    public function registerAssets()
    {
        $view = $this->getView();
        ContentmenuAsset::register($view);
    }


}