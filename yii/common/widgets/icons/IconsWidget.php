<?php
namespace common\widgets\icons;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use common\components\Ant;
use common\widgets\icons\IconsAsset;


class IconsWidget extends Widget
{

    public function init()
    {
        parent::init();
        $this->registerAssets();
    }

    public function run()
    {
        return $this->render('index',[

        ]);
    }

    public function registerAssets()
    {
        $view = $this->getView();
        IconsAsset::register($view);
    }


}