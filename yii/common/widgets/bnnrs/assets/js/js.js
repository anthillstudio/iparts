$(document).ready(function() {

    $(".widget-news-item").hover(function () {
        var id = $(this).attr("id");
        $("#"+id).removeClass("widget-news-def");
        $("#"+id).addClass("widget-news-over");
    }, function () {
        var id = $(this).attr("id");
        $("#"+id).removeClass("widget-news-over");
        $("#"+id).addClass("widget-news-def");
    });

});