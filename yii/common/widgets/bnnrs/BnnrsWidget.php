<?php
namespace common\widgets\bnnrs;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use common\components\Ant;
use common\widgets\bnnrs\BnnrsAsset;
use common\models\Bnnrs;


class BnnrsWidget extends Widget
{

    public function init()
    {
        parent::init();
        $this->registerAssets();
    }

    public function run()
    {
        return $this->render('index',[
        ]);
    }

    public function registerAssets()
    {
        $view = $this->getView();
        BnnrsAsset::register($view);
    }


}