<div class="bnnrsx-wrapper">
    <div class="fix">
        <div class="swiper-container bnnrsx-swiper-container">
            <div class="swiper-wrapper">

                <div class="bnnrsx-item FNTC bnnrsx-item1 swiper-slide">
                    <div class="bnnrx-img">
                        <img src="/img/cliparts/bnnrs/1.png">
                        <div class="bnnrx-info">
                            <div class="bnnrx-info-title"><a href="/oplata-uslug">Оплата услуг</a></div>
                            <a class="bttn-dec" href="/oplata-uslug"></a>
                        </div><!-- bnnr-info -->
                    </div>
                </div><!-- bnnrsx-item -->

                <div class="bnnrsx-item FNTC bnnrsx-item2 swiper-slide">
                    <div class="bnnrx-img">
                        <img src="/img/cliparts/bnnrs/2.png">
                        <div class="bnnrx-info">
                            <div class="bnnrx-info-title"><a href="/korporativnim-klientam/depoziti">Депозиты для бизнеса</a></div>
                            <a class="bttn-dec" href="/korporativnim-klientam/depoziti"></a>
                        </div><!-- bnnr-info -->
                    </div>
                </div><!-- bnnrsx-item -->

                <div class="bnnrsx-item FNTC bnnrsx-item3 swiper-slide">
                    <div class="bnnrx-img"><img src="/img/cliparts/bnnrs/3.png"></div>
                    <div class="bnnrx-info">
                        <div class="bnnrx-info-title"><a href="/fizicheskim-licam/krediti/ekspress-krediti/refinansirovanie-kreditov">Рефинансирование кредитов</a></div>
                        <a class="bttn-dec" href="/fizicheskim-licam/krediti/ekspress-krediti/refinansirovanie-kreditov"></a>
                    </div><!-- bnnr-info -->
                </div><!-- bnnrsx-item -->

                <div class="bnnrsx-item FNTC bnnrsx-item4 swiper-slide">
                    <div class="bnnrx-img"><img src="/img/cliparts/bnnrs/4.png"></div>
                    <div class="bnnrx-info">
                        <div class="bnnrx-info-title"><a href="/korporativnim-klientam/raschetno-kassovoe-obsluzhivanie">Расчетно-кассовое обслуживание</a></div>
                        <a class="bttn-dec" href="/korporativnim-klientam/raschetno-kassovoe-obsluzhivanie"></a>
                    </div><!-- bnnr-info -->
                </div><!-- bnnrsx-item -->

                <div class="bnnrsx-item FNTC bnnrsx-item6 swiper-slide">
                    <div class="bnnrx-img"><img src="/img/cliparts/bnnrs/6.png"></div>
                    <div class="bnnrx-info">
                        <div class="bnnrx-info-title"><a href="/korporativnim-klientam/credit-for-business">Кредиты для бизнеса</a></div>
                        <a class="bttn-dec" href="/korporativnim-klientam/credit-for-business"></a>
                    </div><!-- bnnr-info -->
                </div><!-- bnnrsx-item -->

            </div><!-- swiper-wrapper -->
        </div><!-- bnnrsx-wrapper -->
        <div id="bnnrsx-pagination" class="swiper-pagination"></div>
    </div>
</div><!-- bnnrsx-wrapper -->
<script>
    var swiper = new Swiper('.bnnrsx-swiper-container', {

        pagination: '#bnnrsx-pagination',
        nextButton: '#bnnrsx-button-next',
        prevButton: '#bnnrsx-button-prev',
        slidesPerView: 3,
        paginationClickable: true,
        effect: 'slide',
        spaceBetween: 20,
        autoplay: 3000,
        freeMode: true,
        breakpoints: {
            // when window width is <= 320px
            1280: {
                slidesPerView: 2,
                spaceBetween: 20
            },
            // when window width is <= 480px
            1024: {
                slidesPerView: 1,
                spaceBetween: 10
            },
        },
    });
</script>