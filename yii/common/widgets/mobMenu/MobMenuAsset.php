<?php
namespace common\widgets\mobMenu;
use yii\web\AssetBundle;
class MobMenuAsset extends AssetBundle
{
    public $publishOptions = ['forceCopy' => true];
	public $sourcePath = '@common/widgets/mobMenu/assets';
	public $css = ['css/style.css','css/media.css'];
	public $js = ['js/js.js'];
	public $depends = [
		//'yii\web\YiiAsset',
		//'yii\bootstrap\BootstrapAsset',
	];
}