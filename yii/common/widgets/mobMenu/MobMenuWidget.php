<?php
namespace common\widgets\mobMenu;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use common\components\Ant;
use common\widgets\mobMenu\MobMenuAsset;
//use common\models\News;

class MobMenuWidget extends Widget
{

    public function init()
    {
        parent::init();
        $this->registerAssets();
    }

    public function run()
    {
        //$elements = News::find()->all();
        return $this->render('index');
    }

    public function registerAssets()
    {
        $view = $this->getView();
        MobMenuAsset::register($view);
    }


}