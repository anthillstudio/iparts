<?php

namespace common\external_api\access;

class TokenManager
{
    /**
     * Получаем новый токен
     * @param string $pass - пароль
     * @return string - токен, если пустая строка, то пароль не верный
     */
    public function getToken($pass)
    {
        $token = "";
        if ($pass == $this->getPass()) {
            $token = $this->generateToken();
        }
        return $token;
    }

    /**
     * Проверяем валиден ли полученный токен
     * @param string $token - токен для проверки
     * @return bool - true = валиден, false = невалиден
     */
    public function isValidToken($token)
    {
        return true;
    }

    protected function getPass()
    {
        // TODO: Необходимо перенести хранение пароля в настройки приложения
        return "PASSWORD";
    }

    protected function generateToken()
    {
        return "TOKEN";
    }
}