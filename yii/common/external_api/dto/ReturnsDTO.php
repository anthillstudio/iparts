<?php

namespace common\external_api\dto;

class ReturnsDTO
{
    protected $id;
    protected $partner_id;
    protected $doc_id;
    protected $date;
    protected $status;
    protected $region_id;
    protected $returnsElements;

    public function __construct($partner_id, $doc_id, $date, $status, $region_id, $returnsElements)
    {
        $this->partner_id = $partner_id;
        $this->doc_id = $doc_id;
        $this->date = $date;
        $this->status = $status;
        $this->region_id = $region_id;
        $this->returnsElements = $returnsElements;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getPartnerId()
    {
        return $this->partner_id;
    }

    public function getDocId()
    {
        return $this->doc_id;
    }

    public function getRegionId()
    {
        return $this->region_id;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function getReturnsElements()
    {
        return $this->returnsElements;
    }

}