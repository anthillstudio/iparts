<?php

namespace common\external_api\dto;

class PartnerCardDTO
{
    protected $name;
    protected $code;
    protected $status;

    public function __construct($name, $code, $status)
    {    
        $this->name = $name;
        $this->code = $code;
        $this->status = $status;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function getStatus()
    {
        return $this->status;
    }
}