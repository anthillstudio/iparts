<?php

namespace common\external_api\dto;

class OrderDTO
{
    protected $id;
    protected $internalId;

    public function __construct($id, $internalId)
    {
        $this->id = $id;
        $this->internalId = $internalId;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getInternalId()
    {
        return $this->internalId;
    }

}