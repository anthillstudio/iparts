<?php

namespace common\external_api\dto;

class PriceDTO
{
    protected $id;
    protected $price;
    protected $price2;
    protected $isAction;
    protected $prevPrice;

    public function __construct($id, $price, $price2, $isAction, $prevPrice)
    {
        $this->id = $id;
        $this->price = $price;
        $this->price2 = $price2;
        $this->isAction = $isAction;
        $this->prevPrice = $prevPrice;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function getPrice2()
    {
        return $this->price2;
    }

    public function getIsAction()
    {
        return $this->isAction;
    }

    public function getPrevPrice()
    {
        return $this->prevPrice;
    }

}