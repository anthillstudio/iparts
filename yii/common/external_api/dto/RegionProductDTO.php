<?php

namespace common\external_api\dto;

class RegionProductDTO
{
    protected $regionId;
    protected $productIds;

    public function __construct($regionId, $productIds)
    {
        $this->regionId = $regionId;
        $this->productIds = $productIds;
    }

    public function getRegionId()
    {
        return $this->regionId;
    }

    public function getProductIds()
    {
        return $this->productIds;
    }
}