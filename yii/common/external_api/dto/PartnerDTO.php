<?php

namespace common\external_api\dto;

class PartnerDTO
{
    protected $id;
    protected $name;
    protected $email;
    protected $isDeleted;
    protected $turnover_current;
    protected $turnover_prev;
    protected $card;
    protected $cards;

    public function __construct($id, $name, $email, $isDeleted, $turnover_current, $turnover_prev, $card, $cards)
    {    
        $this->id = $id;
        $this->name = $name;
        $this->email = $email;
        $this->isDeleted = $isDeleted;
        $this->turnover_current = $turnover_current;
        $this->turnover_prev = $turnover_prev;
        $this->card = $card;
        $this->cards = $cards;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getCard()
    {
        return $this->card;
    }

    public function getIsDeleted()
    {
        return $this->isDeleted;
    }

    public function getTurnoverCurrent()
    {
        return $this->turnover_current;
    }

    public function getTurnoverPrev()
    {
        return $this->turnover_prev;
    }

    public function getCards()
    {
        return $this->cards;
    }
}