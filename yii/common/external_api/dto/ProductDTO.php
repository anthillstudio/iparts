<?php

namespace common\external_api\dto;

class ProductDTO
{
    protected $id;
    protected $orderId;
    protected $count;
    protected $price;
    protected $discountedPrice;

    public function __construct($id, $orderId, $count, $price, $discountedPrice)
    {
        $this->id = $id;
        $this->orderId = $orderId;
        $this->count = $count;
        $this->price = $price;
        $this->discountedPrice = $discountedPrice;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getOrderId()
    {
        return $this->orderId;
    }

    public function getCount()
    {
        return $this->count;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function getDiscountedPrice()
    {
        return $this->discountedPrice;
    }
}