<?php

namespace common\external_api\dto;

class StatementDTO
{
    protected $internalId;
    protected $userId;
    protected $startDate;
    protected $endDate;
    protected $startBalance;
    protected $endBalance;
    protected $statementElements;

    // TODO: Возможно elements надо сделать через отдельные методы addElement и getElement
    public function __construct($internalId, $userId, $startDate, $endDate, $startBalance, $endBalance, $statementElements)
    {
        $this->internalId = $internalId;
        $this->userId = $userId;
        $this->startDate = $startDate;
        $this->endDate = $endDate;
        $this->startBalance = $startBalance;
        $this->endBalance = $endBalance;
        $this->statementElements = $statementElements;
    }

    public function getInternalId()
    {
        return $this->internalId;
    }

    public function getUserId()
    {
        return $this->userId;
    }

    public function getStartDate()
    {
        return $this->startDate;
    }

    public function getEndDate()
    {
        return $this->endDate;
    }

    public function getStartBalance()
    {
        return $this->startBalance;
    }

    public function getEndBalance()
    {
        return $this->endBalance;
    }

    public function getStatementElements()
    {
        return $this->statementElements;
    }
}