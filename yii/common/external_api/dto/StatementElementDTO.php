<?php

namespace common\external_api\dto;

class StatementElementDTO
{
    protected $descr;
    protected $date;
    protected $summ;

    public function __construct($descr, $date, $summ)
    {
        $this->descr = $descr;
        $this->date = $date;
        $this->summ = $summ;
    }

    public function getDescr()
    {
        return $this->descr;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function getSumm()
    {
        return $this->summ;
    }
}