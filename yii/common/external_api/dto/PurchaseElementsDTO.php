<?php

namespace common\external_api\dto;

class PurchaseElementsDTO
{
    protected $good_id;
    protected $count;
    protected $price;

    public function __construct($good_id, $count, $price)
    {
        $this->good_id = $good_id;
        $this->count = $count;
        $this->price = $price;
    }

    public function getGoodId()
    {
        return $this->good_id;
    }

    public function getCount()
    {
        return $this->count;
    }

    public function getPrice()
    {
        return $this->price;
    }
}