<?php

namespace common\external_api\dto;

class FinalOrderDTO
{
    protected $id;
    protected $comment;
    protected $products;

    // TODO: Возможно products надо сделать через отдельные методы addProduct и getProduct
    public function __construct($id, $comment, $products)
    {
        $this->id = $id;
        $this->comment = $comment;
        $this->products = $products;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getComment()
    {
        return $this->comment;
    }

    public function getProducts()
    {
        return $this->products;
    }
}