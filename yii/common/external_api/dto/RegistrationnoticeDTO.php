<?php

namespace common\external_api\dto;

class RegistrationnoticeDTO
{
    protected $id;

    public function __construct($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

}