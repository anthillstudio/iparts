<?php

namespace common\external_api\dto;

class CatalogDTO
{
    protected $id;
    protected $name;
    protected $isGroup;
    protected $parentId;
    protected $art;
    protected $description;
    protected $price;
    protected $price2;
    protected $picture;
    protected $isAction;
    protected $prevPrice;

    public function __construct($id, $name, $isGroup, $parentId, $art, $description, $price, $price2, $picture, $isAction, $prevPrice)
    {
        $this->id = $id;
        $this->name = $name;
        $this->isGroup = $isGroup;
        $this->parentId = $parentId;
        $this->art = $art;
        $this->description = $description;
        $this->price = $price;
        $this->price2 = $price2;
        $this->picture = $picture;
        $this->isAction = $isAction;
        $this->prevPrice = $prevPrice;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getIsGroup()
    {
        return $this->isGroup;
    }

    public function getParentId()
    {
        return $this->parentId;
    }

    public function getArt()
    {
        return $this->art;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function getPrice2()
    {
        return $this->price2;
    }

    public function getPicture()
    {
        return $this->picture;
    }

    public function getIsAction()
    {
        return $this->isAction;
    }

    public function getPrevPrice()
    {
        return $this->prevPrice;
    }

}