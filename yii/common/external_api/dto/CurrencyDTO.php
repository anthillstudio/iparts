<?php

namespace common\external_api\dto;

class CurrencyDTO
{
    protected $currency;
    protected $rate;

    public function __construct($currency, $rate)
    {
        $this->currency = $currency;
        $this->rate = $rate;
    }

    public function getCurrency()
    {
        return $this->currency;
    }

    public function getRate()
    {
        return $this->rate;
    }
}