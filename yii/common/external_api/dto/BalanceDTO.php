<?php

namespace common\external_api\dto;

class BalanceDTO
{
    protected $id;
    protected $balance;
    protected $turnover_current;
    protected $turnover_prev;
    protected $cards;

    public function __construct($id, $balance, $turnover_current, $turnover_prev, $cards)
    {
        $this->id = $id;
        $this->balance = $balance;
        $this->turnover_current = $turnover_current;
        $this->turnover_prev = $turnover_prev;
        $this->cards = $cards;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getBalance()
    {
        return $this->balance;
    }

    public function getTurnoverCurrent()
    {
        return $this->turnover_current;
    }

    public function getTurnoverPrev()
    {
        return $this->turnover_prev;
    }

    public function getCards()
    {
        return $this->cards;
    }
}