<?php

namespace common\external_api\dto;

class RegionDTO
{
    protected $id;
    protected $name;
    protected $activity;

    public function __construct($id, $name, $activity)
    {
        $this->id = $id;
        $this->name = $name;
        $this->activity = $activity;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getActivity()
    {
        return $this->activity;
    }
}