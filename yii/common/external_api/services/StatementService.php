<?php

namespace common\external_api\services;

use common\external_api\dto\StatementDTO;
use common\external_api\dto\StatementElementDTO;
use common\models\StatementRequest;
use common\models\Statement;
use common\models\StatementElement;

class StatementService implements ServiceInterface
{
    /**
     * @param $dtoList - array of StatementDTO
     */
    public function load($dtoList)
    {
        /**
         * @var int $key
         * @var StatementDTO $value
         */
        foreach ($dtoList as $key => $value) {
            $statement = new Statement();
            $statement->statement_request_id = $value->getInternalId();
            $statement->user_id = $value->getUserId();
            $statement->start_date = $value->getStartDate();
            $statement->end_date = $value->getEndDate();
            $statement->start_balance = $value->getStartBalance();
            $statement->end_balance = $value->getEndBalance();
            $statement->save();
            /**
             * @var integer $keyP
             * @var StatementElementDTO $valueP
             */
            foreach ($value->getStatementElements() as $keyP => $valueP) {
                $statementElement = new StatementElement();
                $statementElement->statement_id = $statement->id;
                $statementElement->descr = $valueP->getDescr();
                $statementElement->date = $valueP->getDate();
                $statementElement->summ = $valueP->getSumm();
                $statementElement->save();
            }
            /** @var StatementRequest $request */
            $request = StatementRequest::findOne($value->getInternalId());
            $request->markAsProcessed();
        }
    }

    public function getStatementRequests()
    {
        $requests = StatementRequest::getStatementRequests();
        /**
         * @var int $key
         * @var StatementRequest $value
         */
        foreach ($requests as $key => $value) {
            $value->markAsInProcess();
        }
        return $requests;
    }
}