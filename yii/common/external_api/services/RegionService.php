<?php

namespace common\external_api\services;

use \common\external_api\dto\RegionDTO;
use \common\models\Region;
use \common\models\RegionProduct;

class RegionService implements ServiceInterface
{
    /**
     * @param $dtoList - array of RegionDTO
     */
    public function load($dtoList)
    {
        /**
         * @var int $key
         * @var RegionDTO $value
         */
        foreach ($dtoList as $key => $value) {
            $region = Region::findOne($value->getId());
            if (is_null($region)) {
                $region = new Region();
                $region->id = $value->getId();
            }
            $region->name = $value->getName();
            $region->is_active = $value->getActivity();

            $region->save();

            // Удаляем все привязки товаров по деактивированному региону
            if ($region->is_active == Region::STATUS_INACTIVE) {
                RegionProduct::deleteAll(['region_id' => $region->id]);
            }
        }
    }
}