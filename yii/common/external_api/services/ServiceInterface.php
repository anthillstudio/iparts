<?php

namespace common\external_api\services;

interface ServiceInterface
{
    public function load($dtoList);
}