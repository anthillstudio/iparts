<?php

namespace common\external_api\services;

use \common\external_api\dto\PriceDTO;
use \common\models\CatalogElements;

class PriceService implements ServiceInterface
{
    /**
     * @param $dtoList - array of PriceDTO
     */
    public function load($dtoList)
    {
        /**
         * @var int $key
         * @var PriceDTO $value
         */
        foreach ($dtoList as $key => $value) {
            $catalogElement = CatalogElements::findOne($value->getId());
            if (!is_null($catalogElement)) {
                $catalogElement->price1 = $value->getPrice();
                $catalogElement->price2 = $value->getPrice2();
                $catalogElement->prev_price = $value->getPrevPrice();
                $catalogElement->is_action = $value->getIsAction();

                $catalogElement->save();
            }
        }
    }
}