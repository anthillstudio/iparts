<?php

namespace common\external_api\services;

use \common\external_api\dto\RegionProductDTO;
use \common\models\RegionProduct;

class RegionProductService implements ServiceInterface
{
    /**
     * @param $dtoList - array of RegionProductDTO
     */
    public function load($dtoList)
    {
        /**
         * @var int $key
         * @var RegionProductDTO $value
         */
        foreach ($dtoList as $key => $value) {
            RegionProduct::deleteAll(['region_id' => $value->getRegionId()]);

            foreach ($value->getProductIds() as $keyP => $valueP) {
                $regionProduct = new RegionProduct();
                $regionProduct->region_id = $value->getRegionId();
                $regionProduct->product_id = $valueP;
                $regionProduct->save();
            }
        }
    }
}