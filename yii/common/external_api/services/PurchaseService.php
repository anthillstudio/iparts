<?php

namespace common\external_api\services;

use Yii;
use common\external_api\dto\PurchaseDTO;
use common\models\Purchase;
use common\models\User;
use common\components\Ant;
use common\models\PurchaseElements;

class PurchaseService implements ServiceInterface
{
    /**
     * @param $dtoList - array of PurchaseDTO
     */
    public function load($dtoList)
    {
        /**
         * @var int $key
         * @var PurchaseDTO $value
         */

        foreach ($dtoList as $key=>$value){

            Purchase::deleteAll(['doc_id'=>$value->getDocId()]);
            
            $returns = new Purchase();
            $returns->partner_id = $value->getPartnerId();
            $returns->date = $value->getDate();
            $returns->status = $value->getStatus();
            $returns->doc_id = $value->getDocId();
            $returns->order_id = $value->getOrderId();
            $returns->region_id = $value->getRegionId();
            $returns->save();
            $returnsId = $returns->getPrimaryKey();
            foreach ($value->getPurchaseElements() as $keyP => $valueP) {
                $returnsElement = new PurchaseElements();
                $returnsElement->purchase_id = $returnsId;
                $returnsElement->good_id = $valueP->getGoodId();
                $returnsElement->count = $valueP->getCount();
                $returnsElement->price = $valueP->getPrice();
                //$returnsElement->discounted_price = $valueP->getDiscountedPrice();   
                $returnsElement->save();
            }

            $re = PurchaseElements::find()->where(['purchase_id'=>$returnsId])->all();        
            $user = User::findOne($value->getPartnerId());

            if (!is_null($user)) {
                $ant = new Ant();
                $emailfrom = $ant->emailfrom;
                $theme = "iParts :: Заказ {$returnsId}";
                $mssg = Yii::$app->getView()->render('@frontend/views/layouts/inc/_mail_final_order_to_client.php', ['re' => $re]);

                $error = '';
                $validator = new \yii\validators\EmailValidator();
                if ($user->isSendEmail() && ($validator->validate($user->email, $error))) {
                    if (filter_var($user->email, FILTER_VALIDATE_EMAIL)) {
                        Yii::$app->mailer->compose()
                            ->setFrom($emailfrom)
                            ->setTo([$user->email])
                            ->setSubject($theme)
                            ->setHtmlBody($mssg)
                            ->send();
                    }
                }
            }
        }
    }
}