<?php

namespace common\external_api\services;

use common\external_api\dto\PartnerCardDTO;
use Yii;
use common\components\Ant;
use common\external_api\dto\PartnerDTO;
use common\models\User;

class PartnerService implements ServiceInterface
{
    /**
     * @param $dtoList - array of PartnerDTO
     */
    public function load($dtoList)
    {


        /**
         * @var int $key
         * @var PartnerDTO $value
         */
        foreach ($dtoList as $key => $value) {

            // TODO: Тут необходимо реализовать обработку пользователей
            $user = User::findOne($value->getId());

            if (is_null($user)) {            

                $user = new User();
                $user->id = $value->getId();

                // TODO: Тут надо как-то обыграть по другому, сейчас email в БД должен быть уникален,
                // но он есть не у всех
                $user->email = $value->getEmail();
                if (empty($user->email)) {
                    $user->email = $value->getId();
                }

                // TODO: Эти поля идут как unique и не могут повторяться пустые значения, надо либо убрать unique либо
                // генерировать уникальное значение
                $user->username = $value->getId();

                // TODO: Тут необходимо реализовать информирование новых пользователей
                ////////////////////////
                // Устанавливаем пароль
                //$pwd = rand(10000,9999);
                //$user->setPassword($pwd);
                //$user->generateAuthKey();
                $user->save();

                
                /*
                $ant = new Ant;
                $emailfrom = $ant->emailfrom;
                $user->card = "";
                $emails = ["{$user->email}"];
                $theme = "iParts :: регистрация";
                $mssg = Yii::$app->getView()->render('@frontend/views/layouts/inc/_registration.php',[
                    'name'=>$user->username,
                    'pwd'=>$pwd,
                    'email'=>$user->email
                ]);

                Yii::$app->mailer->compose()
                    ->setFrom($emailfrom)
                    ->setTo($emails)
                    ->setSubject($theme)
                    ->setHtmlBody($mssg)
                    ->send();
                */
                /////////////////////////
                /////////////////////////


            }else{
                $user->isDeleted = ($value->getIsDeleted()=='true'?1:0);
                if (!empty($value->getEmail())) {
                    $user->email = $value->getEmail();
                }
            }

            //$user->balance = $value->getBalance();
            $user->card = $value->getCard();

            // TODO: Код дублируется в BalanceService, надо избавится от дублирования!!!
            // Сохраняем все карты лояльности пользователя в виде строки. Пока был выбран такой вариант как наиболее простой и
            // удовлетворяющий бизнес целям. В будущем, если потребуется, то надо вынести карты в отдельную таблицу БД.
            $cardList = array();
            $cards = $value->getCards();
            if (!empty($cards)) {
                /**
                 * @var int $keyCard
                 * @var PartnerCardDTO $valueCard
                 */
                foreach ($cards as $keyCard => $valueCard) {
                    if ($valueCard->getStatus() == "Действует") {
                        $cardList[] = $valueCard->getName();
                    }
                }
            }
            $user->cards = !empty($cardList) ? implode(' ; ', $cardList) : "";

            $user->turnover_current = $value->getTurnoverCurrent();
            $user->turnover_prev = $value->getTurnoverPrev();

            $user->status = User::STATUS_ACTIVE;
            $user->name = $value->getName();
            $user->save();
        }
    }
}