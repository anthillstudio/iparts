<?php

namespace common\external_api\services;

use Yii;
use common\external_api\dto\RegistrationnoticeDTO;
use common\models\User;

class RegistrationnoticeService implements ServiceInterface
{
    /**
     * @param $dtoList - array of RegistrationnoticeDTO
     */
    public function load($dtoList)
    {

        /**
         * @var int $key
         * @var RegistrationnoticeDTO $value
         */
        foreach ($dtoList as $key => $value) {

            $user = User::findOne($value->getId());

            ////////////////////////
            // Устанавливаем пароль
            $pwd = rand(10000,99999);
            $user->setPassword($pwd);
            $user->generateAuthKey();
            // Отправляем письмо с паролем $pwd и емейлом $user->email
            $emailfrom = 'info@anthill.by';
            $emails = ['info@anthill.by'];
            $theme = "iParts :: регистрация";
            $mssg = Yii::$app->getView()->render('@frontend/views/layouts/inc/_registration.php',[
                'name'=>$user->username,
                'pwd'=>$pwd,
                'email'=>$user->email
            ]);
            Yii::$app->mailer->compose()
                ->setFrom($emailfrom)
                ->setTo($emails)
                ->setSubject($theme)
                ->setHtmlBody($mssg)
                ->send();
            /////////////////////////
            /////////////////////////

            $user->save();

            die;

        }
    }
}