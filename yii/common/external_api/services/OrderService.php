<?php

namespace common\external_api\services;

use common\modules\basket\models\Basket;
use common\external_api\dto\OrderDTO;

class OrderService implements ServiceInterface
{
    /**
     * @param $dtoList - array of OrderDTO
     */
    public function load($dtoList)
    {
        /**
         * @var int $key
         * @var OrderDTO $value
         */
        foreach ($dtoList as $key => $value) {
            $order = Basket::findOne($value->getInternalId());
            if (!is_null($order)) {
                // Проводим обновление ключа из 1C
                $order->external_id = $value->getId();
                $order->save();

                // Помечаем заказ как выгруженный
                $order->markAsExported();
            }
        }
    }

    public function getNewOrders()
    {
        $orders = Basket::getNewOrders();
        /**
         * @var int $key
         * @var Basket $value
         */
        foreach ($orders as $key => $value) {
            $value->markAsInProcess();
        }
        return $orders;
    }

}