<?php

namespace common\external_api\services;

use common\external_api\dto\CurrencyDTO;
use common\models\Settings;

class CurrencyService implements ServiceInterface
{
    /**
     * @param $dtoList - array of CurrencyDTO
     */
    public function load($dtoList)
    {
        /**
         * @var int $key
         * @var CurrencyDTO $value
         */
        foreach ($dtoList as $key => $value) {
            // TODO: Костыль т.к. валюты храним не в отдельной сущности, а в таблице настроек
            if ($value->getCurrency() == "USDR") {
                Settings::setUsdCurrency($value->getRate());
            }
        }
    }

}