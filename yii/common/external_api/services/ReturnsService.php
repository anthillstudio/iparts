<?php

namespace common\external_api\services;


use common\external_api\dto\ReturnsDTO;
use common\models\Returns;
use common\models\ReturnsElements;
use common\models\User;

class ReturnsService implements ServiceInterface
{
    /**
     * @param $dtoList - array of ReturnsDTO
     */
    public function load($dtoList)
    {
        /**
         * @var int $key
         * @var ReturnsDTO $value
         */
        foreach ($dtoList as $key=>$value){

            Returns::deleteAll(['doc_id'=>$value->getDocId()]);

            $returns = new Returns();
            $returns->partner_id = $value->getPartnerId();
            $returns->date = $value->getDate();
            $returns->status = $value->getStatus();
            $returns->doc_id = $value->getDocId();
            $returns->region_id = $value->getRegionId();

            $returns->save();
            $returnsId = $returns->getPrimaryKey();
            foreach ($value->getReturnsElements() as $keyP => $valueP) {
                $returnsElement = new ReturnsElements();
                $returnsElement->return_id = $returnsId;
                $returnsElement->good_id = $valueP->getGoodId();
                $returnsElement->count = $valueP->getCount();
                $returnsElement->price = $valueP->getPrice();
                $returnsElement->save();
            }
            ///** @var StatementRequest $request */
            //$request = StatementRequest::findOne($value->getInternalId());
            //$request->markAsProcessed();
        }

    }
}