<?php

namespace common\external_api\services;

use common\external_api\dto\PartnerCardDTO;
use common\external_api\dto\BalanceDTO;
use common\models\User;

class BalanceService implements ServiceInterface
{
    /**
     * @param $dtoList - array of BalanceDTO
     */
    public function load($dtoList)
    {
        /**
         * @var int $key
         * @var BalanceDTO $value
         */
        foreach ($dtoList as $key => $value) {
            /** @var User $user */
            $user = User::findOne($value->getId());
            if (!is_null($user)) {
                // Проводим обновление баланса
                $user->setNewBalance($value->getBalance());
                $user->turnover_current = $value->getTurnoverCurrent();
                $user->turnover_prev = $value->getTurnoverPrev();

                // TODO: Код дублируется в PartnerService, надо избавится от дублирования!!!
                // Сохраняем все карты лояльности пользователя в виде строки. Пока был выбран такой вариант как наиболее простой и
                // удовлетворяющий бизнес целям. В будущем, если потребуется, то надо вынести карты в отдельную таблицу БД.
                $cardList = array();
                $cards = $value->getCards();
                if (!empty($cards)) {
                    /**
                     * @var int $keyCard
                     * @var PartnerCardDTO $valueCard
                     */
                    foreach ($cards as $keyCard => $valueCard) {
                        if ($valueCard->getStatus() == "Действует") {
                            $cardList[] = $valueCard->getName();
                        }
                    }
                }
                $user->cards = !empty($cardList) ? implode(' ; ', $cardList) : "";

                $user->save();

            }
        }
    }
}