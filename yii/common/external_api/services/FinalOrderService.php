<?php

namespace common\external_api\services;

use Yii;
use common\external_api\dto\ProductDTO;
use common\external_api\dto\FinalOrderDTO;
use common\models\ReportOrder;
use common\models\ReportOrderElement;
use common\components\Ant;
use common\modules\basket\models\Basket;

class FinalOrderService implements ServiceInterface
{
    /**
     * @param $dtoList - array of FinalOrderDTO
     */
    public function load($dtoList)
    {
        /**
         * @var int $key
         * @var ReportDTO $value
         */

        foreach ($dtoList as $key => $value) {
            // Проводим удаление загруженных ранее заказов и элементов
            ReportOrder::deleteAll(['order_id' => $value->getId()]);
            ReportOrderElement::deleteAll(['order_id' => $value->getId()]);

            // Добавляем полученные данные
            $reportOrder = new ReportOrder();
            $order_id = $reportOrder->order_id = $value->getId();
            $reportOrder->comment = $value->getComment();
            $reportOrder->save();
            /**
             * @var integer $keyP
             * @var ProductDTO $valueP
             */
            $idstr = "";

            foreach ($value->getProducts() as $keyP => $valueP) {
                $reportOrderElement = new ReportOrderElement();
                $reportOrderElement->report_order_id = $reportOrder->id;
                $reportOrderElement->order_id = $valueP->getOrderId();
                $reportOrderElement->element_id = $valueP->getId();
                $reportOrderElement->count = $valueP->getCount();
                $reportOrderElement->price = $valueP->getPrice();
                $reportOrderElement->discounted_price = $valueP->getDiscountedPrice();            
                $reportOrderElement->save();
                $idstr .= "'".$valueP->getId()."',";
                //$idstr .= $valueP->getId().",";
            }
            $idstr = mb_substr($idstr,0,-1);

            $re = ReportOrderElement::find()->where(['order_id'=>$order_id])->all();

            $basket = Basket::find()->where(['external_id'=>$order_id])->One();        

            $ant = new Ant();
            $emailfrom = $ant->emailfrom;        
            $theme = "iParts :: Заказ {$order_id}";        
            $mssg = Yii::$app->getView()->render('@frontend/views/layouts/inc/_mail_final_order_to_client.php',['re'=>$re]);

            $error = '';
            $validator = new \yii\validators\EmailValidator();
            if ($basket->user->isSendEmail() && ($validator->validate($basket->user->email, $error))) {
                // Пока отключил данную рассылку, т.к. предположительно по бизнес процессу она не нужна и вероятно работает не верно.
                // Необходимо будет в будущем проговорить с бизнесом надо ли её восстанавливать
                /*Yii::$app->mailer->compose()
                    ->setFrom($emailfrom)
                    ->setTo([$basket->user->email])
                    ->setSubject($theme)
                    ->setHtmlBody($mssg)
                    ->send();*/
            }
        }
    }
}