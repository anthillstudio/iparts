<?php

namespace common\external_api\services;

use common\components\Api;
use \common\external_api\dto\CatalogDTO;
use \common\models\Catalog;
use \common\models\CatalogElements;

class CatalogService implements ServiceInterface
{
    private $imageFileMinWH = 50;

    /**
     * @param $dtoList - array of CatalogDTO
     */
    public function load($dtoList)
    {
        $wasCatalogUpdated = false;
        /**
         * @var int $key
         * @var CatalogDTO $value
         */
        foreach ($dtoList as $key => $value) {
            if ($this->isCatalog($value)) { // Загружаем каталоги
                $catalog = Catalog::findOne($value->getId());
                if (is_null($catalog)) {
                    $catalog = new Catalog();
                    $catalog->insert_date = date("Y-m-d H:i:s");
                }
                $catalog->id = $value->getId();
                $catalog->parent = $value->getParentId();
                $catalog->update_date = date("Y-m-d H:i:s");
                $catalog->user_id = 0;
                $catalog->title = $value->getName();
                $catalog->level = $this->getXLevel($value);
                $catalog->show = 1;

                $res = $catalog->save();
                if (!$res) {
                    die("error");
                }
                $wasCatalogUpdated = true;
            }
            else { // Загружаем товары
                $catalogElement = CatalogElements::findOne($value->getId());
                if (is_null($catalogElement)) {
                    $catalogElement = new CatalogElements();
                    $catalogElement->insert_date = date("Y-m-d H:i:s");
                }
                $catalogElement->id = $value->getId();
                $catalogElement->parent = $value->getParentId();
                $catalogElement->update_date = date("Y-m-d H:i:s");
                $catalogElement->user_id = 0;
                $catalogElement->image_file = $value->getPicture();
                $catalogElement->image_file_mini = $this->getImgMiniFromBase64($value->getPicture(), $this->imageFileMinWH);
                $catalogElement->ref = $value->getArt();
                $catalogElement->title = $value->getName();
                // TODO: Выяснить надо ли загружать цены вместе с каталогом
                // Почему-то цены не загружаются вместе со всем каталогом, это странно, пока решил не менять
                // Предыдущую цену тоже пока добавляю закомментированной
                //$catalogElement->price1 = $value->getPrice();
                //$catalogElement->price2 = $value->getPrice2();
                //$catalogElement->prev_price = $value->getPrevPrice();
                $catalogElement->is_action = $value->getIsAction();
                $catalogElement->show = 1;

                $res = $catalogElement->save();
                if (!$res) {
                    die("error");
                }
            }
        }

        // Обновляем иерархию каталогов
        if ($wasCatalogUpdated) {
            Api::buildCatJson();
        }
    }

    /**
     * Проверяем является ли объект DTO каталогом
     * @param CatalogDTO $dto
     * @return bool - true = каталог, false = элемент
     */
    protected function isCatalog(CatalogDTO $dto)
    {
        $isCatalog = false;
        if (($dto->getIsGroup() == 1) && ($dto->getParentId() != 'null')) {
            $isCatalog = true;
        }
        return $isCatalog;
    }

    /**
     * Получаем значение xLevel
     * @param CatalogDTO $dto
     * @return int - значение xLevel
     */
    protected function getXLevel(CatalogDTO $dto)
    {
        if ($dto->getId() == '933a9c06-7c0b-11e5-9092-086266371bb4') {
            $xlevel = 0;
        }
        else {
            $xlevel = 10;
        }
        return $xlevel;
    }

    /**
     * Получаем миниатюру из исходной картинки
     * @param $img исходное изображение
     * @param $minSize размер миниатюры
     */
    protected function getImgMiniFromBase64($img, $minSize)
    {
        if (!empty($img)) {
            $image = new \Imagick();
            $image->readImageBlob(base64_decode($img));
            $w = $minSize;
            $h = 0;
            if ($image->getImageWidth() < $image->getImageHeight()) {
                $w = 0;
                $h = $minSize;
            }
            $image->thumbnailImage($w, $h);
            return base64_encode($image->getImageBlob());
        }
        else {
            return '';
        }
    }
}