<?php

namespace common\external_api\mappers;

/**
 * Class AbstractMapper
 * @package common\external_api\mappers
 */

abstract class AbstractMapper implements MapperInterface
{
    protected $key;

    public function __construct($key = null)
    {
        $this->key = $key;
    }

    public function mappingXml($xml)
    {

        $obj = new \SimpleXMLElement($xml);

        if ($this->key != null) {         
            if (is_array($this->key)) {                
                $objToProcess = $obj;            
                foreach ($this->key as $k => $v) {
                    $objToProcess = $objToProcess->{$v};
                }        
            }
            else {                                    
                $objToProcess = $obj->{$this->key};                                
            }
        }
        else {
            $objToProcess = $obj;
        }
           

        $dtoList = $this->mapping($objToProcess);
        return $dtoList;
    }
}