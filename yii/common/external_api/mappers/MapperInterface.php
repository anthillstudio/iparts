<?php

namespace common\external_api\mappers;

interface MapperInterface
{
    public function mappingXml($xml);

    public function asXml($dtoList);

    public function mapping($xmlObj);
}