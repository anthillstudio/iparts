<?php

namespace common\external_api\mappers;

use common\external_api\dto\StatementDTO;
use common\external_api\dto\StatementElementDTO;

/**
 * Class StatementMapper
 * @package common\external_api\mappers

Ожидаем xml вида:

<?xml version="1.0" encoding="UTF-8"?>
<account_statements>
    <account_statement>
        <internal_id></internal_id>
        <partner_id></partner_id>
        <start_date></start_date>
        <end_date></end_date>
        <start_balance></start_balance>
        <account_movements>
            <doc>
                <descr></descr>
                <date></date>
                <summ></summ>
            </doc>
            <doc>
                <descr></descr>
                <date></date>
                <summ></summ>
            </doc>
        </account_movements>
        <end_balance></end_balance>
    </account_statement>
</account_statements>

Отдаём xml вида:

<?xml version="1.0" encoding="UTF-8"?>
<account_statements>
    <account_statement>
        <internal_id></internal_id>
        <partner_id></partner_id>
        <start_date></start_date>
        <end_date></end_date>
    </account_statement>
    <account_statement>
        <internal_id></internal_id>
        <partner_id></partner_id>
        <start_date></start_date>
        <end_date></end_date>
    </account_statement>
</account_statements>

 */

class StatementMapper extends AbstractMapper
{
    public function mapping($xmlObj)
    {
        $dtoList = array();
        foreach ($xmlObj as $value) {
            $elements = array();
            foreach ($value->account_movements->doc as $keyElement => $valueElement) {
                $dtoElement = new StatementElementDTO(
                    (string) $valueElement->descr,
                    (string) $valueElement->date,
                    (string) $valueElement->summ
                );
                $elements[] = $dtoElement;
            }
            $dto = new StatementDTO(
                (string) $value->internal_id,
                (string) $value->partner_id,
                (string) $value->start_date,
                (string) $value->end_date,
                (string) $value->start_balance,
                (string) $value->end_balance,
                $elements
            );
            $dtoList[] = $dto;
        }
        return $dtoList;
    }

    /**
     * @param $list - массив объектов StatementRequest
     */
    public function asXml($list)
    {
        $xml = '<?xml version="1.0" encoding="UTF-8"?>' . PHP_EOL;
        $xml .= '<account_statements>' . PHP_EOL;
        /**
        /**
         * @var integer $key
         * @var StatementRequest $value
         */
        foreach ($list as $key => $value) {
            $xml .= '    <account_statement>' . PHP_EOL;
            $xml .= '        <internal_id>' . $value->id . '</internal_id>' . PHP_EOL;
            $xml .= '        <partner_id>' . $value->user_id . '</partner_id>' . PHP_EOL;
            $xml .= '        <start_date>' . $value->date_from . '</start_date>' . PHP_EOL;
            $xml .= '        <end_date>' . $value->date_to . '</end_date>' . PHP_EOL;
            $xml .= '    </account_statement>' . PHP_EOL;
        }
        $xml .= '</account_statements>' . PHP_EOL;
        return $xml;
    }
}