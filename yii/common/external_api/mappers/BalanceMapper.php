<?php

namespace common\external_api\mappers;

use common\external_api\dto\PartnerCardDTO;
use common\external_api\dto\BalanceDTO;
use common\models\User;

/**
 * Class BalanceMapper
 * @package common\external_api\mappers

Ожидаем xml вида:

<partner>
    <id></id>
    <balance></balance>
    <turnover_current></turnover_current>
    <turnover_prev></turnover_prev>
    <cards>
        <card>
            <cardname></cardname>
            <cardcode></cardcode>
            <cardstatus></cardstatus>
        </card>
    </cards>
</partner>

Отдаём xml вида:

 */

class BalanceMapper extends AbstractMapper
{
    public function mapping($xmlObj)
    {
        $dtoList = array();
        foreach ($xmlObj as $value) {
            $elements = array();
            if (isset($value->cards->card) && !empty($value->cards->card)) {
                foreach ($value->cards->card as $keyElement => $valueElement) {
                    $dtoElement = new PartnerCardDTO(
                        (string) $valueElement->cardname,
                        (string) $valueElement->cardcode,
                        (string) $valueElement->cardstatus
                    );
                    $elements[] = $dtoElement;
                }
            }

            $dto = new BalanceDTO(
                (string) $value->id,
                (string) $value->balance,
                (string) $value->turnover_current,
                (string) $value->turnover_prev,
                $elements
            );
            $dtoList[] = $dto;
        }
        /*print_r("<pre>");
        print_r($dtoList);
        print_r("</pre>");
        die;*/
        return $dtoList;
    }

    /**
     * @param $list - массив объектов User
     */
    public function asXml($list)
    {

    }
}