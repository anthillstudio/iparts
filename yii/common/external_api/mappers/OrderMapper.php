<?php

namespace common\external_api\mappers;

use common\external_api\dto\OrderDTO;
use common\modules\basket\models\Basket;
use common\modules\basket\models\BasketElements;

/**
 * Class OrderMapper
 * @package common\external_api\mappers

Ожидаем xml вида:

<?xml version="1.0" encoding="UTF-8"?>
<orders>
    <order>
        <id></id>
        <internal_id></internal_id>
        <status></status>
    </order>
    <order>
        <id></id>
        <internal_id></internal_id>
        <status></status>
    </order>
</orders>

Отдаём xml вида:

<?xml version="1.0" encoding="UTF-8"?>
<orders>
    <order>
        <internal_id></internal_id>
        <insert_date></insert_date>
        <partner></partner>
        <comment></comment>
        <products>
            <product>
                <id></id>
                <count></count>
            </product>
        </products>
    </order>
</orders>

 */

class OrderMapper extends AbstractMapper
{
    public function mapping($xmlObj)
    {
        $dtoList = array();
        foreach ($xmlObj as $value) {
            // TODO: От 1С получаем статус заказа (<status>), не ясно надо ли его сохранять/обрабатывать
            $dto = new OrderDTO(
                (string) $value->id,
                (string) $value->internal_id
            );
            $dtoList[] = $dto;
        }
        return $dtoList;
    }

    /**
     * @param $orderList - массив объектов Basket
     */
    public function asXml($orderList)
    {
        $xml = '<?xml version="1.0" encoding="UTF-8"?>' . PHP_EOL;
        $xml .= '<orders>' . PHP_EOL;
        /**
         * @var integer $key
         * @var Basket $value
         */
        foreach ($orderList as $keyOrder => $valueOrder) {
            $order = '    <order>' . PHP_EOL;
            $order .= '        <internal_id>' . $valueOrder->id . '</internal_id>' . PHP_EOL;
            $order .= '        <insert_date>' . $valueOrder->insert_date . '</insert_date>' . PHP_EOL;
            $order .= '        <partner>' . $valueOrder->user_id . '</partner>' . PHP_EOL;
            $order .= '        <comment>' . $valueOrder->comment . '</comment>' . PHP_EOL;
            $order .= '        <products>' . PHP_EOL;
            /**
             * @var integer $keyProduct
             * @var BasketElements $valueProduct
             */
            foreach ($valueOrder->basketElements as $keyProduct => $valueProduct) {
                $product = '            <product>' . PHP_EOL;
                $product .= '                <id>' . $valueProduct->catalog_element_id . '</id>' . PHP_EOL;
                $product .= '                <count>' . $valueProduct->count . '</count>' . PHP_EOL;
                $product .= '            </product>' . PHP_EOL;
                $order .= $product;
            }
            $order .= '        </products>' . PHP_EOL;
            $order .= '    </order>' . PHP_EOL;
            $xml .= $order;
        }
        $xml .= '</orders>' . PHP_EOL;
        return $xml;
    }
}