<?php

namespace common\external_api\mappers;

use common\external_api\dto\PartnerCardDTO;
use common\external_api\dto\PartnerDTO;

/**
 * Class PartnerMapper
 * @package common\external_api\mappers

Ожидаем xml вида:

<?xml version="1.0" encoding="UTF-8"?>
<partners>
    <partner>
        <id></id>
        <name></name>
        <email></email>
        <turnover_current></turnover_current>
        <turnover_prev></turnover_prev>
        <cards>
            <card>
                <cardname></cardname>
                <cardcode></cardcode>
                <cardstatus></cardstatus>
            </card>
        </cards>
    </partner>
</partners>

 */

class PartnerMapper extends AbstractMapper
{
    public function mapping($xmlObj)
    {

        $dtoList = array();
        foreach ($xmlObj as $value) {
            $elements = array();
            if (isset($value->cards->card) && !empty($value->cards->card)) {
                foreach ($value->cards->card as $keyElement => $valueElement) {
                    $dtoElement = new PartnerCardDTO(
                        (string) $valueElement->cardname,
                        (string) $valueElement->cardcode,
                        (string) $valueElement->cardstatus
                    );
                    $elements[] = $dtoElement;
                }
            }

            $dto = new PartnerDTO(
                (string) $value->id,
                (string) $value->name,
                (string) $value->email,
                (string) $value->isDeleted,
                (string) $value->turnover_current,
                (string) $value->turnover_prev,
                (string) $value->cards->card->cardcode,
                $elements
            );
            $dtoList[] = $dto;
        }
        return $dtoList;
    }

    public function asXml($dtoList)
    {
        // TODO: Implement asXml() method.
    }
}