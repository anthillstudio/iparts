<?php

namespace common\external_api\mappers;

use common\external_api\dto\RegistrationnoticeDTO;

/**
 * Class PartnerMapper
 * @package common\external_api\mappers

Ожидаем xml вида:

<?xml version="1.0" encoding="UTF-8"?>
<partners>
<partner>
<id></id>
</partner>
<partner>
<id></id>
</partner>
</partners>
 */

class RegistrationnoticeMapper extends AbstractMapper
{
    public function mapping($xmlObj)
    {
        $dtoList = array();
        foreach ($xmlObj as $value) {
            $dto = new RegistrationnoticeDTO(
                (string) $value->id
            );
            $dtoList[] = $dto;
        }
        return $dtoList;
    }

    public function asXml($dtoList)
    {
        // TODO: Implement asXml() method.
    }
}