<?php

namespace common\external_api\mappers;

use \common\external_api\dto\CatalogDTO;

/**
 * Class CatalogMapper
 * @package common\external_api\mappers

Ожидаем xml вида:

<?xml version="1.0" encoding="UTF-8"?>
<catalog>
    <product>
        <id></id>
        <name></name>
        <isGroup></isGroup>
        <parenId></parenId>
        <art/>
        <Description/>
    </product>
    <product>
        <id></id>
        <name></name>
        <isGroup></isGroup>
        <parenId></parenId>
        <art></art>
        <Description/>
        <prices>
            <currency></currency>
            <price></price>
            <price2></price2>
            <prevPrice></prevPrice>
        </prices>
        <isAction></isAction>
    </product>
</catalog>

 */

class CatalogMapper extends AbstractMapper
{
    public function mapping($xmlObj)
    {
        $dtoList = array();
        foreach ($xmlObj as $key => $value) {
            $dto = new CatalogDTO(
                (string) $value->id,
                (string) $value->name,
                (string) $value->isGroup,
                (string) $value->parenId,
                (string) $value->art,
                (string) $value->Description,
                (string) $value->prices->price,
                (string) $value->prices->price2,
                (string) $value->picture,
                (string) (isset($value->isAction) ? $value->isAction : "0"),
                (string) (isset($value->prices->prevPrice) ? $value->prices->prevPrice : "0")
            );
            $dtoList[] = $dto;
        }
        return $dtoList;
    }

    public function asXml($dtoList)
    {
        // TODO: Implement asXml() method.
    }
}