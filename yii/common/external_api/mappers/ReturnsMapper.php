<?php

namespace common\external_api\mappers;

use common\external_api\dto\ReturnsDTO;
use common\external_api\dto\ReturnsElementsDTO;
//use common\models\ReturnsElements;
//use common\models\User;

/**
 * Class ReturnsMapper
 * @package common\external_api\mappers

Ожидаем xml вида:

<data_upd>
    <purchase_returns_upd>
        <partner>
            <id></id>
            <doc_id></doc_id>
            <date></date>
            <region_id></region_id>
            <status></status>
            <products>
                <product>
                    <id></id>
                    <count></count>
                    <price></price>
                </product>
            </products>
        </partner>
    </purchase_returns_upd>
</data_upd>

Отдаём xml вида:

 */



class ReturnsMapper extends AbstractMapper
{
    public function mapping($xmlObj)
    {
        $dtoList = array();
        foreach ($xmlObj as $value) {
            $elements = array();
            foreach ($value->products->product as $keyElement => $valueElement) {
                $dtoElement = new ReturnsElementsDTO(
                    (string) $valueElement->id,
                    (string) $valueElement->count,
                    (string) $valueElement->price
                );
                $elements[] = $dtoElement;
            }
            $dto = new ReturnsDTO(
                (string) $value->id,
                (string) $value->doc_id,
                (string) $value->date,
                (int) (($value->status == "true") ? 1 : 0),
                (string) $value->region_id,
                $elements
            );
            $dtoList[] = $dto;
        }

        return $dtoList;
    }

    public function asXml($dtoList)
    {
        // TODO: Implement asXml() method.
    }
}