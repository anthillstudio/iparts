<?php

namespace common\external_api\mappers;

use \common\external_api\dto\PriceDTO;

/**
 * Class PriceMapper
 * @package common\external_api\mappers

Ожидаем xml вида:

<product>
    <id></id>
    <prices>
        <currency></currency>
        <price></price>
        <price2></price2>
        <prevPrice></prevPrice>
    </prices>
    <isAction></isAction>
</product>

 */

class PriceMapper extends AbstractMapper
{
    public function mapping($xmlObj)
    {
        $dtoList = array();
        foreach ($xmlObj as $key => $value) {
            $dto = new PriceDTO(
                (string) $value->id,
                (string) $value->prices->price,
                (string) $value->prices->price2,
                (string) (isset($value->isAction) ? $value->isAction : "0"),
                (string) (isset($value->prices->prevPrice) ? $value->prices->prevPrice : "0")
            );
            $dtoList[] = $dto;
        }
        return $dtoList;
    }

    public function asXml($dtoList)
    {
        // TODO: Implement asXml() method.
    }
}