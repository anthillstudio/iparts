<?php

namespace common\external_api\mappers;

use \common\external_api\dto\RegionDTO;

/**
 * Class RegionMapper
 * @package common\external_api\mappers

Ожидаем xml вида:

<?xml version="1.0" encoding="UTF-8"?>
<regions>
    <region>
        <id></id>
        <name></name>
        <activity></activity>
    </region>
    <region>
        <id></id>
        <name></name>
        <activity></activity>
    </region>
</regions>

 */

class RegionMapper extends AbstractMapper
{
    public function mapping($xmlObj)
    {
        $dtoList = array();
        foreach ($xmlObj as $key => $value) {
            $dto = new RegionDTO(
                (string) $value->id,
                (string) $value->name,
                (int) (($value->activity == "true") ? 1 : 0)
            );
            $dtoList[] = $dto;
        }
        return $dtoList;
    }

    public function asXml($dtoList)
    {
        // TODO: Implement asXml() method.
    }
}