<?php

namespace common\external_api\mappers;

use common\external_api\dto\PurchaseDTO;
use common\external_api\dto\PurchaseElementsDTO;

/**
 * Class PurchaseMapper
 * @package common\external_api\mappers

Ожидаем xml вида:

<data_upd>
    <purchase_upd>
        <partner>
            <id></id>
            <doc_id></doc_id>
            <order_id></order_id>
            <date></date>
            <region_id></region_id>
            <status></status>
            <products>
                <product>
                    <id></id>
                    <count></count>
                    <price></price>
                </product>
            </products>
        </partner>
    </purchase_upd>
</data_upd>

Отдаём xml вида:

 */



class PurchaseMapper extends AbstractMapper
{
    public function mapping($xmlObj)
    {
        $dtoList = array();
        foreach ($xmlObj as $value) {
            $elements = array();
            foreach ($value->products->product as $keyElement => $valueElement) {
                $dtoElement = new PurchaseElementsDTO(
                    (string) $valueElement->id,
                    (string) $valueElement->count,
                    (string) $valueElement->price
                );
                $elements[] = $dtoElement;
            }
            $dto = new PurchaseDTO(
                (string) $value->id,
                (string) $value->doc_id,
                (string) (isset($value->order_id) ? $value->order_id : ""),
                (string) $value->date,
                (int) (($value->status == "true") ? 1 : 0),
                (string) $value->region_id,
                $elements
            );
            $dtoList[] = $dto;
        }

        return $dtoList;
    }

    public function asXml($dtoList)
    {
        // TODO: Implement asXml() method.
    }
}