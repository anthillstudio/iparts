<?php

namespace common\external_api\mappers;

use common\external_api\dto\CurrencyDTO;

/**
 * Class CurrencyMapper
 * @package common\external_api\mappers

Ожидаем xml вида:

<?xml version="1.0" encoding="UTF-8"?>
<currency_rates>
    <currency_rate>
        <currency></currency>
        <rate></rate>
    </currency_rate>
    <currency_rate>
        <currency></currency>
        <rate></rate>
    </currency_rate>
</currency_rates>

 */

class CurrencyMapper extends AbstractMapper
{
    public function mapping($xmlObj)
    {
        $dtoList = array();
        foreach ($xmlObj as $value) {
            $dto = new CurrencyDTO(
                (string) $value->currency,
                (string) $value->rate
            );
            $dtoList[] = $dto;
        }
        return $dtoList;
    }

    public function asXml($dtoList)
    {
        // TODO: Implement asXml() method.
    }
}