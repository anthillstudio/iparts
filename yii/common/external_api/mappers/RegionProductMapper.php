<?php

namespace common\external_api\mappers;

use \common\external_api\dto\RegionProductDTO;

/**
 * Class RegionProductMapper
 * @package common\external_api\mappers

Ожидаем xml вида:

<?xml version="1.0" encoding="UTF-8"?>
<regions_products>
    <region>
        <region_id></region_id>
        <products>
            <product_id></product_id>
            <product_id></product_id>
        </products>
    </region>
    <region>
        <region_id></region_id>
        <products>
            <product_id></product_id>
            <product_id></product_id>
            <product_id></product_id>
        </products>
    </region>
</regions_products>

 */

class RegionProductMapper extends AbstractMapper
{
    public function mapping($xmlObj)
    {
        $dtoList = array();
        foreach ($xmlObj as $key => $value) {
            $dto = new RegionProductDTO(
                (string) $value->region_id,
                (array) $value->products->product_id
            );
            $dtoList[] = $dto;
        }
        return $dtoList;
    }

    public function asXml($dtoList)
    {
        // TODO: Implement asXml() method.
    }
}