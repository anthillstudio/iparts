<?php

namespace common\external_api\mappers;

use common\external_api\dto\ProductDTO;
use common\external_api\dto\FinalOrderDTO;

/**
 * Class FinalOrderMapper
 * @package common\external_api\mappers

Ожидаем xml вида:

<?xml version="1.0" encoding="UTF-8"?>
<orders>
    <order>
        <order_id></ order_id>
        <comment></ comment >
        <products>
            <product>
                <id></id>
                <count></count>
                <price></price>
                <discounted_price></discounted_price>
            </product>
        </products>
    </order>
</orders>

Отдаём xml вида:

 */

class FinalOrderMapper extends AbstractMapper
{
    public function mapping($xmlObj)
    {
        $dtoList = array();
        foreach ($xmlObj as $value) {
            $products = array();
            foreach ($value->products->product as $keyProduct => $valueProduct) {
                $dtoProduct = new ProductDTO(
                    (string) $valueProduct->id,
                    (string) $value->order_id,
                    (string) $valueProduct->count,
                    (string) $valueProduct->price,
                    (string) $valueProduct->discounted_price
                );
                $products[] = $dtoProduct;
            }
            $dto = new FinalOrderDTO(
                (string) $value->order_id,
                (string) $value->comment,
                $products
            );
            $dtoList[] = $dto;
        }
        return $dtoList;
    }

    public function asXml($dtoList)
    {
        // TODO: Implement asXml() method.
    }
}