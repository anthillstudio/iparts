<?php

namespace common\external_api\managers;

use common\external_api\mappers\MapperInterface;
use common\external_api\services\ServiceInterface;

class LoadManager
{
    /** @var MapperInterface $mapper */
    protected $mapper;
    /** @var ServiceInterface $service */
    protected $service;

    public function __construct(MapperInterface $mapper, ServiceInterface $service)
    {

        $this->mapper = $mapper;
        $this->service = $service;
    }

    public function loadFromXml($xml)
    {        
        $list = $this->mapper->mappingXml($xml);    
        $this->service->load($list);
    }
}