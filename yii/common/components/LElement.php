<?php

namespace common\components;

use Yii;
use common\components\Ant;

class LElement extends Ant {




   /* public $loadFilesSettings = array(
        "icon" => array(
            "dir" => "icon",			//папка в которую загружать файл
            "height" => 700,			//высота
            "width" => 700,				//ширина
            "isCatFile" => true,		//обрезать файл (true - да, false - нет)
        ),
        "show" => array(
            "dir" => "show",			//папка в которую загружать файл
            "height" => 400,			//высота
            "width" => 400,				//ширина
            "isCatFile" => true,		//обрезать файл (true - да, false - нет)
        ),
        "max" => array(
            "dir" => "max",				//папка в которую загружать файл
            "height" => 600,			//высота
            "width" => 600,				//ширина
            "isCatFile" => false,		//обрезать файл (true - да, false - нет)
        ),
    );*/

   /*public $whereToShow;
    public $fieldToOrder;
    public $directionToOrder;
    public $orderByLevel;
    public $orderByInsertDate;
    public $viewElementsByPage = true;
    public $startElementByPage;
    public $countElementByPage;
    public $countAllElementByPage;*/


    function LElement($dataBaseObject, $params, $securityObject) {
        //$this->init($dataBaseObject, $params, $securityObject);
    }


    /*
     * Функция проверки наличия красивой ссылки и в случае её наличия добавляет к ней "-[случайное число]"
     * $beautifulURL - красивая ссылка для проверки
     * $realURL - реальная ссылка соответствующая данной красивой
     * Возвращает:
     * true если такая красивая ссылка ещё не используется
     * false если такая красивая ссылка уже используется
     */


    /*
     * СОРТИРОВКА ЭЛЕМЕНТОВ (ПЕРЕМЕЩЕНИЕ БОЛЕЕ ЧЕМ ЧЕРЕЗ ОДИН ЭЛЕМЕНТ)
     */
    function moveAfterIdRecord($object) {

        $elementA = $object::findOne($_GET["el"]);
        $elementB = $object::findOne($_GET["val"]);

        if (
            isset($elementA)
            && !empty($elementA)
            && isset($elementB)
            && !empty($elementB)
        ) {

            $levelA = $elementA->level;
            $levelB = $elementB->level;

            if ($levelA > $levelB){

                $elements = $object::find()
                    ->where(['<','level',$levelA])
                    ->andWhere(['>','level',$levelB])
                    ->orderBy(['level'=>SORT_DESC])
                    ->all();

                foreach($elements as $key=>$value){
                    $vel = $object::findOne($value->id);
                    $vel->level = $value->level + 10;
                    $vel->save();
                }

                $elementA->level = $levelB + 10;
                $elementA->save();

            }else{

                $elements = $object::find()
                    ->where(['>','level',$levelA])
                    ->andWhere(['<=','level',$levelB])
                    ->orderBy(['level'=>SORT_DESC])
                    ->all();

                foreach($elements as $key=>$value){
                    $vel = $object::findOne($value->id);
                    $vel->level = $value->level - 10;
                    $vel->save();
                }

                $elementA->level = $levelB;
                $elementA->save();

            }

        }else{
            return false;
        }

    }





}