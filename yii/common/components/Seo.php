<?php

namespace common\components;

use Yii;


class Seo
{

    //public $title;

    static function metategs($element){


        if (isset($element['title'])) {
            $seo_description = $element['title'];
            $seo_keywords = $element['title'];
        }

        if (trim($element['seo_description'])!='')
        $seo_description = $element['seo_description'];

        if (trim($element['seo_keywords'])!='')
        $seo_keywords = $element['seo_keywords'];

        Yii::$app->view->registerMetaTag([
            'name' => 'description',
            'content' => $seo_description,
        ]);
        Yii::$app->view->registerMetaTag([
            'name' => 'keywords',
            'content' => $seo_keywords,
        ]);
    }


    static function settitle($element){

        $seo_title = $element['title'];

        if (trim($element['seo_title'])!='')
            $seo_title = $element['seo_title'];

        return $seo_title;

    }


}