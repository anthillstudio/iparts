<?php

namespace common\components;

use Yii;
//use yii\imagine\Image;

class Files
{

    public function addWatermark($imgPathFrom, $imgPathTo, $wmPath="", $position="0"){

        $toReturn = false;
        switch ($position){
            case "":
                break;
            default:

                //$wpPath = Yii::getAlias("@yii/images/wm.png");
                //$wpPath = dirname(__DIR__)."/images/wm.png";
                $wmSize = getimagesize($wmPath);
                $wmW = $wmSize[0]; $wmH = $wmSize[1];

                $imgSize = getimagesize($imgPathFrom);
                $imgW = $imgSize[0]; $imgH = $imgSize[1];

                //$x = new Image;
                //print_r($x);
                //die;*/

                Image::watermark(
                    $imgPathFrom,
                    $wmSize, [10,10]
                )->save(
                    $imgPathTo
                );

                $toReturn = true;
                break;
        }

        return $toReturn;
    }


}
