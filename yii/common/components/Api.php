<?php

namespace common\components;

use Yii;
use common\models\Catalog;
use common\models\CatalogElements;

class Api
{

    public $parents;

    public function importcats()
    {

        $xmlstring = Yii::$app->params['catalogimport'];

        $xmlstring = file_get_contents($xmlstring);

        $xml = simplexml_load_string($xmlstring);
        $json = json_encode($xml);
        $array = json_decode($json, TRUE);

        Yii::$app->db->createCommand("UPDATE `iparts_catalog` SET `show`=0")->execute();

        $level = 10;
        foreach ($array['product'] as $key => $value) {

            if ($value['isGroup']==1 && $value['parenId']!='null') {

                if ($value['id']=='933a9c06-7c0b-11e5-9092-086266371bb4'){
                    $xlevel = 0;
                }else{
                    $xlevel = 10;
                }

                $arr = [
                    'id' => $value['id'],
                    'parent' => $value['parenId'],
                    'insert_date' => date("Y-m-d H:i:s"),
                    'update_date' => date("Y-m-d H:i:s"),
                    'user_id' => 0,
                    'title' => str_replace("'","`",$value['name']),
                    'level' => $xlevel,
                    'show' => 1,
                ];

                $xk = "";
                $xv = "";

                foreach ($arr as $k => $v) {
                    $xk .= "`{$k}`, ";
                    $xv .= "'{$v}', ";
                }
                $xk = substr($xk, 0, -2);
                $xv = substr($xv, 0, -2);

                $sql = "INSERT INTO `iparts_catalog` ({$xk}) VALUES ({$xv});";
                Yii::$app->db->createCommand()->delete("iparts_catalog", "id = '{$value['id']}'")->execute();
                Yii::$app->db->createCommand($sql)->execute();


                $level = $level + 10;
            }

        }

        $this->buildCatJson();


    }

    public static function getPCatJson()
    {
        $url = "../site/files/parents.json";
        $content = file_get_contents($url);
        $content = json_decode($content,true);
        return $content;
    }

    public static function getCatJson()
    {
        $url = "../site/files/children.json";
        $content = file_get_contents($url);
        $content = json_decode($content,true);
        return $content;
    }

    public static function buildCatJson(){
        global $childcat;
        $catalogs = Catalog::find()->all();
        $xxx = $yyy = "{";
        $childcat = [];
        $arrx = [];

        foreach ($catalogs as $key=>$val) {
            $arrx[$val->id] = Api::getChildCat($val->id);
            $arrx[$val->id][] = $val->id;

            $py = implode(",",$arrx[$val->id]);
            $yyy .= "\"{$val->id}\":\"{$py}\",";

            $childcat = [];
        }

        foreach ($catalogs as $key=>$val) {
            $child = "";
            foreach ($arrx as $k=>$v) {
                if (in_array($val->id,$v)){
                    $child .= "{$k},";
                }
            }
            $child = substr($child,0,-1);

            $xxx .= "\"{$val->id}\":\"{$child}\",";
            $childcat = "";
        }
        $xxx = substr($xxx,0,-1);
        $yyy = substr($yyy,0,-1);



        $xxx .= "}";
        $fp = fopen("../site/files/children.json", "w");
        fwrite($fp, $xxx);
        fclose($fp);


        $yyy .= "}";
        $fp = fopen("../site/files/parents.json", "w");
        fwrite($fp, $yyy);
        fclose($fp);


    }

    public static function getChildCat($id){
        global $childcat;
        $el = Catalog::find()->where(['id'=>$id])->One();
            if (!empty($el)) {
                $childcat[] = $el->parent;
                Api::getChildCat($el->parent);
            }
        return $childcat;
    }


    public function importgoods()
    {

        $xmlstring = Yii::$app->params['catalogimport'];

        $xmlstring = file_get_contents($xmlstring);

        $xml = simplexml_load_string($xmlstring);
        $json = json_encode($xml);
        $array = json_decode($json, TRUE);

        Yii::$app->db->createCommand("UPDATE `iparts_catalog_elements` SET `show`=0")->execute();

        foreach ($array['product'] as $key => $value) {

            if ($value['isGroup']==0) {

                $arr = [
                    'id' => $value['id'],
                    'parent' => $value['parenId'],
                    'insert_date' => date("Y-m-d H:i:s"),
                    'update_date' => date("Y-m-d H:i:s"),
                    'user_id' => 0,
                    'image_file' => (!empty($value['picture'])?$value['picture']:""),
                    'ref' => (is_array($value['art'])?"":$value['art']),
                    'title' => str_replace("'","`",$value['name']),
                    'price1' => $value['prices']['price'],
                    'price2' => $value['prices']['price2'],
                    'show' => 1,
                ];

                $xk = "";
                $xv = "";

                foreach ($arr as $k => $v) {
                    $xk .= "`{$k}`, ";
                    $xv .= "'{$v}', ";
                }
                $xk = substr($xk, 0, -2);
                $xv = substr($xv, 0, -2);


                $sql = "INSERT INTO `iparts_catalog_elements` ({$xk}) VALUES ({$xv});";
                Yii::$app->db->createCommand()->delete("iparts_catalog_elements", "id = '{$value['id']}'")->execute();
                Yii::$app->db->createCommand($sql)->execute();

                //echo "<div style='font-size: 8pt; width: 500px; margin-bottom: 10px'>{$sql}</div>";

            }

        }

    }



}