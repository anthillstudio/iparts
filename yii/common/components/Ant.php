<?php

namespace common\components;

use Yii;

class Ant
{


    public $emailfrom = "info@iparts.by";

    public $typeOfPayment = [
        '1'=>'Наличными',
        '2'=>'Карточкой',
    ];



    public $titleStyle = [
        '1' => 'Default',
        '2' => 'Изображение в шапке',
        '3' => 'Mini',
        '4' => 'Без заголовка',
        '5' => 'Default (с датой для мероприятий)',
        '6' => 'Изображение в шапке (с датой для мероприятий)',
    ];

    public $emails = [
        'messages' => [ // Электронное обращение
            1 => [
                'info@anthill.by',
                'info@iparts.by',
                'iparts.by@gmail.com'
                //'bezbiz@bk.ru',
            ],
            2 => [],
            3 => [],
        ],
        'orders' => [ // Электронное обращение
            1 => [
                'info@anthill.by',
                'info@iparts.by',
                'iparts.by@gmail.com',
                //'bezbiz@bk.ru'
            ],
            2 => [],
            3 => [],
        ],
    ];


    public function getBasketCount(){
        $session = Yii::$app->session;
        if (!$session->isActive) {
            $session->open();
        }
        $bcount = 0;
        if (isset($_SESSION['basket'])){
            if (count($_SESSION['basket'])!=0){
                foreach ($_SESSION['basket'] as $key=>$val) {
                    foreach ($val as $k=>$v) {
                        $bcount++;
                    }
                }
            }
        }
        return $bcount;
    }

    /*public function buildBasketBttn(){
        $bcount = $this->getBasketCount();
        $toReturn = "<a href='/basket'>Корзина (<i>{$bcount}</i>)</a>";
        return $toReturn;
    }*/

/*    static function getProjectsList(){
        $params = Yii::$app->params;
        $toReturn = [];
        foreach($params['projects'] as $key=>$value){
            $toReturn[$key] = $value['title'];
        }
        return $toReturn;
    }

    function getProjects(){
        $params = Yii::$app->params;
        return $params['projects'];
    }*/

    static function uploaderInfo()
    {
        $params = Yii::$app->params;
        $uploader = $params['uploader'];
        return $uploader;
    }

    /*static function getProjectById($id)
    {
        $params = Yii::$app->params;
        $project = $params['projects'][$id];
        return $project;
    }

    */
    static function buildFileName($id,$imageFile){
        return $id.'X.'.$imageFile->extension;
        //return $id.'X'.rand(1,99999).'.'.$imageFile->extension;
    }

    public function buildDate($date,$d='',$t=''){
        $toReturn = "<div class='datebox'>";
        $toReturn .= "<span class='datebox-date'>".Yii::$app->formatter->asDate($date, "{$d}")."</span>";
        if ($t!='') $toReturn .= "<span class='datebox-time'>".Yii::$app->formatter->asDate($date, " {$t} ")."</span>";
        $toReturn .= "</div>";
        return $toReturn;
    }



    public function sort($el1,$el2,$object)
    {

        $el1. " - " .$el2;

        $row1 = $object::findOne($el1);
        $row2 = $object::findOne($el2);

        $elements = $object::find()
            ->where(['<','level',$row1->level])
            ->andWhere(['>','level',$row2->level])
            ->orderBy(['level'=>SORT_DESC])
            ->all();

        $row1->level = $row2->level + 10;
        $row1->save();

        foreach($elements as $key=>$value){
            $vel = $object::findOne($value->id);
            $vel->level = $value->level + 10;
            $vel->save();
        }

    }


    public function verifyBeautifulURL($model) {
        if (Ant::verifyExistBeautifulURL($model->beautiful_url)) {
            $beautifulURLOld = $model->beautiful_url;
            $isGenerate = false;
            while (!$isGenerate) {
                $model->beautiful_url = $beautifulURLOld . "-" . rand(1, 999999);
                if (!Ant::verifyExistBeautifulURL($model->beautiful_url)) {
                    $isGenerate = true;
                }
            }
        }
        //return $model->beautiful_url;
    }

    /*
     * Функция проверки наличия красивой ссылки
     * $beautifulURL - красивая ссылка для проверки
     * $realURL - реальная ссылка соответствующая данной красивой
     * Возвращает:
     * true если такая красивая ссылка ещё не используется
     * false если такая красивая ссылка уже используется
     */
    public function verifyExistBeautifulURL($url) {
        $toReturn = false;
        $element = Yii::$app->db->createCommand("
          SELECT *
          FROM {{%beautiful_url}}
          WHERE `url` = '{$url}'
        ")->queryAll();
        if (count($element)!=0){
            $toReturn = true;
        }
        return $toReturn;
    }



    public function getMaxLevel($table,$parent){
        $element = Yii::$app->db->createCommand("
          SELECT max(level) as level
          FROM {$table}
        ")->queryOne();
        return $element['level'];
    }

    public function xurl(){
        if (isset($_GET['base'])) $url = "{$_GET['base']}/";
        if (isset($_GET['url'])) $url .= "{$_GET['url']}/";
        $url = substr($url,0,-1);
        return $url;
    }



    public function beautyURL($model, $column = 'title_lng'){

        if (trim($model->beautiful_url) == ''){

            /*
             * Запись новая или
             * ссылка удалена для
             * автоматического
             * формирования
             */

            $model->beautiful_url = $model->baseUrlPath.Ant::buildURL($model->$column);
            Ant::verifyBeautifulURL($model);

            //echo $model->beautiful_url; die;

            if ($model->oldAttributes['beautiful_url']==''){
                /*
                 * Запись новая
                 */
                Yii::$app->db->createCommand("
                    INSERT INTO {{%beautiful_url}}
                    SET `url` = '{$model->beautiful_url}'
                    "
                )->execute();
            }else{
                /*
                 * Ссылка удалена для
                 * автоматического
                 * формирования
                 */
                Yii::$app->db->createCommand("
                    UPDATE {{%beautiful_url}}
                    SET `url` = '{$model->beautiful_url}'
                    WHERE `url`= '{$model->oldAttributes['beautiful_url']}' "
                )->execute();
            }


        }else{

            /*
             * Ссылка прописана для новой записи (чтобы не формировать автоматически)
             * или ссылка не изменена в существующей записи
             */

            //echo $model->oldAttributes['beautiful_url']." - ".$model->beautiful_url; die;

            if ($model->oldAttributes['beautiful_url'] != $model->beautiful_url){
                Yii::$app->db->createCommand("
                    UPDATE {{%beautiful_url}}
                    SET `url` = '{$model->beautiful_url}'
                    WHERE `url`= '{$model->oldAttributes['beautiful_url']}' "
                )->execute();
            }
        }

        return $model->beautiful_url;
    }

    public function buildURL($url = ""){

        $new = "";
        if ($url!="") {

            $urls = array(
                "-" => "-",
                "_" => "-",
                " " => "-",
                "." => "-",

                "0" => "0",
                "1" => "1",
                "2" => "2",
                "3" => "3",
                "4" => "4",
                "5" => "5",
                "6" => "6",
                "7" => "7",
                "8" => "8",
                "9" => "9",
                "10" => "10",

                "а" => "a",
                "А" => "a",
                "б" => "b",
                "Б" => "b",
                "в" => "v",
                "В" => "v",
                "г" => "g",
                "Г" => "g",
                "д" => "d",
                "Д" => "d",
                "е" => "e",
                "Е" => "e",
                "ё" => "e",
                "Ё" => "e",
                "ж" => "zh",
                "Ж" => "zh",
                "з" => "z",
                "З" => "z",
                "и" => "i",
                "И" => "i",
                "й" => "i",
                "Й" => "i",
                "к" => "k",
                "К" => "k",
                "л" => "l",
                "Л" => "l",
                "м" => "m",
                "М" => "m",
                "н" => "n",
                "Н" => "n",
                "о" => "o",
                "О" => "o",
                "п" => "p",
                "П" => "p",
                "р" => "r",
                "Р" => "r",
                "с" => "s",
                "С" => "s",
                "т" => "t",
                "Т" => "t",
                "у" => "u",
                "У" => "u",
                "ф" => "f",
                "Ф" => "f",
                "х" => "h",
                "Х" => "h",
                "ц" => "c",
                "Ц" => "c",
                "ч" => "ch",
                "Ч" => "ch",
                "ш" => "sh",
                "Ш" => "sh",
                "щ" => "sh",
                "Щ" => "sh",
                "ы" => "i",
                "Ы" => "i",
                "ъ" => "",
                "Ъ" => "",
                "ь" => "",
                "Ь" => "",
                "э" => "e",
                "Э" => "e",
                "ю" => "yu",
                "Ю" => "yu",
                "я" => "ya",
                "Я" => "ya",

                "A" => "a",
                "a" => "a",
                "B" => "b",
                "b" => "b",
                "C" => "c",
                "c" => "c",
                "D" => "d",
                "d" => "d",
                "E" => "e",
                "e" => "e",
                "F" => "f",
                "f" => "f",
                "G" => "g",
                "g" => "g",
                "H" => "h",
                "h" => "h",
                "I" => "i",
                "i" => "i",
                "J" => "j",
                "j" => "j",
                "K" => "k",
                "k" => "k",
                "L" => "l",
                "l" => "l",
                "M" => "m",
                "m" => "m",
                "N" => "n",
                "n" => "n",
                "O" => "o",
                "o" => "o",
                "P" => "p",
                "p" => "p",
                "Q" => "q",
                "q" => "q",
                "R" => "r",
                "r" => "r",
                "S" => "s",
                "s" => "s",
                "T" => "t",
                "t" => "t",
                "U" => "u",
                "u" => "u",
                "V" => "v",
                "v" => "v",
                "W" => "w",
                "w" => "w",
                "X" => "x",
                "x" => "x",
                "Y" => "y",
                "y" => "y",
                "Z" => "z",
                "z" => "z",
                "&" => "-and-",
                "&#246;" => "o",

            );

            $new = "";
            if (trim($url) != "") {
                for ($i = 0; $i < mb_strlen($url); $i++) {
                    $char = mb_substr($url, $i, 1);
                    if (isset($urls[$char])) $new .= $urls[$char];
                }
            }

            $new = str_replace("---", "-", $new);
            $new = str_replace("--", "-", $new);
            $new = str_replace("--", "-", $new);

            // Если первая буква "-" - удаляем ее
            if ($new[0] == '-') $new = substr($new, 1);
            // Если последняя буква "-" - удаляем ее
            if ($new[strlen($new) - 1] == '-') $new = substr($new, 0, -1);

        }

        return $new;
    }


    function fixedText($text, $limit){
        $textLength = strlen($text);
        if ($textLength > $limit){
            $text = substr($text, 0, $limit);
            $newTextLength = strlen($text);
            $marker = '';

            for ($i=0;$i<$newTextLength;$i++){
                if ($text[$i] == ' ') $marker = $i;
            }
            if ($marker != ''){
                $text = substr($text, 0, $marker);
            }else{
                $text = "";
            }
        }
        if ($textLength > $limit) $text .= " ... ";
        return $text;
    }



}