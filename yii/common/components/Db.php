<?php

namespace common\components;

use Yii;

class Db
{

    public static function irequest($type,$par=[])
    {

        $ftpcont = Yii::$app->params['ftp'];
        $conn_id = ftp_connect($ftpcont['server'],21);
        $login_result = ftp_login($conn_id, $ftpcont['name'], $ftpcont['pass']);
        $user = Yii::$app->user->getIdentity()->userInfo;

        if ((!$conn_id) || (!$login_result)) {
            echo "FTP connection has failed!";
        } else {

            switch ($type){
                case "planer":
                    $date = date("Y-m-d_H-i-s");
                    $file_a = "/requests/incoming/XXX_{$user->id}_{$date}.xml";
                    $file_b = "../yii/frontend/views/layouts/inc/requests/orders/XXX_{$user->id}_{$date}.xml";

                    $session = Yii::$app->session;
                    if (!$session->isActive) {
                        $session->open();
                    }

                    $file_b_content = "";
                    $file_b_content .= "<?xml version='1.0' encoding='WINDOWS-1251'?>\n";
                    $file_b_content .= "<Request>\n";
                    $file_b_content .= "</Request>\n";

                    /*$fp = fopen($file_b, "w");
                    fwrite($fp, $file_b_content);
                    fclose($fp);*/

                    $upload = ftp_put($conn_id, $file_a, $file_b, FTP_BINARY);
                    if (!$upload) {
                        echo "Error: FTP upload has failed!";
                    } else {
                        echo "Good: Uploaded to {$ftpcont['server']}";
                    }
                    break;
                case "add_order":
                    // Передаем информацию о заказе

                    $date = date("Y-m-d_H-i-s");
                    $file_a = "/requests/incoming/IN_{$user->id}_{$date}.xml";
                    $file_b = "../yii/frontend/views/layouts/inc/requests/orders/IN_{$user->id}_{$date}.xml";

                    $session = Yii::$app->session;
                    if (!$session->isActive) {
                        $session->open();
                    }

                    $file_b_content = "";
                    $file_b_content .= "<?xml version='1.0' encoding='WINDOWS-1251'?>\n";
                    $file_b_content .= "<Request>\n";
                    $file_b_content .= "<Command>ORDER_GOODS</Command>\n";
                    $file_b_content .= "<ClientID>{$user->id}</ClientID>\n";
                    $file_b_content .= "<StartDate>0000-00-00 00:00:00</StartDate>\n";
                    $file_b_content .= "<EndtDate>0000-00-00 00:00:00</EndtDate>\n";
                    $file_b_content .= "<Goods>\n";
                    foreach ($_SESSION['basket'] as $key=>$value){
                        $file_b_content .= "<Good>\n";
                        $file_b_content .= "<UID_>{$value['id']}</UID_>\n";
                        $file_b_content .= "<Count>{$value['count']}</Count>\n";
                        $file_b_content .= "</Good>\n";
                    }
                    $file_b_content .= "</Goods>\n";
                    $file_b_content .= "</Request>\n";

                    $fp = fopen($file_b, "w");
                    fwrite($fp, $file_b_content);
                    fclose($fp);

                    $upload = ftp_put($conn_id, $file_a, $file_b, FTP_BINARY);
                    if (!$upload) {
                        echo "Error: FTP upload has failed!";
                    } else {
                        echo "Good: Uploaded to {$ftpcont['server']}";
                    }
                    break;
                case "select_orders":
                    // Выбрать заказы

                    $date = date("Y-m-d_H-i-s");
                    $file_a = "/requests/incoming/IN_{$user->id}_{$date}.xml";
                    $file_b = "../yii/frontend/views/layouts/inc/requests/orders/IN_{$user->id}_{$date}.xml";

                    switch ($_GET['status']){
                        case "1": $code = "ORDERS_ALL";
                            break;
                        case "2": $code = "ORDERS_TRUE";
                            break;
                        case "3": $code = "ORDERS_FALSE";
                            break;
                    }

                    $file_b_content = "";
                    $file_b_content .= "<?xml version='1.0' encoding='WINDOWS-1251'?>\n";
                    $file_b_content .= "<Request>\n";
                    $file_b_content .= "<Command>{$code}</Command>\n";
                    $file_b_content .= "<ClientID>{$user->id}</ClientID>\n";
                    $file_b_content .= "<StartDate>0000-00-00 00:00:00</StartDate>\n";
                    $file_b_content .= "<EndtDate>0000-00-00 00:00:00</EndtDate>\n";
                    $file_b_content .= "</Request>\n";

                    $fp = fopen($file_b, "w");
                    fwrite($fp, $file_b_content);
                    fclose($fp);

                    $upload = ftp_put($conn_id, $file_a, $file_b, FTP_BINARY);
                    if (!$upload) {
                        echo "Error: FTP upload has failed!";
                    } else {
                        echo "Good: Uploaded !!! to {$ftpcont['server']}";
                    }

                    break;
                case "get_balance":
                    // Получить баланс

                    $date = date("Y-m-d_H-i-s");
                    $file_a = "/requests/incoming/{$user->id}_{$date}.xml";
                    $file_b = "../yii/frontend/views/layouts/inc/requests/orders/IN_{$user->id}_{$date}.xml";

                    $file_b_content = "";
                    $file_b_content .= "<?xml version='1.0' encoding='WINDOWS-1251'?>\n";
                    $file_b_content .= "<Request>\n";
                    $file_b_content .= "<Command>\n";
                    $file_b_content .= "<Code>CURRENT_BALANCE</Code>\n";
                    $file_b_content .= "<UserID>{$user->id}</UserID>\n";
                    $file_b_content .= "<InvNo>{$user->id}</InvNo>\n";
                    $file_b_content .= "<StartDate>0000-00-00 00:00:00</StartDate>\n";
                    $file_b_content .= "<EndtDate>0000-00-00 00:00:00</EndtDate>\n";
                    $file_b_content .= "</Command>\n";
                    $file_b_content .= "</Request>";

                    $fp = fopen($file_b, "w");
                    fwrite($fp, $file_b_content);
                    fclose($fp);

                    $upload = ftp_put($conn_id, $file_a, $file_b, FTP_BINARY);
                    if (!$upload) {
                        echo "Error: FTP upload has failed!";
                    } else {
                        echo "Good: Uploaded !!! to {$ftpcont['server']}";
                    }

                    break;

            }

        }
        ftp_close($conn_id);
    }


    public static function settings($key){

        /*$xmlstring = "../cloud/db/Settings.xml";
        $xmlstring = file_get_contents($xmlstring);
        $xml = simplexml_load_string($xmlstring);
        $json = json_encode($xml);
        $array = json_decode($json,TRUE);*/
        //return $array[$key]['Val'];
        return Yii::$app->params['currency'];

    }

}