<?php
Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@management', dirname(dirname(__DIR__)) . '/management');
Yii::setAlias('@frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('@backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');
Yii::setAlias('@cloud', dirname(dirname(__DIR__)) . '/../cloud');