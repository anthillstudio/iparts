<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
    'uploader' => [
        'domen' => 'http://cloud.iparts.anthill.by',
        'dir' => 'cloud'
    ],
    'currency' => 2,
    'partnerType' => ['1' => 'ИП', '2' => 'Физ. лицо', '3' => 'Юр. лицо']
];
