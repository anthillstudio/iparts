<?php

namespace common\models;

use Yii;
use common\components\Ant;

/**
 * This is the model class for table "{{%news}}".
 *
 * @property integer $id
 * @property string $insert_date
 * @property string $update_date
 * @property string $date
 * @property integer $user_id
 * @property string $title_lng
 * @property string $beautiful_url
 * @property string $notice_lng
 * @property string $content_lng
 * @property string $image_file
 * @property string $seo_title_lng
 * @property string $seo_description_lng
 * @property string $seo_keywords_lng
 * @property integer $show
 * @property integer $level
 */
class News extends \yii\db\ActiveRecord
{

    public $imageFileDelete;
    public $imageFileTitleDelete;
    public $page;

    public $baseUrlPath = "news/";

    public $config = [
        'dir' => 'news',
        'model' => 'news'
    ];

    public function getMaxLevel(){
        return Ant::getMaxLevel($this->tableName());
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%news}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['insert_date', 'update_date','date'], 'safe'],
            [['user_id', 'show', 'level', 'title_style'], 'integer'],
            [['date', 'title_lng'], 'required'],
            [['content_lng', 'seo_description_lng'], 'string'],
            [['title_lng', 'beautiful_url', 'notice_lng', 'image_file', 'image_file_title', 'seo_title_lng', 'seo_keywords_lng'], 'string', 'max' => 255],
            [['beautiful_url'], 'unique'],
            [['image_file','image_file_title'], 'image', 'extensions' => 'png, jpg, jpeg, svg, gif',
                'minWidth' => 300, 'maxWidth' => 2500,
                'minHeight' => 300, 'maxHeight' =>2500,
                'maxSize' => 1024*1024,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'insert_date' => 'Дата добавления',
            'update_date' => 'Дата обновления',
            'date' => 'Дата',
            'user_id' => 'ID пользователя',
            'title_lng' => 'Название',
            'beautiful_url' => 'Ссылка',
            'notice_lng' => 'Кратко',
            'content_lng' => 'Подробно',
            'image_file' => 'Файл',
            'image_file_title' => 'Файл',
            'title_style' => 'Стиль заголовка',
            'seo_title_lng' => 'Seo заголовок',
            'seo_description_lng' => 'Seo опиание',
            'seo_keywords_lng' => 'Seo ключевые слова',
            'show' => 'Отображать',
            'level' => 'Уровень',
        ];
    }

    public function getPageByUrl($url){
        $page = News::find()->where(['beautiful_url' => $url])->one()->attributes;
        return $page;
    }

}
