<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%basket_elements}}".
 *
 * @property integer $id
 * @property integer $order_id
 * @property string $catalog_element_id
 * @property string $count
 * @property string $price
 * @property integer $active
 */
class BasketElements extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%basket_elements}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'active'], 'integer'],
            [['price'], 'number'],
            [['catalog_element_id', 'count'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'catalog_element_id' => 'Catalog Element ID',
            'count' => 'Count',
            'price' => 'Price',
            'active' => 'Active',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent0()
    {
        return $this->hasOne(Basket::className(), ['id' => 'order_id']);
    }


    public function getCatalogElements()
    {
        return $this->hasOne(CatalogElements::className(), ['id' => 'catalog_element_id']);
    }


}
