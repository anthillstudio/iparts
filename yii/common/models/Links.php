<?php

namespace common\models;

use Yii;
use common\components\Ant;

/**
 * This is the model class for table "{{%links}}".
 *
 * @property integer $id
 * @property string $insert_date
 * @property string $update_date
 * @property integer $user_id
 * @property integer $cat_id
 * @property string $title_lng
 * @property string $url
 * @property integer $show
 * @property integer $level
 */
class Links extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%links}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['insert_date', 'update_date'], 'safe'],
            [['user_id', 'cat_id', 'show', 'level'], 'integer'],
            [['url'], 'required'],
            [['title_lng', 'url'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'insert_date' => 'Дата добавления',
            'update_date' => 'Дата обновления',
            'user_id' => 'Пользователь',
            'cat_id' => 'Категория',
            'title_lng' => 'Название',
            'url' => 'Ссылка',
            'show' => 'Отображать',
            'level' => 'Уровень',
        ];
    }

    public function getMaxLevel($parent=''){
        return Ant::getMaxLevel($this->tableName(),$parent);
    }

}
