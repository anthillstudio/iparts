<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%user_settings}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $key
 *
 * @property UserSettingsValues[] $userSettingsValues
 */
class UserSettings extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_settings}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'key'], 'required'],
            [['title', 'key'], 'string', 'max' => 255],
            [['type'], 'integer'],
            [['key'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'key' => 'Ключ',
            'ензу' => 'Тип'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserSettingsValues()
    {
        return $this->hasMany(UserSettingsValues::className(), ['key_id' => 'id']);
    }
}
