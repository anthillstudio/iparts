<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%user_partners}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $partner_id
 *
 * @property Articles $user
 * @property ArticlesCatalogs $partner
 */
class UserPartners extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_partners}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'partner_id'], 'integer'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Articles::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['partner_id'], 'exist', 'skipOnError' => true, 'targetClass' => ArticlesCatalogs::className(), 'targetAttribute' => ['partner_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'partner_id' => 'Partner ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Articles::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPartner()
    {
        return $this->hasOne(ArticlesCatalogs::className(), ['id' => 'partner_id']);
    }
}
