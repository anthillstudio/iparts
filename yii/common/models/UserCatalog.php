<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%user_catalog}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $catalog_id
 *
 * @property Articles $user
 * @property ArticlesCatalogs $catalog
 */
class UserCatalog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_catalog}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'catalog_id'], 'integer'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Articles::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['catalog_id'], 'exist', 'skipOnError' => true, 'targetClass' => ArticlesCatalogs::className(), 'targetAttribute' => ['catalog_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'catalog_id' => 'Catalog ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Articles::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCatalog()
    {
        return $this->hasOne(ArticlesCatalogs::className(), ['id' => 'catalog_id']);
    }
}
