<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%spages}}".
 *
 * @property integer $id
 * @property string $insert_date
 * @property string $update_date
 * @property integer $user_id
 * @property string $content_lng
 * @property string $image_file
 * @property string $seo_title_lng
 * @property string $seo_description_lng
 * @property string $seo_keywords_lng
 * @property integer $level
 */
class Spages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%spages}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['insert_date', 'update_date'], 'safe'],
            [['user_id', 'level'], 'integer'],
            [['content_lng', 'title_lng', 'seo_description_lng'], 'string'],
            [['title_lng'], 'required'],
            [['image_file', 'seo_title_lng', 'seo_keywords_lng'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'insert_date' => 'Дата добавления',
            'update_date' => 'Дата обновления',
            'user_id' => 'Пользователь',
            'title_lng' => 'Название',
            'content_lng' => 'Описание',
            'image_file' => 'Изображение',
            'seo_title_lng' => 'Seo заголовок',
            'seo_description_lng' => 'Seo описание',
            'seo_keywords_lng' => 'Seo ключевые слова',
            'level' => 'Уровень',
        ];
    }
}
