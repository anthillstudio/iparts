<?php

namespace common\models;

use Yii;
use common\components\Ant;

class ArticlesCatalogs extends \yii\db\ActiveRecord
{

    public $config = [
        'dir' => 'articlesCatalogs',
        'model' => 'articles-catalogs'
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%articles_catalogs}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title_lng'], 'required'],
            [['insert_date', 'update_date'], 'safe'],
            [['user_id', 'level'], 'integer'],
            [['seo_description_lng', 'content_lng'], 'string'],
            [['seo_title_lng', 'seo_keywords_lng', 'beautiful_url', 'title_lng', 'image_title'], 'string', 'max' => 255],
            [['image_file'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'insert_date' => 'Дата добавления',
            'update_date' => 'Дата обновления',
            'user_id' => 'ID пользователя',
            'seo_title_lng' => 'Seo название',
            'seo_description_lng' => 'Seo описание',
            'seo_keywords_lng' => 'Seo ключевые слова',
            'beautiful_url' => 'Красивая ссылка',
            'title_lng' => 'Название',
            'content_lng' => 'Описание',
            'image_title' => 'Подпись к изображению',
            'image_file' => 'Изображение',
            'show' => 'Отображение',
            'level' => 'Сортировка',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticlesWithArticlesCatalogs()
    {
        return $this->hasMany(ArticlesWithArticlesCatalogs::className(), ['articles_catalogs_id' => 'id']);
    }

    public function getTags(){
        $elements = ArticlesCatalogs::find()->where(['show'=>1])->asArray()->all();
        $toReturn = [];
        foreach ($elements as $key=>$value){
            $toReturn[$value['id']] = $value['title_lng'];
        }
        return $toReturn;
    }

    public function getActiveTags($id){
        
        $elements = ArticlesWithArticlesCatalogs::find()->where(['articles_id'=>$id])->asArray()->all();
        $toReturn = [];
        foreach ($elements as $key=>$value){
            $toReturn[$value['id']] = $value['articles_catalogs_id'];
        }


        return $toReturn;
    }

    public function getMaxLevel(){
        return Ant::getMaxLevel($this->tableName());
    }

}
