<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Catalog;

/**
 * CatalogSearch represents the model behind the search form about `common\models\Catalog`.
 */
class CatalogSearch extends Catalog
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'parent', 'insert_date', 'update_date', 'date', 'title', 'long_title', 'beautiful_url', 'notice', 'content', 'image_file', 'tpl', 'seo_title', 'seo_description', 'seo_keywords'], 'safe'],
            [['user_id', 'show', 'level'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Catalog::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'insert_date' => $this->insert_date,
            'update_date' => $this->update_date,
            'date' => $this->date,
            'user_id' => $this->user_id,
            'show' => $this->show,
            'level' => $this->level,
        ]);

        $query->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'parent', $this->parent])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'long_title', $this->long_title])
            ->andFilterWhere(['like', 'beautiful_url', $this->beautiful_url])
            ->andFilterWhere(['like', 'notice', $this->notice])
            ->andFilterWhere(['like', 'content', $this->content])
            ->andFilterWhere(['like', 'image_file', $this->image_file])
            ->andFilterWhere(['like', 'tpl', $this->tpl])
            ->andFilterWhere(['like', 'seo_title', $this->seo_title])
            ->andFilterWhere(['like', 'seo_description', $this->seo_description])
            ->andFilterWhere(['like', 'seo_keywords', $this->seo_keywords]);

        return $dataProvider;
    }
}
