<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%statement}}".
 */
class Statement extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%statement}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [

        ];
    }


    public function getStatementElement()
    {
        return $this->hasMany(StatementElement::className(), ['statement_id' => 'id'])->orderBy(['id' => SORT_ASC]);
    }


}
