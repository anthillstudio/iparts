<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%region}}".
 */
class Region extends \yii\db\ActiveRecord
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%region}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [

        ];
    }

    /*public function getPurchase()
    {
        return $this->hasMany(PurchaseElements::className(), ['purchase_id' => 'id']);
    }*/

}
