<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%catalog_elements}}".
 *
 * @property string $id
 * @property string $parent
 * @property string $insert_date
 * @property string $update_date
 * @property integer $user_id
 * @property string $title
 * @property string $ref
 * @property string $price1
 * @property string $price2
 * @property string $beautiful_url
 * @property string $notice
 * @property string $content
 * @property string $image_file
 * @property string $image_file_mini
 * @property integer $hit
 * @property integer $new
 * @property string $seo_title
 * @property string $seo_description
 * @property string $seo_keywords
 * @property integer $show
 * @property integer $level
 * @property integer $is_action
 * @property integer $prev_price
 */
class CatalogElements extends \yii\db\ActiveRecord
{
    const STATUS_SHOW = 1;
    const STATUS_HIDE = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%catalog_elements}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'parent'], 'required'],        
            [['insert_date', 'update_date'], 'safe'],
            [['user_id', 'hit', 'new', 'show', 'level', 'is_action'], 'integer'],
            [['price1', 'price2', 'prev_price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent' => 'Parent',
            'insert_date' => 'Insert Date',
            'update_date' => 'Update Date',
            'user_id' => 'User ID',
            'title' => 'Title',
            'ref' => 'Ref',
            'price1' => 'Price1',
            'price2' => 'Price2',
            'beautiful_url' => 'Beautiful Url',
            'notice' => 'Notice',
            'content' => 'Content',
            'image_file' => 'Image File',
            'image_file_mini' => 'Image File Mini',
            'hit' => 'Hit',
            'new' => 'New',
            'seo_title' => 'Seo Title',
            'seo_description' => 'Seo Description',
            'seo_keywords' => 'Seo Keywords',
            'show' => 'Show',
            'level' => 'Level',
            'is_action' => 'Is Action',
            'prev_price' => 'Prev Price',
        ];
    }

    public static function buildList($els,$str){
        $resStr = "";
        foreach ($els as $k=>$v){
            $price = (!Yii::$app->user->isGuest ? $v['price1'] : $v['price2']);
            $resStr .= "<div class=\"pricelist-subitem\" id=\"pricelist-subitem{$v['id']}\">
                <div class=\"pricelist-subitem-title\"><a href=\"javascript:showDetals('{$v['id']}')\">{$v['title']}</a></div>
                <div class=\"pricelist-subitem-notice\">{$v['notice']}</div>
                <div class=\"pricelist-subitem-price\">Цена: <span>{$price}</span> USD</div>
            ";
            if ((!Yii::$app->user->isGuest) && ($price!='0.00')){
                $resStr .= "<div class=\"pricelist-subitem-bttn\"><a href=\"javascript:addToBasketFromSearch('{$v['id']}')\"><img src=\"/img/icons/add.png\"></a></div>";
            }
            $resStr .= "</div><!-- pricelist-subitem -->";
        }
        return $resStr;
    }

    // Выбираем все категории вложенные в текущую
    public static function getAllParentsForCat($parents,$cat){
        $newArr = [];
        foreach($parents as $k=>$v){
            if (in_array($cat,$v)) $newArr = array_merge($newArr,$v);
        }
        $arr = [];
        foreach ($newArr as $key=>$val){
            if ($val != $cat) $arr[] = $val;
        }
        $arr[] = $cat;
        return $arr;
    }

    public static function getElementsByStr($str){
        $elements = Yii::$app->db->createCommand("
          SELECT cc.*           
          FROM {{%catalog_elements}} as cc        
          WHERE (cc.title like '%{$str}%' || cc.ref like '%{$str}%') AND `cc`.`show` = '1' 
          ORDER BY cc.id DESC
          LIMIT 20
        ")->queryAll();
        return $elements;
    }

    public static function deactivateAll()
    {
        self::updateAll(['show' => self::STATUS_HIDE]);
    }

    public static function getAllActive()
    {
        return self::find()->where(['show' => self::STATUS_SHOW])->all();
    }

    public function getRegionProduct()
    {
        return $this->hasMany(RegionProduct::className(), ['product_id' => 'id']);
    }

    public function isActionElement()
    {
        return ($this->is_action == '1' ? true : false);
    }

    /**
     * Получаем статус изменения цены.
     * @return int : 1 - подорожало, 0 - цена не изменилась, -1 - подешевело
     */
    public function getPriceChange()
    {
        $changeResult = '0';
        if ($this->prev_price != '0') {
            if ($this->prev_price > $this->price1) {
                $changeResult = '-1';
            }
            else if ($this->prev_price < $this->price1) {
                $changeResult = '1';
            }
        }
        return $changeResult;
    }

}
