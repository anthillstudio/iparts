<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%basket}}".
 *
 * @property integer $id
 * @property string $user_id
 * @property string $phone
 * @property string $email
 * @property integer $status_id
 * @property string $comment
 */
class Basket extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%basket}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [

        ];
    }


    public function getReportOrder()
    {
        return $this->hasOne(ReportOrder::className(), ['order_id' => 'external_id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBasketElements()
    {
        return $this->hasMany(BasketElements::className(), ['order_id' => 'id'])->orderBy(['id' => SORT_ASC]);
    }

    public function getReportOrderElement()
    {
        return $this->hasMany(ReportOrderElement::className(), ['order_id' => 'external_id'])->orderBy(['id' => SORT_ASC]);
    }

}
