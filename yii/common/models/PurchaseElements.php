<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%purchase_elements}}".
 *
 * @property integer $id
 * @property integer $purchase_id
 * @property string $good_id
 * @property integer $count
 * @property string $price
 *
 * @property Purchase $return
 */
class PurchaseElements extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%purchase_elements}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['purchase_id', 'count'], 'integer'],
            [['price'], 'number'],
            [['purchase_id'], 'exist', 'skipOnError' => true, 'targetClass' => Purchase::className(), 'targetAttribute' => ['purchase_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'purchase_id' => 'Purchase ID',
            'good_id' => 'Good ID',
            'count' => 'Count',
            'price' => 'Price',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReturn()
    {
        return $this->hasOne(Purchase::className(), ['id' => 'purchase_id']);
    }

    public function getCatalogElements()
    {
        return $this->hasOne(CatalogElements::className(), ['id' => 'good_id']);
    }
}
