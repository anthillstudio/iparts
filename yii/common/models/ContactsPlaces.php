<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%contacts_places}}".
 *
 * @property integer $id
 * @property integer $city_id
 * @property string $insert_date
 * @property string $update_date
 * @property integer $user_id
 * @property string $title_lng
 * @property string $content_lng
 * @property string $addr
 * @property string $phone
 * @property string $time_work
 * @property string $cash_service
 * @property integer $row_num
 * @property string $boss
 * @property string $boss_post
 * @property integer $type
 * @property string $ext
 * @property string $img_url
 * @property integer $show
 * @property integer $level
 *
 * @property ContactsCities $city
 */
class ContactsPlaces extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%contacts_places}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city_id', 'user_id', 'row_num', 'type', 'show', 'level'], 'integer'],
            [['insert_date', 'update_date', 'addr', 'phone', 'time_work', 'cash_service', 'boss', 'boss_post', 'img_url'], 'required'],
            [['insert_date', 'update_date'], 'safe'],
            [['content_lng', 'addr', 'coordinates', 'time_work', 'cash_service'], 'string'],
            [['title_lng', 'phone', 'boss', 'boss_post', 'ext', 'img_url'], 'string', 'max' => 255],
            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => ContactsCities::className(), 'targetAttribute' => ['city_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'city_id' => 'City ID',
            'insert_date' => 'Insert Date',
            'update_date' => 'Update Date',
            'user_id' => 'User ID',
            'title_lng' => 'Title Lng',
            'content_lng' => 'Content Lng',
            'addr' => 'Addr',
            'coordinates' => 'Координаты',
            'phone' => 'Phone',
            'time_work' => 'Time Work',
            'cash_service' => 'Cash Service',
            'row_num' => 'Row Num',
            'boss' => 'Boss',
            'boss_post' => 'Boss Post',
            'type' => 'Type',
            'ext' => 'Ext',
            'img_url' => 'Img Url',
            'show' => 'Show',
            'level' => 'Level',
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(ContactsCities::className(), ['id' => 'city_id']);
    }
}