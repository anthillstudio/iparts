<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%articles_with_articles_catalogs}}".
 *
 * @property integer $id
 * @property integer $articles_id
 * @property integer $articles_catalogs_id
 *
 * @property Articles $articles
 * @property ArticlesCatalogs $articlesCatalogs
 */
class ArticlesWithArticlesCatalogs extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%articles_with_articles_catalogs}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['articles_id', 'articles_catalogs_id'], 'integer'],
            [['articles_id'], 'exist', 'skipOnError' => true, 'targetClass' => Articles::className(), 'targetAttribute' => ['articles_id' => 'id']],
            [['articles_catalogs_id'], 'exist', 'skipOnError' => true, 'targetClass' => ArticlesCatalogs::className(), 'targetAttribute' => ['articles_catalogs_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'articles_id' => 'Articles ID',
            'articles_catalogs_id' => 'Articles Catalogs ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticles()
    {
        return $this->hasOne(Articles::className(), ['id' => 'articles_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticlesCatalogs()
    {
        return $this->hasOne(ArticlesCatalogs::className(), ['id' => 'articles_catalogs_id']);
    }


    public function addTags($id, $tags){

        $articlesWithArticlesCatalogs = new ArticlesWithArticlesCatalogs();
        $articlesWithArticlesCatalogs::deleteAll(" `articles_id` = {$id} ");

        foreach ($tags as $key=>$value){
            $model = new ArticlesWithArticlesCatalogs();
            $model->articles_id = $id;
            $model->articles_catalogs_id = $value;
            $model->insert();
        }

    }

}
