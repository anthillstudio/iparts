<?php

namespace common\models;

use Yii;
use common\components\Ant;

/**
 * This is the model class for table "{{%content_catalog}}".
 *
 * @property integer $id
 * @property integer $parent
 * @property string $insert_date
 * @property string $update_date
 * @property integer $user_id
 * @property string $title_lng
 * @property string $long_title_lng
 * @property string $price
 * @property string $price_pv
 * @property string $beautiful_url
 * @property string $notice_lng
 * @property string $content_lng
 * @property string $image_file
 * @property string $tpl
 * @property string $seo_title_lng
 * @property string $seo_description_lng
 * @property string $seo_keywords_lng
 * @property integer $show
 * @property integer $level
 */
class ContentCatalog extends \yii\db\ActiveRecord
{

    public $title_style;
    public $baseUrlPath;

    public $config = [
        'dir' => 'catalog',
        'model' => 'contentCatalog'
    ];

    public function getMaxLevel(){
        return Ant::getMaxLevel($this->tableName());
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%content_catalog}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'show', 'new', 'hit', 'level'], 'integer'],
            [['insert_date', 'update_date'], 'safe'],
            [['title_lng'], 'required'],
            [['price', 'price_pv'], 'number'],
            [['parent', 'content_lng', 'seo_description_lng'], 'string'],
            [['title_lng', 'long_title_lng', 'ref', 'beautiful_url', 'notice_lng', 'image_file', 'image_file_title', 'seo_title_lng', 'seo_keywords_lng'], 'string', 'max' => 255],
            [['tpl'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent' => 'Parent',
            'insert_date' => 'Дата добавления',
            'update_date' => 'Дата обновления',
            'user_id' => 'ID администратора',
            'title_lng' => 'Название',
            'long_title_lng' => 'Расширенное название',
            'price' => 'Цена',
            'price_pv' => 'Цена (PV)',
            'beautiful_url' => 'Красивая ссылка',
            'notice_lng' => 'Краткое описание',
            'content_lng' => 'Подробное описание',
            'image_file' => 'Изображение',
            'image_file_title' => 'Изображение (шапка)',
            'tpl' => 'Шаблон',
            'seo_title_lng' => 'Seo название',
            'seo_description_lng' => 'Seo описание',
            'seo_keywords_lng' => 'Seo ключевые слова',
            'show' => 'Отображать',
            'price' => 'Цена',
            'price_pv' => 'Цена (PV)',
            'ref' => 'Артикул',
            'new' => 'Новинка',
            'hit' => 'HIT',
            'level' => 'Уровень',
        ];
    }

    public function getCatalogElements($pageid){
        return $elements = ContentCatalog::find()->where(['parent' => $pageid])->all();
    }


    public static function getElements($where=" 1 ",$limit=""){

        $elements = Yii::$app->db->createCommand("
        SELECT cc.*, c.id as c_id, c.title_lng as c_title_lng, c.beautiful_url as c_beautiful_url
        FROM {{%content_catalog}} as cc
        LEFT JOIN {{%content}} as c ON c.id = cc.parent
        WHERE {$where}
        {$limit}
        ")->queryAll();
        return $elements;
    }

    /*public function getCatImpEls(){
        $elements = Yii::$app->db->createCommand("
        SELECT cc.*, c.title_lng as c_title_lng, c.beautiful_url as c_beautiful_url
        FROM {{%content_catalog}} as cc
        LEFT JOIN {{%content}} as c ON c.id = cc.parent
        WHERE cc.hit = 1
        LIMIT 8
        ")->queryAll();

        return $elements;
    }

    public function getCatNewEls(){
        $elements = Yii::$app->db->createCommand("
        SELECT cc.*, c.title_lng as c_title_lng, c.beautiful_url as c_beautiful_url
        FROM {{%content_catalog}} as cc
        LEFT JOIN {{%content}} as c ON c.id = cc.parent
        WHERE cc.new = 1
        LIMIT 8
        ")->queryAll();

        return $elements;
    }*/

    /*public function xurl(){

        print_r("<pre>");
        print_r($_GET);
        print_r("</pre>");

        $url = "{$_GET['base']}";
        if (isset($_GET['pageurl'])) $url .= "/{$_GET['pageurl']}";
        if (isset($_GET['catalogelementurl'])) $url .= "/{$_GET['catalogelementurl']}";
        return $url;
    }*/

    public function getElementByUrl(){
        return $element = ContentCatalog::find()->where(['beautiful_url' => Content::xurl()])->one();
    }

    public function verifyBeautifulURL(){
        die("d");
    }






}
