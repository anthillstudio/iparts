<?php

namespace common\models;

use Yii;
use common\components\Ant;

/**
 * This is the model class for table "{{%bnnrs}}".
 *
 * @property integer $id
 * @property string $insert_date
 * @property string $update_date
 * @property string $date
 * @property integer $user_id
 * @property string $title_lng
 * @property string $image_file
 * @property integer $show
 * @property integer $level
 */
class Bnnrs extends \yii\db\ActiveRecord
{
    public $imageFileDelete;

    public $config = [
        'dir' => 'bnnrs',
        'model' => 'bnnrs'
    ];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%bnnrs}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['insert_date', 'update_date'], 'safe'],
            [['user_id', 'show', 'level'], 'integer'],
            [['title_lng', 'url', 'image_file'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'insert_date' => 'Дата добавления',
            'update_date' => 'Дата обновления',
            'user_id' => 'Пользователь',
            'title_lng' => 'Название',
            'url' => 'Ссылка',
            'image_file' => 'Изображение',
            'show' => 'Отображать',
            'level' => 'Сортировка',
        ];
    }

    public function getMaxLevel(){
        return Ant::getMaxLevel($this->tableName());
    }

}
