<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Links;

/**
 * LinksSearch represents the model behind the search form about `common\models\Links`.
 */
class LinksSearch extends Links
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'cat_id', 'show', 'level'], 'integer'],
            [['insert_date', 'update_date', 'title_lng', 'url'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Links::find()->where(['cat_id'=>$_GET['parent']])->orderBy(['level'=>SORT_DESC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'insert_date' => $this->insert_date,
            'update_date' => $this->update_date,
            'user_id' => $this->user_id,
            'cat_id' => $this->cat_id,
            'show' => $this->show,
            'level' => $this->level,
        ]);

        $query->andFilterWhere(['like', 'title_lng', $this->title_lng])
            ->andFilterWhere(['like', 'url', $this->url]);

        return $dataProvider;
    }
}
