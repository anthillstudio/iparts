<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%report_order_element}}".
 */
class ReportOrderElement extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%report_order_element}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [

        ];
    }

    public function getCatalogElements()
    {
        return $this->hasOne(CatalogElements::className(), ['id' => 'element_id']);
    }



}
