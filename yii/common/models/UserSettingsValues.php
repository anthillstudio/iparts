<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%user_settings_values}}".
 *
 * @property integer $id
 * @property string $user_id
 * @property integer $key_id
 * @property string $value
 *
 * @property UserSettings $key
 */
class UserSettingsValues extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_settings_values}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'key_id', 'value'], 'required'],
            [['key_id'], 'integer'],
            [['value'], 'string'],
            [['user_id'], 'string', 'max' => 255],
            [['key_id'], 'exist', 'skipOnError' => true, 'targetClass' => UserSettings::className(), 'targetAttribute' => ['key_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'key_id' => 'Key ID',
            'value' => 'Value',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKey()
    {
        return $this->hasOne(UserSettings::className(), ['id' => 'key_id']);
    }
}
