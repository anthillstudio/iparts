<?php

namespace common\models;

use Yii;
use common\components\Api;
//use creocoder\nestedsets\NestedSetsBehavior;

/**
 * This is the model class for table "{{%catalog}}".
 *
 * @property string $id
 * @property string $parent
 * @property string $insert_date
 * @property string $update_date
 * @property integer $user_id
 * @property string $title
 * @property string $long_title
 * @property string $beautiful_url
 * @property string $notice
 * @property string $content
 * @property string $image_file
 * @property string $tpl
 * @property string $seo_title
 * @property string $seo_description
 * @property string $seo_keywords
 * @property integer $show
 * @property integer $level
 */
class Catalog extends \yii\db\ActiveRecord
{

    public $parents;
    public  $root_id = '00000000001';
    const STATUS_SHOW = 1;
    const STATUS_HIDE = 0;


    public function behaviors() {
        /*return [
            'tree' => [
                'class' => NestedSetsBehavior::className(),
            ],
        ];*/
    }

    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    public static function find()
    {
        return new CatalogQuery(get_called_class());
    }


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%catalog}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'parent'], 'required'],
            [['insert_date', 'update_date'], 'safe'],
            [['user_id', 'show', 'level'], 'integer'],
            [['content', 'seo_description'], 'string'],
        ];
    }


    public static function buildPath($el=0){
        global $path;
        if ($el!=0) {
            $element = Catalog::findOne($el);
            if (!empty($element)) {
                Catalog::buildPath($element->parent);
                $path[$element->id] = $element->title;
            }
        }
        return $path;
    }


    public function getAllParentForElements(){
        global $parentarr;
        $catalogs = Catalog::find()->all();
        $xxx = [];
        foreach ($catalogs as $key=>$val){
            $xxx[$val->id] = $this->getAllParentForElement($val->id);
            $parentarr = [];
        }
        $this->parents = $xxx;
    }


    public function getAllParentForElement($id=''){

        global $parentarr;

        if ($id!=''){
            $el = Catalog::find()->where(['id'=>$id])->One();

            if (!empty($el)) {
                $parentarr[] = $el->id;
                if ($el->id!=$this->root_id) {
                    if (trim($el->parent)!='')
                        $this->getAllParentForElement($el->parent);
                }
            }
        }
        return $parentarr;
    }

    public function filtersMenu(){


        $curr = [
            'byn'=>'BYN',
            'ue'=>'USD'
        ];


        $reg = Region::find()->where(['is_active' => Region::STATUS_ACTIVE])->all();

        $toReturn = "";

        $session = Yii::$app->session;
        if (!$session->isActive){
            $session->open();
        }
        //echo $session['curr'];

        $toReturn .= "<div class='good-list-filters'>";
        $f_curr = (isset($session['curr'])?$_GET['curr']:"ue");
        $f_reg = (isset($_GET['city'])?$_GET['city']:"all");
        $toReturn .= "<input type='hidden' id='f_curr' value='{$f_curr}'>";
        $toReturn .= "<input type='hidden' id='f_reg' value='{$f_reg}'>";
        $toReturn .= "<ul>";

        $x = explode("?",$_SERVER['REQUEST_URI']);

        $session = Yii::$app->session;
        if (!$session->isActive){
            $session->open();
        }

        /*if (isset($session['curr'])){
            $all_url = $all_url."?city=".$session['curr'];
        }*/
        $toReturn .= "<li><a href='{$x[0]}'>Все</a></li>";
        foreach ($reg as $key=>$val){

            $url = $x[0]."?city={$val->id}";

            foreach (explode("&",$x[1]) as $k=>$v){
                $vpar = explode("=",$v);
                if ($vpar[0]!='city'){
                    if (isset($vpar[1]))
                        $url .= "&{$vpar[0]}={$vpar[1]}";
                }
            }

            $active = ($session['city']==$val->id?"good-list-filters-item-active":"");
            $toReturn .= "<li class='{$active}'><a href='{$url}'>{$val->name}</a></li>";
        }
        $toReturn .= '</ul>';



        $toReturn .= "<ul>";
        foreach ($curr as $key=>$val){

            $url = $x[0];
            $url .= "?curr={$key}";

            foreach (explode("&",$x[1]) as $k=>$v){
                $vpar = explode("=",$v);
                if ($vpar[0]!='curr'){
                    if (isset($vpar[1]))
                    $url .= "&{$vpar[0]}={$vpar[1]}";
                }
            }
            $active = ($session['curr']==$key?"good-list-filters-item-active":"");
            $toReturn .= "<li class='{$active}'><a href='{$url}'>{$val}</a></li>";
        }
        $toReturn .= '</ul>';


        $toReturn .= "</div><!-- good-list-filters -->";


        return $toReturn;
    }

    public function filters(){

        $session = Yii::$app->session;
        if (!$session->isActive){
            $session->open();
        }

        $toReturn = "";
        $issetfilter = false;
        if (!empty($session['city'])){
            $issetfilter = true;
            $arr['city'] = $session['city'];
        }
        if (!empty($session['curr'])){
            $issetfilter = true;
            $arr['curr'] = $session['curr'];
        }
        if ($issetfilter){
            $toReturn .= "?";
            foreach ($arr as $k=>$v){
                $toReturn .= "$k={$v}&";
            }
            $toReturn = substr($toReturn,0,-1);
        }
        return $toReturn;
    }

    public function buildMenu()
    {
        // Надо получить список ключевых элементов вложенности, т.е. узлов ветвления, чтобы понять какие именно элементы раскрывать
        $arr = [];
        if (isset($_GET['cat'])) {
            $catalogList = Catalog::find()->where(['show' => self::STATUS_SHOW])->orderBy(['level' => SORT_ASC, 'title' => SORT_ASC])->indexBy('id')->all();
            $arr = $this->recursiveBuildNodes($catalogList, $_GET['cat']);
        }

        global $menu;

        $catalogs = Catalog::find()
            ->where(['parent'=>$this->root_id,'show' => self::STATUS_SHOW])
            ->orderBy(['level'=>SORT_ASC,'title'=>SORT_ASC])
            ->all();

        $menu .= "<div class='cat-menu FNTC'>";
        foreach($catalogs as $key=>$value) {
            $menu .= "<div class='cat-menu-item cat-menu-item-level0'><a href='/catalog/{$value->id}{$this->filters()}'>{$value->title}</a></div>";
            if (in_array($value->id, $arr)) {
                $catalogs_ch = Catalog::find()
                    ->where(['parent' => $value->id, 'show' => self::STATUS_SHOW])
                    ->orderBy(['level'=>SORT_ASC,'title'=>SORT_ASC])
                    ->asArray()
                    ->all();
                if (!empty($catalogs_ch) && count($catalogs_ch) != 0) {
                    $menu .= $this->buildChMenu($value->id, $arr);
                }
            }
        }
        $menu .= "</div><!-- cat-menu -->";
        return $menu;
    }

    public function buildChMenu($parent='', $arr = array(), $lvl=1){
        $menu = '';
        if (isset($_GET['cat'])) {
            $catalogs = Catalog::find()
                ->where(['parent' => $parent])
                ->orderBy(['level'=>SORT_ASC,'title'=>SORT_ASC])
                ->all();
            foreach ($catalogs as $key => $value) {
                $menu .= "<div class='cat-menu-item cat-menu-item-level{$lvl}'><a href='/catalog/{$value->id}{$this->filters()}'>{$value->title}</a></div>";
                if (in_array($value->id, $arr)) {
                    $menu .= $this->buildChMenu($value->id, $arr, $lvl + 1);
                }
            }
        }
        return $menu;
    }

    protected function recursiveBuildNodes($catalogList, $key)
    {
        $array = [];
        if (isset($catalogList[$key])) {
            $parentKey = $catalogList[$key]->parent;
            $array = $this->recursiveBuildNodes($catalogList, $parentKey);
        }
        $array[] = $key;

        return $array;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent' => 'Parent',
            'insert_date' => 'Insert Date',
            'update_date' => 'Update Date',
            'user_id' => 'User ID',
            'title' => 'Title',
            'long_title' => 'Long Title',
            'beautiful_url' => 'Beautiful Url',
            'notice' => 'Notice',
            'content' => 'Content',
            'image_file' => 'Image File',
            'tpl' => 'Tpl',
            'seo_title' => 'Seo Title',
            'seo_description' => 'Seo Description',
            'seo_keywords' => 'Seo Keywords',
            'show' => 'Show',
            'level' => 'Level',
        ];
    }

    public static function deactivateAll()
    {
        self::updateAll(['show' => self::STATUS_HIDE]);
    }

    public static function getAllActive()
    {
        return self::find()->where(['show' => self::STATUS_SHOW])->all();
    }

}
