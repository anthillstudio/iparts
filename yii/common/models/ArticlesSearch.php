<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Articles;

/**
 * ArticlesSearch represents the model behind the search form about `common\models\Articles`.
 */
class ArticlesSearch extends Articles
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'show', 'level'], 'integer'],
            [['insert_date', 'update_date', 'date', 'seo_title_lng', 'seo_description_lng', 'seo_keywords_lng', 'title_lng', 'beautiful_url', 'notice_lng', 'content_lng', 'image_file', 'image_title'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Articles::find()->orderBy(['level'=>SORT_DESC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'insert_date' => $this->insert_date,
            'update_date' => $this->update_date,
            'date' => $this->date,
            'user_id' => $this->user_id,
            'show' => $this->show,
            'level' => $this->level,
        ]);

        $query->andFilterWhere(['like', 'seo_title_lng', $this->seo_title_lng])
            ->andFilterWhere(['like', 'seo_description_lng', $this->seo_description_lng])
            ->andFilterWhere(['like', 'seo_keywords_lng', $this->seo_keywords_lng])
            ->andFilterWhere(['like', 'title_lng', $this->title_lng])
            ->andFilterWhere(['like', 'beautiful_url', $this->beautiful_url])
            ->andFilterWhere(['like', 'notice_lng', $this->notice_lng])
            ->andFilterWhere(['like', 'content_lng', $this->content_lng])
            ->andFilterWhere(['like', 'image_file', $this->image_file])
            ->andFilterWhere(['like', 'image_title', $this->image_title]);

        return $dataProvider;
    }
}
