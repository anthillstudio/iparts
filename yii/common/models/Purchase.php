<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%returns}}".
 *
 * @property integer $id
 * @property integer $partner_id
 * @property string $doc_id
 * @property string $date
 * @property string $region_id
 *
 * @property ReturnsElements[] $returnsElements
 */
class Purchase extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%purchase}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date'], 'safe'],
            [['doc_id'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'partner_id' => 'ID партнера',
            'doc_id' => 'Doc ID',
            'date' => 'Date',
            'region_id' => 'Region ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPurchaseElements()
    {
        return $this->hasMany(PurchaseElements::className(), ['purchase_id' => 'id']);
    }

    public function getRegion()
    {
        return $this->hasOne(Region::className(), ['id' => 'region_id']);
    }
}
