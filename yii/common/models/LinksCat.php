<?php

namespace common\models;

use Yii;
use common\components\Ant;

/**
 * This is the model class for table "{{%links_cat}}".
 *
 * @property integer $id
 * @property string $insert_date
 * @property string $update_date
 * @property integer $user_id
 * @property string $title_lng
 * @property integer $show
 * @property integer $level
 */
class LinksCat extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%links_cat}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['insert_date', 'update_date'], 'safe'],
            [['user_id', 'show', 'level'], 'integer'],
            [['title_lng'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'insert_date' => 'Дата добавления',
            'update_date' => 'Дата обновления',
            'user_id' => 'Пользователь',
            'title_lng' => 'Название',
            'show' => 'Отображать',
            'level' => 'Уровень',
        ];
    }

    public function getMaxLevel($parent=''){
        return Ant::getMaxLevel($this->tableName(),$parent);
    }

}
