<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%contacts_cities}}".
 *
 * @property integer $id
 * @property string $insert_date
 * @property string $update_date
 * @property integer $user_id
 * @property integer $region_id
 * @property string $title_lng
 * @property integer $top
 * @property integer $left
 * @property string $beautiful_url
 * @property string $seo_title_lng
 * @property string $seo_description_lng
 * @property string $seo_keywords_lng
 * @property integer $show
 * @property integer $level
 *
 * @property ContactsRegions $region
 * @property ContactsPlaces[] $contactsPlaces
 */
class ContactsCities extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%contacts_cities}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['insert_date', 'update_date', 'top', 'left', 'beautiful_url', 'seo_title_lng', 'seo_description_lng', 'seo_keywords_lng'], 'required'],
            [['insert_date', 'update_date'], 'safe'],
            [['user_id', 'region_id', 'top', 'left', 'show', 'level'], 'integer'],
            [['seo_description_lng'], 'string'],
            [['title_lng', 'beautiful_url', 'seo_title_lng', 'seo_keywords_lng'], 'string', 'max' => 255],
            [['region_id'], 'exist', 'skipOnError' => true, 'targetClass' => ContactsRegions::className(), 'targetAttribute' => ['region_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'insert_date' => 'Insert Date',
            'update_date' => 'Update Date',
            'user_id' => 'User ID',
            'region_id' => 'Region ID',
            'title_lng' => 'Title Lng',
            'top' => 'Top',
            'left' => 'Left',
            'beautiful_url' => 'Beautiful Url',
            'seo_title_lng' => 'Seo Title Lng',
            'seo_description_lng' => 'Seo Description Lng',
            'seo_keywords_lng' => 'Seo Keywords Lng',
            'show' => 'Show',
            'level' => 'Level',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegion()
    {
        return $this->hasOne(ContactsRegions::className(), ['id' => 'region_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContactsPlaces()
    {
        return $this->hasMany(ContactsPlaces::className(), ['city_id' => 'id']);
    }
}
