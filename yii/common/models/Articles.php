<?php

namespace common\models;

use Yii;
use common\components\Ant;
use common\models\ArticlesCatalogs;
use common\models\ArticlesWithArticlesCatalogs;

class Articles extends \yii\db\ActiveRecord
{


    public $page;
    public $taglist;
    public $tags;

    public $config = [
        'dir' => 'articles',
        'model' => 'articles'
    ];

    public function init(){
        $this->taglist = ArticlesCatalogs::getTags();
    }

    public function getPageByUrl($url)
    {
        $page = Articles::find()->where(['beautiful_url' => $url])->one()->attributes;
        return $page;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%articles}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title_lng'], 'required'],
            [['insert_date', 'update_date', 'date'], 'safe'],
            [['user_id', 'show'], 'integer'],
            [['seo_description_lng', 'notice_lng', 'content_lng'], 'string'],
            [['seo_title_lng', 'seo_keywords_lng', 'title_lng', 'beautiful_url', 'image_file', 'image_title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'insert_date' => 'Дата добавления',
            'update_date' => 'Дата обновления',
            'date' => 'Дата',
            'user_id' => 'ID пользователя',
            'seo_title_lng' => 'Seo заголовок',
            'seo_description_lng' => 'Seo описание',
            'seo_keywords_lng' => 'Seo ключевые слова',
            'title_lng' => 'Заголовок',
            'beautiful_url' => 'Красивая ссылка',
            'notice_lng' => 'Краткое описание',
            'content_lng' => 'Подробное описание',
            'image_file' => 'Изображение',
            'image_title' => 'Подпись к изображению',
            'show' => 'Отображать',
            'level' => 'Cортировка',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticlesWithArticlesCatalogs()
    {
        return $this->hasMany(ArticlesWithArticlesCatalogs::className(), ['articles_id' => 'id']);
    }

    public function getMaxLevel(){
        return Ant::getMaxLevel($this->tableName());
    }

    public function getElements($tag=""){

        $where = "";
        if ($tag!="") $where = " AND ac.beautiful_url = '{$tag}' ";

        $posts = Yii::$app->db->createCommand("
          SELECT a.*
          FROM {{%articles}} as a        
          LEFT JOIN {{%articles_with_articles_catalogs}} as awac ON a.id = awac.articles_id
          LEFT JOIN {{%articles_catalogs}} as ac ON ac.id = awac.articles_catalogs_id
          WHERE a.show = 1 AND ac.show = 1 {$where}
        ")->queryAll();


        return $posts;
    }

}
