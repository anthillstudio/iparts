<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%statement_element}}".
 */
class StatementElement extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%statement_element}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

        ];
    }



    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [

        ];
    }

}
