<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%statement_request}}".
 */
class StatementRequest extends \yii\db\ActiveRecord
{
    const REQUEST_RECEIVED = 0; // Отчёт получен, можно показывать пользователю
    const REQUEST_ASKED = 1; // Необходимо отправить запрос на получение отчёта
    const REQUEST_IN_PROCESS = 2; // Запрос на получение отчёта обрабатывается на стороне 1С

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%statement_request}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['state'], 'integer'],
            [['date_from', 'date_to', 'created', 'updated'], 'date', 'format' => 'php:Y-m-d H:i:s'],
            [['user_id'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [

        ];
    }

    /**
     * Добавить запрос на получение данных из 1С
     * @param $userId
     * @param $dateFrom
     * @param $dateTo
     */
    public function askStatement($userId, $dateFrom, $dateTo)
    {
        $reportRequest = new StatementRequest();
        $reportRequest->user_id = $userId;
        $reportRequest->date_from = $dateFrom;
        $reportRequest->date_to = $dateTo;
        $reportRequest->created = date("Y-m-d H:i:s");
        $reportRequest->state = self::REQUEST_ASKED;
        $reportRequest->updated = date("Y-m-d H:i:s");
        $reportRequest->save();
    }

    /**
     * Получаем все запросы для 1С
     * @return mixed
     */
    public static function getStatementRequests()
    {
        $requests = self::findAll(['state' => self::REQUEST_ASKED]);
        return $requests;
    }

    /**
     * Помечаем запрос, как находящийся на обработке в 1С
     * @return mixed
     */
    public function markAsInProcess()
    {
        $this->state = self::REQUEST_IN_PROCESS;
        $this->updated = date("Y-m-d H:i:s");
        $this->save();
    }

    /**
     * Помечаем запрос, как обработанный 1С
     * @return mixed
     */
    public function markAsProcessed()
    {
        $this->state = self::REQUEST_RECEIVED;
        $this->updated = date("Y-m-d H:i:s");
        $this->save();
    }


    public static function addToRequest($from,$to)
    {
        // Добавляем запрос в БД для 1С
        $model = new StatementRequest();
        $model->date_from = $from." 00:00:00";
        $model->date_to = $to." 23:59:59";
        $model->created = date("Y-m-d H:i:s");
        $model->updated = date("Y-m-d H:i:s");
        $model->state = 1;
        $model->user_id = Yii::$app->user->id;
        $model->save();
        return Yii::$app->db->lastInsertID;
        die;
    }



    public function getAllElements($id)
    {

        $id = 1;

        $model = new Statement();
        $elements = $model->find()->where(['statement_request_id'=>$id])->orderBy(['start_date'=>SORT_DESC])->all();

        /*echo Yii::$app->getView()->render('@common/modules/profile/views/report/statement_rows.php',[
            'elements'=>$elements,
        ]);*/
        die;
    }



}
