<?php

namespace common\models;

use Yii;
use common\models\ContentCatalog;
use common\components\Ant;
use yii\imagine\Image;
use yii\web\UploadedFile;

/**
 * This is the model class for table "{{%content}}".
 *
 * @property integer $id
 * @property integer $parent
 * @property string $insert_date
 * @property string $update_date
 * @property string $date
 * @property integer $user_id
 * @property string $title_lng
 * @property string $long_title_lng
 * @property string $beautiful_url
 * @property string $notice_lng
 * @property string $content_lng
 * @property string $image_file
 * @property integer $tpl
 * @property integer $write_date
 * @property string $seo_title_lng
 * @property string $seo_description_lng
 * @property string $seo_keywords_lng
 * @property integer $show
 * @property integer $level
 */
class Content extends \yii\db\ActiveRecord
{

    public $layout = "default";
    public $navType;
    public $pageTpl;
    public $page;
    public $parentId;
    public $issetNavigation = false;
    public $catalogElements;
    public $orientation;

    public $imageFileDelete;
    public $imageFileTitleDelete;

    public $contentCatalogObject;
    public $contentObject;

    public $menuType = [
        '1' => 'Вертикальное',
        '2' => 'Горизонтальное'
    ];

    public $config = [
        'dir' => 'content',
        'model' => 'content'
    ];

    public $tpls = [
        'default' => 'Default',
        'contacts' => 'Контакты',
        'contacts_obratnaya_svyaz' => 'Контакты | Обратная связь',
        'contacts_cbu' => 'Контакты | ЦБУ',
        'contacts_bankomati' => 'Контакты | Банкоматы',
        'stocks' => 'Склады',

        'catalog_start' => 'Каталог (главная)',
        'catalog' => 'Каталог',

        'news_element' => 'Новости',

        'dostavka' => 'Доставка',

        'list_elements_a' => 'Спиисок дочерних элементов | A',
        'list_elements_b' => 'Спиисок дочерних элементов | B',

        'visa' => 'Специальные предложения от VISA',


    ];

    public $baseUrlPath;

    public function init(){
        $this->contentCatalogObject = new ContentCatalog();
    }

    public function getSubMenu($id){
        return Yii::$app->getView()->render("@common/widgets/mainMenu/views/inc/_{$id}.php");
    }

    public function buildBaseUrl($id){
        global $baseUrl;
        $el = Content::find()->where(['id'=>$id])->one();

        $pos = strpos($el->beautiful_url, '/');
        if ($pos === false){
            $dir = $el->beautiful_url;
        }else{
            $dir = strrchr($el->beautiful_url,"/");
            $dir = str_replace('/','',$dir);
        }
        $dir .= "/";

        $baseUrl = "{$dir}{$baseUrl}";

        if ($el->parent!=0) {
            return Content::buildBaseUrl($el->parent);
        }else{
            return $baseUrl;
        }
    }

    public function getFirstUrl($id){
        global $baseUrl;
        $el = Content::find()->where(['id'=>$id])->one();
        $baseUrl = $el->beautiful_url;
        if ($el->parent!=0) {
            return Content::getFirstUrl($el->parent);
        }else{
            return "{$baseUrl}/";
        }
    }


    public function getMaxLevel(){
        return Ant::getMaxLevel($this->tableName());
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%content}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'write_date', 'title_style', 'menu_type', 'show', 'level'], 'integer'],
            [['insert_date', 'update_date', 'date'], 'safe'],
            [['title_lng'], 'required'],
            [['id', 'parent', 'content_lng', 'notice_lng', 'seo_description_lng'], 'string'],
            [['title_lng', 'long_title_lng', 'beautiful_url', 'url', 'image_file', 'image_file_title', 'seo_title_lng', 'seo_keywords_lng'], 'string', 'max' => 255],
            [['tpl'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent' => 'Радительская страница',
            'insert_date' => 'Дата добавления',
            'update_date' => 'Дата обновления',
            'date' => 'Дата',
            'user_id' => 'Пользователь',
            'title_lng' => 'Название страницы',
            'long_title_lng' => 'Расширенное название',
            'beautiful_url' => 'Красивая ссылка',
            'notice_lng' => 'Краткое описание',
            'content_lng' => 'Подробное описание',
            'url' => 'Ссылка',
            'image_file' => 'Изображение',
            'image_file_title' => 'Изображение заголовка',
            'tpl' => 'Шаблон',
            'menu_type' => 'Тип меню',
            'title_style' => 'Тип заголовка',
            'write_date' => 'Выводить дату',
            'seo_title_lng' => 'Seo название',
            'seo_description_lng' => 'Seo описание',
            'seo_keywords_lng' => 'Seo ключевые слова',
            'show' => 'Отображать',
            'level' => 'Уровень',
        ];
    }


    public static function xurl(){
        $url = "";
        if (isset($_GET['base'])) $url = "{$_GET['base']}/";
        if (isset($_GET['url'])) $url .= "{$_GET['url']}/";
        if (isset($_GET['catalogelementurl'])) $url .= "{$_GET['catalogelementurl']}/";
        $url = substr($url,0,-1);
        return $url;
    }


    public function getParentId($id,$arr=[]){
        // Определение id основного родительского элемента по id любого дочернего
        $page = Content::find()->where(['id' => $id])->one();
        $toReturn = $page->id;
        $arr[] = $page->parent;
        if ($page->parent == 0){
            $toReturn = $page->id;
        }else{
            $toReturn = $this->getParentId($page->parent,$arr);
        }
        return $toReturn;
    }


    public function getTypeMenu($pageid){
        $parent = $this->getParentId($pageid);
        //$page = Content::find()->where(['id' => $parent])->one();
        $page = Content::find()->where(['id' => $pageid])->one();
        return $page->menu_type;
    }


    public function getPageByUrl($url){
        return Content::find()->where(['beautiful_url' => $url])->one();
    }

    public function getPageById($id){
        return Content::findOne($id);
    }

    public function getOrientation($type){

        switch ($type){
            case "1": $orientation = "v"; break;
            case "2": $orientation = "h"; break;
            case "3": $orientation = "v"; break;
        }
        return $orientation;
    }

    public function buildContentSubMenu($parentId, $curpage,  $level, $navType){

        // $type:
        // 1 - Вертикальное (все дерево)
        // 2 - первый уровень детей
        // 3 - Вертикальное (все уровни детей)


        if (!isset($_GET['action'])) $_GET['action'] = '';
        switch ($_GET['action']){
            case "showCatalogElement":
                $path = "/";
                break;
            case "showCatalog":
                $path = "/";
                break;
            default:
                $path = "/";
                break;
        }



        //print_r($_GET['url']);

        //$curpage

        // echo "{$parentId} - {$curpage}"; die;

        $showElements = "content-menu-element-hidden";
        if($level == 0) $showElements = "content-menu-element-show";

        //print_r(Content::getAllParentId($curpage)); die;

        if (in_array($parentId,Content::getAllParentId($curpage))) $showElements = "content-menu-element-show";

        //$showElements = "content-menu-element-show";



        //if ($curpage == $value->id) break;

        switch ($navType){
            case "1":
                $childs = Content::find()->where(['parent' => $parentId, 'show' => '1'])->orderBy(['level'=>SORT_DESC])->all();
                if (count($childs)!=0){
                    $level++;
                    echo "<ul class='content-menu-level{$level} {$showElements} content-menu-parent{$parentId}'>";
                    foreach($childs as $key=>$value){



                        $contentMenuItemActive = "";
                        if ($value->id==$curpage) $contentMenuItemActive = "content-menu-item-active";
                        echo "<li class='content-menu-item {$contentMenuItemActive}'>";
                        echo "<a class='FNTC' href='/{$value->beautiful_url}'>{$value->title_lng}</a>";
                        $childs = Content::find()->where(['parent' => $value->id])->all();
                        if (count($childs)!=0) $this->buildContentSubMenu($value->id, $curpage, $level, $navType);
                        echo "</li><!-- content-menu-item-active -->";
                    }
                    echo "</ul>";
                }
                break;
            case "2":

                $childs = Content::find()->where(['parent' => $parentId, 'show' => '1'])->orderBy(['level'=>SORT_DESC])->all();

                if (count($childs)!=0){
                    $level++;
                    echo "<ul class='content-menu-level{$level} content-menu-parent{$parentId}'>";
                    foreach($childs as $key=>$value){
                        $contentMenuItemActive = "";
                        if ($value->id==$curpage) $contentMenuItemActive = "content-menu-item-active";
                        echo "<li class='content-menu-item {$contentMenuItemActive}'>";
                        echo "<a class='FNTC' href='{$path}{$value->beautiful_url}'>{$value->title_lng}</a>";
                        echo "</li><!-- content-menu-item-active -->";
                    }
                    echo "</ul>";
                }

                break;
            case "3":

                $childs = Content::find()->where(['parent' => $parentId, 'show' => '1'])->orderBy(['level'=>SORT_DESC])->all();
                if (count($childs)!=0){
                    $level++;
                    echo "<ul class='content-menu-level{$level} content-menu-parent{$parentId}'>";
                    foreach($childs as $key=>$value){
                        $contentMenuItemActive = "";
                        if ($value->id==$curpage) $contentMenuItemActive = "content-menu-item-active";
                        echo "<li class='content-menu-item {$contentMenuItemActive}'>";
                        echo "<a class='FNTC' href='{$value->beautiful_url}'>{$value->title_lng}</a>";
                        $childs = Content::find()->where(['parent' => $value->id])->all();
                        if (count($childs)!=0) $this->buildContentSubMenu($value->id, $curpage, $level);
                        echo "</li><!-- content-menu-item-active -->";
                    }
                    echo "</ul>";
                }

                break;
        }


    }

    public function getPageTpl($tpl=''){
        if ($tpl=='') $tpl = 'def';
        return $tpl;
    }

    public function issetNavigation($parent){
        $toReturn = false;
        $elements = Content::find()->where(['parent' => $parent])->all();
        if (count($elements)!=0){
            $toReturn = true;
        }

        return $toReturn;
    }

    public function buildContentMenu($p=0)
    {
        // print_r($_GET);
        // $type = $this->getTypeMenu($pageid);
        // $orientation:
        // h - горизонтальное
        // v - вертикальное

        $navType = $this->navType;
        $parent = $this->parentId;
        $pageid = $this->page['id'];

        $content = "";

        if ($p!=0) $parent = $p;

        switch ($navType){
            case "1":

                $page = Content::find()->where(['parent' => $parent, 'show' => '1'])->orderBy(['level'=>SORT_DESC])->one();



                if (count($page)!=0){
                    ob_start();
                    echo "<div class='content-menu-wrapper'>";
                    echo "<div class='content-menu'>";
                    $this->buildContentSubMenu($parent, $pageid, 0, $navType);
                    echo "</div><!-- content-menu -->";
                    echo "</div><!-- content-menu-wrapper -->";
                    $content = ob_get_contents();
                    ob_end_clean();
                }

                break;
            case "2":

                ob_start();
                echo "<div class='content-menu'>";
                $this->buildContentSubMenu($parent, $pageid, $level = 0, $navType);
                echo "</div><!-- content-menu -->";
                $content = ob_get_contents();
                ob_end_clean();

                break;
            case "3":
                $parentId = 0;
                break;
        }
        return $content;
    }

    public function buildCatalogElements($pageId){
        $elements = $this->contentCatalogObject->getCatalogElements($pageId);
        if (count($elements)!=0){
            foreach ($elements as $key=>$value){
                echo "<div>{$value->title_lng}</div>";
            }
        }
    }



    public function getAllParentId($id){
        global $arrx;
        if (count($arrx)==0) $arrx[] = $id;
        $page = Content::find()->where(['id' => $id])->one();
        $arrx[] = $page->parent;
        if ($page->parent != 0){
            return  Content::getAllParentId($page->parent);
        }
        return $arrx;
    }

    public function getPathMenuElements($id){
        global $arr;
        $page = Content::find()->where(['id' => $id])->one();
        $arr[] = ['url'=>$page->beautiful_url,'title'=>$page->title_lng];
        if ($page->parent != 0){
            return  Content::getPathMenuElements($page->parent);
        }
        return $arr;
    }



    /*public function getFirstUrl($id){
        global $baseUrl;
        $el = Content::find()->where(['id'=>$id])->one();
        $baseUrl = $el->beautiful_url;
        if ($el->parent!=0) {
            return Content::getFirstUrl($el->parent);
        }else{
            return "{$baseUrl}/";
        }
    }*/


    public static function elementCreate($model){

        $model->baseUrlPath = Content::buildBaseUrl($model->parent);
        Ant::beautyURL($model);

        $model->level = $model->getMaxLevel()+10;
        $model->save();

        //$elementId = $model->getPrimaryKey();

        $imageFile = UploadedFile::getInstance($model, 'image_file');
        if (isset($imageFile)){
            $uploaderInfo = Ant::uploaderInfo();
            $model->image_file = 'EL'.Ant::buildFileName($model->id,$imageFile);
            if ($model->validate()) {
                $path = "../site/{$uploaderInfo['dir']}/{$model->config['dir']}/";
                $imageFile->saveAs("{$path}{$model->image_file}");
                Image::thumbnail("{$path}{$model->image_file}", 400, 400)
                    ->save(Yii::getAlias("{$path}m{$model->image_file}"), ['quality' => 100]);
            }
        }

        $imageFileTitle = UploadedFile::getInstance($model, 'image_file_title');
        if (isset($imageFileTitle)){
            $uploaderInfo = Ant::uploaderInfo();
            $model->image_file_title = 'T'.Ant::buildFileName($model->id,$imageFileTitle);
            if ($model->validate()) {
                $path = "../site/{$uploaderInfo['dir']}/{$model->config['dir']}/";
                $imageFile->saveAs("{$path}{$model->image_file_title}");
                Image::thumbnail("{$path}{$model->image_file_title}", 400, 400)
                    ->save(Yii::getAlias("{$path}m{$model->image_file_title}"), ['quality' => 100]);
            }
        }

        $model->save();

    }


    public static function elementUpdate($model){


        $model->update_date = date("Y-m-d H:i:00");

        /*
         * Добавление / удаление
         * изображения
        */
        $uploaderInfo = Ant::uploaderInfo();
        $path = "../site/{$uploaderInfo['dir']}/{$model->config['dir']}/";
        if ($model->image_file == '') unset($model->image_file);
        if ($post['Content']['imageFileDelete']==1){
            unlink($path.$model['oldAttributes']['image_file']);
            $model->image_file = '';
            $model->save();
        }
        $imageFile = UploadedFile::getInstance($model, 'image_file');
        if (isset($imageFile)){
            $model->image_file = Ant::buildFileName($model->id,$imageFile);
            if ($model->validate()) {
                $imageFile->saveAs("{$path}{$model->image_file}");
                Image::thumbnail("{$path}{$model->image_file}", 400, 400)
                    ->save(Yii::getAlias("{$path}m{$model->image_file}"), ['quality' => 100]);
            }
        }

        /*
         * Добавление / удаление
         * изображения для заголовка
        */
        $uploaderInfo = Ant::uploaderInfo();
        $path = "../site/{$uploaderInfo['dir']}/{$model->config['dir']}/";
        if ($model->image_file_title == '') unset($model->image_file_title);
        if ($post['Content']['imageFileTitleDelete']==1){
            unlink($path.$model['oldAttributes']['image_file_title']);
            $model->image_file = '';
            $model->save();
        }
        $imageFileTitle = UploadedFile::getInstance($model, 'image_file_title');
        if (isset($imageFileTitle)){
            $model->image_file_title = 'T'.Ant::buildFileName($model->id,$imageFileTitle);
            if ($model->validate()) {
                $imageFileTitle->saveAs("{$path}{$model->image_file_title}");
                Image::thumbnail("{$path}{$model->image_file_title}", 400, 400)
                    ->save(Yii::getAlias("{$path}m{$model->image_file_title}"), ['quality' => 100]);
            }
        }

        $model->baseUrlPath = Content::buildBaseUrl($model->parent);
        //$model->baseUrlPath = Content::getFirstUrl($model->parent);

        Ant::beautyURL($model);

        $model->save();
    }


}



































/*

namespace common\models;

use Yii;



class Content extends \yii\db\ActiveRecord
{
    public $config = [
        'dir' => 'content',
    ];
    //public $image_file;


    public static function tableName()
    {
        return '{{%content}}';
    }


    public function rules()
    {
        return [
            [['parent', 'user_id', 'tpl', 'show', 'level'], 'integer'],
            [['title_lng','beautiful_url'], 'required'],
            [['insert_date', 'update_date', 'date'], 'safe'],
            [['content_lng', 'seo_description_lng'], 'string'],
            [['title_lng', 'long_title_lng', 'beautiful_url', 'notice_lng', 'seo_title_lng', 'seo_keywords_lng'], 'string', 'max' => 255],
            [['image_file'], 'image', 'extensions' => 'png, jpg', 'maxSize' => 11024000, 'tooBig' => 'Не больше 500KB'],
        ];
    }


    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent' => 'ID родительской страницы',
            'insert_date' => 'Дата добавления',
            'update_date' => 'Дата обновления',
            'date' => 'Дата',
            'user_id' => 'ID пользователя',
            'title_lng' => 'Название',
            'long_title_lng' => 'Название длинное',
            'beautiful_url' => 'Красивая ссылка',
            'notice_lng' => 'Краткое описание',
            'content_lng' => 'Подробное описание',
            //'content_b_lng' => 'Подробное описание 2',
            //'bnnrs' => 'Баннер',
            //'url' => 'Ссылка',
            //'target' => 'Открыть в новом окне',
            'image_file' => 'Ext',
            'tpl' => 'Шаблон',
            //'write_date' => 'Отображать дату',
            'seo_title_lng' => 'Seo Title',
            'seo_description_lng' => 'Seo Description',
            'seo_keywords_lng' => 'Seo Keywords',
            'show' => 'Отображать',
            'level' => 'Уровень',
        ];
    }

    public function getItemsForMenu($id){
        $el = Content::find()->where(['id'=>$id])->one();
        $arr = array();
        if ($el['parent']==0){
            $items = Content::find()->where(['parent'=>$el['id']])->all();
        }else{
            $items = Content::find()->where(['parent'=>$el['parent']])->all();
        }

        $request = Yii::$app->request->get();



        foreach($items as $key=>$value){
            $active = false;
            if ($request['element'] == $value['beautiful_url']) $active = true;
            $arr[] =
                [
                    'id'=>$value['id'],
                    'title'=>$value['title_lng'],
                    'url'=>$value['beautiful_url'],
                    'active'=>$active,
                ];
        }
        return $arr;
    }

}

*/