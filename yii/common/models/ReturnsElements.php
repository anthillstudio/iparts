<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%returns_elements}}".
 *
 * @property integer $id
 * @property integer $return_id
 * @property string $good_id
 * @property integer $count
 * @property string $price
 *
 * @property Returns $return
 */
class ReturnsElements extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%returns_elements}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['return_id', 'count'], 'integer'],
            [['price'], 'number'],
            [['return_id'], 'exist', 'skipOnError' => true, 'targetClass' => Returns::className(), 'targetAttribute' => ['return_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'return_id' => 'Return ID',
            'good_id' => 'Good ID',
            'count' => 'Count',
            'price' => 'Price',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReturn()
    {
        return $this->hasOne(Returns::className(), ['id' => 'return_id']);
    }

    public function getCatalogElements()
    {
        return $this->hasOne(CatalogElements::className(), ['id' => 'good_id']);
    }
}
