<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\CatalogElements;
use common\components\Api;

/**
 * CatalogElementsSearch represents the model behind the search form about `common\models\CatalogElements`.
 */
class CatalogElementsSearch extends CatalogElements
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'parent', 'insert_date', 'update_date', 'title_lng', 'ref', 'beautiful_url', 'notice_lng', 'content_lng', 'image_file', 'tpl', 'seo_title', 'seo_description', 'seo_keywords'], 'safe'],
            [['user_id', 'hit', 'new', 'show', 'level'], 'integer'],
            [['price1', 'price2'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CatalogElements::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'insert_date' => $this->insert_date,
            'update_date' => $this->update_date,
            'user_id' => $this->user_id,
            'price1' => $this->price1,
            'price2' => $this->price2,
            'hit' => $this->hit,
            'new' => $this->new,
            'show' => $this->show,
            'level' => $this->level,
        ]);

        $query->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'parent', $this->parent])
            ->andFilterWhere(['like', 'title_lng', $this->title_lng])
            ->andFilterWhere(['like', 'ref', $this->ref])
            ->andFilterWhere(['like', 'beautiful_url', $this->beautiful_url])
            ->andFilterWhere(['like', 'notice_lng', $this->notice_lng])
            ->andFilterWhere(['like', 'content_lng', $this->content_lng])
            ->andFilterWhere(['like', 'image_file', $this->image_file])
            ->andFilterWhere(['like', 'tpl', $this->tpl])
            ->andFilterWhere(['like', 'seo_title', $this->seo_title])
            ->andFilterWhere(['like', 'seo_description', $this->seo_description])
            ->andFilterWhere(['like', 'seo_keywords', $this->seo_keywords]);

        return $dataProvider;
    }

    /**
     * Ужасное название функции, но на момент написания не придумал ничего лучше
     * Выносим из контроллера получение данных в этот класс
     *
     * Метод связан с методом countElementsForAjax
     */
    public function findElementsForAjax($catalog, $region, $limit = 1500, $page = 0)
    {
        $offset = $page * $limit;

        $query = $this->getElementsForAjaxQuery($catalog, $region);

        $els = $query
            ->limit($limit)
            ->offset($offset)
            ->orderBy(['title' => SORT_ASC])
            ->all();

        return $els;
    }

    /**
     * Ужасное название функции, но на момент написания не придумал ничего лучше
     *
     * Метод связан с методом findElementsForAjax
     */
    public function countElementsForAjax($catalog, $region)
    {
        $query = $this->getElementsForAjaxQuery($catalog, $region);

        $count = $query
            ->count();

        return $count;
    }

    public function getElementsForAjaxQuery($catalog, $region)
    {
        $arr = $this->getArr($catalog);
        $regionWhere = $this->getRegionWhere($region);

        return \common\models\CatalogElements::find()
            ->where($arr)
            ->andWhere(['show' => 1])
            ->andWhere($regionWhere)
            ->andWhere(['!=','parent','00-00000370'])
            ->joinWith('regionProduct r')
            ->groupBy(['id']); // Добавил группировку, иначе не верно считалось количество элементов запросом (строки дублятся из-за джойна)
    }

    /**
     * Получение списка дочерних каталогов по которым предполагается получать элементы
     *
     * @param $catalog
     * @return array
     */
    public function getArr($catalog)
    {
        $p = Api::getCatJson();

        $arr = [];

        if (!empty($catalog) && count($catalog)!=0){
            if ($catalog != '')
                $arr = ['parent'=>explode(",",$p[$catalog])];
        }else{
            $pall = "";
            foreach ($p as $k=>$v) { $pall .= "{$k},"; }
            $pall = substr($pall,0,-1);
            $arr = ['parent'=>explode(",",$pall)];
        }

        return $arr;
    }

    public function getRegionWhere($region)
    {
        return ((($region == 'all') || empty($region)) ? [] : ['r.region_id' => $region]);
    }

}
