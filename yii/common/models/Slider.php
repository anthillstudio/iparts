<?php

namespace common\models;

use Yii;
use common\components\Ant;

/**
 * This is the model class for table "{{%slider}}".
 *
 * @property integer $id
 * @property string $insert_date
 * @property string $update_date
 * @property integer $user_id
 * @property string $title_lng
 * @property string $notice_lng
 * @property string $tpl
 * @property integer $show
 * @property integer $level
 */
class Slider extends \yii\db\ActiveRecord
{

    public function getMaxLevel(){
        return Ant::getMaxLevel($this->tableName());
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%slider}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['insert_date', 'update_date'], 'safe'],
            [['user_id', 'show', 'level'], 'integer'],
            [['notice_lng', 'tpl'], 'required'],
            [['title_lng', 'notice_lng'], 'string', 'max' => 255],
            [['tpl'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'insert_date' => 'Дата добавления',
            'update_date' => 'Дата обновления',
            'user_id' => 'Пользователь',
            'title_lng' => 'Название',
            'notice_lng' => 'Описание',
            'tpl' => 'Шаблон',
            'show' => 'Отображать',
            'level' => 'Уровень',
        ];
    }
}
