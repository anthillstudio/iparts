<?php

namespace common\models;

use Yii;
use common\models\User;

/**
 * This is the model class for table "{{%user_info}}".
 *
 * @property integer $id
 * @property string $insert_date
 * @property string $update_date
 * @property integer $user_id
 * @property string $code
 * @property string $name
 * @property string $phone
 * @property string $comment
 * @property integer $show
 * @property integer $image_file
 * @property integer $addr
 */
class UserInfo extends \yii\db\ActiveRecord
{

    public $password;
    public $email;

    public static $config = [
        'dir' => 'user',
        'model' => 'UserInfo'
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_info}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['insert_date', 'update_date'], 'safe'],
            [['user_id', 'show'], 'integer'],
            //[['code'], 'required'],
            [['name', 'code', 'phone', 'comment', 'addr'], 'string', 'max' => 255],
            [['pasport'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'insert_date' => 'Дата добавления',
            'update_date' => 'Дата обновления',
            'user_id' => 'Пользователь',
            'code' => 'Код',
            'name' => 'Имя',
            'phone' => 'Телефон',
            'addr' => 'Адрес',
            'pasport' => 'Паспорт',
            'comment' => 'Комментарий',
            'show' => 'Отображать',
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
