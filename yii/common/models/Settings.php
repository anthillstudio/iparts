<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%settings}}".
 *
 * @property integer $id
 * @property string $key
 * @property string $value
 */
class Settings extends \yii\db\ActiveRecord
{
    const USD_CURRENCY = 'usd_currency';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%settings}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['key', 'value'], 'required'],
            [['value'], 'string'],
            [['key'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'key' => 'Ключ',
            'value' => 'Значение',
        ];
    }

    public static function getSettings(){

        $data = Settings::find()->all();
        $settings = [];
        foreach ($data as $key=>$value){
            $settings[$value->key] = $value->value;
        }

        return $settings;
    }

    public static function getSettingsByKey($key)
    {
        $data = Settings::find()->where(['key'=>$key])->one();
        return $data;
    }

    public static function getUsdCurrency()
    {
        $data = Settings::getSettingsByKey(Settings::USD_CURRENCY);
        $value = '';
        if (!is_null($data)) {
            $value = $data->value;
        }
        return $value;
    }

    public static function setUsdCurrency($value)
    {
        $data = Settings::getSettingsByKey(Settings::USD_CURRENCY);
        $data->value = $value;
        $data->save();
    }

}
