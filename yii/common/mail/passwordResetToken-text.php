<?php

/* @var $this yii\web\View */
/* @var $user common\models\User */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/reset-password', 'token' => $user->password_reset_token]);

$name = property_exists($user, 'name') ? $user->name : (property_exists($user, 'username') ? $user->username : '');
?>
Здравствуйте <?= $name ?>,

перейдите по ссылке для изменения парооля:

<?= $resetLink ?>
