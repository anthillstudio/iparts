<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/reset-password', 'token' => $user->password_reset_token]);

$name = property_exists($user, 'name') ? $user->name : (property_exists($user, 'username') ? $user->username : '');
?>
<div class="password-reset">
    <p>Здравствуйте. <?= Html::encode($name) ?>,</p>
    <p>перейдите по ссылке для изменения пароля:</p>
    <p><?= Html::a(Html::encode($resetLink), $resetLink) ?></p>
</div>
